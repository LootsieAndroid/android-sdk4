Readme.txt

Eclipse Build Instructions:

Step 1: Set Resource Variables
	For eclipse builds, set the following resource variables:
		project properties -> resource -> linked resources -> path variables:
	or
		go to project -> properties -> java build path -> source -> link source
	
	set HYBRID_PUBLIC_SRC 
		/Users/jerrylootsie/source/bitbucket/public-android-sdk4-master/AndroidStudioProjects/LootsieHybrid/library/src/main/java		
		as src **
	set HYBRID_PUBLIC_RES
		/Users/jerrylootsie/source/bitbucket/public-android-sdk4-master/AndroidStudioProjects/LootsieHybrid/library/src/main/res
		as res **

Step 2: Set android build version	
	go to project -> properties ->  android
	select the proper android sdk version : prefer android 4.0.3

Step 3: Modify com.lootsie.sdk.utils/LootsieGlobals.java
	set APPKEY_LIVE = "PUT_YOUR_LOOTSIE_APP_KEY_HERE"

Step 4: Build the LootsieHybrid library project
	When it compiles properly, you should see LootsieHybrid/bin/lootsiehybrid.jar in the eclipse package explorer.
	Copy the lootsiehybrid.jar file here:
	eclipse/LootsieApp/libs/lootsiehybrid.jar

--

Android Studio integration, Lootsie SDK + AdMob

Step 1. Clone the repository, run test application to make sure you have working development environment and learn more about the SDK.

Step 2. Link library module found in the SDK "library" folder to your application module. 

Step 3. Edit your application build.graddle file and add line:

	compile project(path: ':library', configuration: 'admobRelease')
	
into the dependencies section to use Lootsie video ad rewards platform, 
or 

	compile project(':library') 


otherwise. Other source ad networks may be added later. 

Note. Lootsie will offer rewarded video ad in the rewards marketplace.	


