package com.dev.game;


import android.os.Bundle;

import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.preference.PreferenceScreen;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.ListPreference;
import android.util.Log;

import android.content.SharedPreferences;

import com.lootsie.sdk.lootsiehybrid.LOOTSIE_CONFIGURATION;
import com.lootsie.sdk.lootsiehybrid.Lootsie;
import com.lootsie.sdk.lootsiehybrid.R;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

import com.dev.game.LootsieApp.*;

/**
 * Created by jerrylootsie on 2/27/15.
 */
public class MyPreferencesActivity extends PreferenceActivity implements OnPreferenceClickListener, OnSharedPreferenceChangeListener {


    private static String TAG = "DevGame MyPreferencesActivity";

//    private DialogPreference dialogBasedPrefCat;
    private PreferenceScreen dialogBasedPrefCat;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Logs.v(TAG, "onCreate");

        addPreferencesFromResource(com.dev.game.R.xml.preferences);


//        // trying to create the listpreference programatically - useless
//        try {
//            // Root
//            PreferenceScreen root = getPreferenceManager().createPreferenceScreen(this);
//            root.setTitle("Preference Screen Title");
//
////            dialogBasedPrefCat = new DialogPreference();
////            dialogBasedPrefCat.setTitle("Category Title");
////            root.addPreference(dialogBasedPrefCat); //Adding a category
//
//            CharSequence[] entries = {"One", "Two", "Three"};
//            CharSequence[] entryValues = {"1", "2", "3"};
//
//            // List preference under the category
//            ListPreference listPref = new ListPreference(this);
//            listPref.setKey("keyName"); //Refer to get the pref value
//            listPref.setEntries(entries);
//            listPref.setEntryValues(entryValues);
//            listPref.setDialogTitle("Dialog Title");
//            listPref.setTitle("Title");
//            listPref.setSummary("Summary");
//
//
//            root.addPreference(listPref);
//
////            dialogBasedPrefCat.addPreference(listPref);
//        } catch (Exception e) {
//            Logs.e(TAG, "Error: exceptin: " + e.getMessage());
//        }

        final ListPreference listPreference = (ListPreference) findPreference("cacheDuration");
        if (listPreference != null) {
	        // THIS IS REQUIRED IF YOU DON'T HAVE 'entries' and 'entryValues' in your XML
	        setListPreferenceData(listPreference);
	
	        listPreference.setOnPreferenceClickListener(this);
	        
//	        listPreference.setOnPreferenceClickListener(new OnPreferenceClickListener() {
//          @Override
//          public boolean onPreferenceClick(Preference preference) {
//
//              setListPreferenceData(listPreference);
//              return false;
//          }
//      });	 
	        
	        listPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
	            @Override
	            public boolean onPreferenceChange(Preference preference, Object o) {
	                //SharedPreferences myPref = getSharedPreferences();
	                SharedPreferences myPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

	                myPref.getString("cacheDuration", (String) o);
	                //myPref.getInt("cacheDuration",Integer.valueOf((String) o));

	                Logs.v(TAG, "cacheDuration: " + (String) o);

	                Lootsie.setCacheDurationInSeconds(Integer.valueOf((String) o));

	                listPreference.setValue((String) o);

	                return false;
	            }
	        });	        
        }




 

    }

    @Override
    public void onResume() {
    	super.onResume();
    	
        final CheckBoxPreference cbPerformUpdates = (CheckBoxPreference) findPreference("perform_updates");
        cbPerformUpdates.setOnPreferenceClickListener(MyPreferencesActivity.this);

        final CheckBoxPreference cbDebugging = (CheckBoxPreference) findPreference("enable_debugging");
        cbDebugging.setOnPreferenceClickListener(MyPreferencesActivity.this);

        final CheckBoxPreference cbEnableGPS = (CheckBoxPreference) findPreference("enable_gps");
        cbEnableGPS.setOnPreferenceClickListener(MyPreferencesActivity.this);

        final CheckBoxPreference cbEnableCache = (CheckBoxPreference) findPreference("enable_cache");
        cbEnableCache.setOnPreferenceClickListener(MyPreferencesActivity.this);    	
    }
    
    protected static void setListPreferenceData(ListPreference lp) {
    	
    	if (lp == null) {
    		Log.e(TAG, "MyPreferencesActivity: setListPreferenceData: data is null!");
    		return;
    	}
    	
        CharSequence[] entries = { "10 sec", "3 Minutes", "1 hour", "12 hours" };
        // times in seconds
        CharSequence[] entryValues = {"10" , "180", "3600", "43200"};
        lp.setEntries(entries);
        lp.setDefaultValue("10");
        lp.setEntryValues(entryValues);
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {

        Logs.v(TAG, "onPreferenceClick");

        if ((preference instanceof CheckBoxPreference) && (preference.getKey().equals("enable_debugging"))) {
            CheckBoxPreference cb = (CheckBoxPreference) preference;
            if (cb.isChecked()) {
                Logs.v(TAG, "enable_debugging: on");
            } else {
                Logs.v(TAG, "enable_debugging: off");
            }

            checkPref();
        }

        if ((preference instanceof CheckBoxPreference) && (preference.getKey().equals("enable_gps"))) {
            CheckBoxPreference cb = (CheckBoxPreference) preference;
            if (cb.isChecked()) {
                Logs.v(TAG, "enable_gps: on");
            } else {
                Logs.v(TAG, "enable_gps: off");
            }

            checkPref();
        }

        if ((preference instanceof CheckBoxPreference) && (preference.getKey().equals("enable_cache"))) {
            CheckBoxPreference cb = (CheckBoxPreference) preference;
            if (cb.isChecked()) {
                Logs.v(TAG, "enable_cache: on");
            } else {
                Logs.v(TAG, "enable_cache: off");
            }

            checkPref();
        }

        if((preference instanceof ListPreference) && (preference.getKey().equals("cacheDuration"))){
            ListPreference lp = (ListPreference)preference;
            //setListPreferenceData(lp);
            Logs.v(TAG, "cacheDuration: " + lp.getValue());
            //return true;
            //checkPref();
        }
        return false;

    }

    private void checkPref(){

        //SharedPreferences myPref = PreferenceManager.getDefaultSharedPreferences(MyPreferencesActivity.this);
        SharedPreferences myPref = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());

        Logs.d(TAG,"preference: perform_updates: " +  myPref.getBoolean("perform_updates", false));
        Logs.d(TAG,"preference: enable_debugging: " +  myPref.getBoolean("enable_debugging", LootsieApp.Default_enable_debugging));
        Logs.d(TAG, "preference: enable_gps: " + myPref.getBoolean("enable_gps", LootsieApp.Default_enable_gps));
        Logs.d(TAG, "preference: enable_cache: " + myPref.getBoolean("enable_cache", LootsieApp.Default_enable_cache));

        //Logs.d(TAG, "preference: cacheDuration: " + myPref.getString("cacheDuration", String.valueOf(LootsieGlobals.NETWORK_CACHE_TIME_SECONDS)));

        if (myPref.getBoolean("enable_debugging", LootsieApp.Default_enable_debugging)) {
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
        } else {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.DISABLED);
        }

        // ASDK-400 ASDK-4 Remove GPS tracking from SDK layer, and put it in app layer as an option
        // lootsie engine should no longer care about this configuration!
        if (myPref.getBoolean("enable_gps", LootsieApp.Default_enable_gps)) {
            //Lootsie.setConfiguration(LOOTSIE_CONFIGURATION.ENABLE_GPS);
        } else {
            //Lootsie.setConfiguration(LOOTSIE_CONFIGURATION.DISABLE_GPS);
        }

        if (myPref.getBoolean("enable_cache", LootsieApp.Default_enable_cache)) {
            Lootsie.setConfiguration(LOOTSIE_CONFIGURATION.ENABLE_CACHE);
        } else {
            Lootsie.setConfiguration(LOOTSIE_CONFIGURATION.DISABLE_CACHE);
        }


    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Logs.v(TAG, "onSharedPreferenceChanged");

    }


    @Override
    public void onContentChanged() {
        super.onContentChanged();

        Logs.v(TAG, "onContentChanged");

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

    }


    @Override
    public void onStop() {
        super.onStop();

        Logs.v(TAG, "onStop");
    }
}