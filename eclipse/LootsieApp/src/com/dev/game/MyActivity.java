package com.dev.game;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
//import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.content.pm.ActivityInfo;

import android.text.Layout;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;
import android.util.Log;

import android.os.AsyncTask;

import java.util.ArrayList;

import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.lootsiehybrid.Lootsie;
import com.lootsie.sdk.lootsiehybrid.R;
import com.lootsie.sdk.model.Achievement;
import com.lootsie.sdk.model.Reward;
import com.lootsie.sdk.model.User;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.callbacks.IAchievementReached;
import com.lootsie.sdk.callbacks.IRedeemReward;
import com.lootsie.sdk.callbacks.ILogData;
import com.lootsie.sdk.callbacks.ISignOutCallback;



//public class MyActivity extends ActionBarActivity implements ILogData {
public class MyActivity extends Activity implements ILogData {

    private static final String TAG = MyActivity.class.getName();

    private static ArrayList<Achievement> appAchievements = null;
    private static ArrayList<String> achievements = new ArrayList<String>();

    private static ArrayList<Reward> userRewards = null;
    private static int rewardIndex = 0;
    
    private static int achievementIndex = 0; // reset to match the list

    private static TextView textView = null;

    private static int debugLevel = 1;

    private static AppLocationUpdates location = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.dev.game.R.layout.activity_my);

        textView = (TextView) findViewById(com.dev.game.R.id.textView2);
        if (textView != null) {
        	textView.setMovementMethod(new ScrollingMovementMethod());
        }
        
        //textView.setText("MainActivity: onCreate success");
        String buf = "Lootsie NOT RUNNING ";

        Lootsie.setLogCallback(this);
        Lootsie.setActivityContext(this);

        // pre-populate achievements list with Lootsie app achievements for testing queued commands
        achievements.add("back1");
        achievements.add("back2");
        achievements.add("installed");
        achievements.add("test_ach");
    }

//    public void onClickMenuSettings(MenuItem view) {
//        Logs.i(TAG, "MainActivity: onClickMenuSettings");
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        Logs.i(TAG, "MainActivity: onCreateOptionsMenu");

        getMenuInflater().inflate(com.dev.game.R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        Logs.i(TAG, "MainActivity: onOptionsItemSelected");

        int id = item.getItemId();
        if (id == com.dev.game.R.id.action_settings) {
            startActivity(new Intent(this, MyPreferencesActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setEnable()
    {
        // NOTE: this function may be called from a background thread
        // switch to the UI thread to make the change
        View rootView = getWindow().getDecorView().findViewById(android.R.id.content);

        rootView.post(new Runnable() {
            public void run() {
                /* the desired UI update */
                Logs.v(TAG, "MainActivity: onInitSuccess ui update on thread:" + getThreadSignature());

            }
        });


        SharedPreferences myPref = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        boolean value = myPref.getBoolean("enable_gps", false);
        Logs.d(TAG, "preference: enable_gps: " + value);
        if (value) {
            if (location == null) {
                location = new AppLocationUpdates(this.getApplicationContext());
            }
            if (location != null) {
                location.getCurrentLocation();
            }
        } else {
            Logs.d(TAG, "MainActivity: gps disabled!");
        }

        Logs.i(TAG, "MainActivity: Buttons enabled");

        if (!runningUpdateTask) {
            runningUpdateTask = true;
            new WaitForUpdateTask().execute("");
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        //LootsieManager.initcb.setActivity(MainActivity.this);
        InitCallback callback = LootsieApp.getInitCallback();
        if (callback != null) {
        	callback.setActivity(MyActivity.this);
        }


//        Lootsie.setLogCallback(this);
//        Lootsie.setActivityContext(this);
        
		Lootsie.onResume();
    }


    public void getAchievements(View v) {
        Logs.v(TAG, "MainActivity: getAchievements");

        appAchievements = Lootsie.getAppAchievements();
        // ASDK-403 getting achievements before sdk initialized results in null!
        if (appAchievements != null) {
            achievementIndex = 0; // reset to match the list

            achievements = new ArrayList<String>();
            for (int i = 0; i < appAchievements.size(); i++) {
                Achievement achievement = appAchievements.get(i);
                achievements.add(achievement.id);
            }
        } else {
            Logs.v(TAG, "MainActivity: getAchievements failed!");
        }

    }
    
    public void onClickGetUserRewards(View v) {
    	Logs.v(TAG, "MainActivity: onClickGetUserRewards"); 
    	
    	userRewards = Lootsie.getUserRewards();
    	
    	if (userRewards != null) {
    		rewardIndex = 0;    		
        } else {
            Logs.v(TAG, "MainActivity: getUserRewards failed!");
        }
    }

    public void onClickRedeemReward(View v) {
    	Logs.v(TAG, "MainActivity: onClickRedeemReward"); 
    	
    	if ((userRewards != null) && (userRewards.size() > 0) && (rewardIndex < userRewards.size())) {
    		Reward reward = userRewards.get(rewardIndex);
    		String emailStr = "lootsietest@lootsie.com";
    		
    		//Lootsie.redeemReward(emailStr, reward.id);
    		Lootsie.redeemReward(emailStr,  reward.id, redeemRewardCallback);
    		
    	} else {
    		Logs.v(TAG, "MainActivity: getUserRewards invalid userRewards or rewardIndex!");
    	}
    }
    
    class RedeemRewardCallback implements IRedeemReward {

        @Override
        public void onRedeemFailed(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT result, String json) {
            Logs.v(TAG, "MainActivity: onRedeemFailed: " + result.toString());

        }

        @Override
        public void onRedeemSuccess(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT result, String json) {
            Logs.v(TAG, "MainActivity: onRedeemSuccess: " + result.toString());

        }
    	
    }
    
    RedeemRewardCallback redeemRewardCallback = new RedeemRewardCallback();
    
    class AchievementReachedCallback implements IAchievementReached {

        @Override
        public void onLootsieBarClosed() {
            Logs.v(TAG, "MainActivity: onLootsieBarClosed");
        }

        @Override
        public void onLootsieBarExpanded() {
            Logs.v(TAG, "MainActivity: onLootsieBarExpanded");
        }

        @Override
        public void onLootsieFailed(String mesg) {
            Logs.v(TAG, "MainActivity: onLootsieFailed: " + mesg);
        }

        @Override
        public void onLootsieSuccess() {
            Logs.v(TAG, "MainActivity: onLootsieSuccess");
        }

        @Override
        public void onNotificationData(String json) {
            Logs.v(TAG, "MainActivity: onNotificationData: " + json);
        }
    }

    AchievementReachedCallback achievementReachedCallback = new AchievementReachedCallback();

    public void achievementReached(View v) {
        Logs.v(TAG, "MainActivity: achievementReached");

//        if ((appAchievements != null) && (appAchievements.size() > achievementIndex)) {
//            Achievement a = appAchievements.get(achievementIndex);
//            Logs.v(TAG, "MainActivity: achievementReached: " + a.name);
//
//            //Lootsie.AchievementReached(this, a.id);
//            Lootsie.AchievementReached(this, a.id, Lootsie.DEFAULT_POSITION, achievementReachedCallback);
//
//            achievementIndex++;
//        }

        if ((achievements != null) && (achievements.size() > achievementIndex)) {
            String achievementId = achievements.get(achievementIndex);
            Logs.v(TAG, "MainActivity: achievementReached: " +achievementId);

            //Lootsie.AchievementReached(this, a.id);
            Lootsie.AchievementReached(this, achievementId, LootsieEngine.DEFAULT_POSITION, achievementReachedCallback);

            achievementIndex++;
        }

    }

    public void showRewardsPage(View v) {

        Logs.v(TAG, "MainActivity: showRewardsPage");

        Lootsie.showRewardsPage(this);
    }

    public void showAchievementsPage(View v) {

        Logs.v(TAG, "MainActivity: showAchievementsPage");

        Lootsie.showAchievementsPage(this);
    }

    static class InternalSignOutCallback implements ISignOutCallback {

 		@Override
 		public void onSignOutSuccess() {
 			Logs.v(TAG, "MainActivity: SignOutSuccess");				
 		}

 		@Override
 		public void onSignOutFailure() {
 			Logs.v(TAG, "MainActivity: SignOutFailure");				
 		}
 		
 	}
    
    public void onClickSignOut(View v) {
        Logs.v(TAG, "MainActivity: onClickSignOut");

        InternalSignOutCallback signoutCallback = new InternalSignOutCallback();
        //Lootsie.signOut();
        Lootsie.signOut(signoutCallback);
    }
    
    public void onClickGetUserAccount(View v) {
    	Logs.v(TAG, "MainActivity: onClickGetUserAccount");
    	
    	User lootsieUser = LootsieEngine.getInstance().getUserAccount();
    	if (lootsieUser != null) {
    		Logs.v(TAG, "user: isGuest: " + lootsieUser.isGuest);
        	Logs.v(TAG, "user: email: " + lootsieUser.email);        	
    	} else {
    		Logs.v(TAG, "user: data not yet available!");
    	}
    }
    
    public void onClickRegisterUser(View v) {
    	Logs.v(TAG, "MainActivity: onClickRegisterUser");
    	
    	LootsieEngine.registerUserEmail("lootsietest@lootsie.com");
    	
    }

    public static String getThreadSignature()
    {
        Thread t = Thread.currentThread();
        long l = t.getId();
        String name = t.getName();
        long p = t.getPriority();
        String gname = t.getThreadGroup().getName();
        return (name + ":(id)" + l + ":(priority)" + p
                + ":(group)" + gname);
    }


    @Override
    public void onLogEvent(String data) {
        // WARNING - don't use logs.v here, it will create an infinite loop!  Use log.v instead.
        if (!runningUpdateTask) {
            runningUpdateTask = true;
            new WaitForUpdateTask().execute("");
        }
    }

    private static long lastTime = 0;
    private static boolean runningUpdateTask = false;

    //private static AsyncTask<String, Void, String>updateTask = null;

    // we need to wait at least 1 second of messages before writing update to textview
    // otherwise, we would get flooded with too much information
    class WaitForUpdateTask extends AsyncTask<String, Void, String> {


        long lastTime = System.currentTimeMillis();
        long currentTime = System.currentTimeMillis();

        @Override
        protected String doInBackground(String... arg0) {

            if (debugLevel > 1) Log.v(TAG, "MainActivity: WaitForUpdateTask: starting");

            // wait to capture at least 1 second of log messages
            while ((currentTime - lastTime) < 1000) {
                if (debugLevel > 1) Log.v(TAG, "MainActivity: WaitForUpdateTask: waiting for update...");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                currentTime = System.currentTimeMillis();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // Runs on the UI thread after doInBackground
            //if (textView != null && Lootsie.isRunning()) {
            if (textView != null) {
                String buf = Lootsie.getLogBuffer();
                textView.setText(buf);

                // scroll to the end
                final Layout layout = textView.getLayout();
                if(layout != null){
                    int scrollDelta = layout.getLineBottom(textView.getLineCount() - 1)
                            - textView.getScrollY() - textView.getHeight();
                    if(scrollDelta > 0)
                        textView.scrollBy(0, scrollDelta);
                }

            }
            runningUpdateTask = false;

            if (debugLevel > 1) Log.v(TAG, "MainActivity: WaitForUpdateTask: finished");
        }
    }



    @Override
    protected void onPause() {
        Lootsie.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        Lootsie.onDestroy();
        super.onDestroy();
    }

    // ASDK-354 Android-SDK4 LEG says use portrait or landscape - setRenderingMode - optional portrait, landscape or automatic mode
    // ASDK-363 Android-SDK4 lock orientation of android activity view based on rendering mode
    public void onClickSetPortrait(View v) {
        Logs.e(TAG, "MainActivity: SetPortraitClick");
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // ANDSDK-97 [Native SDK] lock orientation of webview based on rendering mode
        Lootsie.setRenderingMode(LootsieEngine.RENDER_PORTRAIT);
    }

    public void onClickSetLandscape(View v) {
        Logs.e(TAG, "MainActivity: SetLandscapeClick");
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        // ANDSDK-97 [Native SDK] lock orientation of webview based on rendering mode
        Lootsie.setRenderingMode(LootsieEngine.RENDER_LANDSCAPE);
    }

    public void onClickSetAutoOrient(View v) {
        Logs.e(TAG, "MainActivity: SetAutoOrientClick");
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);

        // ANDSDK-97 [Native SDK] lock orientation of webview based on rendering mode
        Lootsie.setRenderingMode(LootsieEngine.RENDER_DEFAULT);
    }
}
