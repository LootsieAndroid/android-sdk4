package com.dev.game;

/**
 * Created by jerrylootsie on 2/9/15.
 */

import com.lootsie.sdk.lootsiehybrid.Lootsie;
import com.lootsie.sdk.lootsiehybrid.LOOTSIE_CONFIGURATION;
import com.lootsie.sdk.lootsiehybrid.LOOTSIE_NOTIFICATION_CONFIGURATION;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.utils.LootsieGlobals.LOOTSIE_LOGGING_LEVEL;


import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 *  This is a developer's app clas simulated here to call init.
 *  Develoepr has to call lootsie init under onCreate() method
 */
public class LootsieApp extends Application
{
    private static final String TAG = LootsieApp.class.getName();

    static InitCallback callback = null;

    public static boolean Default_enable_debugging = true;
    public static boolean Default_enable_gps = false;
    public static boolean Default_enable_cache = false;
    public static int Default_cache_duration = 10;

    @Override
    public void onCreate() {
        super.onCreate();

        Logs.v(TAG,"LootsieApp: App init");
        Logs.LOG_LEVEL = 4;

        callback = new InitCallback();



        SharedPreferences myPref = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());

        boolean enableDebuggingValue = myPref.getBoolean("enable_debugging", LootsieApp.Default_enable_debugging);
        Logs.d(TAG, "preference: enable_debugging: " + enableDebuggingValue);
        if (enableDebuggingValue) {
//            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Lootsie.setLogLevel(LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
        } else {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.DISABLED);
        }

        // ASDK-400 ASDK-4 Remove GPS tracking from SDK layer, and put it in app layer as an option
        // lootsie engine should no longer care about this configuration!
        // see MyActivity: setEnable
        boolean enableGPSValue = myPref.getBoolean("enable_gps", LootsieApp.Default_enable_gps);
        Logs.d(TAG, "preference: enable_gps: " + enableGPSValue);

        boolean enableCachevalue = myPref.getBoolean("enable_cache", LootsieApp.Default_enable_cache);
        Logs.d(TAG, "preference: enable_cache: " + enableCachevalue);
        if (enableCachevalue) {
            Lootsie.setConfiguration(LOOTSIE_CONFIGURATION.ENABLE_CACHE);
        } else {
            Lootsie.setConfiguration(LOOTSIE_CONFIGURATION.DISABLE_CACHE);
        }

//        Lootsie.setConfiguration(LOOTSIE_CONFIGURATION.ENABLE_CACHE);
        
        //int cacheDurationValue = myPref.getInt("cacheDuration", LootsieApp.Default_cache_duration);
        String cacheDurationStr = myPref.getString("cacheDuration", String.valueOf(LootsieApp.Default_cache_duration));
        int cacheDurationValue = Integer.valueOf(cacheDurationStr);
//        int cacheDurationValue = 60; // 60 seconds - 1 min
        Logs.d(TAG, "preference: cacheDuration: " + cacheDurationValue);
        Lootsie.setCacheDurationInSeconds(cacheDurationValue);

        //Lootsie.setNotificationConfiguration(LOOTSIE_NOTIFICATION_CONFIGURATION.notify_to_achievementsPage);
        //Lootsie.setNotificationConfiguration(LOOTSIE_NOTIFICATION_CONFIGURATION.notify_to_disabled);
        
//        Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
        Lootsie.init(this, LootsieGlobals.APPKEY, callback);
        
        // ANDSDK-42 [Discovered, Platform] Adobe AIR Support
        // ASDK-336 create a path to retrieve IAN data when IAN gui is disabled for whitelabel solutions
//		 Lootsie.setConfiguration(LOOTSIE_CONFIGURATION.DISABLE_GPS);
//		 Lootsie.setConfiguration(LOOTSIE_CONFIGURATION.DISABLE_IN_GAME_HOOK_WINDOW);
    }

    public static InitCallback getInitCallback() {
        return callback;
    }

    @Override
    public void onTerminate() {
        Logs.v(TAG,"LootsieApp: onTerminate");

        Lootsie.onDestroy();

        super.onTerminate();
    }

}
