package com.lootsie.sdk.viewcontrollers.base;

import android.app.ListActivity;

import com.lootsie.sdk.lootsiehybrid.LootsieEngine;

/**
 * Created by jerrylootsie on 2/23/15.
 */
// updating android activity from a background thread
// http://cogitolearning.co.uk/?p=793
public abstract class UpdatableListActivity extends ListActivity {

    public final void updateActivityFromBgThread() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateActivity();
            }
        });
    }

    public abstract void updateActivity();

    @Override
    protected void onStart() {
        super.onStart();

        // register activity callbacks with Lootsie instance? for updates and such.
        // we could use broadcast receiver for this?
        LootsieEngine.getInstance().setUpdatableListActivity(this);
    }

    @Override
    protected void onStop() {

        // unregister activity reference with Lootsie instance
        LootsieEngine.getInstance().setUpdatableListActivity(null);
        super.onStop();
    }
}
