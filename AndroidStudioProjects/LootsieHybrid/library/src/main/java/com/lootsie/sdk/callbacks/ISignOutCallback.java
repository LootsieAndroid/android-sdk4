package com.lootsie.sdk.callbacks;

public interface ISignOutCallback {
    public void onSignOutSuccess();
    public void onSignOutFailure();
}
