package com.lootsie.sdk.lootsiehybrid.sequences;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.callbacks.IUpdateUserAccountCallback;
import com.lootsie.sdk.lootsiehybrid.LootsieAccountManager;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.model.User;
import com.lootsie.sdk.netutil.RESTGetUserAccountTask;
import com.lootsie.sdk.netutil.RESTGetUserAchievementsTask;
import com.lootsie.sdk.netutil.RESTPostUserSessionEmailOnlyTask;
import com.lootsie.sdk.netutil.RESTPutUserAccountTask;
import com.lootsie.sdk.netutil.RestResult;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jerrylootsie on 5/12/15.
 */
public class UpdateUserAccountSequence {

    private static final String TAG = "Lootsie UpdateUserAccountSequence";

    private static LootsieAccountManager lootsieAccountManager = null;

    private static IUpdateUserAccountCallback callback = null;

    private static User user = null;

    //public static RESTPostUserSessionEmailOnlyTask restPostUserSessionEmailOnlyTask = null;
    public static IGenericAsyncTask<String> restPostUserSessionEmailOnlyTask = null;
    //public static RESTPutUserAccountTask restPutUserAccountTask = null;
    public static IGenericAsyncTask<String> restPutUserAccountTask = null;
    //public static RESTGetUserAccountTask restGetUserAccountTask = null;
    public static IGenericAsyncTask<String> restGetUserAccountTask = null;

    /**
     * we initialize the resttasks here, and check because they may already have been initialized as mock objects earlier for testing
     */
    private static void init() {

        if (restPostUserSessionEmailOnlyTask == null) {
            restPostUserSessionEmailOnlyTask = new RESTPostUserSessionEmailOnlyTask();
        }

        if (restPutUserAccountTask == null) {
            restPutUserAccountTask = new RESTPutUserAccountTask();
        }

        if (restGetUserAccountTask == null) {
            restGetUserAccountTask = new RESTGetUserAccountTask();
        }
    }

    /**
     * we need to create a new task every time because a task can only be executed once!
     */
    private static void cleanup() {
        restPostUserSessionEmailOnlyTask = null;
        restPutUserAccountTask = null;
        restGetUserAccountTask = null;
    }


    public static void start(User inputUser, IUpdateUserAccountCallback inputCallback) {

        user = inputUser;

        callback = inputCallback;

        DebugLog("UpdateUserAccountSequence: start - email: " + user.email);

        lootsieAccountManager = LootsieEngine.getInstance().getLootsieAccountManager();

        init();

        if (user.isGuest) {
            if ((user.email != null) && (user.email.length() > 0)) {
                registerUser(user.email);
            } else {
                // "Error: Need an email address to register guests!"
                if (callback != null) callback.onUpdateUserAccountFailure(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.UPDATE_USER_ACCOUNT_SEQUENCE_FAILURE_INVALID_EMAIL);
                cleanup();
            }

        } else {
            // put /user/account
            updateUserAccount();
        }


    }

    public static void registerUser(String emailStr) {

        /*
        POST https://api-v2.lootsie.com/v2/user/session/emailonly
        X-Lootsie-App-Secret
        X-Lootsie-User-Session-Token
        {“email”,”testuser@domain.com”}
        */

        if (isEmailValid(emailStr)) {

            DataModel.user.email = emailStr;

            String jsonStr = "{\"email\": \"" + emailStr + "\"}";

            IRESTCallback restCallback = new IRESTCallback() {
                @Override
                public void restResult(RestResult result) {
                    UpdateUserAccountSequence.PostUserSessionEmailOnlyResult(result);
                }
            };


            restPostUserSessionEmailOnlyTask.setCallback(restCallback);
            restPostUserSessionEmailOnlyTask.executeTask(jsonStr);
        } else {
            // ASDK-402 Android Hybrid: No error message when trying to redeem to an invalid email address.
            // don't bother sending it to the server, we know the email is wrong.
            // "Invalid Email Address"
            if (callback != null) callback.onUpdateUserAccountFailure(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.UPDATE_USER_ACCOUNT_SEQUENCE_FAILURE_INVALID_EMAIL);
            cleanup();
        }

    }

    public static void PostUserSessionEmailOnlyResult(RestResult result) {
        if ((result.status == 200) || (result.status == 204) || (result.status == 409)) {

            if (result.content != null) {
                DebugLog("UpdateUserAccountSequence: PostUserSessionEmailOnlyResult: " + result.content);
            } else {
                DebugLog("UpdateUserAccountSequence: PostUserSessionEmailOnlyResult: null response");
            }

            // save the updated user email info
            //LootsieEngine.getInstance().getLootsieAccountManager().saveUserInfo(DataModel.user);

            updateUserAccount();

        } else {
            // responseCode: 409
            // Note: When making this call with a session token belonging to a registered user, it will return 409
            if (result.content != null) {
                DebugLog("UpdateUserAccountSequence: PostUserSessionEmailOnlyResult[" + result.status + "]: " + result.content);

                // ASDK-402 Android Hybrid: No error message when trying to redeem to an invalid email address.
                // "Register Email Failure"
                if (callback != null) callback.onUpdateUserAccountFailure(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.UPDATE_USER_ACCOUNT_SEQUENCE_FAILURE_REGISTER_EMAIL);
                cleanup();
            } else {
                DebugLog("UpdateUserAccountSequence: PostUserSessionEmailOnlyResult[" + result.status + "]: null response");

                // ASDK-402 Android Hybrid: No error message when trying to redeem to an invalid email address.
                // "Register Email Failure"
                if (callback != null) callback.onUpdateUserAccountFailure(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.UPDATE_USER_ACCOUNT_SEQUENCE_FAILURE_REGISTER_EMAIL);
                cleanup();
            }

        }
    }

    static Pattern emailPattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

    private static boolean isEmailValid(String email) {
        Matcher m = emailPattern.matcher(email);
        return m.matches();
    }


    public static void updateUserAccount() {

        String jsonStr = "{" +
                "\"first_name\": \"" + user.firstName + "\"," +
                "\"last_name\": \"" + user.lastName + "\"," +
                "\"email\": \"" + user.email + "\"," +
                "\"city\": \"" + user.city + "\"," +
                "\"zipcode\":" + Integer.toString(user.zipcode) +
                "}";

        IRESTCallback restCallback = new IRESTCallback() {
            @Override
            public void restResult(RestResult result) {
                UpdateUserAccountSequence.PutUserAccountResult(result);
            }
        };

        restPutUserAccountTask.setCallback(restCallback);
        restPutUserAccountTask.executeTask(jsonStr);
    }

    public static void PutUserAccountResult(RestResult result) {
        //if ((result.status == 200) || (result.status == 204) || (result.status == 409)) {
        // catch invalid email results
        if ((result.status == 200) || (result.status == 204)) {

            if (result.content != null) {
                DebugLog("UpdateUserAccountSequence: PutUserAccountResult: " + result.content);
            } else {
                DebugLog("UpdateUserAccountSequence: PutUserAccountResult: null response");
            }

            // save the updated user email info
            //LootsieEngine.getInstance().getLootsieAccountManager().saveUserInfo(DataModel.user);

//            Toast.makeText(LootsieEngine.getInstance().getActivityContext(), "Update User Account Success", Toast.LENGTH_SHORT).show();

//            LootsieEngine.getInstance().updateCurrentActivitiesAndFragments();

            GetUserAccount();

        } else {
            // responseCode: 409
            // Note: When making this call with a session token belonging to a registered user, it will return 409
            if (result.content != null) {
                DebugLog("UpdateUserAccountSequence: PutUserAccountResult[" + result.status + "]: " + result.content);

                // ASDK-402 Android Hybrid: No error message when trying to redeem to an invalid email address.
                // "PutUserAccount Failure"
                if (callback != null) callback.onUpdateUserAccountFailure(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.UPDATE_USER_ACCOUNT_SEQUENCE_FAILURE_REGISTER_EMAIL);
                cleanup();
            } else {
                DebugLog("UpdateUserAccountSequence: PutUserAccountResult[" + result.status + "]: null response");

                // ASDK-402 Android Hybrid: No error message when trying to redeem to an invalid email address.
                // "PutUserAccount Failure"
                if (callback != null) callback.onUpdateUserAccountFailure(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.UPDATE_USER_ACCOUNT_SEQUENCE_FAILURE_REGISTER_EMAIL);
                cleanup();
            }

        }
    }


    public static void GetUserAccount() {

        DebugLog("UpdateUserAccountSequence: GetUserAccount");


        IRESTCallback restCallback = new IRESTCallback() {
            @Override
            public void restResult(RestResult result) {
                UpdateUserAccountSequence.GetUserAccountResult(result);
            }
        };


        restGetUserAccountTask.setCallback(restCallback);
        restGetUserAccountTask.executeTask("test");

    }

    public static void GetUserAccountResult(RestResult RestResult) {
        if (RestResult.status == 200) {
            try {
                JSONObject json = new JSONObject(RestResult.content);

                // store this stuff in the data model
                if (DataModel.user == null) {
                    DataModel.user = new User();
                }
                DataModel.user.parseFromJSONUserAccountInfo(json);

                // use account manager to save and restore data
                DebugLog("UserAccount: %s", DataModel.user.getDisplayName());
                lootsieAccountManager.createUser(DataModel.user, DataModel.userSessionToken);
                if (LootsieGlobals.debugLevel > 0) DataModel.user.print();

            } catch (JSONException ex) {
                Logs.e(TAG, "GetUserAccountResult: exception: " + ex.getMessage());
            } finally {
                // "Update User Account Success"
                if (callback != null) callback.onUpdateUserAccountSuccess(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.UPDATE_USER_ACCOUNT_SEQUENCE_SUCCESS);
            }
        } else {
            // "Update User Account Failed to retrieve settings."
            if (callback != null) callback.onUpdateUserAccountFailure(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.UPDATE_USER_ACCOUNT_SEQUENCE_FAILURE);
        }

        cleanup();
    }
    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (com.lootsie.sdk.utils.LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }
}
