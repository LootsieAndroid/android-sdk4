package com.lootsie.sdk.net;


import com.lootsie.sdk.netutil.RestResult;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.utils.LootsieGlobals.*;

import java.net.HttpURLConnection;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.io.DataOutputStream;
import java.io.DataInputStream;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import java.io.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Created by jerrylootsie on 2/9/15.
 */

/**
 * RestClient
 *
 * @author Alexander Salnikov
 */
public class RestClient {

    public static final int SERVER_PORT = 443;


    //private static String apiIntegrationUrl = LootsieApi.BASE_API_URL;
    private static String apiIntegrationUrl = "";
    private static int mMaxRetries = 5;
//    private static HashMap<String, Header> headers = new HashMap<String, Header>(2);
//    private static DefaultHttpClient mHttpClient = getNewHttpClient();

    private static HashMap<String, String> headersMap = new HashMap<String, String>(3);


    //private static String TAG = RestClient.class.getName();
    private static String TAG = "Lootsie RestClient";

    // http://forums.androidcentral.com/google-nexus-5/425589-problems-sending-receiving-sms-t-mobile.html
    // Regrettably, T-Mobile does not grant any third-party messaging apps access to the T-Mobile WiFi Calling feature for MMS. T-Mobile use something called GBA authentication which only system apps or apps digitally signed by T-Mobile can use (so not Textra).
    // On Galaxy Note ( I believe it is T-mobile version ), T-mobile modified APACHE HTTP Client to include GBA Client for Authentication over WiFi especially for MMS over WiFi.

    // post by passing json object
    public static RestResult doPost(String path, JSONObject params) throws ResponseException {
    
        RestResult result = new RestResult();
        int retries = 0;

        DebugLog("RestClient: doPost(json): " + apiIntegrationUrl + path);

        // Construct the POST data.
        String content = params.toString();
    
        DebugLog("RestClient: doPost(json): content: " + content);


        URL url = null;
        try {
        	url = new URL(apiIntegrationUrl + path);
        } catch (MalformedURLException ex) {
            Log.e(TAG, ex.toString());
        }

        HttpURLConnection urlConn = null;
        try {
            // URL connection channel.
            urlConn = (HttpURLConnection) url.openConnection();
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
        }

        // Let the run-time system (RTS) know that we want input.
        urlConn.setDoInput (true);

        // Let the RTS know that we want to do output.
        urlConn.setDoOutput (true);

        // No caching, we want the real thing.
        urlConn.setUseCaches (false);

        try {
            urlConn.setRequestMethod("POST");
        } catch (ProtocolException ex) {
        	Log.e(TAG, "RestClient: doPost(json): setRequestMethod: " + ex.toString());
        }

        // set request properties for doPost(json)
        // set request properties before connection is made! 
        //urlConn.setRequestProperty("Accept", "application/json");
        urlConn.setRequestProperty("Accept", "*/*");
        //urlConn.setRequestProperty("Content-type", "application/json");

        Iterator<Entry<String, String>> headerIter = headersMap.entrySet().iterator();
        while (headerIter.hasNext()) {
            Entry<String, String> entry = headerIter.next();

            urlConn.setRequestProperty(entry.getKey(), entry.getValue());

            DebugLog("RestClient: doPost(json): header: " + entry.getKey() + " " + entry.getValue());
            

        }

//        try {
//            urlConn.connect();
//        } catch (IOException ex) {
//            Log.e(TAG, "doPost(json): connect: " + ex.toString());
//        }        
        
        // write out the json
        // http://www.mkyong.com/webservices/jax-rs/restfull-java-client-with-java-net-url/
		try {
			OutputStream os = urlConn.getOutputStream();
			os.write(content.getBytes());
			os.flush(); 			
		} catch (IOException ex) {
			Log.e(TAG, "doPost(json): getOutputStream: " + ex.toString());
		}
       
        DataInputStream input = null;

        // You can get a FileNotFoundException from HttpUrlConnection (and OkHttpClient)
        // if your server returns >= HTTPStatus.BAD_REQUEST (400).
        // You should check the status code first to check what stream you need to read.

        int status = org.apache.http.HttpStatus.SC_OK;
        try {
            status = urlConn.getResponseCode();
        } catch (IOException ex) {
            Log.e(TAG, "doPost(json): getResponseCode: " + ex.toString());
        }
        
        DebugLog("RestClient: doPost: responseCode: " + status);

        if(status >= org.apache.http.HttpStatus.SC_BAD_REQUEST) {
        	try {
        		// this will get EOFException
        		
//                input = new DataInputStream (urlConn.getErrorStream());
//
//                StringBuilder sb = new StringBuilder();
//                String s;
//                while (true) {
//                    s = input.readUTF();
//                    if (s == null || s.length() == 0) {
//                        break;
//                    }
//                    sb.append(s);
//                }
//                String responseStr = sb.toString();
//                if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "HTTP response:" + responseStr);


        		// this method works!
        		BufferedReader br = new BufferedReader(new InputStreamReader(
        				(urlConn.getErrorStream())));  
        		String responseStr = "";
        		String output;
        		DebugLog("RestClient: inputStream:");
        		while ((output = br.readLine()) != null) {
        			DebugLog("RestClient: inputStream:" + output);
        			responseStr += output;
        		}
        		
                result.status = status;
                result.content = responseStr;
                return result;
            } catch (IOException ex) {
                Log.e(TAG, "getErrorStream: " + ex.toString());
            }        	
        } else {    	
	    	try {
	            //BufferedReader br = new BufferedReader(new InputStreamReader(urlConn.getInputStream(),"utf-8"));
	    		BufferedReader br = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
	            String line = null;
	
	            StringBuilder sb = new StringBuilder();
	            while ((line = br.readLine()) != null) {
	                sb.append(line + "\n");
	            }
	            br.close();
	
	            String responseStr = sb.toString();
	            DebugLog("HTTP response:" + responseStr);
	
	            result.status = status;
	            result.content = responseStr;
	            return result;            		
	        } catch (IOException ex) {
	            Log.e(TAG, "getInputStream: " + ex.toString());
	        }
        }
        
        return result;
    }    	
    
    
    
    // post by passing parameters in the url string
    public static RestResult doPost(String path, Map<String, Object> params) throws ResponseException {
//    
        RestResult result = new RestResult();
        int retries = 0;

        DebugLog("RestClient: doPost(Map): " + apiIntegrationUrl + path);

        // Construct the POST data.
        String content = "";

        if (params != null) {
            //convert parameters into content string
            try {
                StringBuilder sb = new StringBuilder();
                Iterator<Entry<String, Object>> iter = params.entrySet().iterator();

                sb.append('?');
                while (iter.hasNext()) {
                    Entry<String, Object> entry = iter.next();
                    sb.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                    sb.append('=');
                    //sb.append('"');
                    sb.append(URLEncoder.encode((String) entry.getValue(), "UTF-8"));
                    //sb.append('"');
                    if (iter.hasNext()) {
                        sb.append('&');
                    }
                }
                content = sb.toString();
                
            } catch (UnsupportedEncodingException ex) {
                Log.e(TAG, ex.toString());
            }
        }

        if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "RestClient: doPost: content: " + content);


        URL url = null;
        try {
            url = new URL(apiIntegrationUrl + path + content);
        } catch (MalformedURLException ex) {
            Log.e(TAG, ex.toString());
        }

        HttpURLConnection urlConn = null;
        try {
            // URL connection channel.
            urlConn = (HttpURLConnection) url.openConnection();
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
        }

        // Let the run-time system (RTS) know that we want input.
        urlConn.setDoInput (true);

        // Let the RTS know that we want to do output.
        urlConn.setDoOutput (true);

        // No caching, we want the real thing.
        urlConn.setUseCaches (false);

        try {
            urlConn.setRequestMethod("POST");
        } catch (ProtocolException ex) {
            Log.e(TAG, ex.toString());
        }

        // Specify the content type if needed.
        //urlConn.setRequestProperty("Content-Type",
        //  "application/x-www-form-urlencoded");

        // set request properties before connection is made!
        urlConn.setRequestProperty("Accept", "application/json");
        urlConn.setRequestProperty("Content-type", "application/json");

        Iterator<Entry<String, String>> headerIter = headersMap.entrySet().iterator();
        while (headerIter.hasNext()) {
            Entry<String, String> entry = headerIter.next();

            urlConn.setRequestProperty(entry.getKey(), entry.getValue());

            if (LootsieGlobals.debugNetworkLevel > 0) {
                Log.v(TAG, "RestClient: doPost: header: " + entry.getKey() + " " + entry.getValue());
            }

        }

        try {
            urlConn.connect();
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
        }

        DataOutputStream output = null;
        DataInputStream input = null;

        try {
            output = new DataOutputStream(urlConn.getOutputStream());
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
        }

        // You can get a FileNotFoundException from HttpUrlConnection (and OkHttpClient)
        // if your server returns >= HTTPStatus.BAD_REQUEST (400).
        // You should check the status code first to check what stream you need to read.

        try {
            int status = urlConn.getResponseCode();
        
            if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "RestClient: doPost: responseCode: " + status);

        if(status >= org.apache.http.HttpStatus.SC_BAD_REQUEST) {

//        	input = new DataInputStream (urlConn.getErrorStream());
//
//            StringBuilder sb = new StringBuilder();
//            String s;
//            while (true) {
//                s = input.readUTF();
//                if (s == null || s.length() == 0) {
//                    break;
//                }
//                sb.append(s);
//            }
//            String responseStr = sb.toString();
//            if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "HTTP response:" + responseStr);

       		// this method works!
    		BufferedReader br = new BufferedReader(new InputStreamReader(
    				(urlConn.getErrorStream())));  
    		String responseStr = "";
    		String outputStr = "";
    		DebugLog("RestClient: inputStream:");
    		while ((outputStr = br.readLine()) != null) {
    			DebugLog("RestClient: inputStream:" + outputStr);
    			responseStr += outputStr;
    		}
    		
            result.status = status;
            result.content = responseStr;
            return result;
        	
        } else {
                //input = new DataInputStream(urlConn.getInputStream());
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        urlConn.getInputStream(),"utf-8"));
                String line = null;

                StringBuilder sb = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();

                String responseStr = sb.toString();
                if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "HTTP response:" + responseStr);

                result.status = status;
                result.content = responseStr;
                return result;            		
            }




            } catch (IOException ex) {
            Log.e(TAG, ex.toString());
        }

        return result;
    }

    // write json data using output stream
    public static RestResult doPost(String path, String params) throws ResponseException {
        RestResult result = new RestResult();
        int retries = 0;

        if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "RestClient: doPost: " + apiIntegrationUrl + path);

        if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "RestClient: doPost: content: " + params);


        URL url = null;
        try {
            url = new URL(apiIntegrationUrl + path);
        } catch (MalformedURLException ex) {
            Log.e(TAG, ex.toString());
        }

        HttpURLConnection urlConn = null;
        try {
            // URL connection channel.
            urlConn = (HttpURLConnection) url.openConnection();
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
        }

        // Let the run-time system (RTS) know that we want input.
        urlConn.setDoInput (true);

        // Let the RTS know that we want to do output.
        urlConn.setDoOutput (true);

        // No caching, we want the real thing.
        urlConn.setUseCaches (false);

        try {
            urlConn.setRequestMethod("POST");
        } catch (ProtocolException ex) {
            Log.e(TAG, ex.toString());
        }


        // set request properties before connection is made!
        // urlConn.setRequestProperty("Accept", "application/json");
        urlConn.setRequestProperty("Accept", "*/*");


        Iterator<Entry<String, String>> headerIter = headersMap.entrySet().iterator();
        while (headerIter.hasNext()) {
            Entry<String, String> entry = headerIter.next();

            urlConn.setRequestProperty(entry.getKey(), entry.getValue());

            if (LootsieGlobals.debugNetworkLevel > 0) {
                Log.v(TAG, "RestClient: doPost: header: " + entry.getKey() + " " + entry.getValue());
            }

        }

        urlConn.setRequestProperty("Content-Length", String.valueOf(params.length()));
        //urlConn.setRequestProperty("Content-type", "application/json");
        urlConn.setRequestProperty("Content-type","application/x-www-form-urlencoded");





        try {
            urlConn.connect();
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
        }


        DataInputStream input = null;

//        DataOutputStream output = null;
//        try {
//            output = new DataOutputStream(urlConn.getOutputStream());
//            output.writeBytes(params);
//            //output.write(params.getBytes("UTF-8"));
//        } catch (IOException ex) {
//            Log.e(TAG, ex.toString());
//        }

        try {
            OutputStreamWriter wr = new OutputStreamWriter(urlConn.getOutputStream());
            wr.write(params);
            wr.close();
            //urlConn.getOutputStream().close();
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
        }


        // You can get a FileNotFoundException from HttpUrlConnection (and OkHttpClient)
        // if your server returns >= HTTPStatus.BAD_REQUEST (400).
        // You should check the status code first to check what stream you need to read.

        int status = 0;
        try {
            status = urlConn.getResponseCode();
        
            if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "RestClient: doPost: responseCode: " + status);

        if(status >= org.apache.http.HttpStatus.SC_BAD_REQUEST) {
//                input = new DataInputStream (urlConn.getErrorStream());
//
//                StringBuilder sb = new StringBuilder();
//                String s;
//                while (true) {
//                    s = input.readUTF();
//                    if (s == null || s.length() == 0) {
//                        break;
//                    }
//                    sb.append(s);
//                }
//                String responseStr = sb.toString();
//                if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "HTTP response:" + responseStr);

        		// this method works!
        		BufferedReader br = new BufferedReader(new InputStreamReader(
        				(urlConn.getErrorStream())));  
        		String responseStr = "";
        		String output;
        		DebugLog("RestClient: inputStream:");
        		while ((output = br.readLine()) != null) {
        			DebugLog("RestClient: inputStream:" + output);
        			responseStr += output;
        		}                
                
                result.status = status;
                result.content = responseStr;
                return result;
        	
        } else {
                //input = new DataInputStream(urlConn.getInputStream());
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        urlConn.getInputStream(),"utf-8"));
                String line = null;

                StringBuilder sb = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();

                String responseStr = sb.toString();
                if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "HTTP response:" + responseStr);

                result.status = status;
                result.content = responseStr;
                return result;            		
            }

            } catch (IOException ex) {
            Log.e(TAG, ex.toString());
        }
        
        result.status = status;
//        result.content = "";
        return result;
    }


    public static RestResult doPut(String path, String params) throws ResponseException {
        RestResult result = new RestResult();
        int retries = 0;

        if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "RestClient: doPut: " + apiIntegrationUrl + path);

        if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "RestClient: doPut: content: " + params);


        URL url = null;
        try {
            url = new URL(apiIntegrationUrl + path);
        } catch (MalformedURLException ex) {
            Log.e(TAG, ex.toString());
        }

        HttpURLConnection urlConn = null;
        try {
            // URL connection channel.
            urlConn = (HttpURLConnection) url.openConnection();
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
        }

        // Let the run-time system (RTS) know that we want input.
        //urlConn.setDoInput (true);

        // Let the RTS know that we want to do output.
        urlConn.setDoOutput (true);

        // No caching, we want the real thing.
        urlConn.setUseCaches (false);

        try {
            urlConn.setRequestMethod("PUT");
        } catch (ProtocolException ex) {
            Log.e(TAG, ex.toString());
        }

//        URL url = new URL("http://www.example.com/resource");
//        HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
//        httpCon.setDoOutput(true);
//        httpCon.setRequestMethod("PUT");
//        OutputStreamWriter out = new OutputStreamWriter(
//                httpCon.getOutputStream());
//        out.write("Resource content");
//        out.close();
//        httpCon.getInputStream();


        // set request properties before connection is made!
        // urlConn.setRequestProperty("Accept", "application/json");
        urlConn.setRequestProperty("Accept", "*/*");


        Iterator<Entry<String, String>> headerIter = headersMap.entrySet().iterator();
        while (headerIter.hasNext()) {
            Entry<String, String> entry = headerIter.next();

            urlConn.setRequestProperty(entry.getKey(), entry.getValue());

            if (LootsieGlobals.debugNetworkLevel > 0) {
                Log.v(TAG, "RestClient: doPut: header: " + entry.getKey() + " " + entry.getValue());
            }

        }

        urlConn.setRequestProperty("Content-Length", String.valueOf(params.length()));
        //urlConn.setRequestProperty("Content-type", "application/json");
        urlConn.setRequestProperty("Content-type","application/x-www-form-urlencoded");





        try {
            urlConn.connect();
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
        }


        DataInputStream input = null;

//        DataOutputStream output = null;
//        try {
//            output = new DataOutputStream(urlConn.getOutputStream());
//            output.writeBytes(params);
//            //output.write(params.getBytes("UTF-8"));
//        } catch (IOException ex) {
//            Log.e(TAG, ex.toString());
//        }

        try {
            OutputStreamWriter wr = new OutputStreamWriter(urlConn.getOutputStream());
            wr.write(params);
            wr.close();
            //urlConn.getOutputStream().close();
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
        }


        // You can get a FileNotFoundException from HttpUrlConnection (and OkHttpClient)
        // if your server returns >= HTTPStatus.BAD_REQUEST (400).
        // You should check the status code first to check what stream you need to read.

        int status = 0;
        try {
            status = urlConn.getResponseCode();

            if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "RestClient: doPut: responseCode: " + status);

            if(status >= org.apache.http.HttpStatus.SC_BAD_REQUEST) {
                input = new DataInputStream (urlConn.getErrorStream());

                StringBuilder sb = new StringBuilder();
                String s;
                while (true) {
                    s = input.readUTF();
                    if (s == null || s.length() == 0) {
                        break;
                    }
                    sb.append(s);
                }
                String responseStr = sb.toString();
                if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "HTTP response:" + responseStr);

                result.status = status;
                result.content = responseStr;
                return result;

            } else {
                //input = new DataInputStream(urlConn.getInputStream());
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        urlConn.getInputStream(),"utf-8"));
                String line = null;

                StringBuilder sb = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();

                String responseStr = sb.toString();
                if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "HTTP response:" + responseStr);

                result.status = status;
                result.content = responseStr;
                return result;
            }

        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
        }

        result.status = status;
//        result.content = "";
        return result;
    }

    private static JSONObject getJsonObjectFromMap(Map<String, Object> params) throws JSONException {
        //all the passed parameters from the post request
        //iterator used to loop through all the parameters
        //passed in the post request
        Iterator<Map.Entry<String, Object>> iter = params.entrySet().iterator();

        //Stores JSON
        JSONObject holder = new JSONObject();

        while (iter.hasNext()) {
            Map.Entry<String, Object> pairs = iter.next();
            if (pairs.getValue() instanceof Collection) {
                holder.put(pairs.getKey(), new JSONArray((Collection) pairs.getValue()));
            } else {
                holder.put(pairs.getKey(), pairs.getValue());
            }
        }

        return holder;
    }

    public static String doGet(String path, Map<String, Object> params) throws ResponseException {
        int retries = 0;
        BufferedReader buf = null;

        if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "RestClient: doGet: " + apiIntegrationUrl + path);

        String content = "";

        if (params != null) {
            //convert parameters into content string

            try {
                StringBuilder sb = new StringBuilder();
                Iterator<Entry<String, Object>> iter = params.entrySet().iterator();

                sb.append('?');
                while (iter.hasNext()) {
                    Entry<String, Object> entry = iter.next();
                    sb.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                    sb.append('=');
                    //sb.append('"');
                    sb.append(URLEncoder.encode((String) entry.getValue(), "UTF-8"));
                    //sb.append('"');
                    if (iter.hasNext()) {
                        sb.append('&');
                    }
                }
                content = sb.toString();

            } catch (UnsupportedEncodingException ex) {
                Log.e(TAG, ex.toString());
            }
        }

        if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "RestClient: doGet: content: " + content);

        URL url = null;
        try {
            url = new URL(apiIntegrationUrl + path + content);
        } catch (MalformedURLException ex) {
            Log.e(TAG, ex.toString());
        }

        HttpURLConnection urlConn = null;
        try {
            // URL connection channel.
            urlConn = (HttpURLConnection) url.openConnection();
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
        }

        // Let the run-time system (RTS) know that we want input.
        urlConn.setDoInput (true);


        // No caching, we want the real thing.
        urlConn.setUseCaches (false);

        try {
            urlConn.setRequestMethod("GET");
        } catch (ProtocolException ex) {
            Log.e(TAG, ex.toString());
        }

        // Specify the content type if needed.
        // set request properties before connection is made!
        urlConn.setRequestProperty("Accept", "application/json");
        urlConn.setRequestProperty("Content-type", "application/json");

        Iterator<Entry<String, String>> headerIter = headersMap.entrySet().iterator();
        while (headerIter.hasNext()) {
            Entry<String, String> entry = headerIter.next();

            urlConn.setRequestProperty(entry.getKey(), entry.getValue());

            if (LootsieGlobals.debugNetworkLevel > 0) {
                Log.v(TAG, "RestClient: doGet: header: " + entry.getKey() + " " + entry.getValue());
            }

        }

        try {
            urlConn.connect();
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
        }

        DataInputStream input = null;

        // You can get a FileNotFoundException from HttpUrlConnection (and OkHttpClient)
        // if your server returns >= HTTPStatus.BAD_REQUEST (400).
        // You should check the status code first to check what stream you need to read.

        try {
            int status = urlConn.getResponseCode();

            if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "RestClient: doGet: responseCode: " + status);

            if(status >= org.apache.http.HttpStatus.SC_BAD_REQUEST) {
                input = new DataInputStream (urlConn.getErrorStream());

                StringBuilder sb = new StringBuilder();
                String s;
                while (true) {
                    s = input.readUTF();
                    if (s == null || s.length() == 0) {
                        break;
                    }
                    sb.append(s);
                }
                String responseStr = sb.toString();
                if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "HTTP response:" + responseStr);
                return responseStr;

            } else {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        urlConn.getInputStream(),"utf-8"));
                String line = null;

                StringBuilder sb = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();

                String responseStr = sb.toString();
                if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "HTTP response:" + responseStr);
                return responseStr;
            }

        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
        }


        return "";
    }

    public static RestResult doDelete(String path, String params) throws ResponseException {

        int retries = 0;
        BufferedReader buf = null;
        RestResult result = new RestResult();

        // TODO: doDelete REST call
        if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "RestClient: doDelete: " + apiIntegrationUrl + path);

        if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "RestClient: doDelete: content: " + params);


        URL url = null;
        try {
            url = new URL(apiIntegrationUrl + path);
        } catch (MalformedURLException ex) {
            Log.e(TAG, ex.toString());
        }

        HttpURLConnection urlConn = null;
        try {
            // URL connection channel.
            urlConn = (HttpURLConnection) url.openConnection();
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
        }

        // Let the run-time system (RTS) know that we want input.
        urlConn.setDoInput (true);

        // Let the RTS know that we want to do output.
        urlConn.setDoOutput (true);

        // No caching, we want the real thing.
        urlConn.setUseCaches (false);

        try {
            urlConn.setRequestMethod("DELETE");
        } catch (ProtocolException ex) {
            Log.e(TAG, ex.toString());
        }


        // set request properties before connection is made!
        // urlConn.setRequestProperty("Accept", "application/json");
        urlConn.setRequestProperty("Accept", "*/*");


        Iterator<Entry<String, String>> headerIter = headersMap.entrySet().iterator();
        while (headerIter.hasNext()) {
            Entry<String, String> entry = headerIter.next();

            urlConn.setRequestProperty(entry.getKey(), entry.getValue());

            if (LootsieGlobals.debugNetworkLevel > 0) {
                Log.v(TAG, "RestClient: doDelete: header: " + entry.getKey() + " " + entry.getValue());
            }

        }

        urlConn.setRequestProperty("Content-Length", String.valueOf(params.length()));
        //urlConn.setRequestProperty("Content-type", "application/json");
        urlConn.setRequestProperty("Content-type","application/x-www-form-urlencoded");





        try {
            urlConn.connect();
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
        }


        DataInputStream input = null;

//        DataOutputStream output = null;
//        try {
//            output = new DataOutputStream(urlConn.getOutputStream());
//            output.writeBytes(params);
//            //output.write(params.getBytes("UTF-8"));
//        } catch (IOException ex) {
//            Log.e(TAG, ex.toString());
//        }

        try {
            OutputStreamWriter wr = new OutputStreamWriter(urlConn.getOutputStream());
            wr.write(params);
            wr.close();
            //urlConn.getOutputStream().close();
        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
        }


        // You can get a FileNotFoundException from HttpUrlConnection (and OkHttpClient)
        // if your server returns >= HTTPStatus.BAD_REQUEST (400).
        // You should check the status code first to check what stream you need to read.

        int status = 0;
        try {
            status = urlConn.getResponseCode();

            if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "RestClient: doDelete: responseCode: " + status);

            if(status >= org.apache.http.HttpStatus.SC_BAD_REQUEST) {
                input = new DataInputStream (urlConn.getErrorStream());

                StringBuilder sb = new StringBuilder();
                String s;
                while (true) {
                    s = input.readUTF();
                    if (s == null || s.length() == 0) {
                        break;
                    }
                    sb.append(s);
                }
                String responseStr = sb.toString();
                if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "HTTP response:" + responseStr);

                result.status = status;
                result.content = responseStr;
                return result;

            } else {
                //input = new DataInputStream(urlConn.getInputStream());
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        urlConn.getInputStream(),"utf-8"));
                String line = null;

                StringBuilder sb = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();

                String responseStr = sb.toString();
                if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "HTTP response:" + responseStr);

                result.status = status;
                result.content = responseStr;
                return result;
            }

        } catch (IOException ex) {
            Log.e(TAG, ex.toString());
        }

        result.status = status;

        return result;
    }


    public static void addHeader(String header, String value) {
        //headers.put(header, new BasicHeader(header, value));
        headersMap.put(header, value);
    }

    public static void removeHeader(String header) {
        headersMap.remove(header);
    }

    public static void clearSessionToken() {
//        headers.remove(Headers.USER_TOKEN);
    }



    public static void setTimeout(int timeout) {
        if (LootsieGlobals.debugNetworkLevel > 0) Log.v(TAG, "Set timeout: " + timeout/1000 + "s");
//        HttpConnectionParams.setConnectionTimeout(mHttpClient.getParams(), timeout);
    }

    public static void setMaxRetries(int maxRetries) {
        mMaxRetries = maxRetries;
    }

//    public static void setApiIntegrationUrl(String uri) {
//        if ((uri == null) || (uri.length() == 0)) {
//        	Log.e(TAG, "RestClient : setApiIntegrationUrl ERROR: apiIntegrationUrl is invalid!");
//        } else {
//        	apiIntegrationUrl = uri;
//        }
//    }
    
    
    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugNetworkLevel > 0) {
        Logs.v(TAG, String.format(msg, args));
        }
    }
    
}

