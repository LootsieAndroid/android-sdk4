package com.lootsie.sdk.callbacks;

import com.lootsie.sdk.utils.LootsieGlobals;

/**
 * Created by jerrylootsie on 9/2/15.
 */

public interface IDurationCallback {

    public void reportDurationMillisec(long durationMs);

}
