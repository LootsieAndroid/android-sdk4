package com.lootsie.sdk.viewcontrollers.base;

/**
 * Created by jerrylootsie on 3/2/15.
 */
public interface IUpdatableFragment {

    public void setUpdatableFragment(UpdatableFragment fragment);
    public void updateCurrentFragment();

}
