package com.lootsie.sdk.lootsiehybrid;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

//import com.lootsie.sdk.net.Headers;
//import com.lootsie.sdk.net.ResponseException;
//import com.lootsie.sdk.net.RestClient;
import com.lootsie.sdk.utils.RUtil;
import com.lootsie.sdk.model.Achievement;
import com.lootsie.sdk.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jerrylootsie on 2/17/15.
 *
 * @author Alexander Salnikov
 */
public class LootsieAccountManager {


    private static final String TAG = "Lootsie LootsieAccountManager";
//    private static final WindowManager.LayoutParams mlp =
//            new WindowManager.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0, 0, 2, 40, -3);

    private Context mContext;
    private static Account account = null;
    AccountManager am;

    private final Handler handler = new Handler();

    public LootsieAccountManager(Context context) {
        this.mContext = context;
        am = AccountManager.get(mContext);

        if (LootsieGlobals.debugLevel > 0) {
            Log.v(TAG, "UserManager: account manager: KEY_ACCOUNT_NAME : " + am.KEY_ACCOUNT_NAME);
            Log.v(TAG, "UserManager: account manager: KEY_ACCOUNT_TYPE : " + am.KEY_ACCOUNT_TYPE);
            Log.v(TAG, "UserManager: account manager: KEY_AUTH_TOKEN_LABEL : " + am.KEY_AUTH_TOKEN_LABEL);
            Log.v(TAG, "UserManager: account manager: KEY_AUTHTOKEN : " + am.KEY_AUTHTOKEN);
        }
        readAccount();
    }

    private Account readAccount() {
        try {
            Account[] accounts = am.getAccountsByType(LootsieGlobals.ACCOUNT_TYPE);
            if (accounts.length > 0) {
                account = accounts[0];
            }
            return account;
        } catch (Exception e) {
            Logs.e(TAG, "readAccount: exception: " + e.getMessage() );
            return null;
        }
    }

    private void checkCallingUidAgainstAuthenticator(Account account) {
        final int uid = Binder.getCallingUid();
        //if (account == null || !hasAuthenticatorUid(account.type, uid)) {
        if (account == null) {
            String msg = "caller uid " + uid + " is different than the authenticator's uid";
            Log.w(TAG, msg);
            //throw new SecurityException(msg);
        }
        //if (Log.isLoggable(TAG, Log.VERBOSE)) {
        Log.v(TAG, "caller uid " + uid + " is the same as the authenticator's uid");
        //}
    }


    public void logout() {

        // AccountManagerCallback<Boolean> callback
        final AccountManagerCallback<Boolean> callback = new AccountManagerCallback<Boolean>() {
            @Override
            public void run(AccountManagerFuture<Boolean> result) {
                // TODO Auto-generated method stub

                try {
                    Log.v(TAG, "logout: AccountManagerCallback: " + Boolean.toString(result.getResult()));
                } catch (OperationCanceledException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (AuthenticatorException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        };

        // TODO: handle logout
        // DELETE /user/session � Sign Out
//        String response;
//        try {
//
//            response = RestClient.doDelete(LootsieApi.SIGN_OUT_URL.getUri());
//
//            if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "logout: " + response);
//
//            if (response.length() == 0) {
//                RestClient.clearSessionToken();
//
//                if (account != null) {
//                    am.removeAccount(account, callback, null);
//                } else {
//                    Log.v(TAG, "logout: no account to remove from account manager");
//                }
//            }
//
//        } catch (ResponseException e) {
//            e.printStackTrace();  //todo
//        }

    }

    public void removeAccount() {

        final AccountManagerCallback<Boolean> callback = new AccountManagerCallback<Boolean>() {
            @Override
            public void run(AccountManagerFuture<Boolean> result) {
                // TODO Auto-generated method stub

                try {
                    Log.v(TAG, "logout: AccountManagerCallback: " + Boolean.toString(result.getResult()));
                } catch (OperationCanceledException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (AuthenticatorException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        };

        try {
            if (account != null) {
                am.removeAccount(account, callback, null);
            } else {
                Log.v(TAG, "logout: no account to remove from account manager");
            }
        } catch (Exception e) {
            Logs.e(TAG, "removeAccount: exception: " + e.getMessage() );
        }
    }


    /**
     * Method provides login as guest.
     * Method required call from separate thread
     *
     * LootsieImpl : startSession -> UserManager : loginAsGuest
     */
    public boolean loginAsGuest() {

        if (LootsieGlobals.debugLevel > 0) {
            Log.v(TAG, "UserManager: loginAsGuest");
        }

        boolean success = false;

//
//            Account account = null;
//            Account[] accounts = am.getAccountsByType(LootsieGlobals.ACCOUNT_TYPE);
//            if (accounts.length == 0) {
//                if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "loginAsGuest: creating new account");
//
//                // see res/layout/xml/authenticator.xml : android:accountType
//                // see res/layout/xml/syncadapter.xml : android:accountType
//                account = new Account(LootsieGlobals.ACCOUNT_GUEST_NAME, LootsieGlobals.ACCOUNT_TYPE);

//                // store data in account manager
//                // crashes with security exception from accountmanager lootsie-1243
//                // account, password, userdata
//                Bundle userdata = new Bundle();
//                //am.addAccountExplicitly(account, "", null);
//                am.addAccountExplicitly(account, "", userdata);
//
//                // we need to make the token unique to each application since the accountmanager is shared between several games on the device
//                String appId = Lootsie.getInstance().getAppId();
//
//                am.setUserData(account, LootsieGlobals.ACCOUNT_TYPE  + LootsieGlobals.APP_ID_SEPARATOR + appId, LootsieGlobals.ACCOUNT_GUEST_NAME);
//            } else {
//                if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "loginAsGuest: reusing existing account");
//
//                // existing account exists in account manager
//                account = accounts[0];
//
//            }
//
//            // we need to make the token unique to each application since the accountmanager is shared between several games on the device
//            String appId = Lootsie.getInstance().getAppId();
//            am.setUserData(account, LootsieGlobals.KEY_AUTHTOKEN + LootsieGlobals.APP_ID_SEPARATOR + appId, authToken);
//            if (LootsieGlobals.debugLevel > 0) Log.v(TAG,"store uniqueKeyAuthToken: " + LootsieGlobals.KEY_AUTHTOKEN + LootsieGlobals.APP_ID_SEPARATOR + appId);
//
//
//            long loginTime = SystemClock.elapsedRealtime() - startTime;
//            if (LootsieGlobals.debugLevel > 0) {
//                Log.d(TAG, String.format("loginAsGuest took %d", loginTime));
//            }
//
//            //set new auth token
//            RestClient.addHeader(Headers.USER_TOKEN, authToken);
//


        return success;


    }




    // AuthenticatorActivity : finishLogin -> UserManager : createUser
    // destroys existing account and creates a new one in account manager
    public User createUser(User user, String authToken) {
        if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "createUser: email:" + user.email);

        try {
            Account[] accounts = am.getAccountsByType(LootsieGlobals.ACCOUNT_TYPE);
            if (accounts != null && accounts.length > 0) {
                for (Account account : accounts) {
                    am.removeAccount(account, null, null);
                }
            }

            if (user.isGuest) {
                // see res/layout/xml/authenticator.xml : android:accountType
                // see res/layout/xml/syncadapter.xml : android:accountType
                account = new Account(LootsieGlobals.ACCOUNT_GUEST_NAME, LootsieGlobals.ACCOUNT_TYPE);
            } else {
                account = new Account(user.getDisplayName(), LootsieGlobals.ACCOUNT_TYPE);
            }
            am.addAccountExplicitly(account, "", null);

            // we need to make the token unique to each application since the accountmanager is shared between several games on the device
            String appId = LootsieEngine.getInstance().getAppId();
            am.setUserData(account, LootsieGlobals.KEY_AUTHTOKEN + LootsieGlobals.APP_ID_SEPARATOR + appId, authToken);

            if (user.isGuest) {
                am.setUserData(account, LootsieGlobals.ACCOUNT_TYPE + LootsieGlobals.APP_ID_SEPARATOR + appId, LootsieGlobals.ACCOUNT_GUEST_NAME);
            }

            saveUserInfo(user);
        } catch (Exception e) {
            Logs.e(TAG, "createUser: exception: " + e.getMessage() );
        }
        return user;
    }

    // AuthenticatorActivity : finishLogin -> UserManager : createUser -> saveUserInfo
    public void saveUserInfo(User user) {

        try {
            if (account != null || readAccount() != null) {

                // will this step on the account data of a user logged in with another lootsie enabled game?
                // user data gets overridden by REST call get /user/account with a specific token from the server!

                // we need to make the token unique to each application since the accountmanager is shared between several games on the device
                String appId = LootsieEngine.getInstance().getAppId();

                am.setUserData(account, User.FIRST_NAME + LootsieGlobals.APP_ID_SEPARATOR + appId, user.firstName);
                am.setUserData(account, User.LAST_NAME + LootsieGlobals.APP_ID_SEPARATOR + appId, user.lastName);
                am.setUserData(account, User.EMAIL + LootsieGlobals.APP_ID_SEPARATOR + appId, user.email);
                am.setUserData(account, User.TOTAL_LP + LootsieGlobals.APP_ID_SEPARATOR + appId, "" + user.totalLp);
                am.setUserData(account, User.ACHIEVED_LP + LootsieGlobals.APP_ID_SEPARATOR + appId, "" + user.achievedLp);
                am.setUserData(account, User.IS_GUEST + LootsieGlobals.APP_ID_SEPARATOR + appId, "" + user.isGuest);
                am.setUserData(account, User.OLD_LP + LootsieGlobals.APP_ID_SEPARATOR + appId, "" + user.oldLp);

                am.setUserData(account, User.PHOTO_URL + LootsieGlobals.APP_ID_SEPARATOR + appId, user.userimg);
            }

            if (TextUtils.isEmpty(user.firstName) && TextUtils.isEmpty(user.lastName) && !TextUtils.isEmpty(user.email)) {
                String name = user.email;
                int index = name.indexOf("@");
                if (index > 0) {
                    name = name.substring(0, index);
                    user.firstName = name;
                }
            }
            saveAchievements(user.achievements);
        } catch (Exception e) {
            Logs.e(TAG, "saveUserInfo: exception: " + e.getMessage() );
        }
    }

    public void updateUsersLp(int totalLp, int oldLp) {

        try {
            if (account != null || readAccount() != null) {
                // we need to make the token unique to each application since the accountmanager is shared between several games on the device
                String appId = LootsieEngine.getInstance().getAppId();

                am.setUserData(account, User.TOTAL_LP + LootsieGlobals.APP_ID_SEPARATOR + appId, "" + totalLp);
                am.setUserData(account, User.OLD_LP + LootsieGlobals.APP_ID_SEPARATOR + appId, "" + oldLp);
            }
        } catch (Exception e) {
            Logs.e(TAG, "updateUsersLp: exception: " + e.getMessage());
        }
    }

    public User getUser() {
        final AccountManagerFuture<Bundle> amf;

        if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "getUser:");

        User user = null;

        try {

            //if (account != null && readAccount() != null) {
            if (account != null || readAccount() != null) {
                user = new User();

                // separate data based on appId to avoid collisions between Lootsie Enabled Games (LEGs)
                String appId = LootsieEngine.getInstance().getAppId();

                // caller uid 10107 is different than the authenticator's uid

                // request an auth token for the selected accounts
                // http://developer.android.com/reference/android/accounts/AccountManager.html
//                amf = am.getAuthToken(account, LootsieGlobals.ACCOUNT_TYPE, true,
//                        new AccountManagerCallback<Bundle>() {
//
//                            @Override
//                            public void run(AccountManagerFuture<Bundle> arg0) {
//
//                                try {
//                                    Bundle result;
//                                    Intent i;
//                                    String token;
//
//                                    result = arg0.getResult();
//                                    if (result.containsKey(AccountManager.KEY_INTENT)) {
//                                        i = (Intent)result.get(AccountManager.KEY_INTENT);
//                                        if (i.toString().contains("GrantCredentialsPermissionActivity")) {
//                                            // Will have to wait for the user to accept
//                                            // the request therefore this will have to
//                                            // run in a foreground application
//                                            //cbt.startActivity(i);
//                                            Logs.d(TAG,"LootsieAccountManager: GrantCredentialsPermissionActivity");
//                                        } else {
//                                            //cbt.startActivity(i);
//                                            Logs.d(TAG,"LootsieAccountManager: OtherActivity");
//                                        }
//
//                                    } else {
//                                        token = (String)result.get(AccountManager.KEY_AUTHTOKEN);
//
//                                        /*
//                                         * work with token
//                                         */
//
//                                        Logs.d(TAG, "LootsieAccountManager: got token: " + token);
//
//                                        // Remember to invalidate the token if the web service rejects it
//                                        // if(response.isTokenInvalid()){
//                                        //    accMgr.invalidateAuthToken(authTokenType, token);
//                                        // }
//
//
//
//                                    }
//                                } catch (OperationCanceledException e) {
//                                    // TODO Auto-generated catch block
//                                    e.printStackTrace();
//                                } catch (AuthenticatorException e) {
//                                    // TODO Auto-generated catch block
//                                    e.printStackTrace();
//                                } catch (IOException e) {
//                                    // TODO Auto-generated catch block
//                                    e.printStackTrace();
//                                }
//
//                            }
//                        }, handler);


//                AccountManagerCallback<Bundle> callback=new AccountManagerCallback<Bundle>(){
//                    public void run(    AccountManagerFuture<Bundle> future){
//                        try {
//                            Bundle bundle=future.getResult();
//                            bundle.keySet();
//
//                            //Set<String> keys = bundle.get.keySet();
//                            if (bundle.containsKey(AccountManager.KEY_INTENT)) {
//                                final Intent i = (Intent) bundle.get(AccountManager.KEY_INTENT);
//                                i.setFlags(0);
//
//                                //LootsieEngine.getInstance().getApplicationContext().startActivity(i);
//                                //LootsieEngine.getInstance().getActivityContext().startActivity(i);  // crashes app
//
//                                String hint = i.toString() + " -> " + i.getExtras().keySet();
//
//                                Log.d(TAG,"LootsieAccountManager: account added: " + hint);
//
//                                for (String key : bundle.keySet()) {
//                                    Object value = bundle.get(key);
//                                    Log.d(TAG, String.format("bundle: %s %s (%s)", key,
//                                            value.toString(), value.getClass().getName()));
//                                }
//
//                                for (String key : i.getExtras().keySet()) {
//                                    Object value = i.getExtras().get(key);
//                                    Log.d(TAG, String.format("extras: %s %s (%s)", key,
//                                            value.toString(), value.getClass().getName()));
//                                }
//
//                            } else {
//                                String hint = bundle.toString();
//                                Log.d(TAG,"LootsieAccountManager: account added: " + bundle);
//                            }
//
//
//                        }
//                        catch (      OperationCanceledException e) {
//                            Log.d(TAG,"LootsieAccountManager: addAccount was canceled");
//                        }
//                        catch (      IOException e) {
//                            Log.d(TAG,"LootsieAccountManager: addAccount failed: " + e);
//                        }
//                        catch (      AuthenticatorException e) {
//                            Log.d(TAG,"LootsieAccountManager: addAccount failed: " + e);
//                        }
//                    }
//                };

                //am.get(this.mContext.getApplicationContext()).addAccount(LootsieGlobals.ACCOUNT_TYPE,LootsieGlobals.ACCOUNT_TYPE,null,null,AdmobActivity.this,callback,null);
                //  addAccount (String accountType, String authTokenType, String[] requiredFeatures, Bundle addAccountOptions, Activity activity, AccountManagerCallback<Bundle> callback, Handler handler)
                //am.get(this.mContext).addAccount(LootsieGlobals.ACCOUNT_TYPE, "", null, null, null, callback, handler);
                //am.addAccount(LootsieGlobals.ACCOUNT_TYPE, "", null, null, null, callback, handler);

                //am.getAuthToken (Account account, String authTokenType, Bundle options, boolean notifyAuthFailure, AccountManagerCallback<Bundle> callback, Handler handler)
//                amf = am.getAuthToken(account, LootsieGlobals.ACCOUNT_TYPE, null, true, callback, handler);


                user.firstName = am.getUserData(account, User.FIRST_NAME + LootsieGlobals.APP_ID_SEPARATOR + appId);
                user.lastName = am.getUserData(account, User.LAST_NAME + LootsieGlobals.APP_ID_SEPARATOR + appId);
                user.email = am.getUserData(account, User.EMAIL + LootsieGlobals.APP_ID_SEPARATOR + appId);

                String achievedLp = am.getUserData(account, User.ACHIEVED_LP + LootsieGlobals.APP_ID_SEPARATOR + appId);
                String totalLp = am.getUserData(account, User.TOTAL_LP + LootsieGlobals.APP_ID_SEPARATOR + appId);
                String oldLp = am.getUserData(account, User.OLD_LP + LootsieGlobals.APP_ID_SEPARATOR + appId);

                if (!TextUtils.isEmpty(achievedLp) && TextUtils.isDigitsOnly(achievedLp)) {
                    user.achievedLp = Integer.valueOf(achievedLp);
                }
                if (!TextUtils.isEmpty(totalLp) && TextUtils.isDigitsOnly(totalLp)) {
                    user.totalLp = Integer.valueOf(totalLp);
                }
                if (!TextUtils.isEmpty(oldLp) && TextUtils.isDigitsOnly(oldLp)) {
                    user.oldLp = Integer.valueOf(oldLp);
                }

                user.isGuest = LootsieGlobals.ACCOUNT_GUEST_NAME.equalsIgnoreCase(
                        am.getUserData(account, LootsieGlobals.ACCOUNT_TYPE  + LootsieGlobals.APP_ID_SEPARATOR + appId));

                user.achievements = strToAchievements(am.getUserData(account, getUniqueUserAchievementsKey()));
                
                // user account integrity check
                if ((!user.isGuest) && ((user.email == null) || (!isEmailValid(user.email)))) {
                	Log.w(TAG, "getUser: account manager data failed integrity check!");
                	return null;                	
                }
                
            } else {
                Log.w(TAG, "getUser: no accounts!");
            }
        } catch (Exception e) {
            Logs.e(TAG, "getUser: exception: " + e.getMessage() );
        }

        return user;
    }

    static Pattern emailPattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

    private static boolean isEmailValid(String email) {
        Matcher m = emailPattern.matcher(email);
        return m.matches();
    }
    
    public String getUniqueUserAchievementsKey() {
        String appId = LootsieEngine.getInstance().getAppId();
        String uniqueAchievementsKey = User.ACHIEVEMENTS + LootsieGlobals.APP_ID_SEPARATOR + appId;
        return uniqueAchievementsKey;
    }

    public String getUniqueUserResendAchievementsKey() {
        String appId = LootsieEngine.getInstance().getAppId();
        String uniqueResendAchievementsKey = User.RESEND_ACHIEVEMENTS + LootsieGlobals.APP_ID_SEPARATOR + appId;
        return uniqueResendAchievementsKey;
    }


    public List<Achievement> getUserAchievements() {
        return getUserAchievements(getUniqueUserAchievementsKey());
    }

    public List<Achievement> getUserAchievementsToResend() {
        return getUserAchievements(getUniqueUserResendAchievementsKey());
    }

    private List<Achievement> getUserAchievements(String settings) {
        if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "getUserAchievements: " + settings);

        List<Achievement> achievements = new ArrayList<Achievement>(0);

        if (account != null) {
            String str = am.getUserData(account, settings);
            achievements = strToAchievements(str);
        }
        return achievements;
    }

    public void saveAchievements(List<Achievement> newAchievements) {
        saveAchievements(getUniqueUserAchievementsKey(), newAchievements);
    }

    public void saveAchievementsToResend(List<Achievement> newAchievements) {

        try {
            if (account != null || readAccount() != null) {
                if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "saveAchievementsToResend: " + getUniqueUserResendAchievementsKey());

                String str = am.getUserData(account, getUniqueUserResendAchievementsKey());

                JSONArray array = new JSONArray();
                //get old user achievements
                List<Achievement> achievements = strToAchievements(str);
                //add new achievements
                achievements.addAll(newAchievements);
                for (Achievement a : achievements) {
                    try {
                        array.put(a.toJson());
                    } catch (JSONException e) {
                    }
                }
                //save user achievements
                am.setUserData(account, getUniqueUserResendAchievementsKey(), array.toString());
            } else {
                if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "saveAchievementsToResend: no account exists!");
            }
        } catch (Exception e) {
            Logs.e(TAG, "saveAchievementsToResend: exception: " + e.getMessage() );
        }
    }

    public void clearAchievementsToResend() {
        int totalLp = 0;

//        if (account != null || readAccount() != null) {
//            if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "clearAchievementsToResend: " + getUniqueUserResendAchievementsKey());
//
//            List<Achievement> newAchievements = getUserAchievementsToResend();
//            for (Achievement a : newAchievements) {
//                //calculate new user's LP
//                totalLp += a.lp;
//            }
//            JSONArray array = new JSONArray();
//            //get old user achievements
//            String str = am.getUserData(account, getUniqueUserAchievementsKey());
//            List<Achievement> achievements = strToAchievements(str);
//            //add new achievements
//            achievements.addAll(newAchievements);
//            for (Achievement a : achievements) {
//                try {
//                    array.put(a.toJson());
//                } catch (JSONException e) {
//                }
//            }
//            //save user achievements
//            am.setUserData(account, getUniqueUserAchievementsKey(), array.toString());
//
//            String appId = ((LootsieImpl) LootsieImpl.getInstance()).getAppId();
//            String sCurrentTotalLp = am.getUserData(account, User.TOTAL_LP + Constants.APP_ID_SEPARATOR + appId);
//            if (sCurrentTotalLp != null) {
//                try {
//                    int currentTotalLp = Integer.valueOf(sCurrentTotalLp);
//                    am.setUserData(account, User.OLD_LP + Constants.APP_ID_SEPARATOR + appId, "" + currentTotalLp);
//                    totalLp += currentTotalLp;
//                } catch (NumberFormatException ignored) {
//                }
//            }
//            am.setUserData(account, User.TOTAL_LP  + Constants.APP_ID_SEPARATOR + appId, "" + totalLp);
//            //clear user achievements to resend
//            am.setUserData(account, getUniqueUserResendAchievementsKey(), null);
//        } else {
//            if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "clearAchievementsToResend: no account exists!");
//        }

    }

    private void saveAchievements(final String settings, final List<Achievement> userAchievements) {
        if (userAchievements == null) {
            Log.w(TAG, "UserManager: saveAchievements: userAchievements is null!");
            return;
        }

        try {
            //        int totalLp = 0;
            ArrayList<Achievement> newAchievements = new ArrayList<Achievement>(userAchievements);
            //        Collections.copy(newAchievements, userAchievements);

            if (account != null || readAccount() != null) {
                if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "saveAchievements: " + settings);

                String str = am.getUserData(account, settings);
                List<Achievement> achievements = new ArrayList<Achievement>();
                if (!TextUtils.isEmpty(str)) {
                    //get old user achievements
                    achievements.addAll(strToAchievements(str));
                    newAchievements.removeAll(achievements);
                }
                JSONArray array = new JSONArray();
                //add new achievements
                achievements.addAll(newAchievements);
                for (Achievement a : achievements) {
                    try {
                        array.put(a.toJson());
                    } catch (JSONException e) {
                    }
                }
                //save user achievements
                // this relies on the achievements list being unique on a per app basis!
                am.setUserData(account, settings, array.toString());
            } else {
                if (LootsieGlobals.debugLevel > 0)
                    Log.v(TAG, "saveAchievements: no account exists!");
            }
        } catch (Exception e) {
            Logs.e(TAG, "saveAchievements: exception: " + e.getMessage() );
        }

    }


    private static List<Achievement> strToAchievements(String str) {
        LinkedList<Achievement> achievements = new LinkedList<Achievement>();
        if (str != null && str.length() > 0) {
            JSONArray array;
            try {
                array = new JSONArray(str);
                int length = array.length();
                for (int i = 0; i < length; i++) {
                    try {
                        JSONObject obj = array.getJSONObject(i);

                        Achievement a = new Achievement();
                        a.parseFromJSON(obj);

                        achievements.add(a);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return achievements;
    }

    public String resetUserPassword(final String email) {
        if (TextUtils.isEmpty(email)) {
            //callback.onErrorResponse(new Exception("Email is empty"));
            return mContext.getString(RUtil.getStringId(mContext, "com_lootsie_login_activity_text_email_wrong"));
        }

        // test the error message
        // return false

//        HashMap<String, Object> map = new HashMap<String, Object>(1);
//        map.put("email", email);
//        JSONObject requestObject = new JSONObject(map);
//
//        try {
//            String response = (String) RestClient.doPost(LootsieApi.USER_ACCOUNT_RESET_PASSWORD_EMAIL_URL.getUri(), map);
//
//            if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "resetUserPassword: response: " + response);
//
//            // there is no response to the reset password api call
//
//        } catch (ResponseException e) {
//            e.printStackTrace();  //todo
//            try {
//                // example: {"errors": [{"field": "gender", "message": "invalid"}]}
//                JSONObject errorJson = new JSONObject(e.message);
//                JSONArray errors = errorJson.getJSONArray("errors");
//                if (errors.length() > 0) {
//                    JSONObject errorsMessage = (JSONObject) errors.get(0);
//                    return errorsMessage.getString("message");
//                }
//
//            } catch (JSONException e1) {
//                e1.printStackTrace();
//                return mContext.getString(RUtil.getStringId(mContext, "com_lootsie_login_activity_text_email_wrong"));
//            }
//            return e.message;
//        }

        return "";
    }

}
