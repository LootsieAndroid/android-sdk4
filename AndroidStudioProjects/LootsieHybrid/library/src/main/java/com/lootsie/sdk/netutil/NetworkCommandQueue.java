package com.lootsie.sdk.netutil;

import android.os.AsyncTask;

import com.lootsie.sdk.lootsiehybrid.sequences.AchievementReachedSequence;
import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.Util;


import java.util.ArrayList;

/**
 * Created by jerrylootsie on 3/6/15.
 */
public class NetworkCommandQueue implements Runnable {

    private static ArrayList<NetworkCommand> commandQueue = null;

    private static boolean finished = false;
    private static String TAG = "Lootsie NetworkCommandQueue";


    public NetworkCommandQueue(ArrayList<NetworkCommand> inputCommandQueue) {
        super();

        finished = false;
        commandQueue = inputCommandQueue;
    }


    public void finishQueue() {
        finished = true;
    }


    @Override
    public void run() {
        int queueFullChecks = 0;
        int queueDrainCheckLimit = 5;

        DebugLog("NetworkCommandQueue: starting ");

        // handle js command queue "webappComQueue"
        while (!Thread.interrupted()  && !finished) {

            try {
                // check callbacks queue size before sending

                if (commandQueue.size() > 0) {
                    final NetworkCommand networkCommand = commandQueue.get(0);

                    DebugLog1("NetworkCommandQueue (outer): " + networkCommand.commandType.toString() + " " + Util.getThreadSignature());

                    // run commands on another thread
                    class PostNetworkCommandTask extends AsyncTask<String, Void, String> {
                        @Override
                        protected String doInBackground(String... arg0) {
                            return null;
                        }

                        @Override
                        protected void onPostExecute(String result) {
                            // run on UI thread
                            DebugLog1("NetworkCommandQueue (inner): " + networkCommand.commandType.toString() + " " + Util.getThreadSignature());

                            switch(networkCommand.commandType) {
                              case ACHIEVEMENT_REACHED:
                                  // kick off the achievement reached sequence
                                  AchievementReachedSequence.start(networkCommand.achievementId, networkCommand.callback);
                                  break;

                              default:
                                   DebugLog("NetworkCommandQueue: unknown achievement type");
                                  break;

                            };

                        }
                    }

                    new PostNetworkCommandTask().execute("");

                    commandQueue.remove(0);
                }
                Thread.sleep(1000);

            } catch (Exception e) {
                DebugLog("LootsieEngine: webappComThread: thread interrupted! " + Util.getThreadSignature());
                Logs.e(TAG, "LootsieJSCommandQueue: run exception " + e.getMessage());
                e.printStackTrace();
            }
        }

        DebugLog("LootsieEngine: LootsieJSCommandQueue: finished ");
    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private void DebugLog1(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 1) {
            Logs.v(TAG, String.format(msg, args));
        }
    }


}
