package com.lootsie.sdk.viewcontrollers;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.utils.RUtil;
import com.lootsie.sdk.viewcontrollers.base.UpdatableFragment;

/**
 * Created by jerrylootsie on 3/4/15.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class TermsConditionsFragment extends UpdatableFragment {

    private static String TAG = "Lootsie TermsConditions Fragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (LootsieGlobals.debugLevel > 0) Logs.d(TAG, "onCreateView");

        //View view = inflater.inflate(R.layout.com_lootsie_terms_fragment, container, false);
        View view = inflater.inflate(RUtil.getLayoutId(this.getActivity().getApplicationContext(), "com_lootsie_terms_conditions_fragment"), container, false);

        view.setTag("termsConditionsFragment");

        return view;
    }

}
