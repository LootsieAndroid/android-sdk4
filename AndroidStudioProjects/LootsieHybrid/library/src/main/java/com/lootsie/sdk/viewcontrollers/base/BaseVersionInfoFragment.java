package com.lootsie.sdk.viewcontrollers.base;

import com.lootsie.sdk.device.Device;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

/**
 * Created by jerrylootsie on 3/2/15.
 */
public class BaseVersionInfoFragment extends UpdatableFragment {

    private static String TAG = "Lootsie BaseVersionInfoFragment";

    protected static Device device = null;

    public BaseVersionInfoFragment() {
    	DebugLog("BaseVersionInfoFragment");

        device = LootsieEngine.getInstance().getDevice();
    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }      
}
