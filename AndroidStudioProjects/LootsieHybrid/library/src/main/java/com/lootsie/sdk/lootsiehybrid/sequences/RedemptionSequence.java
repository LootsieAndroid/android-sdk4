package com.lootsie.sdk.lootsiehybrid.sequences;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRedeemReward;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.lootsiehybrid.LootsieAccountManager;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.model.RedemptionResult;
import com.lootsie.sdk.model.Reward;
import com.lootsie.sdk.model.User;
import com.lootsie.sdk.netutil.RESTGetUserAccountTask;
import com.lootsie.sdk.netutil.RESTPostUserRewardIdRedemptionTask;
import com.lootsie.sdk.netutil.RestResult;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.utils.LootsieGlobals;

import com.lootsie.sdk.netutil.RESTPostUserSessionEmailOnlyTask;
import com.lootsie.sdk.netutil.RESTPostUserRewardEmailRedemptionTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by jerrylootsie on 2/20/15.
 */
public class RedemptionSequence {

    private static final String TAG = "Lootsie RedemptionSequence";

    private static LootsieAccountManager lootsieAccountManager = null;

    private static Reward reward = null;

    private static IRedeemReward callback = null;


    //public static RESTPostUserSessionEmailOnlyTask restPostUserSessionEmailOnlyTask = null;
    public static IGenericAsyncTask<String> restPostUserSessionEmailOnlyTask = null;

    //public static RESTGetUserAccountTask restGetUserAccountTask = null;
    public static IGenericAsyncTask<String> restGetUserAccountTask = null;

    //public static RESTPostUserRewardEmailRedemptionTask restPostUserRewardEmailRedemptionTask = null;
    public static IGenericAsyncTask<String> restPostUserRewardEmailRedemptionTask = null;

    public static IGenericAsyncTask<String> restPostUserRewardRedemptionTask = null;

    /**
     * we initialize the resttasks here, and check because they may already have been initialized as mock objects earlier for testing
     */
    private static void init() {

        if (restPostUserSessionEmailOnlyTask == null) {
            restPostUserSessionEmailOnlyTask = new RESTPostUserSessionEmailOnlyTask();
        }

        if (restGetUserAccountTask == null) {
            restGetUserAccountTask = new RESTGetUserAccountTask();
        }

        if (restPostUserRewardEmailRedemptionTask == null) {
            restPostUserRewardEmailRedemptionTask = new RESTPostUserRewardEmailRedemptionTask();
        }

        if (restPostUserRewardRedemptionTask == null) {
            restPostUserRewardRedemptionTask = new RESTPostUserRewardIdRedemptionTask();
        }
    }

    /**
     * we need to create a new task every time because a task can only be executed once!
     */
    private static void cleanup() {
        restPostUserSessionEmailOnlyTask = null;
        restPostUserRewardEmailRedemptionTask = null;
        restGetUserAccountTask = null;
    }


    public static void start(String emailStr, Reward inputReward, IRedeemReward inputCB) {
        reward = inputReward;
        callback = inputCB;

        init();

        lootsieAccountManager = LootsieEngine.getInstance().getLootsieAccountManager();

        DebugLog("RedemptionSequence: start: email: " + emailStr + " rewardId: " + reward.id);

        //if (DataModel.user.isGuest) {
        // check validity of user as well as guest check!
        if ((!DataModel.user.isGuest) && (DataModel.user.email != null) && (isEmailValid(DataModel.user.email))) {
            // may be a different email than the registered user for gifting!
        	emailReward(emailStr);
        } else {
        	registerUser(emailStr);
        }
    }

    public static void start(Reward inputReward, IRedeemReward inputCB) {
        reward = inputReward;
        callback = inputCB;

        init();

        DebugLog("RedemptionSequence: start: rewardId: " + reward.id);

        reward();
    }

    public static void registerUser(String emailStr) {

        /*
        POST https://api-v2.lootsie.com/v2/user/session/emailonly
        X-Lootsie-App-Secret
        X-Lootsie-User-Session-Token
        {“email”,”testuser@domain.com”}
        */

        if (isEmailValid(emailStr)) {

            DataModel.user.email = emailStr;

            String jsonStr = "{\"email\": \"" + emailStr + "\"}";

            IRESTCallback restCallback = new IRESTCallback() {
                @Override
                public void restResult(RestResult result) {
                    RedemptionSequence.PostUserSessionEmailOnlyResult(result);
                }
            };

            restPostUserSessionEmailOnlyTask.setCallback(restCallback);
            restPostUserSessionEmailOnlyTask.executeTask(jsonStr);
        } else {
            // ASDK-402 Android Hybrid: No error message when trying to redeem to an invalid email address.
            // don't bother sending it to the server, we know the email is wrong.
        	callback.onRedeemFailed(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.REDEMPTION_SEQUENCE_FAILURE_INVALID_EMAIL, null);
            cleanup();
        }

    }

    static Pattern emailPattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

    private static boolean isEmailValid(String email) {
        Matcher m = emailPattern.matcher(email);
        return m.matches();
    }

    public static void PostUserSessionEmailOnlyResult(RestResult result) {

        // 409 Conflict {} When the user has already registered successfully:
        // 409 Conflict {"errors": [{"field": "merge", "message": "LP maxed"}]} When the user reaches maximum LP for the day (100LP from an app).
        // 200 - success
        // 204 No Content

        if ((result.status == 200) || (result.status == 204) || (result.status == 409)) {

            if (result.content != null) {
                DebugLog("RedemptionSequence: PostUserSessionEmailOnlyResult: " + result.content);
            } else {
                DebugLog("RedemptionSequence: PostUserSessionEmailOnlyResult: null response");
            }

            // save the updated user email info
            LootsieEngine.getInstance().getLootsieAccountManager().saveUserInfo(DataModel.user);

            GetUserAccount();

        } else {
            // responseCode: 409
            // Note: When making this call with a session token belonging to a registered user, it will return 409
            if (result.content != null) {
                DebugLog("RedemptionSequence: PostUserSessionEmailOnlyResult[" + result.status + "]: " + result.content);

                // ASDK-402 Android Hybrid: No error message when trying to redeem to an invalid email address.
                callback.onRedeemFailed(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.REDEMPTION_SEQUENCE_FAILURE_REGISTER_EMAIL, result.content);
                cleanup();
            } else {
                DebugLog("RedemptionSequence: PostUserSessionEmailOnlyResult[" + result.status + "]: null response");

                // ASDK-402 Android Hybrid: No error message when trying to redeem to an invalid email address.
                callback.onRedeemFailed(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.REDEMPTION_SEQUENCE_FAILURE_REGISTER_EMAIL, null);
                cleanup();
            }

//            if (callback != null) {
//                callback.onLootsieFailed();
//            }
        }
    }

    public static void GetUserAccount() {

        DebugLog("RedemptionSequence: GetUserAccount");

        User user = null;

        IRESTCallback restCallback = new IRESTCallback() {
            @Override
            public void restResult(RestResult result) {
                RedemptionSequence.GetUserAccountResult(result);
            }
        };

        // make sure tasks are not null!
        init();
        restGetUserAccountTask.setCallback(restCallback);
        restGetUserAccountTask.executeTask("test");

    }

    public static void GetUserAccountResult(RestResult RestResult) {
        if (RestResult.status == 200) {

            try {
                JSONObject json = new JSONObject(RestResult.content);

                // store this stuff in the data model
                if (DataModel.user == null) {
                    DataModel.user = new User();
                }
                DataModel.user.parseFromJSONUserAccountInfo(json);

                // use account manager to save and restore data
                DebugLog("UserAccount: %s", DataModel.user.getDisplayName());
                lootsieAccountManager.createUser(DataModel.user, DataModel.userSessionToken);
                if (LootsieGlobals.debugLevel > 0) DataModel.user.print();

                LootsieEngine.getInstance().updateCurrentActivitiesAndFragments();

                emailReward(DataModel.user.email);

            } catch (JSONException ex) {
                Logs.e(TAG, "GetUserAccountResult: exception: " + ex.getMessage());
                cleanup();
            }

        } else {
            if (callback != null) callback.onRedeemFailed(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.REDEMPTION_SEQUENCE_FAILURE_REGISTER_EMAIL, null);
            cleanup();
        }


    }

    public static void emailReward(String emailStr) {

        /*
        POST https://api-v2.lootsie.com/v2/user/reward/email_redemption
        X-Lootsie-App-Secret
        X-Lootsie-User-Session-Token
        {“reward_ids”:[“#”],”email”:,”testuser@domain.com”}
         */

        // ASDK-505 Gifting of Lootsie Rewards is not working on Live.
        // DataModel.user.email is wrong for gifting, use the input email!

        String jsonStr = "{\"reward_ids\":[\""+ reward.id + "\"], \"email\": \""+emailStr+"\"}";

        IRESTCallback restCallback = new IRESTCallback() {
            @Override
            public void restResult(RestResult result) {
                RedemptionSequence.PostUserRewardRedemptionResult(result);
            }
        };


        restPostUserRewardEmailRedemptionTask.setCallback(restCallback);
        restPostUserRewardEmailRedemptionTask.executeTask(jsonStr);

    }

    public static void reward() {

        /*
        POST https://api-v2.lootsie.com/v2/user/reward/redemptions
        X-Lootsie-App-Secret
        X-Lootsie-User-Session-Token
        {“reward_ids”:[“#”],”email”:,”testuser@domain.com”}
         */

        // ASDK-505 Gifting of Lootsie Rewards is not working on Live.
        // DataModel.user.email is wrong for gifting, use the input email!

        String jsonStr = "{\"reward_ids\":[\""+ reward.id + "\"]}";

        IRESTCallback restCallback = new IRESTCallback() {
            @Override
            public void restResult(RestResult result) {
                RedemptionSequence.PostUserRewardRedemptionResult(result);
            }
        };

        restPostUserRewardRedemptionTask.setCallback(restCallback);
        restPostUserRewardRedemptionTask.executeTask(jsonStr);
    }

    public static void PostUserRewardRedemptionResult(RestResult result) {

        // 402 Payment Required {"errors": [{"field": "reward_ids", "message": "not affordable"}]}
        // 409 Conflict {"errors": [{"field": "reward_ids", "message": "redemption limit is reached"}]}
        // 500 {} ????
        // 200 Success {“reward_redemption_id”: 1, "lp": 50, "total_lp": 150}

        if (result.status == 200) {

            if (result.content != null) {
                DebugLog("RedemptionSequence: PostUserRewardRedemptionResult: " + result.content);

                // TODO: display successful redeem message to the user
                // PostUserRewardRedemptionResult: {"total_lp": 659, "reward_redemption_id": "5032", "lp": -5}

                RedemptionResult redemptionResult = new RedemptionResult();

                try {
                    JSONObject json = new JSONObject(result.content);

                    redemptionResult.parseFromJSON(json);

                    if (LootsieGlobals.debugLevel > 0)
                        redemptionResult.print();

                    // TODO: update internal Model and View of lp points
                    LootsieEngine.getInstance().getLootsieAccountManager().updateUsersLp(redemptionResult.totalLp, DataModel.user.totalLp);
                    DataModel.user.oldLp = DataModel.user.totalLp;
                    DataModel.user.totalLp = redemptionResult.totalLp;

                    LootsieEngine.getInstance().updateCurrentActivitiesAndFragments();

                    // "Redeem Success\n" + "lp " + String.valueOf(redemptionResult.lp
                    callback.onRedeemSuccess(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.REDEMPTION_SEQUENCE_SUCCESS, result.content);

                } catch (JSONException ex) {
                    Logs.e(TAG, "RedemptionSequence: PostUserRewardRedemptionResult: exception: " + ex.getMessage());

                    // redeem success, but a parsing failure!
                    callback.onRedeemSuccess(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.REDEMPTION_SEQUENCE_SUCCESS, null);
                }


            } else {
                DebugLog("RedemptionSequence: PostUserRewardRedemptionResult: null response");
            }


        } else {
            // responseCode: 402
            if (result.status == 402) {
                DebugLog("RedemptionSequence: PostUserRewardRedemptionResult:["+result.status+"]");

                callback.onRedeemFailed(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.REDEMPTION_SEQUENCE_FAILURE_NOT_AFFORDABLE, result.content);

            } else if (result.status == 409) {
                DebugLog("RedemptionSequence: PostUserRewardRedemptionResult:["+result.status+"]");

                callback.onRedeemFailed(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.REDEMPTION_SEQUENCE_FAILURE_REDEMPTION_LIMIT, result.content);

            } else if (result.content != null) {
                DebugLog("RedemptionSequence: PostUserRewardRedemptionResult:["+result.status+"] " + result.content);

                callback.onRedeemFailed(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.REDEMPTION_SEQUENCE_FAILURE, result.content);
            } else {
                DebugLog("RedemptionSequence: PostUserRewardRedemptionResult:["+result.status+"] null response");

                callback.onRedeemFailed(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT.REDEMPTION_SEQUENCE_FAILURE, null);
            }

//            if (callback != null) {
//                callback.onLootsieFailed();
//            }
        }

        cleanup();
    }

    
    //public static Toast makeText(Context context, CharSequence text, int duration)
//    private static void SafeToastMakeText(CharSequence text, int duration) {
//        if (LootsieEngine.getInstance().getActivityContext() != null) {
//        	Toast.makeText(LootsieEngine.getInstance().getActivityContext(), text, duration).show();
//        }
//    }
    
    
    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (com.lootsie.sdk.utils.LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }
}
