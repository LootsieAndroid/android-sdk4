package com.lootsie.sdk.adnets.model;

/**
 * Created by Alexander S. Sidorov on 2/25/16.
 */
public interface InterstitialAd
{
    void load();

    boolean isLoaded();

    void show();
}
