package com.lootsie.sdk.lootsiehybrid.sequences;

import android.text.TextUtils;

import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.callbacks.IIncrementEngineCallback;
import com.lootsie.sdk.netutil.RESTPostToIncrementEngine;
import com.lootsie.sdk.netutil.RestResult;
import com.lootsie.sdk.utils.Logs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Alexander S. Sidorov on 3/1/16.
 * <p/>
 * <p/>
 * <br/><br/>
 * <p/>
 * REQUEST:
 * <p/>
 * curl -H 'Content-type: application/json'
 * -H 'X-Lootsie-App-Secret: BD18FB2E5B47DDDBB0977BED7C2D65CC63836454DC8CD038AAB5B92033D9657E'
 * -H 'X-Lootsie-User-Session-Token: 66E5B78CA1EFB4E23B40A14374478AC9AA9FF4CD38909DA6B16621211E1719C7'
 * -X POST https://increngine.lootsie.com/videoview/ian
 * -d '{"achievement_id": 3}' <br/><br/>
 * <p/>
 * Ensure that the offer ID is an integer and not an integer inside a string.<br/>
 * It wants content type set to application/json, the app and user session tokens as for most core API calls and a
 * JSON body as you see it in the example.<br/>
 * The offer ID will be that of the reward associated with the button to view the video.<br/>
 * Endpoint: https://increngine.lootsie.com/videoview/offer<br/>
 * <p/>
 * <br/><br/>
 * RESPONSE:
 * <p/>
 * {
 * "data": "success"
 * }
 */
public class RewardedVideoSequence
{
    private static final String TAG = RewardedVideoSequence.class.getSimpleName();

    private static final String END_POINT_PATH_OFFER = "videoview/offer";
    private static final String END_POINT_PATH_IAN = "videoview/ian";

    public enum VIDEO_REWARD_CONTEXT {
        /**
         * Rewarded video ad shown from the marketplace, i.e. as user finished watching
         * video opened by clicking "Watch now" button in the "video card" item in the
         * rewards list.
         */
        MARKETPLACE_OFFER,

        /**
         * Rewarded video opened from in-app notification banner.
         */
        IN_APP_NOTIFICATION
    }

    VIDEO_REWARD_CONTEXT videoRewardContext;

    String rewardId;

    IIncrementEngineCallback callBack;

    public RewardedVideoSequence(VIDEO_REWARD_CONTEXT rewardContext, String rewardId, IIncrementEngineCallback offerCallBack) {
        this.videoRewardContext = rewardContext;
        this.rewardId = rewardId;
        this.callBack = offerCallBack;
    }

    public void incrementLootsiePoints() {
        String parameterName = videoRewardContext == VIDEO_REWARD_CONTEXT.MARKETPLACE_OFFER ? "offer_id" :
                "achievement_id";
        String endPointPath = videoRewardContext == VIDEO_REWARD_CONTEXT.MARKETPLACE_OFFER ? END_POINT_PATH_OFFER :
                END_POINT_PATH_IAN;

        JSONObject jsonRequestBody = new JSONObject();
        try {
            jsonRequestBody.put(parameterName, videoRewardContext == VIDEO_REWARD_CONTEXT.MARKETPLACE_OFFER ?
                    Integer.parseInt(rewardId) : rewardId);
        } catch (JSONException e) {
            Logs.e(TAG, "Request body JSON build error: " + e.getMessage());
            e.printStackTrace();

            if (callBack != null) {
                callBack.onIncrementEngineFailed("");
            }
        }

        IRESTCallback restRequestCallback = new IRESTCallback() {
            @Override
            public void restResult(RestResult result) {
                processResponse(result);
            }
        };

        try {
            RESTPostToIncrementEngine restRequest = new RESTPostToIncrementEngine(endPointPath, restRequestCallback);
            restRequest.execute(jsonRequestBody);
        } catch (IllegalStateException e) {
            Logs.e(TAG, "Request execution error: " + e.getMessage());
            e.printStackTrace();

            if (callBack != null) {
                callBack.onIncrementEngineFailed("");
            }
        }
    }

    public void processResponse(RestResult result) {
        if (result.status != 200) {
            if (TextUtils.isEmpty(result.content)) {
                returnFailure("Unknown system error!", null, null);
                return;
            }

            try {
                JSONObject jsonErrorResponseContent = new JSONObject(result.content);
//                JSONArray jsonErrors = jsonErrorResponseContent.getJSONArray("errors");
//                JSONObject jsonError = jsonErrors.getJSONObject(0);
//                String message = jsonError.getString("message");
//                returnFailure(message, jsonErrors.join("; "), null);
                String message = jsonErrorResponseContent.getString("message");
                returnFailure(message, "", null);
            } catch (JSONException e) {
                returnFailure("System error!", "Response parsing error: " + e.getMessage(), e);
            }

            return;
        }

//        try
//        {
//            JSONObject jsonResponseContent = new JSONObject(result.content);
//            if (! jsonResponseContent.getString("data").equals("success")) {
//                returnFailure("System error!", "Wrong JSON structure in the response! " + result.content, null);
//                return;
//            }
//        }
//        catch (JSONException e) {
//            returnFailure("System error!", "Broken JSON structure in the response! " + result.content, e);
//            return;
//        }

        returnSuccess(result);
    }

    private void returnFailure(String userMessage, String logMessage, Exception exception) {
        if (callBack != null) {
            callBack.onIncrementEngineFailed(userMessage);
        }
        if (logMessage != null) {
            Logs.e(TAG, logMessage);
        }
        if (exception != null) {
            exception.printStackTrace();
        }
    }

    private void returnSuccess(RestResult restResult) {
        if (callBack != null)
            callBack.onIncrementEngineSuccess(restResult);
    }
}
