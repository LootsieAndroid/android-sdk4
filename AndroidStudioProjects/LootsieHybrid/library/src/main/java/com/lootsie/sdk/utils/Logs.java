package com.lootsie.sdk.utils;

/**
 * Created by jerrylootsie on 2/9/15.
 */


//import org.apache.commons.collections.buffer.CircularFifoBuffer;
//
//import com.lootsie.sdk.callbacks.model.ILogData;

import android.os.Build;

import com.lootsie.sdk.callbacks.ILogData;

import java.util.Iterator;

/**
 * {@link Logs} is a wrapper class for all android logging mechanisms.
 *
 * @author Raghunandan K 04/30/2014
 *
 */

public class Logs {
    final static String TAG = Logs.class.getName();

    final static int WARN = 1;
    final static int INFO = 2;
    final static int DEBUG = 3;
    final static int VERB = 4;

    public static int LOG_LEVEL;

    //private static CircularFifoBuffer buf = new CircularFifoBuffer(25);
    private static CircularFifoBuffer buf = new CircularFifoBuffer(150);
    private static ILogData logCallback = null;


    // TODO: create a logging mode to log to stdout instead of buffers for testing

    public enum LOGGING_MODE {
        DEFAULT,
        LOG_TO_STDOUT;
    }

    public static LOGGING_MODE loggingMode = LOGGING_MODE.DEFAULT;

    static
    {
//        if ("google_sdk".equals(Build.PRODUCT) || "sdk".equals(Build.PRODUCT)) {
//            LOG_LEVEL = VERB;
//        } else {
//            LOG_LEVEL = INFO;
//        }
    	LOG_LEVEL = VERB;
    }


    /**
     *Error
     */
    public static void e(String tag, String string)
    {
        android.util.Log.e(tag, string);
        logDataToBuffer(string);
    }

    /**
     * Warn
     */
    public static void w(String tag, String string)
    {
        android.util.Log.w(tag, string);
        logDataToBuffer(string);
    }

    /**
     * Info
     */
    public static void i(String tag, String string)
    {
        if(LOG_LEVEL >= INFO)
        {
            android.util.Log.i(tag, string);
            logDataToBuffer(string);
        }
    }

    /**
     * Debug
     */
    public static void d(String tag, String string)
    {
        if(LOG_LEVEL >= DEBUG)
        {
            android.util.Log.d(tag, string);
            logDataToBuffer(string);
        }
    }

    /**
     * Verbose
     */
    public static void v(String tag, String string)
    {
        if(LOG_LEVEL >= VERB)
        {
            android.util.Log.v(tag, string);
            logDataToBuffer(string);
        }
    }

    private static void logDataToBuffer(String string) {

        if (loggingMode == LOGGING_MODE.DEFAULT) {
            // if the logcallback isn't being used, don't bother storing all this stuff in the buffer - speed optimization
            if (logCallback != null) {
                try {
                    buf.enqueue(string);
                    logCallback.onLogEvent(string);
                } catch (Exception e) {
                    android.util.Log.e(TAG, "logDataToBuffer: Exception " + e.getMessage());
                    //e.printStackTrace();
                }
            } else {
                buf.enqueue(string);
            }
        } else if (loggingMode == LOGGING_MODE.LOG_TO_STDOUT) {
            System.out.println(string);
        }


    }


    public static String getBufferDump() {

//        StringBuilder sb = new StringBuilder("");
//        String sep = "\n";
//
//        // instead of emptying the buffer using dequeue, I just want a buffer update of current contents instead!
//        try {
//            if (!buf.isEmpty()) {
//                for (int i = 0; i < buf.size(); i++) {
//                    sb.append(sep).append(buf.dequeue());
//                }
//            }
//        } catch (Exception e) {
//            android.util.Log.e(TAG,"Logs: getBufferDump Error " + e.getMessage());
//            //e.printStackTrace();
//        }
//        return sb.toString();

        return buf.getBufferDump();

    }

    public static void setLogCallback(ILogData callback) {
        logCallback = callback;
    }


}

