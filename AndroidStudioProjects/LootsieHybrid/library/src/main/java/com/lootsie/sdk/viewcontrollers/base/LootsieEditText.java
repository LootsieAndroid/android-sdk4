package com.lootsie.sdk.viewcontrollers.base;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

/**
 * Created by jerrylootsie on 3/13/15.
 *
 * This is needed because the default android edittext has problems interacting with the soft keyboard input, losing focus, etc.
 * http://stackoverflow.com/questions/14727248/edittext-looses-focus-when-keyboard-appears-requires-touching-twice-to-edit
 */
public class LootsieEditText  extends EditText {

    private static String TAG = "Lootsie LootsieEditText";

    /* Must use this constructor in order for the layout files to instantiate the class properly */
    public LootsieEditText(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        this.setImeOptions(EditorInfo.IME_ACTION_DONE);
        
        this.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(final View v, boolean hasFocus) {
                if (hasFocus) {
                    // Request focus in a short time because the
                    // keyboard may steal it away.
                    v.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (!v.hasFocus()) {
                                //DebugLog("editText request focus");
                                v.requestFocus();
                            } else {
                                //DebugLog("editText has focus");
                            }
                        }
                    }, 200);
                }
            }

        });

        this.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(final View v, final MotionEvent ev) {
                v.requestFocusFromTouch();
                return false;
            }
        });

    }

//    @Override
//    public boolean onKeyPreIme (int keyCode, KeyEvent event)
//    {
//        // Return true if I handle the event:
//        // In my case i want the keyboard to not be dismissible so i simply return true
//        // Other people might want to handle the event differently
//        DebugLog("onKeyPreIme " + event);
//
//        if (keyCode == KeyEvent.KEYCODE_BACK & event.getAction() == KeyEvent.ACTION_UP) {
//            this.clearFocus();
//            this.setFocusable(false);
//            this.setFocusableInTouchMode(false);
//            return false;
//        }
//        return super.dispatchKeyEvent(event);
//        //return true;
//    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }

}