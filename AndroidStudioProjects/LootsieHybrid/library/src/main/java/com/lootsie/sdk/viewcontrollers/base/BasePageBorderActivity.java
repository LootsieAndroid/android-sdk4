package com.lootsie.sdk.viewcontrollers.base;

import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

/**
 * Created by jerrylootsie on 3/2/15.
 */
public class BasePageBorderActivity extends UpdatableActivity {

    private static String TAG = "Lootsie BasePageBorderActivity";

    // show the popup menu
    // see res/menu/com_lootsie_menu.xml
    public void showPopup(View v) {
    	DebugLog("showPopup");
    }

    public void onClickMenuRewards(MenuItem item) {
    	DebugLog("onClickMenuRewards");

    }

    public void onClickMenuAchievements(MenuItem item) {
    	DebugLog("onClickMenuAchievements");

    }

    public void onClickMenuVersionInfo(MenuItem item) {
    	DebugLog("onClickMenuVersionInfo");

    }

    public void onClickMenuTerms(MenuItem item) {
    	DebugLog("onClickTerms");

    }

    public void onClickMenuTermsConditions(MenuItem item) {
        DebugLog("onClickTermsConditions");

    }

    public void onClickMenuAccount(MenuItem item) {
    	DebugLog("onClickMenuAccount");

    }

    public void onClickClose(View view) {
    	DebugLog("onClickClose");

    }
    
    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }

    @Override
    public void updateActivity() {
        DebugLog("updateActivity");
    }
}
