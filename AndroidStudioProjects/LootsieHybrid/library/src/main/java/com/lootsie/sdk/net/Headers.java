package com.lootsie.sdk.net;

/**
 * Headers
 *
 * @author Alexander Salnikov
 */
public final class Headers {
    public static final String APP_SECRET = "X-Lootsie-App-Secret";
    public static final String USER_TOKEN = "X-Lootsie-User-Session-Token";
    public static final String SESSION_TOKEN = "X-Lootsie-API-Session-Token";
}
