package com.lootsie.sdk.viewcontrollers;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;

/**
 *
 */
public class CircularVideoPlaybackProgressBar extends View
{
    /** Text view to display current playback second. Updated in sync with
     * animated "progress" member value.
     */
    private TextView progressTextView;

    /** ... just to calculate the text indicator value.
     */
    private Integer durationSeconds;

    /** Start angle of the circular progress indicator counted f
     * rom 0 at 3 hours clock position in CCW direction.
     */
    private int startAngle = -90;

    /** Animated progress value.
     */
    private float progress = 0;

    // graphics

    private RectF circleBox = new RectF();
    private Paint backgroundPaint, foregroundPaint;

    private float strokeWidth = 4;
    private int strokeColor = Color.RED;
    private int backgroundColor = Color.argb(85, 0, 0, 0);

    /**
     *
     * @param context
     * @param attrs
     */
    public CircularVideoPlaybackProgressBar(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        backgroundPaint.setColor(backgroundColor);
        backgroundPaint.setStyle(Paint.Style.FILL);

        foregroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        foregroundPaint.setColor(strokeColor);
        foregroundPaint.setStyle(Paint.Style.STROKE);
        foregroundPaint.setStrokeWidth(strokeWidth);
    }

    /** Draw decreasing arc.
     *
     * @param canvas
     */
    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        canvas.drawOval(circleBox, backgroundPaint);
        canvas.drawArc(circleBox, startAngle, 360 * (1 - progress / 100), false, foregroundPaint);
    }

    /** Measure exact bounding box for the circle drawing.
     * @param widthMeasureSpec
     * @param heightMeasureSpec
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        final int height = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        final int width = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);

        final int min = Math.min(width, height);
        setMeasuredDimension(min, min);

        circleBox.set(0 + strokeWidth / 2, 0 + strokeWidth / 2, min - strokeWidth / 2, min - strokeWidth / 2);
    }

    /** Getter required by the animator.
     *
     * @return
     */
    public float getProgress() {
        return progress;
    }

    /** Setter required by the animator.
     *
     * @param progress
     */
    public void setProgress(float progress)
    {
        if (progress - this.progress < 3)
            return;

        this.progress = (progress <= 100) ? progress : progress % 100;

        if (progressTextView != null) {
            int backwardProgressSeconds = durationSeconds - Math.round(durationSeconds * progress / 100) + 1;
            progressTextView.setText(String.valueOf(backwardProgressSeconds));
        }

        invalidate();
    }

    /**
     * Start video playback progress animation starting from given seek position.
     * <br/><br/>
     * NOTE The playback progress indicated/animated as a value in range of [0..100]. The whole
     * video duration is scaled into the range. See also the draw method.
     *
     * @param startMillis Start position in the video, milliseconds.
     * @param durationMillis Total length of the animation, milliseconds.
     */
    public void startPlaybackAnimation(float startMillis, int durationMillis, TextView progressTextView)
    {
        this.durationSeconds = Math.round(durationMillis / 1000);
        this.progressTextView = progressTextView;

        // starting position of the progress indicator in the 100 units scale
        float start = (startMillis / durationMillis) * 100;

        // set and start object property animator on "progress" member variable
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this, "progress", start, 100.0f);
        objectAnimator.setDuration(durationMillis);
        objectAnimator.setInterpolator(new DecelerateInterpolator());
        objectAnimator.start();
    }
}
