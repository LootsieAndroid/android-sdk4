package com.lootsie.sdk.viewcontrollers;

import com.lootsie.sdk.R;
import com.lootsie.sdk.model.AchievementListEntryModel;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.RUtil;
import com.lootsie.sdk.utils.image.ImageCacheManager;
import com.lootsie.sdk.viewcontrollers.base.AchievementArrayAdapter;
import com.lootsie.sdk.viewcontrollers.base.BaseAchievementsFragment;
import com.lootsie.sdk.viewcontrollers.base.LootsieNetworkImageView;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import static android.view.ViewGroup.LayoutParams.*;

/**
 * Created by jerrylootsie on 2/27/15.
 */

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class AchievementsFragment extends BaseAchievementsFragment {

    static private String TAG = "Lootsie AchievementsFragment";

    // static elements don't work well with fragments - old fragment is destroyed etc
    private TextView appTitle = null;


    private AchievementArrayAdapter achievementsAdapter = null;

    private ListView listViewAchievements = null;

    private LootsieNetworkImageView appIcon = null;

    private LinearLayout achievementsLayout = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

    	DebugLog("onCreateView");

        //View view = inflater.inflate(R.layout.com_lootsie_achievements_fragment,container, false);
        View view = inflater.inflate(RUtil.getLayoutId(this.getActivity().getApplicationContext(),"com_lootsie_achievements_fragment"),
        		container, false);

        // can't find anything when first created - rely on onResume call instead
        //updateEntries();

        view.setTag("achievementsFragment");

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        DebugLog("onResume");

        updateEntries();
    }


//    @SuppressLint("NewApi")
//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//    private static void setElevationLollipop(View view) {
//        DebugLog("setElevationLollipop: ");
//        view.setElevation(20.0f);
//    }

    private void updateEntries() {
        // ASDK-469 - Android Style Guide: Achievements Page: Achievement title and icon should be part of the scrollview (landscape is unreadable.)

        // moved to header
//        if (appTitle == null) {
//            //appTitle = (TextView) this.getActivity().findViewById(R.id.achievementsPageAppTitle);
//            appTitle = (TextView) this.getActivity().findViewById(RUtil.getId(this.getActivity(), "achievementsPageAppTitle"));
//
//            if (appTitle != null) {
//                appTitle.setText(DataModel.app.name);
//            }
//        }

        // moved to header
//        if (appIcon == null) {
//            appIcon = (LootsieNetworkImageView) this.getActivity().findViewById(RUtil.getId(this.getActivity(), "appIcon"));
//            if (appIcon != null) {
//                appIcon.setImageUrl(DataModel.app.icon.S, ImageCacheManager.getInstance().getImageLoader());
//            } else {
//                Logs.e(TAG, "updateEntries: appIcon not found!");
//            }
//        }


        if (achievementsLayout == null) {
            achievementsLayout = (LinearLayout) this.getActivity().findViewById(RUtil.getId(this.getActivity(), "achievementsLayout"));
//            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                setElevationLollipop(achievementsLayout);
//            }
        }


        if (listViewAchievements == null) {
            listViewAchievements = (ListView) this.getActivity().findViewById(RUtil.getId(this.getActivity(), "listViewAchievements"));

            List<AchievementListEntryModel> list = getModel(true);
            List<AchievementListEntryModel> oneTimeAchievmentsList = getModel(false);
            list.addAll(oneTimeAchievmentsList);

            // ASDK-469 - Android Style Guide: Achievements Page: Achievement title and icon should be part of the scrollview (landscape is unreadable.)
            //code to add header and footer to listview
            LayoutInflater inflater = this.getActivity().getLayoutInflater();
            int headerId = RUtil.getLayoutId(this.getActivity(), "com_lootsie_achievements_header");
            ViewGroup header = (ViewGroup) inflater.inflate(headerId, listViewAchievements, false);

            if (appTitle == null) {
                appTitle = (TextView) header.findViewById(RUtil.getId(this.getActivity(), "achievementsPageAppTitle"));

                if (appTitle != null) {
                    appTitle.setText(DataModel.app.name);
                }
            }

            if (appIcon == null) {
                appIcon = (LootsieNetworkImageView) header.findViewById(RUtil.getId(this.getActivity(), "appIcon"));

                if (appIcon != null) {
                    appIcon.setImageUrl(DataModel.app.icon.S, ImageCacheManager.getInstance().getImageLoader());
                } else {
                    Logs.e(TAG, "updateEntries: appIcon not found!");
                }
            }



            listViewAchievements.addHeaderView(header, null, false);

            if ((list.size() > 0) && (listViewAchievements != null)) {
                achievementsAdapter = new AchievementArrayAdapter(this.getActivity(), list);
                //setListAdapter(achievementsAdapter);
                listViewAchievements.setAdapter(achievementsAdapter);
            } else {
                DebugLog("updateFragment: list or listViewAchievements not found!");
            }
        }

    }


    @Override
    public void updateFragment() {
    	DebugLog("updateFragment");

        updateEntries();
    }


    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (com.lootsie.sdk.utils.LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }
}
