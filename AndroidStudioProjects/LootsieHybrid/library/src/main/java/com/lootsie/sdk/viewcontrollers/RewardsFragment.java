package com.lootsie.sdk.viewcontrollers;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import com.lootsie.sdk.R;
import com.lootsie.sdk.model.AchievementListEntryModel;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.model.RewardListEntryModel;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.utils.RUtil;
import com.lootsie.sdk.viewcontrollers.base.AchievementArrayAdapter;
import com.lootsie.sdk.viewcontrollers.base.BaseRewardsFragment;
import com.lootsie.sdk.viewcontrollers.base.RewardArrayAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by jerrylootsie on 2/27/15.
 */


@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class RewardsFragment extends BaseRewardsFragment {

    static private String TAG = "Lootsie RewardsFragment";

    // static elements don't work well with fragments - old fragment is destroyed etc
    private TextView rewardPoints = null;

    private RewardArrayAdapter adapter = null;
    
    private ListView listViewRewards = null;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (LootsieGlobals.debugLevel > 0) Logs.d(TAG, "onCreateView");

        //View view = inflater.inflate(R.layout.com_lootsie_rewards_fragment, container, false);
        View view = inflater.inflate(RUtil.getLayoutId(this.getActivity().getApplicationContext(), "com_lootsie_rewards_fragment"), container, false);

        // can't find anything when first created - rely on onResume call instead
        //updateEntries();

        view.setTag("rewardsFragment");

        return view;
    }



    @Override
    public void onResume() {
        super.onResume();

        if (LootsieGlobals.debugLevel > 0) Logs.d(TAG, "onResume");

        updateEntries();
    }

    @Override
    public void updateFragment() {
        if (LootsieGlobals.debugLevel > 0) Logs.d(TAG, "updateFragment: " + String.valueOf(DataModel.user.totalLp) + " " + DataModel.currency_abbreviation);

        updateEntries();
    }

    private void updateEntries() {
        DebugLog("updateEntries");

        if (listViewRewards == null) {
        	listViewRewards = (ListView) this.getActivity().findViewById(RUtil.getId(this.getActivity(), "rewardList"));

        	List<RewardListEntryModel> list = getModel();

            if ((list.size() > 0) && (listViewRewards != null)) {
            	DebugLog("updateFragment: rewards: " + list.size());
            	
            	adapter = new RewardArrayAdapter(this.getActivity(), list);
                //setListAdapter(achievementsAdapter);
            	listViewRewards.setAdapter(adapter);
            } else {
            	DebugLog("updateFragment: list is empty!");
            }
        }
        
    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }

}
