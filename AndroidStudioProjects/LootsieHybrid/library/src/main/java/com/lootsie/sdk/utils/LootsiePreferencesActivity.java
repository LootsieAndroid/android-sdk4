package com.lootsie.sdk.utils;

import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;

import com.lootsie.sdk.auth.authenticator.AuthenticationService;

import com.lootsie.sdk.R;

/**
 * Created by jerrylootsie on 2/26/15.
 */
public class LootsiePreferencesActivity extends PreferenceActivity {

    private static String TAG = "Lootsie LootsiePreferencesActivity";

    private boolean shouldForceSync = false;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Logs.v(TAG, "onCreate");
//        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
//            addPreferencesFromResource(R.xml.account_preferences);
//        }else{
//            getFragmentManager().beginTransaction().replace(android.R.id.content,
//                    new LootsiePreferencesFragment()).commit();
//        }

    }

    @Override
    public void onPause() {
        super.onPause();

        Logs.v(TAG, "onPause");


        if(shouldForceSync) {
//            AuthenticationService.resyncAccount(this);
        }
    }

    Preference.OnPreferenceChangeListener syncToggle = new Preference.OnPreferenceChangeListener() {
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            Logs.v(TAG, "onPreferenceChange");

            shouldForceSync = true;
            return true;
        }
    };

    Preference.OnPreferenceClickListener urlClick = new Preference.OnPreferenceClickListener() {

        public boolean onPreferenceClick(Preference preference) {

            Logs.v(TAG, "onPreferenceClick");


            Intent i = null;
            if (preference.getKey().equals("tos"))
                i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.last.fm/legal/terms"));
            if (preference.getKey().equals("privacy"))
                i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.last.fm/legal/privacy"));
            if (preference.getKey().equals("changes"))
                i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.last.fm/group/Last.fm+Android/forum/114391/_/589152"));

            if (i != null)
                startActivity(i);
            return false;
        }
    };


    Preference.OnPreferenceChangeListener scrobbletoggle = new Preference.OnPreferenceChangeListener() {
        public boolean onPreferenceChange(Preference preference, Object newValue) {

            Logs.v(TAG, "onPreferenceChange");

//            if(preference.getKey().equals("scrobble")) {
//                if((Boolean)newValue) {
//                    getPackageManager().setComponentEnabledSetting(new ComponentName("fm.last.android", "fm.last.android.scrobbler.MusicIntentReceiver"), PackageManager.COMPONENT_ENABLED_STATE_DEFAULT, PackageManager.DONT_KILL_APP);
//
//                    //Re-enable the rest of the recievers to match the current preference state
//                    if(preference.getSharedPreferences().getBoolean("scrobble_music_player", true))
//                        getPackageManager().setComponentEnabledSetting(new ComponentName("fm.last.android", "fm.last.android.scrobbler.AndroidMusicIntentReceiver"), PackageManager.COMPONENT_ENABLED_STATE_DEFAULT, PackageManager.DONT_KILL_APP);
//                    else
//                        getPackageManager().setComponentEnabledSetting(new ComponentName("fm.last.android", "fm.last.android.scrobbler.AndroidMusicIntentReceiver"), PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
//
//                    if(preference.getSharedPreferences().getBoolean("scrobble_sdroid", true))
//                        getPackageManager().setComponentEnabledSetting(new ComponentName("fm.last.android", "fm.last.android.scrobbler.ScrobbleDroidIntentReceiver"), PackageManager.COMPONENT_ENABLED_STATE_DEFAULT, PackageManager.DONT_KILL_APP);
//                    else
//                        getPackageManager().setComponentEnabledSetting(new ComponentName("fm.last.android", "fm.last.android.scrobbler.ScrobbleDroidIntentReceiver"), PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
//
//                    if(preference.getSharedPreferences().getBoolean("scrobble_sls", true))
//                        getPackageManager().setComponentEnabledSetting(new ComponentName("fm.last.android", "fm.last.android.scrobbler.SLSIntentReceiver"), PackageManager.COMPONENT_ENABLED_STATE_DEFAULT, PackageManager.DONT_KILL_APP);
//                    else
//                        getPackageManager().setComponentEnabledSetting(new ComponentName("fm.last.android", "fm.last.android.scrobbler.SLSIntentReceiver"), PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
//                } else {
//                    getPackageManager().setComponentEnabledSetting(new ComponentName("fm.last.android", "fm.last.android.scrobbler.MusicIntentReceiver"), PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
//                    getPackageManager().setComponentEnabledSetting(new ComponentName("fm.last.android", "fm.last.android.scrobbler.AndroidMusicIntentReceiver"), PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
//                    getPackageManager().setComponentEnabledSetting(new ComponentName("fm.last.android", "fm.last.android.scrobbler.ScrobbleDroidIntentReceiver"), PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
//                    getPackageManager().setComponentEnabledSetting(new ComponentName("fm.last.android", "fm.last.android.scrobbler.SLSIntentReceiver"), PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
//                }
//            } else if(preference.getKey().equals("scrobble_music_player")) {
//                if((Boolean)newValue) {
//                    getPackageManager().setComponentEnabledSetting(new ComponentName("fm.last.android", "fm.last.android.scrobbler.AndroidMusicIntentReceiver"), PackageManager.COMPONENT_ENABLED_STATE_DEFAULT, PackageManager.DONT_KILL_APP);
//                } else {
//                    getPackageManager().setComponentEnabledSetting(new ComponentName("fm.last.android", "fm.last.android.scrobbler.AndroidMusicIntentReceiver"), PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
//                }
//            } else if(preference.getKey().equals("scrobble_sdroid")) {
//                if((Boolean)newValue) {
//                    getPackageManager().setComponentEnabledSetting(new ComponentName("fm.last.android", "fm.last.android.scrobbler.ScrobbleDroidIntentReceiver"), PackageManager.COMPONENT_ENABLED_STATE_DEFAULT, PackageManager.DONT_KILL_APP);
//                } else {
//                    getPackageManager().setComponentEnabledSetting(new ComponentName("fm.last.android", "fm.last.android.scrobbler.ScrobbleDroidIntentReceiver"), PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
//                }
//            } else if(preference.getKey().equals("scrobble_sls")) {
//                if((Boolean)newValue) {
//                    getPackageManager().setComponentEnabledSetting(new ComponentName("fm.last.android", "fm.last.android.scrobbler.SLSIntentReceiver"), PackageManager.COMPONENT_ENABLED_STATE_DEFAULT, PackageManager.DONT_KILL_APP);
//                } else {
//                    getPackageManager().setComponentEnabledSetting(new ComponentName("fm.last.android", "fm.last.android.scrobbler.SLSIntentReceiver"), PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
//                }
//            }

            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            nm.cancel(1338);
            return true;
        }
    };

    @Override
    public void onResume() {
        super.onResume();

        Logs.v(TAG, "onResume");

        try {
//            LastFMApplication.getInstance().tracker.trackPageView("/Preferences");
        } catch (Exception e) {
            //Google Analytics doesn't appear to be thread safe
        }
    }
}