package com.lootsie.sdk.netutil;

import android.os.AsyncTask;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.net.AltRestClient;
import com.lootsie.sdk.net.Headers;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.utils.PreferencesHandler;

import org.json.JSONObject;


/**
 *  Created by Alexander S. Sidorov on 01.03.2016
 *
 * <br/><br/>

 curl -H 'Content-type: application/json'
 -H 'X-Lootsie-App-Secret: BD18FB2E5B47DDDBB0977BED7C2D65CC63836454DC8CD038AAB5B92033D9657E'
 -H 'X-Lootsie-User-Session-Token: 66E5B78CA1EFB4E23B40A14374478AC9AA9FF4CD38909DA6B16621211E1719C7'
 -X POST https://increngine.lootsie.com/videoview/offer
 -d '{"offer_id": 2}' <br/><br/>

 * Ensure that the offer ID is an integer and not an integer inside a string.<br/>
 * It wants content type set to application/json, the app and user session tokens as for most core API calls and a JSON body as you see it in the example.<br/>
 * The offer ID will be that of the reward associated with the button to view the video.<br/>
 * Endpoint: https://increngine.lootsie.com/videoview/offer<br/>
 *
 */
public class RESTPostToIncrementEngine extends AsyncTask<JSONObject , Void, RestResult> implements IGenericAsyncTask<JSONObject>
{
    private static final String TAG = RESTPostToIncrementEngine.class.getSimpleName();

    private static final String SERVICE_URL = "https://increngine.lootsie.com/";


    String endPointPath;
    IRESTCallback callback;

    public RESTPostToIncrementEngine(String endPointPath, IRESTCallback restCallback) {
        this.endPointPath = endPointPath;
        this.callback = restCallback;
    }

    @Override
    protected RestResult doInBackground(JSONObject ... params)
    {
        if (params == null || params.length == 0) {
            return null;
        }

        RestResult response = new RestResult();
        try {
            AltRestClient restClient = new AltRestClient(SERVICE_URL);
            restClient.addHeader(Headers.APP_SECRET, LootsieEngine.getInstance().getAppId());
            restClient.addHeader(Headers.USER_TOKEN, PreferencesHandler
                    .getString(LootsieGlobals.KEY_USER_SESSION_TOKEN,
                            LootsieEngine.getInstance().getApplicationContext()));

            response = restClient.doPost(endPointPath, params[0]);
            if (LootsieGlobals.debugLevel > 0) Logs.v(TAG, "REST Response: " + response.content);
        }
        catch (Exception e) {
            Logs.e(TAG, "Failure: " + e.getMessage());
        }

        return response;
    }

    @Override
    protected void onPostExecute(final RestResult restResult) {
        if (callback != null) {
            callback.restResult(restResult);
        }
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    public void executeTask(JSONObject... params) {
        execute(params);
    }
}
