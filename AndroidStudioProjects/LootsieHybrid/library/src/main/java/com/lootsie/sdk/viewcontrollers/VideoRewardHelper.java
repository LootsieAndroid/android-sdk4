package com.lootsie.sdk.viewcontrollers;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.lootsie.sdk.R;
import com.lootsie.sdk.utils.TimeNotificationReceiver;

import java.sql.Time;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Alexander S. Sidorov on 2/19/16.
 */
public class VideoRewardHelper {
    private static final String STORAGE_KEY_VIDEO_WATCHED_TODAY_COUNT = "15754765410";
    private static final int WATCHED_TODAY_LIMIT = 5;

    private static void showPopup(Activity context, String title, String message) {
        View popupView = context.getLayoutInflater().inflate(R.layout.com_lootsie_popup_reward_video_message, null);
        final PopupWindow popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup
                .LayoutParams.MATCH_PARENT);
        popupWindow.setAnimationStyle(android.R.style.Animation_Dialog);

        ((TextView) popupView.findViewById(R.id.com_lootsie_popup_reward_message_title)).setText(title);
        ((TextView) popupView.findViewById(R.id.com_lootsie_popup_reward_message_text)).setText(message);
        popupView.findViewById(R.id.com_lootsie_popup_reward_message_button_ok).setOnClickListener(new View
                .OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

        popupWindow.showAtLocation(context.findViewById(android.R.id.content), Gravity.CENTER, 0, 0);
    }

    public static void showEarnedPopup(Activity context, int earnedPoints) {
        String title, message;

        title = context.getString(R.string.lootsie_reward_video_message_congrats);
        message = context.getString(R.string.lootsie_reward_video_message_earned_points, earnedPoints);

        showPopup(context, title, message);
    }

    public static void showReachedPopup(Activity context) {
        String title, message;

        title = context.getString(R.string.lootsie_reward_video_message_wow);
        message = context.getString(R.string.lootsie_reward_video_message_reached_limit);

        showPopup(context, title, message);
    }

    private static final String STORAGE_KEY_VIDEO_POINTS_COUNT = VideoRewardHelper.class.getPackage() +
            "$STORAGE_KEY_VIDEO_POINTS_COUNT";

    @Deprecated
    public static boolean isVideoPointLimitReached(Context context) {
        SharedPreferences store = PreferenceManager.getDefaultSharedPreferences(context);
        int count = store.getInt(STORAGE_KEY_VIDEO_POINTS_COUNT, 0);
        return false;//count % 5 == 0;
    }

//    public static int incrementVideoPointCount(Context context) {
//        SharedPreferences store = PreferenceManager.getDefaultSharedPreferences(context);
//        int count = store.getInt(STORAGE_KEY_VIDEO_POINTS_COUNT, 0) + 1;
//        store.edit().putInt(STORAGE_KEY_VIDEO_POINTS_COUNT, count).commit();
//        return count;
//    }

    public static void setPointCount(Context context, int totalPoints) {
        SharedPreferences store = PreferenceManager.getDefaultSharedPreferences(context);
        store.edit().putInt(STORAGE_KEY_VIDEO_POINTS_COUNT, totalPoints).commit();
    }

    public static int getPointCount(Context context) {
        SharedPreferences store = PreferenceManager.getDefaultSharedPreferences(context);
        return store.getInt(STORAGE_KEY_VIDEO_POINTS_COUNT, 0);
    }

    public static void incrementWatchedToday(Context context) {
        SharedPreferences store = PreferenceManager.getDefaultSharedPreferences(context);
        int watchedCounter = store.getInt(STORAGE_KEY_VIDEO_WATCHED_TODAY_COUNT, 0);
        store.edit().putInt(STORAGE_KEY_VIDEO_WATCHED_TODAY_COUNT, watchedCounter + 1).apply();
        restartTimer(context);
    }

    public static Boolean isWatchedTodayLimitReached(Context context) {
        SharedPreferences store = PreferenceManager.getDefaultSharedPreferences(context);
        int watchedCounter = store.getInt(STORAGE_KEY_VIDEO_WATCHED_TODAY_COUNT, 0);
        return watchedCounter >= WATCHED_TODAY_LIMIT;
    }

    public static void resetWatchedToday(Context context) {
        SharedPreferences store = PreferenceManager.getDefaultSharedPreferences(context);
        store.edit().putInt(STORAGE_KEY_VIDEO_WATCHED_TODAY_COUNT, 0).apply();
    }

    private static void restartTimer(Context context) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, TimeNotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT);
        am.cancel(pendingIntent);

        Calendar date = new GregorianCalendar();
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        date.add(Calendar.DAY_OF_MONTH, 1);

        am.set(AlarmManager.RTC_WAKEUP, date.getTimeInMillis(), pendingIntent);
    }
}
