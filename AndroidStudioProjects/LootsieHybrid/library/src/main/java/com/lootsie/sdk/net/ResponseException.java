package com.lootsie.sdk.net;

/**
 * ResponseException
 *
 * @author Aliaksandr Salnikau
 */
public class ResponseException extends Exception {

    public static final int NO_INTERNET_EXCEPTION = -1;

    public int code;
    public String message;

    public ResponseException(String detailMessage) {
        super(detailMessage);
    }

    public ResponseException(Throwable throwable) {
        super(throwable);
    }

    public ResponseException(int statusCode, String message) {
        this.code = statusCode;
        this.message = message;
    }
}
