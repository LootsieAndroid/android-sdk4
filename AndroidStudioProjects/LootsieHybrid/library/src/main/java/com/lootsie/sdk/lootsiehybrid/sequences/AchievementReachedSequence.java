package com.lootsie.sdk.lootsiehybrid.sequences;

import android.app.Activity;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.lootsiehybrid.LOOTSIE_NOTIFICATION_CONFIGURATION;
import com.lootsie.sdk.lootsiehybrid.LootsieApi;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.lootsiehybrid.LootsieAccountManager;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.model.Achievement;
import com.lootsie.sdk.callbacks.IAchievementReached;
import com.lootsie.sdk.net.Headers;
import com.lootsie.sdk.net.RestClient;
import com.lootsie.sdk.netutil.RESTPostUserAchievementsTask;
import com.lootsie.sdk.netutil.RestResult;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

import com.lootsie.sdk.viewcontrollers.NotificationManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jerrylootsie on 2/12/15.
 */
public class AchievementReachedSequence {

    private static String TAG = "Lootsie AchievementReachedSequence";

    private static IAchievementReached callback = null;

    private static NotificationManager notificationManager = null;

    private static String achievementId = "";

    private static LootsieAccountManager lootsieAccountManager = null;

    //public static RESTPostUserAchievementsTask restPostUserAchievementsTask = null;
    public static IGenericAsyncTask<JSONObject> restPostUserAchievementsTask = null;

    /**
     * we initialize the resttasks here, and check because they may already have been initialized as mock objects earlier for testing
     */
    private static void init() {

        if (restPostUserAchievementsTask == null) {
            restPostUserAchievementsTask = new RESTPostUserAchievementsTask();
        }
    }

    /**
     * we need to create a new task every time because a task can only be executed once!
     */
    private static void cleanup() {
        restPostUserAchievementsTask = null;
    }

    public static void start(String inputAchievementId, IAchievementReached inputCB) {

        DebugLog("AchievementReachedSequence: start PostUserAchievements achievementId: %s", inputAchievementId);

        callback = inputCB;

        achievementId = inputAchievementId;

        lootsieAccountManager = LootsieEngine.getInstance().getLootsieAccountManager();

        init();

        // should I rely on InitSequence to setup the headers in RestClient, or do it separately in AchievementReachedSequence
        //RestClient.addHeader(Headers.APP_SECRET, LootsieEngine.getInstance().getAppId());

        /*
            POST https://api-v2.lootsie.com/v2/user/achievements
            {“achievement_ids”,[“#”]}
            X-Lootsie-App-Secret
            X-Lootsie-User-Session-Token
         */

        //String jsonStr = "{\"achievement_ids\": [\""+achievementId+"\"]}";

        IRESTCallback restCallback = new IRESTCallback() {
            @Override
            public void restResult(RestResult result) {
                AchievementReachedSequence.PostUserAchievementsResult(result);
            }
        };

        DataModel.updateNetworkForced("POST_" + LootsieApi.USER_ACHIEVEMENTS_URL.toString());
        //HashMap<String,Object> map = new HashMap<String,Object>();
        //map.put("achievement_ids", achievementId);

        try {
            JSONObject jobj = new JSONObject();
            JSONArray jarr = new JSONArray();
            jarr.put(achievementId);        	
			jobj.put("achievement_ids", jarr);


            restPostUserAchievementsTask.setCallback(restCallback);
            restPostUserAchievementsTask.executeTask(jobj);
		} catch (JSONException e) {
			Logs.e(TAG, "AchievementReachedSequence: start: exception: " + e.getMessage());
			e.printStackTrace();
            cleanup();
            if (callback != null) {
                callback.onLootsieFailed("");
            }
        } catch (Exception e) {
            Logs.e(TAG, "AchievementReachedSequence: start: exception: " + e.getMessage());
            e.printStackTrace();
            cleanup();
            if (callback != null) {
                callback.onLootsieFailed("");
            }
        }
        
        
        //new RESTPostUserAchievementsTask(restCallback).execute(map);
        //new RESTPostUserAchievementsTask(restCallback).execute(jsonStr);

    }

    public static void PostUserAchievementsResult(RestResult RestResult) {
        if (RestResult.status == 200) {
            try {
                JSONObject json = new JSONObject(RestResult.content);

                // todo: parse results of post achievement
                // HTTP response:{"total_lp": 20, "lp": 20}

                if (DataModel.user == null) {
                    Logs.e(TAG, "DataModel.user is invalid!");
                    throw new Exception();
                }
                int oldLp = DataModel.user.totalLp;
                DataModel.user.totalLp = json.getInt("total_lp");
                int lpEarned = json.getInt("lp");
                DataModel.user.achievedLp += lpEarned;

                DebugLog("User: totalLp: %d", DataModel.user.totalLp);
                DebugLog("User: achievedLp: %d", DataModel.user.achievedLp);
                DebugLog("lpEarned: %d", lpEarned);

                // why does eclipse version allow achievment reached to get here?
                // processing command queue, and get a null pointer exception wtf?
                // ASDK-420 Android Hybrid Eclipse: Getting an achievement before Lootsie has finished initializing for the first time causes a crash.
                lootsieAccountManager.updateUsersLp(DataModel.user.totalLp, oldLp);

                callback.onNotificationData(RestResult.content);


                // reverse lookup of achievement_id to achievement
                Achievement achievement = null;
                if (DataModel.app.achievements != null) {
	                for (int i = 0; (i < DataModel.app.achievements.size()) && (achievement == null); i++) {
	                    Achievement ach = DataModel.app.achievements.get(i);
	                    if (ach.id.equalsIgnoreCase(achievementId)) {
	                        achievement = ach;
	                    }
	                }
                }
                
                if (achievement != null) {
                    callback.onLootsieSuccess();

                    
                    LOOTSIE_NOTIFICATION_CONFIGURATION notifyConfig = LootsieEngine.getInstance().getNotificationConfiguration();
                    
                    if (notifyConfig != LOOTSIE_NOTIFICATION_CONFIGURATION.notify_to_disabled) {                    	
	                    // todo: start In-App-Notification banner movement
	                    if (notificationManager == null) {
	                        notificationManager = new NotificationManager((Activity) LootsieEngine.getInstance().getActivityContext());
	                    } else {
	                        // update activity context - if user rotated the device and started a new activity
	                        notificationManager.updateContext((Activity) LootsieEngine.getInstance().getActivityContext());
	                    }
	                    notificationManager.showNotification(achievement, lpEarned, false);
                    } else {
                    	DebugLog("AchievementReachedSequence: notify_to_disabled - no IAN");
                    }
                    
                } else {
                    Logs.e(TAG, "Achievement not found!");
                    callback.onLootsieFailed("Achievement not found");
                }

            } catch (JSONException ex) {
                Logs.e(TAG, "PostUserAchievementsResult: exception: " + ex.getMessage());
                ex.printStackTrace();
                if (callback != null) {
                    callback.onLootsieFailed("");
                }
            } catch (Exception ex) {
                Logs.e(TAG, "PostUserAchievementsResult: exception: " + ex.getMessage());
                ex.printStackTrace();
                if (callback != null) {
                    callback.onLootsieFailed("");
                }            	
            }
        } else {
        	// {"errors": [{"field": "achievement_ids", "message": "LP maxed"}]}
        	// try to parse results
        	if (RestResult.content != null) {
        		try {
        			JSONObject jobj = new JSONObject(RestResult.content);
        			JSONArray errorsArr = jobj.getJSONArray("errors");
        			JSONObject errObj = errorsArr.getJSONObject(0);
        			String mesg = errObj.getString("message");
        			
    	            if (callback != null) {
    	                callback.onLootsieFailed(mesg);
    	            }
        			
        		} catch (JSONException ex) {
    	            if (callback != null) {
    	                callback.onLootsieFailed("");
    	            }
        		}
        		
        	} else {	        	
	            if (callback != null) {
	                callback.onLootsieFailed("");
	            }
        	}
        }

        cleanup();
    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
        Logs.v(TAG, String.format(msg, args));
        }
    }

}
