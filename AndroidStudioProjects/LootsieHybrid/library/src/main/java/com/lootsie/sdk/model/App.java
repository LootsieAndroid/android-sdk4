package com.lootsie.sdk.model;

import com.lootsie.sdk.utils.Logs;

import android.graphics.Color;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by jerrylootsie on 2/11/15.
 */
/*
    {
        "achievable_lp": "637",
        "achievements": [
            {
                "description": "Make your way to the top!",
                "id": "first_o",
                "is_achieved": false,
                "lp": 5,
                "name": "Moving Up!",
                "repeatable": true
            },
            {
                "description": "Keep up your momentum across the game!",
                "id": "first_x",
                "is_achieved": false,
                "lp": 1,
                "name": "Game Changer",
                "repeatable": true
            },
            {
                "description": "Player 1 won a match!",
                "id": "p1_win",
                "is_achieved": false,
                "lp": 5,
                "name": "Player 1 Win",
                "repeatable": true
            },
            {
                "description": "Player 1 won 2 matches!",
                "id": "p1_win2",
                "is_achieved": false,
                "lp": 7,
                "name": "2 Wins by Player 1",
                "repeatable": false
            },
            {
                "description": "Player 1 won 3 matches!",
                "id": "p1_win3",
                "is_achieved": false,
                "lp": 10,
                "name": "3 Wins by Player 1",
                "repeatable": false
            },
            {
                "description": "Player 2 won a match!",
                "id": "p2_win",
                "is_achieved": false,
                "lp": 5,
                "name": "Player 2 Win",
                "repeatable": false
            },
            {
                "description": "Player 2 won 2 matches!",
                "id": "p2_win2",
                "is_achieved": false,
                "lp": 7,
                "name": "2 Wins by Player 2",
                "repeatable": false
            },
            {
                "description": "Player 2 won 3 matches!",
                "id": "p2_win3",
                "is_achieved": false,
                "lp": 10,
                "name": "3 Wins by Player 2",
                "repeatable": false
            },
            {
                "description": "Subscribe to get ahead of your opponents.",
                "id": "second_o",
                "is_achieved": false,
                "lp": 5,
                "name": "Turn the Tide",
                "repeatable": false
            }
        ],
        "background_color": {
            "B": 255,
            "G": 132,
            "R": 0
        },
        "duration": 6.0,
        "engagements": [
            {
                "id": "12",
                "lp": 5,
                "name": "Facebook Share"
            },
            {
                "id": "14",
                "lp": 5,
                "name": "Twitter Share"
            }
        ],
        "genres": [
            {
                "id": 19,
                "name": "Casual"
            }
        ],
        "hook_type": "",
        "id": "172",
        "image_urls": {
            "DETAIL": null,
            "L": null,
            "M": null,
            "S": "https://d1no9q2xfha3ui.cloudfront.net/live/dp-image-s/1399350454111.jpg",
            "XL": null
        },
        "more_apps": 1,
        "name": "TicTacLoot",
        "notification": "bottom_right",
        "orientation": "automatic",
        "pos_x": "10",
        "pos_y": "10",
        "text_color": {
            "B": 255,
            "G": 255,
            "R": 255
        },
        "total_lp_earned": 0
    }
*/
public class App {

    private static String TAG = "Lootsie App";

    public int achievable_lp = 0;
    public int total_lp_earned = 0;

    public int background_color;

    public ArrayList<Achievement> achievements = new ArrayList<Achievement>();


    public String name = null;
    public String notification = null;
    public String orientation = null;

    public int pos_x;
    public int pos_y;

    public int text_color;

    public Icon icon = new Icon();

    public void parseFromJSON(JSONObject json) throws JSONException {

        achievable_lp = json.getInt("achievable_lp");
        DebugLog("App: achievable_lp %d", achievable_lp);

        total_lp_earned = json.getInt("total_lp_earned");
        DebugLog("App: total_lp_earned %d", total_lp_earned);

        name = json.getString("name");
        DebugLog("App: name %s", name);

        JSONArray achievementsArr = json.getJSONArray("achievements");
        achievements = new ArrayList<Achievement>();

        int length = achievementsArr.length();
        for (int i = 0; i < length; i++) {
            try {
                JSONObject obj = achievementsArr.getJSONObject(i);

                Achievement a = new Achievement();
                a.parseFromJSON(obj);

                achievements.add(a);

                DebugLog("App: achievement[%d] %s", i, a.toString());
            } catch (JSONException e) {
                Logs.e(TAG, "App: parseFromJSON: exception: " + e.getMessage());
                e.printStackTrace();
            }
        }

        // ASDK-384 Android Hybrid: Portrait and landscape rendering modes
        if (json.has("orientation")) {
            /*
            valid orientation values are:
            portrait
            landscape
            automatic
            portrait_as_landscape
            landscape_as_portrait
            client_side_override
             */
            orientation = json.getString("orientation");
            DebugLog("App: orientation: %s", orientation);
        }

        // for customized IAN colors
        if (json.has("text_color")) {
            DebugLog("App: text_color");
            JSONObject colorJsonObj = json.getJSONObject("text_color");
            text_color = parseColorFromJSON(colorJsonObj);
        } else {
            Logs.w(TAG, "App: text_color not found!");
            text_color = Color.BLACK;
        }

        // for customized IAN colors
        if (json.has("background_color")) {
            DebugLog("App: background_color");
            JSONObject colorJsonObj = json.getJSONObject("background_color");
            background_color = parseColorFromJSON(colorJsonObj);
        } else {
            Logs.w(TAG, "App: background_color not found!");
            background_color = Color.WHITE;
        }

        if (json.has("image_urls")) {
            JSONObject images = json.getJSONObject("image_urls");
            if (images != null) {
                this.icon.parseFromJSON(images);
            }
        }


    }

    private int parseColorFromJSON(JSONObject colorJsonObj) throws JSONException {

        int red = 0;
        int green = 0;
        int blue = 0;

        if (colorJsonObj.has("R")) {
            red = colorJsonObj.getInt("R");
        }

        if (colorJsonObj.has("G")) {
            green = colorJsonObj.getInt("G");
        }

        if (colorJsonObj.has("B")) {
            blue = colorJsonObj.getInt("B");
        }

        DebugLog("App: color[%d,%d,%d]",red,green,blue);

        return Color.rgb(red,green,blue);

    }


    private JSONObject parseColorToJSON(int inputColor) throws JSONException {

        JSONObject json = new JSONObject();

        // The components are stored as follows (alpha << 24) | (red << 16) | (green << 8) | blue.
        // alpha | red | green | blue
        // FF FF FF FF (base 16)
        // 0xFF000000
        //int alpha = inputColor >> 24;
        int alpha = (inputColor & 0xFF000000 ) >> 24;
        int red = (inputColor & 0x00FF0000) >> 16;
        int green = (inputColor & 0x0000FF00) >> 8;
        int blue = (inputColor & 0x000000FF );

        json.put("R", red);
        json.put("G", green);
        json.put("B", blue);

        return json;

    }

    /**
     * toJSONString
     * used to store and retrieve values from app preferences
     * @return
     */
    public String toJSONString() {
        String jsonStr = "";


        JSONObject json = new JSONObject();
        try {
            json.put("achievable_lp", achievable_lp);
            json.put("total_lp_earned", total_lp_earned);
            json.put("name", name);

            JSONArray achievementsArr = new JSONArray();
            if (achievements != null) {
	            for (int i = 0; i <  achievements.size(); i++) {
	                JSONObject jsonAchievement = achievements.get(i).toJson();
	                achievementsArr.put(i, jsonAchievement);
	            }
            }
            
            json.put("achievements", achievementsArr);

            // save IAN customizations
            JSONObject textColorJson = parseColorToJSON(text_color);
            json.put("text_color", textColorJson);

            JSONObject backgroundColorJson = parseColorToJSON(background_color);
            json.put("background_color", backgroundColorJson);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        jsonStr = json.toString();


        return jsonStr;
    }



    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (com.lootsie.sdk.utils.LootsieGlobals.debugLevel > 0) {
          Logs.v(TAG, String.format(msg, args));
        }
    }

}
