package com.lootsie.sdk.viewcontrollers;

import android.app.Activity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.lootsie.sdk.R;
import com.lootsie.sdk.callbacks.IIncrementEngineCallback;
import com.lootsie.sdk.lootsiehybrid.sequences.RewardedVideoSequence;
import com.lootsie.sdk.netutil.RestResult;

public class GetRewardVideoActivity extends Activity
{
    public static final String TAG = GetRewardVideoActivity.class.getSimpleName();

    public static final String INTENT_KEY_VIDEO_URL = GetRewardVideoActivity.class.getName() + "$INTENT_KEY_VIDEO_URL";
    public static final String STATE_KEY_VIDEO_SEEK_POSITION = "VIDEO_SEEK_POSITION";
    private static final int BACKUP_VIDEO_RESOURCE_ID = R.raw.video0;


    private Uri sourceVideoUri;
    private VideoView videoView;
    private ProgressBar loadingProgressBar;
    private ViewGroup videoProgressContainer;
    private CircularVideoPlaybackProgressBar videoProgressBar;
    private TextView videoProgressText;

    private int initialSeekPosition = 0;

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.com_lootsie_activity_get_reward_video);

        // source
        String videoUriString = getIntent().getStringExtra(INTENT_KEY_VIDEO_URL);
        if (TextUtils.isEmpty(videoUriString)) {
            videoUriString = "android.resource://" + getPackageName() + "/" + BACKUP_VIDEO_RESOURCE_ID;
        }

        try {
            sourceVideoUri = Uri.parse(videoUriString);
        }
        catch(Exception x) {
            Log.e(TAG, "Uri parsing or playback failure: ", x);
            Toast.makeText(this, getString(R.string.lootsie_reward_video_message_not_available), Toast.LENGTH_LONG).show();
            finish();
        }

        if (savedInstanceState != null) {
            initialSeekPosition = savedInstanceState.getInt(STATE_KEY_VIDEO_SEEK_POSITION, 0);
        }

        // link to views
        videoView = (VideoView) findViewById(R.id.com_lootsie_reward_video_view);

        loadingProgressBar = (ProgressBar) findViewById(R.id.com_lootsie_reward_video_loading_progress);

        videoProgressContainer = (ViewGroup) findViewById(R.id.com_lootsie_reward_video_playback_progress_block);
        videoProgressBar = (CircularVideoPlaybackProgressBar) findViewById(R.id.com_lootsie_reward_video_playback_progress_bar);
        videoProgressText = (TextView) findViewById(R.id.com_lootsie_reward_video_playback_progress_text);

        // setup and start playback
        startVideo();
    }

    private void startVideo()
    {
        loadingProgressBar.setVisibility(View.VISIBLE);

        videoView.setVideoURI(sourceVideoUri);
        videoView.setMediaController(null);
        videoView.requestFocus();

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
        {
            @Override
            public void onPrepared(MediaPlayer mp)
            {
                int videoDuration = videoView.getDuration();
                if (videoDuration < 0) {
                    Toast.makeText(GetRewardVideoActivity.this, "Failed to play video ad!", Toast.LENGTH_SHORT).show();
                    setResult(RESULT_CANCELED);
                    finish();
                }

                loadingProgressBar.setVisibility(View.GONE);

                if (initialSeekPosition > 0)
                    videoView.seekTo(initialSeekPosition);

                videoView.start();

                videoProgressContainer.setVisibility(View.VISIBLE);
                videoProgressBar.startPlaybackAnimation(initialSeekPosition, videoView.getDuration(), videoProgressText);
            }
        });

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
        {
            @Override
            public void onCompletion(MediaPlayer mp) {
                videoProgressContainer.setVisibility(View.GONE);
                setResult(RESULT_OK);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_CANCELED);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_KEY_VIDEO_SEEK_POSITION, videoView.getCurrentPosition());
        videoView.pause();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        initialSeekPosition = savedInstanceState.getInt(STATE_KEY_VIDEO_SEEK_POSITION, 0);
        videoView.seekTo(initialSeekPosition);
        startVideo();
    }

    private void reportOnCompletion()
    {
        RewardedVideoSequence rewardedVideoAd = new RewardedVideoSequence(
                RewardedVideoSequence.VIDEO_REWARD_CONTEXT.IN_APP_NOTIFICATION, "AID 121", new IIncrementEngineCallback()
        {
            @Override
            public void onIncrementEngineFailed(String errorMessage) {
                Toast.makeText(GetRewardVideoActivity.this, "Sorry, cannot offer reward for watching video at the moment!", Toast.LENGTH_LONG).show();
                setResult(RESULT_CANCELED);
                finish();
            }

            /**
             * TODO Get acquired points amount from response "videoview/response" and display into the
             */
            @Override
            public void onIncrementEngineSuccess(RestResult result) {
                // greet user
                VideoRewardHelper.showEarnedPopup(GetRewardVideoActivity.this, 1);

                //TODO increment internal/test count of "video points"

                setResult(RESULT_OK);
                finish();
            }
        });

        rewardedVideoAd.incrementLootsiePoints();
    }
}