package com.lootsie.sdk.viewcontrollers;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.lootsie.sdk.R;
import com.lootsie.sdk.adnets.AerServProxy;
import com.lootsie.sdk.adnets.model.AdLoadErrorCode;
import com.lootsie.sdk.adnets.model.AdNetProxy;
import com.lootsie.sdk.adnets.model.InterstitialAd;
import com.lootsie.sdk.adnets.model.InterstitialAdListener;
import com.lootsie.sdk.callbacks.IIncrementEngineCallback;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.lootsiehybrid.sequences.RewardedVideoSequence;
import com.lootsie.sdk.model.Achievement;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.netutil.RestResult;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Constructor;

/**
 * Provider of interstitial video ads to be opened in the Lootsie
 * marketplace or from Lootsie in-app notification.<br/><br/>
 * <p/>
 * It accesses an external ad network for video delivering, which is
 * selected by Lootsie SDK configuration (in the client app
 * build.gradle).
 * <p/>
 * <p/>
 * Created by Alexander S. Sidorov on 3/3/16.
 */
public class VideoAdProvider {
    public static final String TAG = VideoAdProvider.class.getSimpleName();

    private static final String LOOTSIE_ADMOB_REWARDED_VIDEO_AD_UNIT_ID = "ca-app-pub-1144642876810411/1415200481";

    public NotificationManager getNotificationManager() {
        if (notificationManager == null) {
            notificationManager = new NotificationManager(context);
        }
        return notificationManager;
    }

    private NotificationManager notificationManager;
    private boolean shouldBeShown;

    /**
     * Definition of external ad networks mediated by the provider
     */
    public enum AD_NETWORK {

        AerServ("com.lootsie.sdk.adnets.AerServProxy", "1000741"),
        AdMob("com.lootsie.sdk.adnets.AdMobProxy", "ca-app-pub-1144642876810411/1415200481");

        String proxyClassName, adUnitId;

        AD_NETWORK(String proxyClassName, String adUnitId) {
            this.proxyClassName = proxyClassName;
            this.adUnitId = adUnitId;
        }

        public boolean isAvailable() {
            try {
                Class.forName(proxyClassName);
                return true;
            } catch (Exception x) {
                return false;
            }
        }

        public AdNetProxy getProxy() {
            try {
                Class clazz = Class.forName(proxyClassName);
                Constructor ctor = clazz.getConstructor();
                return (AdNetProxy) ctor.newInstance((Class[]) null);
            } catch (Exception x) {
                return null;
            }
        }

        public String getAdUnitId() {
            return adUnitId;
        }
    }

    private Activity context;

    /**
     * Currently single network mediated, i.e. video ads served from a single network,
     * while multiple ad networks are supported.
     */
    private AD_NETWORK adNetworkMediated = AD_NETWORK.AdMob;

    private RewardedVideoSequence.VIDEO_REWARD_CONTEXT rewardContext;

    private String rewardId;

    private AdNetProxy adNetProxy;

    private InterstitialAd interstitialAd;

    private AdLoadErrorCode lastAdLoadErrorCode;


    public VideoAdProvider(Activity context) {
        this.context = context;
        for (AD_NETWORK adNetwork : AD_NETWORK.values()) {
            adNetProxy = adNetwork.getProxy();
            if (adNetProxy != null) {
                adNetworkMediated = adNetwork;
                initInterstitialAd();
                return;
            }
        }
        notificationManager = new NotificationManager(context);
    }

    public void setActivityContext(Activity activityContext) {
        this.context = activityContext;
        initInterstitialAd();
        notificationManager.updateContext(activityContext);
    }

    public boolean isVideoAdAvailable() {
        return adNetProxy != null;
    }

    /**
     * @return True if ad loading actually started.
     */
    public InterstitialAd load(final RewardedVideoSequence.VIDEO_REWARD_CONTEXT rewardContext, final String rewardId) {
        this.rewardContext = rewardContext;
        this.rewardId = rewardId;

        if (null == adNetProxy)
            return null;

        interstitialAd.load();

        return interstitialAd;
    }

    public void setRewardData(final RewardedVideoSequence.VIDEO_REWARD_CONTEXT rewardContext, final String
            rewardId) {
        this.rewardContext = rewardContext;
        this.rewardId = rewardId;
    }

    public void load() {
        interstitialAd.load();
    }

    private void initInterstitialAd() {
        interstitialAd = adNetProxy.createInterstitialAd(context,
                adNetworkMediated.adUnitId,
                new InterstitialAdListener() {
                    @Override
                    public void onInterstitialAdLoaded() {
                        Log.v(TAG, "Successfully loaded video ad from " + adNetworkMediated.name() + "!");
                        lastAdLoadErrorCode = null;
                        if (shouldBeShown) {
                            interstitialAd.show();
                        }
                        shouldBeShown = false;
                    }

                    @Override
                    public void onInterstitialAdFailedToLoad(AdLoadErrorCode errorCode) {
                        Log.w(TAG, "Failed to load video ad from " + adNetworkMediated.name() + "! Error: #" +
                                errorCode.name());
                        lastAdLoadErrorCode = errorCode;
                        shouldBeShown = false;
                    }


                    @Override
                    public void onInterstitialAdOpened() {
                        shouldBeShown = false;
                    }

                    @Override
                    public void onInterstitialAdClosed() {
                        reportAchievement();
                        interstitialAd.load(); // preload for the next time
                        shouldBeShown = false;
                    }

                    @Override
                    public void onInterstitialAdLeftApplication() {
                        shouldBeShown = false;
                    }
                });
        interstitialAd.load();
    }

    public void show() {
        if (interstitialAd != null) {
            if (interstitialAd.isLoaded()) {
                interstitialAd.show();
            } else if (null == lastAdLoadErrorCode) {
                Toast.makeText(context, "Loading video ad ...", Toast.LENGTH_LONG).show();
                shouldBeShown = true;
            } else {
                Toast.makeText(context, "Error loading video ad: " + lastAdLoadErrorCode, Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(context, "AdMob ad video is N/A", Toast.LENGTH_LONG).show();
        }
    }

    public void reportAchievement() {
        RewardedVideoSequence rewardedVideoAd = new RewardedVideoSequence(
                rewardContext,
                rewardId,
                reportAchievementCallback);

        rewardedVideoAd.incrementLootsiePoints();
    }

    private ReportAchievementCallback reportAchievementCallback = new ReportAchievementCallback();

    private class ReportAchievementCallback implements IIncrementEngineCallback {
        @Override
        public void onIncrementEngineFailed(String errorMessage) {
            Toast.makeText(context, "Sorry, cannot offer reward for watching video at the moment!", Toast
                    .LENGTH_LONG).show();
        }

        /**
         * TODO Get acquired points amount from response "videoview/response" and display into the
         */
        @Override
        public void onIncrementEngineSuccess(RestResult result) {
            // greet user
            try {
                JSONObject responseContent = new JSONObject(result.content);
                VideoRewardHelper.setPointCount(context, responseContent.getInt("total_lp"));
                VideoRewardHelper.incrementWatchedToday(context);

                Achievement videoAchievement = DataModel.user.achievements.get(0);
                videoAchievement.name = context.getResources()
                        .getString(R.string.lootsie_reward_video_message_congrats);
                videoAchievement.description = context.getResources()
                        .getString(R.string.watch_another_video_description);
                getNotificationManager().showNotification(videoAchievement, responseContent.getInt("lp"), true);
            } catch (JSONException e) {
                VideoRewardHelper.showEarnedPopup(context, 1);
                e.printStackTrace();
            }
        }

    }

    private void showBackupVideoAd() {
        int videoResourceId = context.getResources().getIdentifier("video0", "raw", context.getPackageName());
        Intent getRewardVideoIntent = new Intent(context, GetRewardVideoActivity.class);
        getRewardVideoIntent.putExtra(GetRewardVideoActivity.INTENT_KEY_VIDEO_URL,
                "android.resource://" + context.getPackageName() + "/" + videoResourceId);
        context.startActivityForResult(getRewardVideoIntent, LootsiePageBorderActivity.ACTIVITY_REQUEST_CODE);
    }
}
