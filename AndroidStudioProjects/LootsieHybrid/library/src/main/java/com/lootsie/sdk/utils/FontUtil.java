package com.lootsie.sdk.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * FontUtil
 *
 * @author Alexander Salnikov
 */
public class FontUtil {


    public static void applyFont(View view, Context context) {
        Typeface mBariolRegular = Typeface.createFromAsset(context.getAssets(), "fonts/Bariol_Regular.otf");
        Typeface mBariolBold = Typeface.createFromAsset(context.getAssets(), "fonts/Bariol_Bold.otf");

        setFont(view, mBariolRegular, mBariolBold);
    }

    /*
    * Sets the font on all TextViews in the ViewGroup. Searches
    * recursively for all inner ViewGroups as well. Just add a
    * check for any other views you want to set as well (EditText,
    * etc.)
    */
    private static void setFont(View view, Typeface regular, Typeface bold) {
        if (view == null) return;
        if (view instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) view;
            int count = group.getChildCount();
            View v;
            for (int i = 0; i < count; i++) {
                v = group.getChildAt(i);
                if (v instanceof TextView) {
                    TextView tv = (TextView) v;
                    if (tv.getTypeface() != null && tv.getTypeface().getStyle() == Typeface.BOLD) {
                        tv.setTypeface(bold);
                    } else {
                        tv.setTypeface(regular);
                    }
                } else if (v instanceof ViewGroup) {
                    setFont(v, regular, bold);
                }
            }
        } else if (view instanceof TextView) {
            TextView tv = (TextView) view;
            if (tv.getTypeface() != null && tv.getTypeface().getStyle() == Typeface.BOLD) {
                tv.setTypeface(bold);
            } else {
                tv.setTypeface(regular);
            }
        }
    }
}
