package com.lootsie.sdk.lootsiehybrid.sequences;

import android.util.Log;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IInitializationCallback;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.device.Device;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.lootsiehybrid.LootsieAccountManager;
import com.lootsie.sdk.lootsiehybrid.LootsieApi;
import com.lootsie.sdk.netutil.RESTGetAppTask;
import com.lootsie.sdk.netutil.RESTGetSessionTask;
import com.lootsie.sdk.netutil.RESTGetSettingsTask;
import com.lootsie.sdk.netutil.RESTGetUserAccountTask;
import com.lootsie.sdk.netutil.RESTGetUserAchievementsTask;
import com.lootsie.sdk.netutil.RESTGetUserRewardTask;
import com.lootsie.sdk.netutil.RESTPostUserLocationTask;
import com.lootsie.sdk.netutil.RESTPostUserSessionGuestTask;
import com.lootsie.sdk.netutil.RestResult;
import com.lootsie.sdk.netutil.TrackingSessionAsyncTask;
import com.lootsie.sdk.net.Headers;
import com.lootsie.sdk.net.RestClient;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.model.App;
import com.lootsie.sdk.model.Reward;
import com.lootsie.sdk.model.User;
import com.lootsie.sdk.model.UserRewards;
import com.lootsie.sdk.utils.PreferencesHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jerrylootsie on 2/10/15.
 */
/*
    InitSequence[1]: GetSession
    InitSequence[2]: GetLocation (asynchronous)
    InitSequence[3]: GetSettings
    InitSequence[4]: PostUserSessionGuest
    InitSequence[5]: GetApp
    InitSequence[6]: GetUserAchievements
    InitSequence[7]: GetUserAccount
    InitSequence[8]: GetUserReward

 */
public class InitSequence {

    private static Device device = null;
    private static String appKey = "";
    private static IInitializationCallback initcb;
    private static LootsieAccountManager lootsieAccountManager = null;

    private static String TAG = "Lootsie InitSequence";

    private static String locationJsonStr = null;

    // prepare all this stuff to run with Mock Objects instead of the real Networking layer underneath!
    // so we use the interface IGenericAsyncTask instead of Objects which inherit from AsyncTask directly.
    public static IGenericAsyncTask<Map<String, Object>> restGetSessionTask = null;
    public static IGenericAsyncTask<Map<String, Object>> restGetSettingsTask = null;
    public static IGenericAsyncTask<String> restPostUserSessionGuestTask = null;

    //static RESTPostUserLocationTask restPostUserLocationTask = null;
    public static IGenericAsyncTask<String> restPostUserLocationTask = null;

    public static IGenericAsyncTask<String> restGetAppTask = null;
    public static IGenericAsyncTask<String> restGetUserAchievementsTask = null;
    public static IGenericAsyncTask<String> restGetUserAccountTask = null;
    public static IGenericAsyncTask<String> restGetUserRewardTask = null;

    //static TrackingSessionAsyncTask trackingSessionTask = null;
    public static IGenericAsyncTask<String> trackingSessionTask = null;

    public InitSequence() {


    }

    /**
     * we initialize the resttasks here, and check because they may already have been initialized as mock objects earlier for testing
     */
    private static void init() {

        // InitSequence[1]: GetSession
        if (restGetSessionTask == null) {
            restGetSessionTask = (RESTGetSessionTask) new RESTGetSessionTask();
        }

        // InitSequence[2]: GetLocation (asynchronous)
        if (restPostUserLocationTask == null) {
            restPostUserLocationTask = new RESTPostUserLocationTask();
        }

        // InitSequence[3]: GetSettings
        if (restGetSettingsTask == null) {
            restGetSettingsTask = new RESTGetSettingsTask();
        }

        // InitSequence[4]: PostUserSessionGuest
        if (restPostUserSessionGuestTask == null) {
            restPostUserSessionGuestTask = new RESTPostUserSessionGuestTask();
        }

        // InitSequence[5]: GetApp
        if (restGetAppTask == null) {
            restGetAppTask = new RESTGetAppTask();
        }

        // InitSequence[6]: GetUserAchievements
        if (restGetUserAchievementsTask == null) {
            restGetUserAchievementsTask = new RESTGetUserAchievementsTask();
        }

        // InitSequence[7]: GetUserAccount
        if (restGetUserAccountTask == null) {
            restGetUserAccountTask = new RESTGetUserAccountTask();
        }

        // InitSequence[8]: GetUserReward
        if (restGetUserRewardTask == null) {
            restGetUserRewardTask = new RESTGetUserRewardTask();
        }

        if (trackingSessionTask == null) {
            trackingSessionTask = new TrackingSessionAsyncTask();
        }

    }

    /**
     * we need to create a new task every time because a task can only be executed once!
     */
    private static void cleanup() {
        // InitSequence[1]: GetSession
        restGetSessionTask = null;

        // InitSequence[2]: GetLocation (asynchronous)
        restPostUserLocationTask = null;

        // InitSequence[3]: GetSettings
        restGetSettingsTask = null;

        // InitSequence[4]: PostUserSessionGuest
        restPostUserSessionGuestTask = null;

        // InitSequence[5]: GetApp
        restGetAppTask = null;

        // InitSequence[6]: GetUserAchievements
        restGetUserAchievementsTask = null;

        // InitSequence[7]: GetUserAccount
        restGetUserAccountTask = null;

        // InitSequence[8]: GetUserReward
        restGetUserRewardTask = null;

        trackingSessionTask = null;
    }

    public static void start(Device inputDevice, String inputAppKey, IInitializationCallback callback) {

        device = inputDevice;
        appKey = inputAppKey;
        initcb = callback;

        lootsieAccountManager = LootsieEngine.getInstance().getLootsieAccountManager();

        init();

        RestClient.addHeader(Headers.APP_SECRET, appKey);
        //RestClient.setMaxRetries(retries);

        String authToken = PreferencesHandler.getString(LootsieGlobals.KEY_API_SESSION_TOKEN, LootsieEngine.getInstance().getApplicationContext());
        if (authToken != null) {
            DebugLog("InitSequence: start - cached api session token");
            DebugLog("InitSequence: api session token: " + authToken);
            DataModel.apiSessionToken = authToken;

            RestClient.addHeader(Headers.SESSION_TOKEN, authToken);

            GetSettings();
        } else {
            DebugLog("InitSequence: start - GetSession");

            // JSONObject obj = device.getJsonRepresentation();
            HashMap<String, Object> map = device.getHashMapRepresentation();

            DataModel.updateNetwork(LootsieApi.INIT_API_SESSION_URL.toString());

            IRESTCallback restCallback = new IRESTCallback() {
                @Override
                public void restResult(RestResult result) {
                    InitSequence.GetSessionResult(result);
                }
            };
            restGetSessionTask.setCallback(restCallback);
            restGetSessionTask.executeTask(map);

        }


    }

    // if the GPS gets information before we have api session token, or user session token
    public static void setLocation(String inputStr) {
        Log.v(TAG, "setLocation: " + inputStr);
        locationJsonStr = inputStr;
    }


    public static void GetSessionResult(RestResult RestResult) {
        if (RestResult.status == 200) {
            try {
                JSONObject json = new JSONObject(RestResult.content);
                String authToken = json.getString(Headers.SESSION_TOKEN);

                if (LootsieGlobals.debugLevel > 0)
                    Log.v(TAG, "GetSessionResult: session token: " + authToken);

                // save api session token somewhere it can be restored from
                PreferencesHandler.saveString(LootsieGlobals.KEY_API_SESSION_TOKEN, authToken, LootsieEngine.getInstance().getApplicationContext());

                DataModel.apiSessionToken = authToken;
                RestClient.addHeader(Headers.SESSION_TOKEN, authToken);

                GetSettings();

            } catch (JSONException ex) {
                Logs.e(TAG, "GetSessionResult: exception: " + ex.getMessage());
                if (initcb != null) initcb.onInitFailure();
                cleanup();
            }
        } else {
            if (initcb != null) initcb.onInitFailure();
            cleanup();
        }
    }

    public static void GetSettings() {

        DebugLog("InitSequence: GetSettings");
        HashMap<String, Object> map = device.getHashMapRepresentation();

        String settingsJsonStr = null;
        if (!DataModel.updateNetwork(LootsieGlobals.KEY_SETTINGS)) {
            settingsJsonStr = PreferencesHandler.getString(LootsieGlobals.KEY_SETTINGS, LootsieEngine.getInstance().getApplicationContext());
        }

        if (settingsJsonStr != null) {
            DebugLog("InitSequence: cached GetSettings data");

            try {
                JSONObject json = new JSONObject(settingsJsonStr);

                JSONObject settingsObj = json.getJSONObject("settings");
                if (settingsObj != null) {
                    boolean enabledFlag = settingsObj.getBoolean("sdk.enabled");

                    if (LootsieGlobals.debugLevel > 0)
                        Log.v(TAG, "GetSettingsResult (cached): sdk.enabled: " + enabledFlag);

                    if (enabledFlag) {
                        PostUserSessionGuest();
                    } else {
                        if (initcb != null) initcb.onInitFailure();
                    }
                }
            } catch (JSONException ex) {
                Logs.e(TAG, "GetSettingsResult: exception: " + ex.getMessage());
                if (initcb != null) initcb.onInitFailure();
                cleanup();
            }
        } else {
            IRESTCallback rest_GetSettings_Callback = new IRESTCallback() {
                @Override
                public void restResult(RestResult result) {
                    InitSequence.GetSettingsResult(result);
                }
            };
            restGetSettingsTask.setCallback(rest_GetSettings_Callback);
            restGetSettingsTask.executeTask(map);
        }

    }

    public static void GetSettingsResult(RestResult RestResult) {
        if (RestResult.status == 200) {
            try {
                JSONObject json = new JSONObject(RestResult.content);

                JSONObject settingsObj = json.getJSONObject("settings");
                if (settingsObj != null) {
                    boolean enabledFlag = settingsObj.getBoolean("sdk.enabled");

                    DebugLog("GetSettingsResult: sdk.enabled: " + enabledFlag);

                    if (settingsObj.has("currency_name")) {
                        DataModel.currency_name = settingsObj.getString("currency_name");
                    }
                    DebugLog("GetSettingsResult: currency_name: " + DataModel.currency_name);


                    if (settingsObj.has("currency_abbreviation")) {
                        DataModel.currency_abbreviation = settingsObj.getString("currency_abbreviation");
                    }
                    DebugLog("GetSettingsResult: currency_abbreviation: " + DataModel.currency_abbreviation);


                    if (enabledFlag) {
                        PostUserSessionGuest();
                    } else {
                        if (initcb != null) initcb.onInitFailure();
                    }
                }

                // save settings somewhere it can be restored from
                PreferencesHandler.saveString(LootsieGlobals.KEY_SETTINGS, RestResult.content, LootsieEngine.getInstance().getApplicationContext());

            } catch (JSONException ex) {
                Logs.e(TAG, "GetSettingsResult: exception: " + ex.getMessage());
                if (initcb != null) initcb.onInitFailure();
                cleanup();
            }
        } else {
            if (initcb != null) initcb.onInitFailure();
            cleanup();
        }
    }


    public static void PostUserSessionGuest() {

        String authToken = PreferencesHandler.getString(LootsieGlobals.KEY_USER_SESSION_TOKEN, LootsieEngine.getInstance().getApplicationContext());
        if (authToken != null) {
            DebugLog("InitSequence: cached user session token");
            DebugLog("InitSequence: user session token: " + authToken);

            DataModel.userSessionToken = authToken;
            RestClient.addHeader(Headers.USER_TOKEN, authToken);

            PostUserLocation();

            GetApp();

        } else {
            DebugLog("InitSequence: PostUserSessionGuest");

            // augment network stats with post user session tag
            DataModel.updateNetwork(LootsieApi.SIGN_IN_GUEST_URL.toString());

            IRESTCallback restCallback = new IRESTCallback() {
                @Override
                public void restResult(RestResult result) {
                    InitSequence.PostUserSessionGuestResult(result);
                }
            };
            restPostUserSessionGuestTask.setCallback(restCallback);
            restPostUserSessionGuestTask.executeTask();
        }

    }


    public static void PostUserSessionGuestResult(RestResult RestResult) {
        if (RestResult.status == 200) {
            try {
                JSONObject json = new JSONObject(RestResult.content);
                String authToken = json.getString(Headers.USER_TOKEN);

                if (LootsieGlobals.debugLevel > 0)
                    Logs.v(TAG, "PostUserSessionGuestResult: user session token: " + authToken);

                // save user session token somewhere it can be restored from
                PreferencesHandler.saveString(LootsieGlobals.KEY_USER_SESSION_TOKEN, authToken, LootsieEngine.getInstance().getApplicationContext());

                DataModel.userSessionToken = authToken;
                RestClient.addHeader(Headers.USER_TOKEN, authToken);

                PostUserLocation();

                GetApp();

            } catch (JSONException ex) {
                Logs.e(TAG, "PostUserSessionGuestResult: exception: " + ex.getMessage());
                if (initcb != null) initcb.onInitFailure();
                cleanup();
            }
        } else {
            if (initcb != null) initcb.onInitFailure();
            cleanup();
        }
    }


    public static void PostUserLocation() {
        DebugLog("InitSequence: PostUserLocation: " + locationJsonStr);


        // have we sent a locaiton update within the LootsieGlobals.NETWORK_CACHE_TIME_SECONDS timeout?
        if ((locationJsonStr != null) && (DataModel.updateNetwork(LootsieApi.USER_LOCATION_URL.toString()))) {
            // TODO: create a post call which uses json object directly, rather than switching from json to string, then to json again!
            restPostUserLocationTask.executeTask(locationJsonStr);
        }

    }

    public static void GetApp() {

        DebugLog("InitSequence: GetApp");

        String appJsonStr = null;
        if (!DataModel.updateNetwork(LootsieGlobals.KEY_APP)) {
            appJsonStr = PreferencesHandler.getString(LootsieGlobals.KEY_APP, LootsieEngine.getInstance().getApplicationContext());
        }

        if (appJsonStr != null) {
            DebugLog("InitSequence: cached GetApp data");

            try {
                JSONObject json = new JSONObject(appJsonStr);

                // store this stuff in the data model
                DataModel.app = new App();
                DataModel.app.parseFromJSON(json);

                processAppSettings();

                GetUserAchievements();

            } catch (JSONException ex) {
                Logs.e(TAG, "GetAppResult: exception: " + ex.getMessage());
                if (initcb != null) initcb.onInitFailure();
                cleanup();
            }
        } else {
            IRESTCallback restCallback = new IRESTCallback() {
                @Override
                public void restResult(RestResult result) {
                    InitSequence.GetAppResult(result);
                }
            };

            restGetAppTask.setCallback(restCallback);
            restGetAppTask.executeTask("test");
        }



    }

    public static void GetAppResult(RestResult RestResult) {
        if (RestResult.status == 200) {
            try {
                JSONObject json = new JSONObject(RestResult.content);

                // store this stuff in the data model
                DataModel.app = new App();
                DataModel.app.parseFromJSON(json);

                processAppSettings();

                // save user session token somewhere it can be restored from
                PreferencesHandler.saveString(LootsieGlobals.KEY_APP, DataModel.app.toJSONString(), LootsieEngine.getInstance().getApplicationContext());

                GetUserAchievements();

            } catch (JSONException ex) {
                Logs.e(TAG, "GetAppResult: exception: " + ex.getMessage());
                if (initcb != null) initcb.onInitFailure();
                cleanup();
            }
        } else {
            if (initcb != null) initcb.onInitFailure();
            cleanup();
        }
    }

    private static void processAppSettings() {

        // ASDK-384 Android Hybrid: Portrait and landscape rendering modes
        /*
        valid orientation values are:
        portrait
        landscape
        automatic
        portrait_as_landscape
        landscape_as_portrait
        client_side_override
         */
        if (DataModel.app.orientation != null) {
            if (DataModel.app.orientation.equalsIgnoreCase("portrait")) {
                LootsieEngine.setRenderingMode(LootsieEngine.RENDER_PORTRAIT);
            } else if (DataModel.app.orientation.equalsIgnoreCase("landscape")) {
                LootsieEngine.setRenderingMode(LootsieEngine.RENDER_LANDSCAPE);
            } else if (DataModel.app.orientation.equalsIgnoreCase("automatic")) {
                LootsieEngine.setRenderingMode(LootsieEngine.RENDER_DEFAULT);
            } else if (DataModel.app.orientation.equalsIgnoreCase("client_side_override")) {
                // do nothing
            }
        }
    }

    public static void GetUserAchievements() {

        DebugLog("InitSequence: GetUserAchievements");


        // use account manager to save and restore data
        User user = null;
        if (!DataModel.updateNetwork(LootsieApi.USER_ACHIEVEMENTS_URL.toString())) {
            user = lootsieAccountManager.getUser();
        }

        if (user == null) {
            IRESTCallback restCallback = new IRESTCallback() {
                @Override
                public void restResult(RestResult result) {
                    InitSequence.GetUserAchievementsResult(result);
                }
            };
            restGetUserAchievementsTask.setCallback(restCallback);

            restGetUserAchievementsTask.executeTask("test");

        } else {
            DebugLog("InitSequence: cached GetUserAchievements data");

            DataModel.user = user;
            DataModel.user.achievements = user.achievements;
            DataModel.user.achievedLp = user.achievedLp;
            DataModel.user.totalLp = user.totalLp;

            if (user.achievements != null) {
            	DebugLog("User achievements: %d", DataModel.user.achievements.size());
            } else {
            	DebugLog("User achievements: is null!");
            }
            DebugLog("User achievedLp: %d", DataModel.user.achievedLp);
            DebugLog("User totalLp: %d", DataModel.user.totalLp);

            GetUserAccount();
        }

    }

    public static void GetUserAchievementsResult(RestResult RestResult) {
        if (RestResult.status == 200) {
            try {
                JSONObject json = new JSONObject(RestResult.content);

                // store this stuff in the data model
//                userAchievements = new UserAchievements();
//                userAchievements.parseFromJSON(json);
                if (DataModel.user == null) {
                    DataModel.user = new User();
                }
                DataModel.user.parseFromJSONUserAchievementsInfo(json);
                
                if (DataModel.user.achievements != null) {
                	DebugLog("User achievements: %d", DataModel.user.achievements.size());
                } else {
                	DebugLog("User achievements: is null!");
                }
                
                DebugLog("User achievedLp: %d", DataModel.user.achievedLp);
                DebugLog("User totalLp: %d", DataModel.user.totalLp);

                GetUserAccount();

            } catch (JSONException ex) {
                Logs.e(TAG, "GetUserAchievementsResult: exception: " + ex.getMessage());
                if (initcb != null) initcb.onInitFailure();
                cleanup();
            }
        } else {
            if (initcb != null) initcb.onInitFailure();
            cleanup();
        }
    }

    public static void GetUserAccount() {

        DebugLog("InitSequence: GetUserAccount");

        User user = null;
//        if (!DataModel.updateNetwork(LootsieApi.USER_ACCOUNT_URL.toString())) {
            // use account manager to save and restore data -possibly from another app
            user = lootsieAccountManager.getUser();
//        }

        if (user == null) {
            IRESTCallback restCallback = new IRESTCallback() {
                @Override
                public void restResult(RestResult result) {
                    InitSequence.GetUserAccountResult(result);
                }
            };
            restGetUserAccountTask.setCallback(restCallback);
            restGetUserAccountTask.executeTask("test");
        } else {
            DebugLog("InitSequence: cached GetUserAccount data");

            // save achievement data we got previously
            user.achievements = DataModel.user.achievements;
            DataModel.user = user;
            //if (LootsieGlobals.debugLevel > 0) DataModel.user.print();
            if (LootsieGlobals.debugLevel > 0) user.print();

            lootsieAccountManager.saveAchievements(user.achievements);

            // skip to next step of init sequence
            GetUserReward();
        }

    }

    public static void GetUserAccountResult(RestResult RestResult) {
        if (RestResult.status == 200) {
            try {
                JSONObject json = new JSONObject(RestResult.content);

                // store this stuff in the data model
                if (DataModel.user == null) {
                    DataModel.user = new User();
                }
                DataModel.user.parseFromJSONUserAccountInfo(json);

                // use account manager to save and restore data
                DebugLog("UserAccount: %s", DataModel.user.getDisplayName());
                lootsieAccountManager.createUser(DataModel.user, DataModel.userSessionToken);
                if (LootsieGlobals.debugLevel > 0) DataModel.user.print();

                GetUserReward();

            } catch (JSONException ex) {
                Logs.e(TAG, "GetUserAccountResult: exception: " + ex.getMessage());
                if (initcb != null) initcb.onInitFailure();
                cleanup();
            }
        } else {
            if (initcb != null) initcb.onInitFailure();
            cleanup();
        }
    }

    public static void GetUserReward() {

        DebugLog("InitSequence: GetUserReward");

        String userRewardJsonStr = null;
        if (!DataModel.updateNetwork(LootsieGlobals.KEY_USER_REWARDS)) {
            userRewardJsonStr = PreferencesHandler.getString(LootsieGlobals.KEY_USER_REWARDS, LootsieEngine.getInstance().getApplicationContext());
        }

        if (userRewardJsonStr != null) {
            DebugLog("InitSequence: cached GetUserReward data");

            try {
                JSONObject json = new JSONObject(userRewardJsonStr);

                // store this stuff in the data model
                DataModel.userRewards = new UserRewards();
                
                // ASDK-441 test empty rewards list - comment out line below
                DataModel.userRewards.parseFromJSON(json);

                if (DataModel.userRewards.rewards.size() == 0) {
                	Reward testReward = new Reward();
                	testReward.generateTestReward();
                	DataModel.userRewards.rewards.add(testReward);
                }
                
                finishInitSequence();

            } catch (JSONException ex) {
                Logs.e(TAG, "GetUserReward: exception: " + ex.getMessage());
                if (initcb != null) initcb.onInitFailure();
                cleanup();
            }

            // we finished so return callback success
            if (initcb != null) initcb.onInitSuccess();

        } else {
            IRESTCallback restCallback = new IRESTCallback() {
                @Override
                public void restResult(RestResult result) {
                    InitSequence.GetUserRewardResult(result);
                }
            };
            restGetUserRewardTask.setCallback(restCallback);

            restGetUserRewardTask.executeTask("test");
        }

    }

    public static void GetUserRewardResult(RestResult RestResult) {
        if (RestResult.status == 200) {
            try {
                JSONObject json = new JSONObject(RestResult.content);

                // store this stuff in the data model
                DataModel.userRewards = new UserRewards();
                
                // ASDK-441 test empty rewards list - comment out line below
                DataModel.userRewards.parseFromJSON(json);

                if (DataModel.userRewards.rewards.size() == 0) {
                	Reward testReward = new Reward();
                	testReward.generateTestReward();
                	DataModel.userRewards.rewards.add(testReward);
                }
                
                // save user session token somewhere it can be restored from
                PreferencesHandler.saveString(LootsieGlobals.KEY_USER_REWARDS, DataModel.userRewards.toJSONString(), LootsieEngine.getInstance().getApplicationContext());

                finishInitSequence();

            } catch (JSONException ex) {
                Logs.e(TAG, "GetUserRewardResult: exception: " + ex.getMessage());
                if (initcb != null) initcb.onInitFailure();
                cleanup();
            }
        } else {
            if (initcb != null) initcb.onInitFailure();
            cleanup();
        }
    }


    private static void finishInitSequence() {

        DataModel.saveNetworkStats();

        // ASDK-356 Android-SDK4 sendUserActivity
        trackingSessionTask.executeTask("startSession");

        // we finished so return callback success
        if (initcb != null) initcb.onInitSuccess();

        cleanup();
    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
           Logs.v(TAG, String.format(msg, args));
        }
    }
}
