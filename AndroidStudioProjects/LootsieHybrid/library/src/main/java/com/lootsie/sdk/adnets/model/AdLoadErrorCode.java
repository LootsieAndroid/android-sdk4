package com.lootsie.sdk.adnets.model;

/**
 * Created by Alexander S. Sidorov on 2/25/16.
 */
public enum AdLoadErrorCode
{
    ERROR_CODE_INTERNAL_ERROR,
    ERROR_CODE_INVALID_REQUEST,
    ERROR_CODE_NETWORK_ERROR,
    ERROR_CODE_NO_FILL,
    ERROR_CODE_UNDEFINED
}
