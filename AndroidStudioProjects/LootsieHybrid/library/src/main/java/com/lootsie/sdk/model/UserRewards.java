package com.lootsie.sdk.model;

import com.lootsie.sdk.utils.Logs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by jerrylootsie on 2/11/15.
 */
/*
{
   "rewards": [
       {
           "id": "4",
           "name": "Free Venti Coffee!",
           "description": "",
           "lp": 2000,
           "tos_url": "https://stage-prime-www.lootsie.com/mp/rewards/4/terms-of-service",
           "tos_text": "Lorem ipsum...",
           "text_to_share": "I just got Free Venti Coffee! at Starbucks from @Lootsie by playing my favorite games!",
           "image_urls": {
               "S": "https://lootsie.s3.amazonaws.com/stage1/ap-image-s/1351080030677..png",
               "M": null,
               "L": null,
               “XL”: null,
               “DETAIL”: null
           },
           "engagements": [
               {
                   "id": "12",
                   "name": "Facebook Share",
                   "lp": 5
               },
               {
                   "id": "14",
                   "name": "Twitter Share",
                   "lp": 5
               }
           ]
}
 */
public class UserRewards {

    private static String TAG = "Lootsie UserAchievements";

    public ArrayList<Reward> rewards = new ArrayList<Reward>();



    // ported from v1 rewardmanager
    public void parseFromJSON(JSONObject json) throws JSONException {

        rewards = new ArrayList<Reward>();

        try {
            JSONArray rewardsArr = json.getJSONArray("rewards");

            /*
                {
                   "rewards": [  {...}  ]
                }
             */
            if (rewardsArr != null && rewardsArr.length() > 0) {
                for (int i = 0; i < rewardsArr.length(); i++) {
                    JSONObject o = rewardsArr.getJSONObject(i);

                    Reward reward = new Reward();
                    reward.parseFromJSON(o);

                    rewards.add(reward);

                    DebugLog("reward[%d] %s", i, reward.toString());
                }
            }
        } catch (JSONException ex) {
            Logs.e(TAG, "UserRewards: parseFromJSON exception: " + ex.getMessage());
            throw ex;
        }

    }


    public String toJSONString() throws JSONException {

        JSONObject rewardsObj = new JSONObject();
        JSONArray rewardsArr = new JSONArray();

        if (rewards != null && rewards.size() > 0) {
            for (int i = 0; i < rewards.size(); i++) {
                Reward reward = rewards.get(i);
                DebugLog("reward[%d] %s", i, reward.toString());

                JSONObject o = reward.toJSON();

                rewardsArr.put(i,o);
            }
        }

        rewardsObj.put("rewards", rewardsArr);

        return rewardsObj.toString();
    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (com.lootsie.sdk.utils.LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }

}
