package com.lootsie.sdk.viewcontrollers;

import android.annotation.TargetApi;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lootsie.sdk.adnets.model.AdNetProxy;
import com.lootsie.sdk.adnets.model.InterstitialAd;
import com.lootsie.sdk.callbacks.IIncrementEngineCallback;
import com.lootsie.sdk.lootsiehybrid.LOOTSIE_NOTIFICATION_CONFIGURATION;
import com.lootsie.sdk.lootsiehybrid.Lootsie;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.lootsiehybrid.sequences.RewardedVideoSequence;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.model.Reward;
import com.lootsie.sdk.netutil.RestResult;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.utils.RUtil;
import com.lootsie.sdk.viewcontrollers.base.BasePageBorderActivity;

/**
 * Created by jerrylootsie on 2/27/15.
 */

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class LootsiePageBorderActivity extends BasePageBorderActivity {

    private static String TAG = "Lootsie LootsiePageBorder";

    public static final int ACTIVITY_REQUEST_CODE = 1001;

    public static final boolean SHOW_VIDEO_IN_INTERSTITIAL = true;
    public static final boolean AD_TEST_MODE = true;


    private String[] mMenuItems;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private RelativeLayout rightRL = null;
    private DrawerLayout drawerLayout = null;

    private TextView userPoints = null;
    private TextView userName = null;

    //
    // AdMob integration
    //
    private AdNetProxy adNetProxy;

    private InterstitialAd interstitialAd;

    private String watchedVideoRewardIdString;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (LootsieGlobals.debugLevel > 0) Logs.d(TAG, "onCreate");

        //setContentView(R.layout.com_lootsie_page_border);
        //setContentView(RUtil.getResourceIdByName(this.getApplicationContext().getPackageName(), "layout",
        // "com_lootsie_page_border"));  // fails?
        setContentView(RUtil.getLayoutId(this.getApplicationContext(), "com_lootsie_page_border"));

//        int screenOrientationMode = Lootsie.getInstance().getScreenOrientation();
        LootsieEngine.getInstance().getScreenOrientationManager().renderScreenWith(this);

        LootsieEngine.getInstance().setSDKShowing(true);

        // -- [[ Rewarded video ad setup

        Lootsie.setActivityContext(this);

        // find reward id for watching the video ad
        int maxRewardLp = 0;
        for (int i = 0; i < DataModel.userRewards.rewards.size(); i++) {
            Reward reward = DataModel.userRewards.rewards.get(i);
            if (maxRewardLp < reward.lp) {
                maxRewardLp = reward.lp;
                watchedVideoRewardIdString = reward.id;
            }
        }

        if (watchedVideoRewardIdString != null) {
            try {
                Integer.valueOf(watchedVideoRewardIdString);
                LootsieEngine.getInstance().getVideoAdProvider()
                        .setRewardData(RewardedVideoSequence.VIDEO_REWARD_CONTEXT.MARKETPLACE_OFFER,
                                watchedVideoRewardIdString);
            } catch (NumberFormatException x) {
                Log.w(TAG, "Cannot find ID for watching video ad.", x);
                LootsieEngine.getInstance().getVideoAdProvider()
                        .setRewardData(RewardedVideoSequence.VIDEO_REWARD_CONTEXT.MARKETPLACE_OFFER, "0");
            }
        } else {
            Log.w(TAG, "Cannot find ID for watching video ad!");
            LootsieEngine.getInstance().getVideoAdProvider()
                    .setRewardData(RewardedVideoSequence.VIDEO_REWARD_CONTEXT.MARKETPLACE_OFFER, "0");
        }

        // -- ]]

        String notifyConfigStr = getIntent().getStringExtra("LOOTSIE_NOTIFICATION_CONFIGURATION");

        if (notifyConfigStr != null) {
            //LOOTSIE_NOTIFICATION_CONFIGURATION notifyConfig = (LOOTSIE_NOTIFICATION_CONFIGURATION) extras.get
            // ("LOOTSIE_NOTIFICATION_CONFIGURATION");
            //String notifyConfigStr = extras.getString("LOOTSIE_NOTIFICATION_CONFIGURATION");

            LOOTSIE_NOTIFICATION_CONFIGURATION notifyConfig = LOOTSIE_NOTIFICATION_CONFIGURATION.valueOf
                    (notifyConfigStr);

            DebugLog("onCreate: LOOTSIE_NOTIFICATION_CONFIGURATION: %s", notifyConfig.toString());
            switch (notifyConfig) {
                // notify_to_disabled

                case notify_to_rewardsPage:
                    // rewards
                    hideAcheivementsFragment();
                    hideVersionInfoFragment();
                    hideTermsFragment();
                    hideTermsConditionsFragment();
                    hideAccountSettingsFragment();
                    break;
                case notify_to_achievementsPage:
                    hideRewardsFragment();
                    // achievments
                    hideVersionInfoFragment();
                    hideTermsFragment();
                    hideTermsConditionsFragment();
                    hideAccountSettingsFragment();
                    break;
                case notify_to_aboutPage:
                    hideRewardsFragment();
                    hideAcheivementsFragment();
                    hideTermsFragment();
                    hideTermsConditionsFragment();
                    // versionINfo
                    hideAccountSettingsFragment();
                    break;
                case notify_to_TOSPage:
                    hideRewardsFragment();
                    hideAcheivementsFragment();
                    hideVersionInfoFragment();
                    // termspage
                    hideTermsConditionsFragment();
                    hideAccountSettingsFragment();
                    break;

                case notify_to_accountPage:
                    hideRewardsFragment();
                    hideAcheivementsFragment();
                    hideTermsFragment();
                    hideTermsConditionsFragment();
                    hideVersionInfoFragment();
                    // accountsettings
                    break;

                // notify_to_webView

                // notify_to_customPage

                default:
                    // rewardspage
                    hideAcheivementsFragment();
                    hideVersionInfoFragment();
                    hideTermsFragment();
                    hideTermsConditionsFragment();
                    hideAccountSettingsFragment();
                    break;
            }
            ;
        } else {
            DebugLog("onCreate: no extras found in intent!");

            // show rewards page by default
            hideAcheivementsFragment();
            hideVersionInfoFragment();
            hideTermsFragment();
            hideTermsConditionsFragment();
            hideAccountSettingsFragment();
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        if (LootsieGlobals.debugLevel > 0) Logs.d(TAG, "onResume");

        updateEntries();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ACTIVITY_REQUEST_CODE) {
            // tell server that user earned what offered
            RewardedVideoSequence rewardedVideoAd = new RewardedVideoSequence(
                    RewardedVideoSequence.VIDEO_REWARD_CONTEXT.IN_APP_NOTIFICATION, "ID#121",
                    new IIncrementEngineCallback() {
                @Override
                public void onIncrementEngineFailed(String errorMessage) {
                    Toast.makeText(LootsiePageBorderActivity.this, "Sorry, cannot offer reward for watching video at " +
                            "the moment!", Toast.LENGTH_LONG).show();
                }

                /**
                 * TODO Get acquired points amount from response "videoview/response" and display into the
                 */
                @Override
                public void onIncrementEngineSuccess(RestResult result) {
                    // greet user
                    VideoRewardHelper.showEarnedPopup(LootsiePageBorderActivity.this, 1);

                    //TODO increment internal/test count of "video points"
                }
            });

            rewardedVideoAd.incrementLootsiePoints();

            // preload interstitial video ad for possible subsequent watching
            interstitialAd.load();
        }
    }

    private void updateEntries() {

        // right menu drawer
        if (mDrawerLayout == null) {
            //mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mDrawerLayout = (DrawerLayout) findViewById(RUtil.getId(this, "drawer_layout"));
            //mDrawerList = (ListView) findViewById(R.id.right_drawer_list);
            mDrawerList = (ListView) findViewById(RUtil.getId(this, "right_drawer_list"));

            //mMenuItems = getResources().getStringArray(R.array.lootsie_menu_array);
            mMenuItems = getResources().getStringArray(RUtil.getArrayId(this, "lootsie_menu_array"));

            if ((mDrawerList != null) && (mMenuItems != null)) {
                // set up the drawer's list view with items and click listener
                int listItemId = RUtil.getLayoutId(this.getApplicationContext(), "com_lootsie_drawer_list_item");
                mDrawerList.setAdapter(new ArrayAdapter<String>(this, listItemId, mMenuItems));

                mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
            } else {
                Logs.e(TAG, "Error: can't find lootsie_menu_array!");
            }

            //rightRL = (RelativeLayout) findViewById(R.id.right_drawer);
            rightRL = (RelativeLayout) findViewById(RUtil.getId(this, "right_drawer"));

            //drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawerLayout = (DrawerLayout) findViewById(RUtil.getId(this, "drawer_layout"));
        }

        if (userPoints == null) {
            //userPoints = (TextView) this.getActivity().findViewById(R.id.achievementsPageUserPoints);
            //userPoints = (TextView) this.getActivity().findViewById(RUtil.getId(this.getActivity(),
            // "achievementsPageUserPoints"));
            userPoints = (TextView) findViewById(RUtil.getId(this, "lootsieBorderPageUserPoints"));
        }

        if (userPoints != null) {
            // ASDK-470 - New - Android Style Guide: Header does not contain the expected information (it’s showing
            // the app’s LP instead of the user's total LP balance).
            String userPointsStr = (String.valueOf(DataModel.user.totalLp) + " " + DataModel.currency_abbreviation);

            DebugLog("userPointsStr: %s", userPointsStr);

            userPoints.setText(userPointsStr);
        } else {
            Logs.e(TAG, "updateEntries: userPoints not found!");
        }

        if (userName == null) {
            userName = (TextView) findViewById(RUtil.getId(this, "lootsieBorderPageUserName"));
        }

        if (userName != null) {
            String userNameStr = String.valueOf(DataModel.user.firstName);
            DebugLog("userNameStr: %s", userNameStr);

            if (DataModel.user.isGuest) {
                userName.setText("Guest");
            } else {
                if (userNameStr != null) {
                    if (userNameStr.length() > 0) {
                        userName.setText("Welcome, " + userNameStr);
                    } else {
                        userName.setText(LootsieGlobals.DEFAULT_WELCOME_NAME);
                    }
                } else {
                    userName.setText(LootsieGlobals.DEFAULT_WELCOME_NAME);
                }
            }
        } else {
            Logs.e(TAG, "updateEntries: userName not found!");
        }

    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {
        Log.d(TAG, "selectItem: " + position);

        switch (position) {
            case 0:
                //onClickMenuRewards();
                showRewardsFragment();

                hideVersionInfoFragment();
                hideAcheivementsFragment();
                hideTermsFragment();
                hideTermsConditionsFragment();
                hideAccountSettingsFragment();
                break;
            case 1:
                // onClickMenuAchievements

                showAchievementsFragment();

                hideVersionInfoFragment();
                hideRewardsFragment();
                hideTermsFragment();
                hideTermsConditionsFragment();
                hideAccountSettingsFragment();
                break;
            case 2:
                // onClickMenuVersionInfo
                showVersionInfoFragment();

                hideRewardsFragment();
                hideAcheivementsFragment();
                hideTermsFragment();
                hideTermsConditionsFragment();
                hideAccountSettingsFragment();
                break;
            case 3:
                // onClickMenuTerms
                showTermsFragment();

                hideVersionInfoFragment();
                hideRewardsFragment();
                hideAcheivementsFragment();
                hideTermsConditionsFragment();
                hideAccountSettingsFragment();
                break;
            case 4:
                // onClickMenuTermsConditions
                showTermsConditionsFragment();

                hideVersionInfoFragment();
                hideRewardsFragment();
                hideAcheivementsFragment();
                hideTermsFragment();
                hideAccountSettingsFragment();
                break;
            case 5:
                // onClickMenuAccount
                showAccountSettingsFragment();

                hideVersionInfoFragment();
                hideRewardsFragment();
                hideAcheivementsFragment();
                hideTermsFragment();
                hideTermsConditionsFragment();
                break;
            default:
                break;
        }
        ;

        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);

        if (drawerLayout != null) {
            drawerLayout.closeDrawer(rightRL);
        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        DebugLog("onConfigurationChange: " + newConfig.orientation);
    }

    @Override
    public void onClickClose(View view) {
        if (LootsieGlobals.debugLevel > 0) Logs.d(TAG, "onClickClose");

        finish();
    }

    @Override
    public void showPopup(View v) {
        if (LootsieGlobals.debugLevel > 0) Logs.d(TAG, "showPopup");

        drawerLayout.openDrawer(rightRL);
    }


    // helper function
//    private android.app.Fragment getFragmentById(int id) {
//        android.app.Fragment fragment = getFragmentManager()
//            .findFragmentById(id);
//        return fragment;
//    }

    // helper function
    private android.app.Fragment getFragment(String fragmentName) {
        android.app.Fragment fragment = getFragmentManager()
                .findFragmentById(RUtil.getResourceId(this.getBaseContext(), fragmentName, "id", this.getPackageName
                        ()));

        return fragment;
    }


    private void showRewardsFragment() {
        // see com_lootsie_page_border.xml for ids of fragments
        RewardsFragment rewardsFragment = (RewardsFragment) getFragment("rewardsFragment");
        //RewardsFragment rewardsFragment = (RewardsFragment) getFragment("com_lootsie_rewards_fragment");
//        RewardsFragment rewardsFragment = (RewardsFragment) getFragmentById(R.id.rewardsFragment);


        if (rewardsFragment != null && rewardsFragment.isInLayout() && rewardsFragment.isHidden()) {
            rewardsFragment.updateFragment();

            FragmentManager fm = getFragmentManager();
            fm.beginTransaction()
//                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                    .show(rewardsFragment)
                    .commit();
        }
    }

    private void hideRewardsFragment() {
        // see com_lootsie_page_border.xml for ids of fragments
        RewardsFragment rewardsFragment = (RewardsFragment) getFragment("rewardsFragment");
        //RewardsFragment rewardsFragment = (RewardsFragment) getFragment("com_lootsie_rewards_fragment");
//        RewardsFragment rewardsFragment = (RewardsFragment) getFragmentById(R.id.rewardsFragment);

        if (rewardsFragment == null) {
            Logs.e(TAG, "hideRewardsFragment: fragment not found!");
        }

        if (rewardsFragment != null && rewardsFragment.isInLayout() && !rewardsFragment.isHidden()) {

            FragmentManager fm = getFragmentManager();
            fm.beginTransaction()
//                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                    .hide(rewardsFragment)
                    .commit();
        }
    }

    private void hideAcheivementsFragment() {
        AchievementsFragment achievementsFragment = (AchievementsFragment) getFragment("achievementsFragment");
//        AchievementsFragment achievementsFragment = (AchievementsFragment) getFragment
// ("com_lootsie_achievements_fragment");
//        AchievementsFragment achievementsFragment = (AchievementsFragment) getFragmentById(R.id.achievementsFragment);

        if (achievementsFragment == null) {
            Logs.e(TAG, "hideAcheivementsFragment: fragment not found!");
        }


        if (achievementsFragment != null && achievementsFragment.isInLayout() && !achievementsFragment.isHidden()) {

            FragmentManager fm = getFragmentManager();
            fm.beginTransaction()
//                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                    .hide(achievementsFragment)
                    .commit();
        }

    }

    private void showAchievementsFragment() {
        AchievementsFragment achievementsFragment = (AchievementsFragment) getFragment("achievementsFragment");
//    	AchievementsFragment achievementsFragment = (AchievementsFragment) getFragment
// ("com_lootsie_achievements_fragment");
//        AchievementsFragment achievementsFragment = (AchievementsFragment) getFragmentById(R.id.achievementsFragment);


        if (achievementsFragment != null && achievementsFragment.isInLayout() && achievementsFragment.isHidden()) {
            achievementsFragment.updateFragment();

            FragmentManager fm = getFragmentManager();
            fm.beginTransaction()
//                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                    .show(achievementsFragment)
                    .commit();
        }

    }

    private void hideVersionInfoFragment() {
        VersionInfoFragment versionInfoFragment = (VersionInfoFragment) getFragment("versionInfoFragment");
//        VersionInfoFragment versionInfoFragment = (VersionInfoFragment) getFragment
// ("com_lootsie_versioninfo_fragment");
//        VersionInfoFragment versionInfoFragment = (VersionInfoFragment) getFragmentById(R.id.versionInfoFragment);


        if (versionInfoFragment == null) {
            Logs.e(TAG, "hideVersionInfoFragment: fragment not found!");
        }

        if (versionInfoFragment != null && versionInfoFragment.isInLayout() && !versionInfoFragment.isHidden()) {

            FragmentManager fm = getFragmentManager();
            fm.beginTransaction()
//                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                    .hide(versionInfoFragment)
                    .commit();
        }

    }

    private void showVersionInfoFragment() {
        VersionInfoFragment versionInfoFragment = (VersionInfoFragment) getFragment("versionInfoFragment");
//        VersionInfoFragment versionInfoFragment = (VersionInfoFragment) getFragment
// ("com_lootsie_versioninfo_fragment");
//        VersionInfoFragment versionInfoFragment = (VersionInfoFragment) getFragmentById(R.id.versionInfoFragment);


        if (versionInfoFragment != null && versionInfoFragment.isInLayout() && versionInfoFragment.isHidden()) {
            versionInfoFragment.updateFragment();

            FragmentManager fm = getFragmentManager();
            fm.beginTransaction()
//                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                    .show(versionInfoFragment)
                    .commit();
        }

    }

    private void hideTermsConditionsFragment() {
        TermsConditionsFragment termsFragment = (TermsConditionsFragment) getFragment("termsConditionsFragment");
//        TermsFragment termsFragment = (TermsFragment) getFragment("com_lootsie_terms_fragment");
//        TermsFragment termsFragment = (TermsFragment) getFragmentById(R.id.termsFragment);

        if (termsFragment == null) {
            Logs.e(TAG, "hideTermsConditionsFragment: fragment not found!");
        }

        if (termsFragment != null && termsFragment.isInLayout() && !termsFragment.isHidden()) {

            FragmentManager fm = getFragmentManager();
            fm.beginTransaction()
//                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                    .hide(termsFragment)
                    .commit();
        }

    }

    private void showTermsConditionsFragment() {
        TermsConditionsFragment termsFragment = (TermsConditionsFragment) getFragment("termsConditionsFragment");
//    	TermsFragment termsFragment = (TermsFragment) getFragment("com_lootsie_terms_fragment");
//        TermsFragment termsFragment = (TermsFragment) getFragmentById(R.id.termsFragment);

        if (termsFragment != null && termsFragment.isInLayout() && termsFragment.isHidden()) {
            termsFragment.updateFragment();

            FragmentManager fm = getFragmentManager();
            fm.beginTransaction()
//                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                    .show(termsFragment)
                    .commit();
        }

    }

    private void hideTermsFragment() {
        TermsFragment termsFragment = (TermsFragment) getFragment("termsFragment");
//        TermsFragment termsFragment = (TermsFragment) getFragment("com_lootsie_terms_fragment");
//        TermsFragment termsFragment = (TermsFragment) getFragmentById(R.id.termsFragment);

        if (termsFragment == null) {
            Logs.e(TAG, "hideTermsFragment: fragment not found!");
        }

        if (termsFragment != null && termsFragment.isInLayout() && !termsFragment.isHidden()) {

            FragmentManager fm = getFragmentManager();
            fm.beginTransaction()
//                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                    .hide(termsFragment)
                    .commit();
        }

    }

    private void showTermsFragment() {
        TermsFragment termsFragment = (TermsFragment) getFragment("termsFragment");
//    	TermsFragment termsFragment = (TermsFragment) getFragment("com_lootsie_terms_fragment");
//        TermsFragment termsFragment = (TermsFragment) getFragmentById(R.id.termsFragment);

        if (termsFragment != null && termsFragment.isInLayout() && termsFragment.isHidden()) {
            termsFragment.updateFragment();

            FragmentManager fm = getFragmentManager();
            fm.beginTransaction()
//                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                    .show(termsFragment)
                    .commit();
        }

    }

    private void hideAccountSettingsFragment() {
        AccountSettingsFragment accountSettingsFragment = (AccountSettingsFragment) getFragment
                ("accountSettingsFragment");

        if (accountSettingsFragment == null) {
            Logs.e(TAG, "hideAccountSettingsFragment: fragment not found!");
        }

        if (accountSettingsFragment != null && accountSettingsFragment.isInLayout() && !accountSettingsFragment
                .isHidden()) {

            FragmentManager fm = getFragmentManager();
            fm.beginTransaction()
//                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                    .hide(accountSettingsFragment)
                    .commit();
        }

    }

    private void showAccountSettingsFragment() {
        AccountSettingsFragment accountSettingsFragment = (AccountSettingsFragment) getFragment
                ("accountSettingsFragment");

        if (accountSettingsFragment != null && accountSettingsFragment.isInLayout() && accountSettingsFragment
                .isHidden()) {
            accountSettingsFragment.updateFragment();

            FragmentManager fm = getFragmentManager();
            fm.beginTransaction()
//                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                    .show(accountSettingsFragment)
                    .commit();
        }

    }

    @Override
    public void updateActivity() {
        DebugLog("updateActivity");

        updateEntries();
    }


    @Override
    public void finish() {
        LootsieEngine.getInstance().getCallback().onLootsieBarClosed();

        super.finish();
    }

    //
    // -- Rewarded video ad support
    //

    public boolean isRewardedVideoSupported() {
        return LootsieEngine.getInstance().getVideoAdProvider().isVideoAdAvailable();
    }

    public void showRewardedVideoAd() {
        LootsieEngine.getInstance().getVideoAdProvider().show();
    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     *
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }


}
