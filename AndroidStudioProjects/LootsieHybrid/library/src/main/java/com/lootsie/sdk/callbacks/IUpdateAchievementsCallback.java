package com.lootsie.sdk.callbacks;

public interface IUpdateAchievementsCallback {
    public void onUpdateAchievementsSuccess();
    public void onUpdateAchievementsFailure();	
}
