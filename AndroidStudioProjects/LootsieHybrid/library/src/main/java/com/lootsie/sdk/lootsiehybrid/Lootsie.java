package com.lootsie.sdk.lootsiehybrid;

import android.app.Activity;
import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.lootsie.sdk.callbacks.IAchievementReached;
import com.lootsie.sdk.callbacks.IRedeemReward;
import com.lootsie.sdk.callbacks.IInitializationCallback;
import com.lootsie.sdk.callbacks.ILogData;
import com.lootsie.sdk.callbacks.ISignOutCallback;
import com.lootsie.sdk.lootsiehybrid.LOOTSIE_CONFIGURATION;
import com.lootsie.sdk.lootsiehybrid.LOOTSIE_NOTIFICATION_CONFIGURATION;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine.InternalSignOutCallback;
import com.lootsie.sdk.model.Achievement;
import com.lootsie.sdk.model.Reward;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

import java.util.ArrayList;

/**
 * Created by jerrylootsie on 3/6/15.
 * class to hide many public LootsieEngine methods from the app developer
 */
public class Lootsie {

    private static String TAG = "Lootsie";

    public static void setConfiguration(LOOTSIE_CONFIGURATION requestedConfiguration) {
        try {
            LootsieEngine.setConfiguration(requestedConfiguration);
        }catch (Exception e) {
            Logs.e(TAG, "setConfiguration: Some Unexpected exception");
            e.printStackTrace();
        }
    }

    // ASDK-355 Android-SDK4 setLogLevel - optional enable or disable debugging
    public static void setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL loglevel) {
        try {
            LootsieEngine.setLogLevel(loglevel);
        }catch (Exception e) {
            Logs.e(TAG, "setLogLevel: Some Unexpected exception");
            e.printStackTrace();
        }
    }


    /*
     * This method registers LEG applications to Lootsie engine. Once registered
     * successfully Lootsie engine will run as part of the calling
     * application and lasts until the apps life cycle. This method MUST be
     * called only ONCE at the start of the app
     */
    public static void init(Application legApp, String appKey, IInitializationCallback callback) {
        try {
            LootsieEngine.init(legApp, appKey, callback);
        }catch (Exception e) {
            Logs.e(TAG, "onDestroy: Some Unexpected exception");
            e.printStackTrace();
        }

    }


    // this method will release the lootsie resources gracefully
    /**
     * This method releases all lootsie resources gracefully. call it on
     * application ondestroy method
     *
     */
    public static void onDestroy() {
        try {
            LootsieEngine.getInstance().onDestroy();
        }catch (Exception e) {
            Logs.e(TAG, "onDestroy: Some Unexpected exception");
            e.printStackTrace();
        }

    }

    // ASDK-356 Android-SDK4 sendUserActivity
    public static void onPause() {
        try {
            LootsieEngine.getInstance().onPause();
        }catch (Exception e) {
            Logs.e(TAG, "onPause: Some Unexpected exception");
            e.printStackTrace();
        }
    }

    // ASDK-356 Android-SDK4 sendUserActivity
    public static void onResume() {
        try {
            LootsieEngine.getInstance().onResume();
        }catch (Exception e) {
            Logs.e(TAG, "setLogCallback: Some Unexpected exception");
            e.printStackTrace();
        }

    }

    /**
     * for getting logging data from engine to display in a texview in real time
     * @param callback
     */
    public static void setLogCallback(ILogData callback) {
        try {
            LootsieEngine.setLogCallback(callback);
        }catch (Exception e) {
            Logs.e(TAG, "setLogCallback: Some Unexpected exception");
            e.printStackTrace();
        }
    }

    /**
     * needed for updating the current app with push notification messages
     * when app activity changes on rotation, etc
     * @param inputActivity
     */
    public static void setActivityContext(Activity inputActivity) {
        try {
            LootsieEngine.getInstance().setActivityContext(inputActivity);
        }catch (Exception e) {
            Logs.e(TAG, "setActivityContext: Some Unexpected exception");
            e.printStackTrace();
        }
    }

    public static ArrayList<Reward> getUserRewards() {
    	try {
    		return LootsieEngine.getUserRewards();
        } catch (Exception e) {
            Logs.e(TAG, "getUserRewards: Some Unexpected exception");
            e.printStackTrace();
        }

        return null;
    }
    
    
    /**
     * getAppAchievements
     * get the list of achievements available in the app
     */
    public static ArrayList<Achievement> getAppAchievements() {
        try {
            return LootsieEngine.getAppAchievements();
        } catch (Exception e) {
            Logs.e(TAG, "getAppAchievements: Some Unexpected exception");
            e.printStackTrace();
        }

        return null;
    }

    /**
     * This method sets the requested Rendering mode to display Lootsie
     * Rewards window. Use this method to lock down portrait or landscape only
     *
     * @param requestedState - state to lock the screen
     * 			 Lootsie.RENDER_PORTRAIT - To render Portrait locked screen
     * 			 Lootsie.RENDER_LANDSCAPE - To render Landscape locked screen
     * 			 Lootsie.RENDER_DEFAULT - To render with default automated screen
     */
    // ASDK-354 Android-SDK4 LEG says use portrait or landscape - setRenderingMode - optional portrait, landscape or automatic mode
    // ASDK-363 Android-SDK4 lock orientation of android activity view based on rendering mode
    public static void setRenderingMode(int requestedState)
    {
        try {
            LootsieEngine.setRenderingMode(requestedState);
        }catch (Exception e) {
            Logs.e(TAG, "setRenderingMode: Some Unexpected exception");
            e.printStackTrace();
        }

    }

    /**
     * This will display Lootsie dialog window on top of the existing activity
     * @param achievementId as set on Developer portal
     * @param windowPosition where to appear on the screen
     * 			Lootsie.DEFAULT_POSITION - appears in default position
     *		    Lootsie.TOP_LEFT_POSITION - appears from top left
     *		    Lootsie.TOP_RIGHT_POSITION - appears from top right
     *		    Lootsie.BOTTOM_LEFT_POSITION - appears from bottom left
     *		    Lootsie.BOTTOM_RIGHT_POSITION - appears from bottom right
     *		    Lootsie.CENTER_LEFT_POSITION - appears from centre left
     *		    Lootsie.CENTER_RIGHT_POSITION - appear from center Right
     *
     * @param callback object or null
     */
    public static void AchievementReached(Activity a, String achievementId, String windowPosition, IAchievementReached callback) {
        try {
            LootsieEngine.AchievementReached(a, achievementId, windowPosition, callback);
        }catch (Exception e) {
            Logs.e(TAG, "AchievementReached: Some Unexpected exception");
            e.printStackTrace();
        }
    }

    public static void redeemReward(String emailStr, String rewardIdStr)  {
        try {
            LootsieEngine.redeemReward(emailStr, rewardIdStr);
        }catch (Exception e) {
            Logs.e(TAG, "redeemReward: Some Unexpected exception");
            e.printStackTrace();
        }
    }
    
    public static void redeemReward(String emailStr, String rewardIdStr, IRedeemReward callback)  {
        try {
            LootsieEngine.redeemReward(emailStr, rewardIdStr, callback);
        }catch (Exception e) {
            Logs.e(TAG, "redeemReward: Some Unexpected exception");
            e.printStackTrace();
        }
    }

    public static void redeemRewardWoEmail(String rewardIdStr, IRedeemReward ... optionalCallback)  {
        try {
            LootsieEngine.redeemRewardWoEmail(rewardIdStr, optionalCallback.length == 0? null: optionalCallback[0]);
        }catch (Exception e) {
            Logs.e(TAG, "redeemReward: Some Unexpected exception");
            e.printStackTrace();
        }
    }

    /**
     * This will display Lootsie full screen window on top of the existing activity
     */
    public static void showRewardsPage(Activity a) {
        try {
            LootsieEngine.showRewardsPage(a);
        }catch (Exception e) {
            Logs.e(TAG, "showRewardsPage: Some Unexpected exception");
            e.printStackTrace();
        }
    }

    /**
     * This will display Lootsie full screen window on top of the existing activity
     */
    public static void showAchievementsPage(Activity a) {
        try {
            LootsieEngine.showAchievementsPage(a);
        }catch (Exception e) {
            Logs.e(TAG, "showAchievementsPage: Some Unexpected exception");
            e.printStackTrace();
        }
    }

    /**
     * log out the current user
     * @return
     */
    public static boolean signOut() {
        try {
            return LootsieEngine.signOut();
        }catch (Exception e) {
            Logs.e(TAG, "signOut: Some Unexpected exception");
            e.printStackTrace();
        }

        return false;
    }

    /**
     * log out the current user
     * @return
     */
    public static boolean signOut(ISignOutCallback signoutCallback) {
        try {
            return LootsieEngine.signOut(signoutCallback);
        }catch (Exception e) {
            Logs.e(TAG, "signOut: Some Unexpected exception");
            e.printStackTrace();
        }

        return false;
    }    
    
    /**
     * getLogBuffer returns a copy of the debugging buffer of Lootsie sdk to show in a text view
     * @return
     */
    public static String getLogBuffer() {
        try {
            return LootsieEngine.getInstance().getLogBuffer();
        }catch (Exception e) {
            Logs.e(TAG, "getLogBuffer: Some Unexpected exception");
            e.printStackTrace();
        }
        return "";
    }

    public static void setLocation(float latitude, float longitude) {
        try {
            LootsieEngine.setLocation(latitude, longitude);
        }catch (Exception e) {
            Logs.e(TAG, "getLogBuffer: Some Unexpected exception");
            e.printStackTrace();
        }
    }

    public static void setCacheDurationInSeconds(int cacheDurationSec) {
        try {
            LootsieEngine.setCacheDurationInSeconds(cacheDurationSec);
        }catch (Exception e) {
            Logs.e(TAG, "getLogBuffer: Some Unexpected exception");
            e.printStackTrace();
        }
    }

    public static void setNotificationConfiguration(LOOTSIE_NOTIFICATION_CONFIGURATION inputNotificationConfig)  {
        try {
            LootsieEngine.setNotificationConfiguration(inputNotificationConfig);
        }catch (Exception e) {
            Logs.e(TAG, "setNotificationConfiguration: Some Unexpected exception");
            e.printStackTrace();
        }        
    }    
    
    public static void registerUser(String emailStr) {
        try {
            LootsieEngine.registerUserEmail(emailStr);
        }catch (Exception e) {
            Logs.e(TAG, "registerUser: Some Unexpected exception");
            e.printStackTrace();
        }      	    	
    }
    
}
