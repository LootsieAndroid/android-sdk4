package com.lootsie.sdk.adnets.model;

import android.content.Context;

/**
 * Created by Alexander S. Sidorov on 2/25/16.
 */
public interface AdNetProxy
{
    InterstitialAd createInterstitialAd(Context context, String adUnitId, InterstitialAdListener adListener);
}
