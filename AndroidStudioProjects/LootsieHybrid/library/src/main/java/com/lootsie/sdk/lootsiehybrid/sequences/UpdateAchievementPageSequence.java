package com.lootsie.sdk.lootsiehybrid.sequences;

import android.content.Intent;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.callbacks.IUpdateAchievementsCallback;
import com.lootsie.sdk.lootsiehybrid.LOOTSIE_NOTIFICATION_CONFIGURATION;
import com.lootsie.sdk.lootsiehybrid.LootsieApi;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.model.User;
import com.lootsie.sdk.netutil.RESTDeleteUserSessionTask;
import com.lootsie.sdk.netutil.RESTGetUserAchievementsTask;
import com.lootsie.sdk.netutil.RestResult;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.viewcontrollers.LootsiePageBorderActivity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by jerrylootsie on 2/24/15.
 */
public class UpdateAchievementPageSequence {

    private static String TAG = "Lootsie UpdateAchievementPageSequence";

    private static IUpdateAchievementsCallback callback = null;

    //public static RESTGetUserAchievementsTask restGetUserAchievementsTask = null;
    public static IGenericAsyncTask<String> restGetUserAchievementsTask = null;



    /**
     * we initialize the resttasks here, and check because they may already have been initialized as mock objects earlier for testing
     */
    private static void init() {

        if (restGetUserAchievementsTask == null) {
            restGetUserAchievementsTask = new RESTGetUserAchievementsTask();
        }
    }

    private static void cleanup() {
        restGetUserAchievementsTask = null;
    }

    public static void start(IUpdateAchievementsCallback inputCB) {

    	callback = inputCB;

        init();
        GetUserAchievements();

    }

    public static void GetUserAchievements() {

        DebugLog("UpdateAchievementPageSequence: GetUserAchievements");

        IRESTCallback restCallback = new IRESTCallback() {
            @Override
            public void restResult(RestResult result) {
                UpdateAchievementPageSequence.GetUserAchievementsResult(result);
            }
        };

        // TODO: we should really force the network stats to update this entry rather than query if we should
        // TODO: we should also mark the achievments list as "modified" and needing an update when user reaches an achievement
        DataModel.updateNetworkForced(LootsieApi.USER_ACHIEVEMENTS_URL.toString());
        restGetUserAchievementsTask.setCallback(restCallback);
        restGetUserAchievementsTask.executeTask("test");

    }

    public static void GetUserAchievementsResult(RestResult RestResult) {
        if (RestResult.status == 200) {
            try {
                JSONObject json = new JSONObject(RestResult.content);

                // store this stuff in the data model
//                userAchievements = new UserAchievements();
//                userAchievements.parseFromJSON(json);
                if (DataModel.user == null) {
                    DataModel.user = new User();
                }
                DataModel.user.parseFromJSONUserAchievementsInfo(json);
                if (DataModel.user.achievements != null) {
                	DebugLog("User achievements: %d", DataModel.user.achievements.size());
                } else {
                	DebugLog("User achievements: is null!");
                }
                DebugLog("User achievedLp: %d", DataModel.user.achievedLp);
                DebugLog("User totalLp: %d", DataModel.user.totalLp);

                // TODO: show page
                if (callback != null) callback.onUpdateAchievementsSuccess();

            } catch (JSONException ex) {
                Logs.e(TAG, "GetUserAchievementsResult: exception: " + ex.getMessage());
                if (callback != null) callback.onUpdateAchievementsFailure();
            }
        } else {
        	if (callback != null) callback.onUpdateAchievementsFailure();
        }

        cleanup();
    }




    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }
}
