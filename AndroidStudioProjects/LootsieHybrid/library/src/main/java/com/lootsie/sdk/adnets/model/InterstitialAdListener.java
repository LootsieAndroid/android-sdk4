package com.lootsie.sdk.adnets.model;

/**
 * Created by Alexander S. Sidorov on 24.02.2016.
 */
public interface InterstitialAdListener
{
    void onInterstitialAdLoaded();

    void onInterstitialAdFailedToLoad(AdLoadErrorCode adLoadErrorCode);

    void onInterstitialAdClosed();

    void onInterstitialAdLeftApplication();

    void onInterstitialAdOpened();
}
