package com.lootsie.sdk.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.lootsie.sdk.utils.Logs;

import org.json.JSONObject;
import org.json.JSONException;

/**
 * Engagement
 *
 * @author Alexander Salnikov
 */
public class Engagement implements Parcelable {
    public String id;
    public String name;
    public int lp;

    private String TAG = "Lootsie Engagement";

    public Engagement() {
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(id);
        out.writeString(name);
        out.writeInt(lp);
    }

    public static final Parcelable.Creator<Engagement> CREATOR
            = new Parcelable.Creator<Engagement>() {
        public Engagement createFromParcel(Parcel in) {
            return new Engagement(in);
        }

        public Engagement[] newArray(int size) {
            return new Engagement[size];
        }
    };

    private Engagement(Parcel in) {
        id = in.readString();
        name = in.readString();
        lp = in.readInt();
    }

    /*
        {
            "id": "12",
            "name": "Facebook Share",
            "lp": 5
        },
     */
    public void parseFromJSON(JSONObject oo) throws JSONException {

        try {
            this.id = oo.getString("id");
            this.name = oo.getString("name");
            this.lp = oo.getInt("lp");
        } catch (JSONException ex) {
            Logs.e(TAG, "Engagement: parseFromJSON exception: " + ex.getMessage());
            throw ex;
        }

    }

    public JSONObject toJSON() throws JSONException {

        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("id", id);
            jsonObj.put("name", name);
            jsonObj.put("lp", lp);

        } catch (JSONException ex) {
            Logs.e(TAG, "Engagement: toJSON exception: " + ex.getMessage());
            throw ex;
        }

        return jsonObj;

    }

}
