package com.lootsie.sdk.viewcontrollers;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.model.RewardListEntryModel;
import com.lootsie.sdk.model.RewardViewHolder;
import com.lootsie.sdk.model.User;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.utils.RUtil;
import com.lootsie.sdk.viewcontrollers.base.UpdatableFragment;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jerrylootsie on 5/12/15.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class AccountSettingsFragment extends UpdatableFragment {

    private static String TAG = "Lootsie TermsFragment";

    private Context context;

    private EditText accountSettingsEditTextEmail = null;

    private EditText accountSettingsEditTextFirstName = null;
    private EditText accountSettingsEditTextLastName = null;
    private EditText accountSettingsEditTextEmail2 = null;
    private EditText accountSettingsEditTextCity = null;
    private EditText accountSettingsEditTextZipcode = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (LootsieGlobals.debugLevel > 0) Logs.d(TAG, "onCreateView");

        //View view = inflater.inflate(R.layout.com_lootsie_terms_fragment, container, false);
        View view = inflater.inflate(RUtil.getLayoutId(this.getActivity().getApplicationContext(), "com_lootsie_accountsettings_fragment"), container, false);

        view.setTag("accountSettingsFragment");

        context = this.getActivity().getApplicationContext();

        Button mergePointsButton = (Button) view.findViewById(RUtil.getResourceId(context, "accountSettingsMergePointsButton", "id", context.getPackageName()));
        if (mergePointsButton != null) {
            mergePointsButton.setOnClickListener(new MergePointsButtonListener());
        }

        TextView accountSettingsPointsTextView = (TextView) view.findViewById(RUtil.getResourceId(context, "accountSettingsPointsTextView", "id", context.getPackageName()));
        if (accountSettingsPointsTextView != null) {
            Resources res = this.getResources();
            // Lootsie Points earned in this app:
            String lootsie_account_settings_mypoints_description = res.getString(RUtil.getStringId(this.getActivity().getApplicationContext(), "lootsie_account_settings_mypoints_description"));


            accountSettingsPointsTextView.setText(lootsie_account_settings_mypoints_description + " " + String.valueOf(DataModel.user.totalLp) + " " + DataModel.currency_abbreviation);
        }

        accountSettingsEditTextEmail = (EditText) view.findViewById(RUtil.getResourceId(context, "accountSettingsEditTextEmail", "id", context.getPackageName()));



        Button updateInfoButton = (Button) view.findViewById(RUtil.getResourceId(context, "accountSettingsUpdateInfoButton", "id", context.getPackageName()));
        updateInfoButton.setOnClickListener(new UpdateInfoButtonListener());

        accountSettingsEditTextFirstName = (EditText) view.findViewById(RUtil.getResourceId(context, "accountSettingsEditTextFirstName", "id", context.getPackageName()));
        accountSettingsEditTextLastName = (EditText) view.findViewById(RUtil.getResourceId(context, "accountSettingsEditTextLastName", "id", context.getPackageName()));
        accountSettingsEditTextEmail2 = (EditText) view.findViewById(RUtil.getResourceId(context, "accountSettingsEditTextEmail2", "id", context.getPackageName()));
        accountSettingsEditTextCity = (EditText) view.findViewById(RUtil.getResourceId(context, "accountSettingsEditTextCity", "id", context.getPackageName()));
        accountSettingsEditTextZipcode = (EditText) view.findViewById(RUtil.getResourceId(context, "accountSettingsEditTextZipcode", "id", context.getPackageName()));


        return view;
    }

    class MergePointsButtonListener implements View.OnClickListener {
        @Override
        public void onClick(View arg0) {
            String emailStr = accountSettingsEditTextEmail.getText().toString();
            DebugLog("onClickMergePoints: email: " + emailStr);

            LootsieEngine.getInstance().mergePoints(emailStr);

        }
    }

    static Pattern emailPattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

    private static boolean isEmailValid(String email) {
        Matcher m = emailPattern.matcher(email);
        return m.matches();
    }

    static Pattern zipcodePattern = Pattern.compile("[0-9-]+");

    private static boolean isZipcodeValid(String zipcodeStr) {
        Matcher m = zipcodePattern.matcher(zipcodeStr);
        return m.matches();
    }

    class UpdateInfoButtonListener implements View.OnClickListener {

        @Override
        public void onClick(View arg0) {
        	try {
	            DebugLog("onClickUpdateInfo");
	
	            DebugLog("onClickMergePoints: firstName: " + accountSettingsEditTextFirstName.getText().toString());
	            DebugLog("onClickMergePoints: lastName: " + accountSettingsEditTextLastName.getText().toString());
	            DebugLog("onClickMergePoints: email: " + accountSettingsEditTextEmail2.getText().toString());
	            DebugLog("onClickMergePoints: city: " + accountSettingsEditTextCity.getText().toString());
	            DebugLog("onClickMergePoints: zipcode: " + accountSettingsEditTextZipcode.getText().toString());
	
	            User user = new User();
	            user.firstName = accountSettingsEditTextFirstName.getText().toString();
	            user.lastName = accountSettingsEditTextLastName.getText().toString();


	            user.email = accountSettingsEditTextEmail2.getText().toString();
                // validate email or return UPDATE_USER_ACCOUNT_SEQUENCE_FAILURE_INVALID_EMAIL
                if (!isEmailValid(user.email)) {
                    // tell the user about it: UPDATE_USER_ACCOUNT_SEQUENCE_FAILURE_INVALID_EMAIL
                    LootsieEngine.getInstance().lootsieMessageDialog("Uh oh!", "Invalid email address.");
                    return;
                }

	            user.city = accountSettingsEditTextCity.getText().toString();


                String zipcodeStr = (String) accountSettingsEditTextZipcode.getText().toString();
                if ((zipcodeStr != null) && (zipcodeStr.length() > 0)) {
                    // validate zipcode or return error UPDATE_USER_ACCOUNT_SEQUENCE_FAILURE_INVALID_ZIPCODE
                    if (!isZipcodeValid(zipcodeStr)) {
                        LootsieEngine.getInstance().lootsieMessageDialog("Uh oh!", "Invalid zip code.");
                        return;
                    }

                    user.zipcode = Integer.parseInt(zipcodeStr);
                } else {
                    user.zipcode = 0;
                }
	
	            // copy from existing user value
	            user.isGuest = DataModel.user.isGuest;

                LootsieEngine.getInstance().updateUserAccount(user);

        	} catch (Exception ex) {
        		Logs.e(TAG, "AccountSettingsFragment: UpdateInfoButtonListener: error: " + ex.getMessage());

                // don't know what happened, just tell the user about it: UPDATE_USER_ACCOUNT_SEQUENCE_FAILURE
                LootsieEngine.getInstance().lootsieMessageDialog("Uh oh!", "Account cannot be updated at this time.");
        	}
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        DebugLog("onResume");

        updateEntries();
    }

    private void updateEntries() {

        if (accountSettingsEditTextEmail != null) {
            accountSettingsEditTextEmail.setText(DataModel.user.email);
        }

        if (accountSettingsEditTextFirstName != null) accountSettingsEditTextFirstName.setText(DataModel.user.firstName);
        if (accountSettingsEditTextLastName != null) accountSettingsEditTextLastName.setText(DataModel.user.lastName);
        if (accountSettingsEditTextEmail2 != null) accountSettingsEditTextEmail2.setText(DataModel.user.email);
        if (accountSettingsEditTextCity != null) accountSettingsEditTextCity.setText(DataModel.user.city);
        if ((accountSettingsEditTextZipcode != null) && (DataModel.user.zipcode != 0)) {
            accountSettingsEditTextZipcode.setText(String.valueOf(DataModel.user.zipcode));
        }

    }

    @Override
    public void updateFragment() {
        DebugLog("updateFragment");

        updateEntries();
    }


    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }
}