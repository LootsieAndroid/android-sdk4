package com.lootsie.sdk.utils.image;

/**
 * Created by jerrylootsie on 7/27/15.
 */

/*
* Cache-related fields and methods.
*
* We use a hard and a soft cache. A soft reference cache is too aggressively cleared by the
* Garbage Collector.
*/

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.utils.LruCache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;




class BitmapLruCache extends LruCache<String, File> {
    private static final String TAG = "BitmapLruCache";
    private Context context = null;


    /**
     * Check if external storage is built-in or removable.
     *
     * @return True if external storage is removable (like an SD card), false
     *         otherwise.
     * LOOTSIE-1257
     */
    @TargetApi(9)
    public boolean isExternalStorageRemovable() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.GINGERBREAD) {
            return Environment.isExternalStorageRemovable();
        }
        return true;
    }

    /**
     * Get the external app cache directory.
     *
     * @param context The context to use
     * @return The external cache dir
     * LOOTSIE-1257
     */
    @TargetApi(8)
    public File getExternalCacheDir(Context context) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.FROYO) {
            return context.getExternalCacheDir();
        }

        // Before Froyo we need to construct the external cache dir ourselves
        final String cacheDir = "/Android/data/" + context.getPackageName() + "/cache/";
        return new File(Environment.getExternalStorageDirectory().getPath() + cacheDir);
    }

    public BitmapLruCache(int maxSize) {
        super(maxSize);
    }

    public synchronized void putBitmap(String url, Bitmap bitmap) {
        try {
            if (LootsieGlobals.debugLevel > 0) Log.w(TAG, "AsyncImageDownloader: BitmapLruImageCache: putBitmap: url: " + url +
                    " bitmap: " + bitmap.getWidth() + "x" + bitmap.getHeight());

            String fileName = url.replaceAll("[^a-z0-9\\-]", "-");
            File f;

            //File dir = context.getExternalCacheDir();

            // Check if media is mounted or storage is built-in, if so, try and use external cache dir
            // otherwise use internal cache dir.
            // LOOTSIE-1257
            final String dir =
                    Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) ||
                            !isExternalStorageRemovable() ? getExternalCacheDir(context).getPath() :
                            context.getCacheDir().getPath();

            if (dir != null) {
                f = new File(dir, fileName);
            } else {
                f = new File(fileName);
            }
            FileOutputStream os = new FileOutputStream(f);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, os);

            put(url, f);
        } catch (FileNotFoundException e) {
            Log.e(TAG, "Could not store cache.");
            e.printStackTrace();
        } catch (IOException e) {
            Log.e(TAG, "Could not store cache.");
            e.printStackTrace();
        }
    }

    public String getKeyForUrl(String url) {
        return url.replaceAll("[^a-z0-9\\-]", "-");
    }

    public synchronized Bitmap getBitmap(String url) {
        FileInputStream is;
        try {
            if (LootsieGlobals.debugLevel > 0) Log.w(TAG, "AsyncImageDownloader: BitmapLruImageCache: getBitmap: url: " + url);

            File f = get(url/*getKeyForUrl(url)*/);
            if (f != null) {
                is = new FileInputStream(f);
                return BitmapFactory.decodeStream(is);
            }
        } catch (FileNotFoundException ex) {
            Log.i(TAG, "File have been deleted.");
        }
        return null;
    }

    @Override
    protected int sizeOf(String key, File value) {
        return (int) value.length();
    }

    public void init(Context inputContext) {
        //File dir = context.getExternalCacheDir();

        context = inputContext;
        // Check if media is mounted or storage is built-in, if so, try and use external cache dir
        // otherwise use internal cache dir
        // LOOTSIE-1257
        File dir =
                Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) ||
                        !isExternalStorageRemovable() ? getExternalCacheDir(context) :
                        context.getCacheDir();

        if (dir != null && dir.listFiles() != null) {
            for (File f : dir.listFiles()) {
                if (LootsieGlobals.debugLevel > 0) Log.w(TAG, "BitmapLruImageCache: put: " + f.getName());
                put(f.getName(), f);
            }
        }
    }
}
