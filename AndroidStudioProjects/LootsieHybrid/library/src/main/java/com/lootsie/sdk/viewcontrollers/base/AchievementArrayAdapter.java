package com.lootsie.sdk.viewcontrollers.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lootsie.sdk.R;
import com.lootsie.sdk.model.AchievementListEntryModel;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.utils.RUtil;

import org.w3c.dom.Text;

import java.util.List;




/**
 * Created by jerrylootsie on 2/23/15.
 */

public class AchievementArrayAdapter extends ArrayAdapter<AchievementListEntryModel> {

    static final String TAG = "Lootsie AchievementArrayAdapter";

    private final Context context;
    private List<AchievementListEntryModel> values;
    //private int color;

    public AchievementArrayAdapter(Context context, List<AchievementListEntryModel> values) {
        //super(context, R.layout.com_lootsie_achievement_row, values);
    	super(context, RUtil.getLayoutId(context, "com_lootsie_achievement_row"), values);
        this.context = context;
        this.values = values;
        //color = context.getResources().getColor(android.R.color.holo_blue_bright);
    }

    @Override
    public int getViewTypeCount() {
        return values.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public static class ViewHolder {
        public TextView t;
        public ImageView i;
        public int position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = null;

        if (convertView != null) {
            rowView = convertView;
        } else {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //rowView = inflater.inflate(R.layout.com_lootsie_achievement_row, parent, false);
            rowView = inflater.inflate(RUtil.getLayoutId(context, "com_lootsie_achievement_row"), parent, false);

            AchievementListEntryModel currentAchievement = values.get(position);

            // save for easy reference through tag later
            ViewHolder holder = new ViewHolder();
            holder.position = position;


            if (currentAchievement.achievementEntryType == AchievementListEntryModel.AchievementEntryType.DEFAULT_ACHIEVEMENT_ENTRY) {
                //TextView achievementNameTextView = (TextView) rowView.findViewById(R.id.achievementName);
                TextView achievementNameTextView = (TextView) rowView.findViewById(RUtil.getResourceId(context, "achievementName", "id", context.getPackageName()));
                achievementNameTextView.setText(currentAchievement.getName().toUpperCase());

                //TextView achievementDescriptionTextView = (TextView) rowView.findViewById(R.id.achievementDescription);
                TextView achievementDescriptionTextView = (TextView) rowView.findViewById(RUtil.getResourceId(context, "achievementDescription", "id", context.getPackageName()));
                achievementDescriptionTextView.setText(currentAchievement.getDescription());

                //TextView achievementPointsTextView = (TextView) rowView.findViewById(R.id.achievementPoints);
                TextView achievementPointsTextView = (TextView) rowView.findViewById(RUtil.getResourceId(context, "achievementPoints", "id", context.getPackageName()));
                achievementPointsTextView.setText(String.valueOf(currentAchievement.getAchievement().lp) + " " + DataModel.currency_abbreviation);

                //ImageView achievementIndicator = (ImageView) rowView.findViewById(R.id.achievementIcon);
                ImageView achievementIndicator = (ImageView) rowView.findViewById(RUtil.getResourceId(context, "achievementIcon", "id", context.getPackageName()));

                //            achievementIndicator.setImageDrawable(this.getContext().getResources().getDrawable(R.drawable.trophy));
                //achievementIndicator.setImageResource(R.drawable.trophy);
                //achievementIndicator.setImageResource(RUtil.getResourceId(context, "trophy", "id", context.getPackageName()));
                achievementIndicator.setImageResource(RUtil.getDrawableId(context, "trophy_grey"));

                // save for easy reference through tag later
                holder.t = achievementNameTextView;
                holder.i = achievementIndicator;

                TextView titleOneTime = (TextView) rowView.findViewById(RUtil.getResourceId(context, "achivementRowOneTimeAchievementsTitle", "id", context.getPackageName()));
                titleOneTime.setVisibility(View.GONE);

                TextView titleRepeatable = (TextView) rowView.findViewById(RUtil.getResourceId(context, "achivementRowRepeatableAchievementsTitle", "id", context.getPackageName()));
                titleRepeatable.setVisibility(View.GONE);

            } else if (currentAchievement.achievementEntryType == AchievementListEntryModel.AchievementEntryType.ONE_TIME_ACHIEVEMENTS_TITLE) {
                TextView titleOneTime = (TextView) rowView.findViewById(RUtil.getResourceId(context, "achivementRowOneTimeAchievementsTitle", "id", context.getPackageName()));
                titleOneTime.setVisibility(View.VISIBLE);

                ImageView achievementTopDivider = (ImageView) rowView.findViewById(RUtil.getResourceId(context, "achievementTopDivider", "id", context.getPackageName()));
                achievementTopDivider.setVisibility(View.VISIBLE);

                ImageView achievementDivider = (ImageView) rowView.findViewById(RUtil.getResourceId(context, "achievementDivider", "id", context.getPackageName()));
                achievementDivider.setVisibility(View.VISIBLE);

                TextView titleRepeatable = (TextView) rowView.findViewById(RUtil.getResourceId(context, "achivementRowRepeatableAchievementsTitle", "id", context.getPackageName()));
                titleRepeatable.setVisibility(View.GONE);

                RelativeLayout rowDetails = (RelativeLayout) rowView.findViewById(RUtil.getResourceId(context, "achievmentRowDetails", "id", context.getPackageName()));
                rowDetails.setVisibility(View.GONE);

            } else if (currentAchievement.achievementEntryType == AchievementListEntryModel.AchievementEntryType.REPEATABLE_ACHIEVMENTS_TITLE) {
                TextView titleRepeatable = (TextView) rowView.findViewById(RUtil.getResourceId(context, "achivementRowRepeatableAchievementsTitle", "id", context.getPackageName()));
                titleRepeatable.setVisibility(View.VISIBLE);

                ImageView achievementTopDivider = (ImageView) rowView.findViewById(RUtil.getResourceId(context, "achievementTopDivider", "id", context.getPackageName()));
                achievementTopDivider.setVisibility(View.VISIBLE);

                ImageView achievementDivider = (ImageView) rowView.findViewById(RUtil.getResourceId(context, "achievementDivider", "id", context.getPackageName()));
                achievementDivider.setVisibility(View.VISIBLE);

                TextView titleOneTime = (TextView) rowView.findViewById(RUtil.getResourceId(context, "achivementRowOneTimeAchievementsTitle", "id", context.getPackageName()));
                titleOneTime.setVisibility(View.GONE);

                RelativeLayout rowDetails = (RelativeLayout) rowView.findViewById(RUtil.getResourceId(context, "achievmentRowDetails", "id", context.getPackageName()));
                rowDetails.setVisibility(View.GONE);

            }


            rowView.setTag(holder);
        }

        ViewHolder tag = (ViewHolder) rowView.getTag();

        if (values.get(position).achievementEntryType == AchievementListEntryModel.AchievementEntryType.DEFAULT_ACHIEVEMENT_ENTRY) {
            // Change the icon for Windows and iPhone
            if (values.get(position).isAchievementReached()) {
                //tag.i.setImageResource(R.drawable.trophy_completed);
                //int tropyId = RUtil.getResourceId(context, "trophy_completed", "id", context.getPackageName());
                //tag.i.setImageResource(tropyId);
                tag.i.setImageResource(RUtil.getDrawableId(context, "trophy_gold"));
            } else {
                //tag.i.setImageResource(R.drawable.trophy);
                //int tropyId = RUtil.getResourceId(context, "trophy", "id", context.getPackageName());
                //tag.i.setImageResource(tropyId);
                tag.i.setImageResource(RUtil.getDrawableId(context, "trophy_grey"));

            }
        }

        //rowView.setBackgroundColor(color);
        return rowView;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        DebugLog("notifyDataSetChanged");

    }

    public void updateModel(List<AchievementListEntryModel> values) {
        DebugLog("updateModel");
        this.values = values;
    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }

}