package com.lootsie.sdk.utils;

import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.lootsie.sdk.R;

public class AlertDialogHelper {
    public static void showPopup(Activity context, String title, String message) {
        View popupView = context.getLayoutInflater().inflate(R.layout.com_lootsie_popup_reward_video_message, null);
        final PopupWindow popupWindow = new PopupWindow(popupView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup
                .LayoutParams.MATCH_PARENT);
        popupWindow.setAnimationStyle(android.R.style.Animation_Dialog);

        ((TextView) popupView.findViewById(R.id.com_lootsie_popup_reward_message_title)).setText(title);
        ((TextView) popupView.findViewById(R.id.com_lootsie_popup_reward_message_text)).setText(message);
        popupView.findViewById(R.id.com_lootsie_popup_reward_message_button_ok).setOnClickListener(new View
                .OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

        popupWindow.showAtLocation(context.findViewById(android.R.id.content), Gravity.CENTER, 0, 0);
    }
}
