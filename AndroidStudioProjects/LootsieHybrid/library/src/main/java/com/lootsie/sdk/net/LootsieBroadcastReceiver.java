package com.lootsie.sdk.net;

import android.app.Activity;
import android.content.Intent;
import android.content.Context;
import android.os.Bundle;

import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

import com.lootsie.sdk.viewcontrollers.NotificationManager;
//import com.parse.ParsePushBroadcastReceiver;



/**
 * Created by jerrylootsie on 2/25/15.
 */
public class LootsieBroadcastReceiver {
//public class LootsieBroadcastReceiver extends ParsePushBroadcastReceiver {

    static final String TAG = "Lootsie LootsieBroadcastReceiver";

    private static NotificationManager notificationManager = null;

    /*
        To achieve further customization, ParsePushBroadcastReceiver can be subclassed.
        When providing your own implementation of ParsePushBroadcastReceiver,
        be sure to change com.parse.PushBroadcastReceiver to the name of your custom subclass in your AndroidManifest.xml.

        You can intercept and override the behavior of entire portions of the push lifecycle by overriding
        ParsePushBroadcastReceiver.onPushReceive(Context, Intent),
        ParsePushBroadcastReceiver.onPushOpen(Context, Intent), or
        ParsePushBroadcastReceiver.onPushDismiss(Context, Intent).

        To make minor changes to the appearance of a notification, override
        ParsePushBroadcastReceiver.getSmallIconId(Context, Intent) or
        ParsePushBroadcastReceiver.getLargeIcon(Context, Intent).

        To completely change the Notification generated, override
        ParsePushBroadcastReceiver.getNotification(Context, Intent).

        To change the Activity launched when a user opens a Notification, override
        ParsePushBroadcastReceiver.getActivity(Context, Intent).
     */

//    @Override
    public void onReceive (Context context, Intent intent) {
    	DebugLog("onRecieve");

        // context.getApplicationContext()

        Context lootsieAppContext = LootsieEngine.getInstance().getApplicationContext();
        if (lootsieAppContext == null) {
        	DebugLog("WARNING: lootsie application context is null!");
        }

        Activity lootsieActivityContext = (Activity) LootsieEngine.getInstance().getActivityContext();
        if (lootsieActivityContext == null) {
        	DebugLog("WARNING: lootsie activity context is null!");
        } else {

            // todo: start In-App-Notification banner movement
            if (notificationManager == null) {
                notificationManager = new NotificationManager(lootsieActivityContext);
            } else {
                // update activity context - if user rotated the device and started a new activity
                notificationManager.updateContext(lootsieActivityContext);
            }

            Bundle extras = intent.getExtras();
            if (extras != null) {

                Set<String> keys = extras.keySet();
                for (String key : keys) {
                	DebugLog("Key is:" + key);
                }

                // TODO: should we use com.parse.channel somehow to filter messages?

                if(extras.containsKey("com.parse.Data")){
                    String jsonData = extras.getString( "com.parse.Data" );

                    String title = "";
                    String message = "";

                    try {
                        JSONObject jsonObj = new JSONObject(jsonData);
                        if (jsonObj.has("title")) {
                            title = jsonObj.getString("title");
                        }
                        if (jsonObj.has("message")) {
                            message = jsonObj.getString("message");
                        }

                        notificationManager.showNotification(title,message);

                    } catch (JSONException ex) {
                        Logs.e(TAG, "onRecieve: exception: " + ex.getMessage());
                    }

                }
            } else {
                Logs.e(TAG, "onReceive: extras missing!");
            }

        }
    }

//    @Override
//    public void onPushOpen(Context context, Intent intent) {
//        DebugLog("onPushOpen");
//
//    }
//
//    @Override
//    public void onPushDismiss(Context context, Intent intent) {
//        DebugLog("onPushOpen");
//
//    }
    
    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    } 
}
