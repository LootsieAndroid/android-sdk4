package com.lootsie.sdk.model;

import android.text.TextUtils;

import com.lootsie.sdk.utils.Logs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.LinkedList;

/**
 * Created by jerrylootsie on 2/11/15.
 */
/*
{
    "is_guest": false,
    "email": "test@example.com",
    "total_lp": 100
    "photo_url": "http://lootsie.s3.amazonaws.com/stage1/profile-pic-with-image-2x.png",

    "first_name": "John",
    "last_name": "Johnson",
    "birthdate": "1980-12-31", // or null
    "gender": "M",

    "address": "630 W. 5th Street",
    "city": "LA",
    "state": "CA",
    "zipcode": 22313, // or null

    “accepted_tos”: true,
    “lootsie_optin”: false,
    “partner_optin”: false,
    “confirmed_age”: 34,
    “home_zipcode”: 22313,

    "interest_groups": [
        {
            "name": "Favorite Movies",
            "liked": true,
            "interests": [
                {
                    "name": "Comedy",
                    "liked": false
                    "id": "123",
                },
                // ...
                {
                    "name": "Thriller",
                    "liked": true
                    "id": "124",
                }
            ]
        }
        // ...
    ]
}
 */
public class User {

    private static String TAG = "Lootsie User";

    public static final String FIRST_NAME = "FIRST_NAME";
    public static final String LAST_NAME = "LAST_NAME";
    public static final String EMAIL = "EMAIL";
    public static final String ACHIEVED_LP = "ACHIEVED_LP";
    public static final String TOTAL_LP = "TOTAL_LP";
    public static final String OLD_LP = "OLD_LP";
    public static final String ACHIEVEMENTS = "ACHIEVEMENTS";
    public static final String RESEND_ACHIEVEMENTS = "RESEND_ACHIEVEMENTS";
    public static final String PHOTO_URL = "PHOTO_URL";
    public static final String IS_GUEST = "IS_GUEST";

    public String firstName;
    public String lastName;
    public String email;
    public int achievedLp;
    public int totalLp = 0;
    public int oldLp = 0;
    public boolean isGuest = true; // all users start out as guests!
    //public boolean isGuest = false;
    public String city;
    public int zipcode;
    public String userimg;
    public List<Achievement> achievements;

    public String getDisplayName() {
        if (TextUtils.isEmpty(firstName) && TextUtils.isEmpty(lastName) &&!TextUtils.isEmpty(email)) {
            String name = email;
            int index = name.indexOf("@");
            if (index > 0) {
                name = name.substring(0, index);
                firstName = name;
            }
        }
        return ((TextUtils.isEmpty(firstName) ) ? "" : firstName)
                + ((!TextUtils.isEmpty(firstName) && !TextUtils.isEmpty(lastName))?" ":"")
                + ((TextUtils.isEmpty(lastName)) ? "" : lastName);
    }

    public void print() {

        DebugLog("user: firstname: %s", firstName);
        DebugLog("user: lastname: %s", lastName);
        DebugLog("user: email: %s", email);

        DebugLog("user: achievedLp: %d", achievedLp);
        DebugLog("user: totalLp: %d", totalLp);
        DebugLog("user: oldLp: %d", oldLp);

        DebugLog("user: city: %s", city);
        DebugLog("user: zipcode: %s", zipcode);

        DebugLog("user: isGuest: %b", isGuest);
        DebugLog("user: userimg: %s", userimg);
        if (achievements != null) {
        	DebugLog("user: achievements: %d", achievements.size());
        } else {
        	DebugLog("user: achievements: is null!");
        }
    }


    /*
     GET /user/account � User Account Info
		{
		    "is_guest": false,
		    "email": "test@example.com",
		    "total_lp": 100
		    "photo_url": "http://lootsie.s3.amazonaws.com/stage1/profile-pic-with-image-2x.png",

		    "first_name": "John",
		    "last_name": "Johnson",
		    "birthdate": "1980-12-31", // or null
		    "gender": "M",

		    "address": "630 W. 5th Street",
		    "city": "LA",
		    "state": "CA",
		    "zipcode": 22313, // or null

		    "interest_groups": [
		        {
		            "name": "Favorite Movies",
		            "liked": true,
		            "interests": [
		                {
		                    "name": "Comedy",
		                    "liked": false
		                    "id": "123",
		                },
		                // ...
		                {
		                    "name": "Thriller",
		                    "liked": true
		                    "id": "124",
		                }
		            ]
		        }
		        // ...
		    ]
		}

     */
    public void parseFromJSONUserAccountInfo(JSONObject json) throws JSONException {

        this.lastName = json.getString("last_name");
        this.firstName = json.getString("first_name");
        this.email = json.getString("email");
        this.isGuest = json.getBoolean("is_guest");
        this.totalLp = json.getInt("total_lp");

        this.city = json.getString("city");

        if (json.has("zipcode") && !json.isNull("zipcode")) {
            this.zipcode = json.getInt("zipcode");
        }

        String img = json.getString("photo_url");
        if (!"null".equalsIgnoreCase(img)) {
            this.userimg = img;
        }
    }

    // GET /user/achievements � Achievements Info
    /*
		{
		   "achievements": [
		       {
		           "id": "l5_5stars",
		           "date": "2012-10-12T13:29:25",
		           "lp": 50
		       }
		   ],
		   "achieved_lp": 50,
		   "total_lp": 150
		}
     */
    public void parseFromJSONUserAchievementsInfo(JSONObject json) throws JSONException {

        this.achievedLp = json.getInt("achieved_lp");
        this.totalLp = json.getInt("total_lp");


        JSONArray achievements = json.getJSONArray("achievements");
        this.achievements = strToAchievements(achievements.toString());

    }

    private static List<Achievement> strToAchievements(String str) {
        LinkedList<Achievement> achievements = new LinkedList<Achievement>();
        if (str != null && str.length() > 0) {
            JSONArray array;
            try {
                array = new JSONArray(str);
                int length = array.length();
                for (int i = 0; i < length; i++) {
                    //Achievement a = new Achievement();
                    try {
                        JSONObject obj = array.getJSONObject(i);

                        Achievement a = new Achievement();
                        a.parseFromJSON(obj);

                        achievements.add(a);

                        DebugLog("achievement[%d] %s", i, a.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return achievements;
    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (com.lootsie.sdk.utils.LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }
}
