package com.lootsie.sdk.lootsiehybrid;

import android.annotation.SuppressLint;

public enum LOOTSIE_NOTIFICATION_CONFIGURATION {
    notify_to_disabled,
    notify_to_rewardsPage,
    notify_to_achievementsPage,
    notify_to_aboutPage,
    notify_to_TOSPage,
    notify_to_accountPage,
    notify_to_webView,
    notify_to_customPage;
    
    //; is required here.

    @SuppressLint("DefaultLocale")
    @Override public String toString() {
        //only capitalize the first letter
        String s = super.toString();
        //return s.substring(0, 1) + s.substring(1).toLowerCase();
        return s;
    }
}