package com.lootsie.sdk.netutil;

import android.os.AsyncTask;
//import android.util.Log;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.lootsiehybrid.LootsieApi;
import com.lootsie.sdk.net.RestClient;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;


import java.util.Map;

/**
 * Created by jerrylootsie on 2/10/15.
 */

/*
    POST https://api-v2.lootsie.com/v2/user/session/guest
    X-Lootsie-App-Secret
    X-Lootsie-API-Session-Token

 */
//public class RESTPostUserSessionGuestTask extends AsyncTask<String, Void, RestResult> {
public class RESTPostUserSessionGuestTask extends AsyncTask<String, Void, RestResult> implements IGenericAsyncTask<String> {

    private final static String TAG = "Lootsie.RESTPostUserSessionGuestTask";

    IRESTCallback callback = null;

    public RESTPostUserSessionGuestTask() {

    }

    public RESTPostUserSessionGuestTask(IRESTCallback restCallback) {
        callback = restCallback;
    }

    private RestResult processError(Exception e) {
        RestResult response = new RestResult();
        String errorResponse = e.getMessage();
        if (errorResponse == null) {
            e.printStackTrace();
        } else {
//            response.status = e.code;
            Logs.e(TAG, "RESTPostUserSessionGuestTask: processError: getMessage: " + e.getMessage());
        }
        return response;
    }

    @Override
    protected RestResult doInBackground(String ... params ) {

//        if (params == null || params.length == 0) {
//            return null;
//        }

        RestResult response = new RestResult();
        try {
            // pull stuff from Device.java
            Map<String, Object> map = null;

            response = RestClient.doPost(LootsieApi.SIGN_IN_GUEST_URL.getUri(), map);

            if (LootsieGlobals.debugLevel > 0) Logs.v(TAG, "RESTPostUserSessionGuestTask:" + response.content);

        } catch (Exception e) {
            Logs.e(TAG, "RESTPostUserSessionGuestTask: processError: getMessage: " + e.getMessage());
        }
        return response;
    }



    @Override
    protected void onPostExecute(final RestResult restResult) {
        if (callback != null) {
            callback.restResult(restResult);
        }
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    public void executeTask(String... params) {
        execute(params);
    }
}