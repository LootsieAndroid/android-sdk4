package com.lootsie.sdk.model;

import android.annotation.SuppressLint;

/**
 * Created by jerrylootsie on 2/23/15.
 */
public class AchievementListEntryModel {
    private boolean selected;
    private String name;
    private String description;
    private Achievement achievement;
    private boolean achievementReached = false;

    public enum AchievementEntryType {
        DEFAULT_ACHIEVEMENT_ENTRY,
        ONE_TIME_ACHIEVEMENTS_TITLE,
        REPEATABLE_ACHIEVMENTS_TITLE;
        //; is required here.

        @SuppressLint("DefaultLocale")
        @Override public String toString() {
            //only capitalize the first letter
            String s = super.toString();
            return s.substring(0, 1) + s.substring(1).toLowerCase();
        }
    }

    public AchievementEntryType achievementEntryType = AchievementEntryType.DEFAULT_ACHIEVEMENT_ENTRY;

    public AchievementListEntryModel() {
        this.achievement = null;
        this.name = null;
        this.description = null;
        selected = false;
    }

    public AchievementListEntryModel(Achievement inputAchievement) {
        this.achievement = inputAchievement;
        this.name = achievement.name;
        this.description = achievement.description;
        selected = false;

        this.achievementEntryType = AchievementEntryType.DEFAULT_ACHIEVEMENT_ENTRY;
    }

    public AchievementListEntryModel(String name) {
        this.name = name;
        selected = false;
    }

    public Achievement getAchievement() {
        return achievement;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public void setAchievementReached(boolean inputReached) {
        achievementReached = inputReached;
    }
    public boolean isAchievementReached() { return achievementReached; }

    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }

}