package com.lootsie.sdk.callbacks;

import com.lootsie.sdk.netutil.RestResult;

/**
 * Created by Alexander S. Sidorov on 3/1/16.
 */
public interface IIncrementEngineCallback
{
    /**
     *  Lootsie will call back this developer implemented method when Lootsie
     *  to call the Increment Engine
     */
    void onIncrementEngineFailed(String errorMessage);


    /**
     *  Lootsie will call back this developer implemented method when Lootsie
     *  successfully received response from the Increment Engine
     */
    void onIncrementEngineSuccess(RestResult restResult);

}
