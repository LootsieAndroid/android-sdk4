package com.lootsie.sdk.viewcontrollers.base;

/**
 * Created by jerrylootsie on 2/19/15.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;

import android.graphics.Rect;
import android.os.Build;
import android.text.TextUtils;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;

import android.app.AlertDialog;


import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Button;
import android.widget.EditText;

import com.lootsie.sdk.callbacks.IRedeemReward;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.model.Reward;
import com.lootsie.sdk.model.RewardListEntryModel;
import com.lootsie.sdk.model.RewardViewHolder;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.utils.RUtil;
import com.lootsie.sdk.utils.image.ImageCacheManager;

import com.lootsie.sdk.R;
import com.lootsie.sdk.viewcontrollers.LootsiePageBorderActivity;
import com.lootsie.sdk.viewcontrollers.VideoRewardHelper;

public class RewardArrayAdapter extends ArrayAdapter<RewardListEntryModel> implements com.lootsie.sdk.callbacks.IDurationCallback {

    static final String TAG = "Lootsie RewardArrayAdapter";

    private final Context context;
    private final List<RewardListEntryModel> values;

    private static PopupWindow redeemPopup = null;
    private static PopupWindow dimPopup = null;
    private static boolean dimPopupEnabled = false;

    private boolean adapterHasAlreadySetOnScrollListener = false;

    private static int mWidth = 0;
    private static int mHeight = 0;

    private boolean testSmallImages = false;

    private long totalDurationMs = 0;

    public RewardArrayAdapter(Context context, List<RewardListEntryModel> values) {
    	super(context, RUtil.getLayoutId(context, "com_lootsie_rowlayout"), values);
        //super(context, R.layout.com_lootsie_rowlayout, values);
        this.context = context;
        this.values = values;

        this.totalDurationMs = 0;
        DebugLog("RewardArrayAdapter: init");

    }

    @Override
    public int getViewTypeCount() {
        return values.size() + 1; // TMP
        //return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
        //return position % 2;
    }

//    @Override
//    public RewardListEntryModel getItem(int position) {
//        DebugLog("getItem: %d", position);
//        return super.getItem(position);
//    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View rowView = null;

        // TMP [[
        if (((LootsiePageBorderActivity) context).isRewardedVideoSupported()) {
            if (position == 0) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rowView = inflater.inflate(RUtil.getLayoutId(context, "com_lootsie_video_card"), parent, false);
                rowView.findViewById(R.id.go_watch_rewarded_video_button).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (VideoRewardHelper.isWatchedTodayLimitReached(context)) {
                            VideoRewardHelper.showReachedPopup((Activity) context);
                        } else {
                            // show available ads
                            ((LootsiePageBorderActivity) context).showRewardedVideoAd();
                        }
                    }
                });

                return rowView;
            } else {
                position = position - 1;
            }
        }
        // TMP ]]

        if (convertView != null) {
            rowView = convertView;
        } else {
            // create the new view by inflating it
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //rowView = inflater.inflate(R.layout.com_lootsie_rowlayout, parent, false);
            rowView = inflater.inflate(RUtil.getLayoutId(context, "com_lootsie_rowlayout"), parent, false);

            RewardListEntryModel currentReward = values.get(position);

            //TextView rewardNameTextView = (TextView) rowView.findViewById(R.id.rewardName);
            TextView rewardNameTextView  = (TextView) rowView.findViewById(RUtil.getResourceId(context, "rewardName", "id", context.getPackageName()));
            rewardNameTextView.setText(currentReward.getName());

            //TextView rewardPointsTextView = (TextView) rowView.findViewById(R.id.rewardPoints);
            TextView rewardPointsTextView  = (TextView) rowView.findViewById(RUtil.getResourceId(context, "rewardPoints", "id", context.getPackageName()));
            rewardPointsTextView.setText(String.valueOf(currentReward.getReward().lp) + " " + DataModel.currency_abbreviation);

            //TextView termsAndConditionsTextView = (TextView) rowView.findViewById(R.id.textViewTerms2);
            //Button termsAndConditionsButton = (Button) rowView.findViewById(R.id.buttonTerms);
            Button termsAndConditionsButton = (Button) rowView.findViewById(RUtil.getResourceId(context, "buttonTerms", "id", context.getPackageName()));


            //ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
            //LootsieNetworkImageView imageView = (LootsieNetworkImageView) rowView.findViewById(R.id.reward_logo);
            LootsieNetworkImageView imageView = (LootsieNetworkImageView) rowView.findViewById(RUtil.getResourceId(context, "reward_logo", "id", context.getPackageName()));
            imageView.setPosition(position);
            // ASDK-369 Android-SDK4 Rewards Page/ Marketplace - Lazy/Priority Loading Loading of Rewards Images
            // imageView.setImageUrl(currentReward.getReward().icon.M, ImageCacheManager.getInstance().getImageLoader());

            //FrameLayout descriptionLayout = (FrameLayout) rowView.findViewById(R.id.DescriptionLayout);
            FrameLayout descriptionLayout = (FrameLayout) rowView.findViewById(RUtil.getResourceId(context, "DescriptionLayout", "id", context.getPackageName()));

            //LinearLayout redeemLayout = (LinearLayout) rowView.findViewById(R.id.RedeemLayout);
            LinearLayout redeemLayout = (LinearLayout) rowView.findViewById(RUtil.getResourceId(context, "RedeemLayout", "id", context.getPackageName()));

            //Button detailsButton = (Button) rowView.findViewById(R.id.detailsButton);
            Button detailsButton = (Button) rowView.findViewById(RUtil.getResourceId(context, "detailsButton", "id", context.getPackageName()));

            //Button redeemButton = (Button) rowView.findViewById(R.id.redeemButton);
            Button redeemButton = (Button) rowView.findViewById(RUtil.getResourceId(context, "redeemButton", "id", context.getPackageName()));


            //EditText editTextEmail = (EditText) rowView.findViewById(R.id.editTextEmail);
            EditText editTextEmail = (EditText) rowView.findViewById(RUtil.getResourceId(context, "editTextEmail", "id", context.getPackageName()));
            if (editTextEmail == null) {
                Logs.e(TAG,"getView: editTextEmail is missing!");
            } else {

                // ASDK-478 - Marketplace: User’s email address does not automatically populate redemption blank.
                if ((DataModel.user.isGuest == false) && (DataModel.user.email != null)) {
                    editTextEmail.setText(DataModel.user.email);
                }

                // trying to toggle focus so I only have to click once to bring up keyboard?
//                editTextEmail.clearFocus();
//                editTextEmail.setFocusable(false);
//                editTextEmail.setFocusableInTouchMode(false);

            }

            //TextView descriptionTextView = (TextView) rowView.findViewById(R.id.descriptionTextView);
            TextView descriptionTextView  = (TextView) rowView.findViewById(RUtil.getResourceId(context, "descriptionTextView", "id", context.getPackageName()));
            
            RewardListEntryModel rewardEntry = values.get(position);
            Reward reward = rewardEntry.getReward();
            descriptionTextView.setText(reward.description);


            // save for easy reference through tag later
            RewardViewHolder holder = new RewardViewHolder();
            holder.t = rewardNameTextView;
            holder.i = imageView;
            holder.position = position;
            holder.descriptionLayout = descriptionLayout;
            holder.redeemLayout = redeemLayout;
            holder.editTextEmail = editTextEmail;
            //holder.termsAndConditionsTextView = termsAndConditionsTextView;
            holder.termsAndConditionsButton = termsAndConditionsButton;

            rewardEntry.rewardViewHolder = holder;
            rowView.setTag(holder);

            //detailsButton.setTag(position);
            detailsButton.setTag(holder);
            detailsButton.setOnClickListener(new DetailsButtonListener());

            redeemButton.setTag(holder);

            //redeemButton.setOnClickListener(new RedeemButtonToDialogListener());
            if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                redeemButton.setOnClickListener(new RedeemButtonListener());
                
                //Button sendRewardButton = (Button) rowView.findViewById(R.id.sendRewardButton);
                Button sendRewardButton = (Button) rowView.findViewById(RUtil.getResourceId(context, "sendRewardButton", "id", context.getPackageName()));
                
                sendRewardButton.setTag(holder);
                sendRewardButton.setOnClickListener(new SendRewardButtonListener());


            } else {
                redeemButton.setOnClickListener(new RedeemButtonToPopupDialogListener());
            }

            termsAndConditionsButton.setTag(holder);
            termsAndConditionsButton.setOnClickListener(new TermsAndConditionsButtonListener());

        }

        RewardViewHolder tag = (RewardViewHolder) rowView.getTag();

        // ASDK-369 Android-SDK4 Rewards Page/ Marketplace - Lazy/Priority Loading Loading of Rewards Images
        // prefetch first element 0
        if ((position == 0) && (!tag.startedImageLoad)) {
            onEnter((ListView) parent, rowView,  position);
        }

        // ASDK-369 Android-SDK4 Rewards Page/ Marketplace - Lazy/Priority Loading Loading of Rewards Images
        if ((parent instanceof ListView) && (!this.adapterHasAlreadySetOnScrollListener)) {
            ListView parentScroll = (ListView) parent;
            parentScroll.setOnScrollListener(new RewardListListener());
            this.adapterHasAlreadySetOnScrollListener = true;
        }

        // set this outside of the view setup so refreshes get it too!
        RewardListEntryModel currentReward = values.get(position);

        if (!currentReward.detailsVisible) {
            tag.descriptionLayout.setVisibility(View.GONE);
        } else {
            DebugLog("RewardArrayAdapter: details[%d] visible", position);
            tag.descriptionLayout.setVisibility(View.VISIBLE);
        }

        if (!currentReward.redeemVisible) {
            tag.redeemLayout.setVisibility(View.GONE);
        } else {
            DebugLog("RewardArrayAdapter: redeem[%d] visible", position);
            tag.redeemLayout.setVisibility(View.VISIBLE);
        }

        return rowView;
    }



    class DetailsButtonListener implements OnClickListener {
        @Override
        public void onClick(View arg0) {
            //int position=(Integer)arg0.getTag();
            RewardViewHolder holder = (RewardViewHolder) arg0.getTag();
            int position = holder.position;

            //Logs.d(TAG, "onClickDetails[" + position +"]:");
            DebugLog("onClickDetails[%d]:", position);

            RewardListEntryModel currentReward = values.get(position);

            if (currentReward.detailsVisible) {
                holder.descriptionLayout.setVisibility(View.GONE);
                currentReward.detailsVisible = false;
            } else {
                holder.descriptionLayout.setVisibility(View.VISIBLE);
                currentReward.detailsVisible = true;
            }

        }
    }



    @SuppressLint("NewApi")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private static void getDisplayDimensions_HoneyComb(Display display) {
        Rect displaySizeRect = new Rect();
        display.getRectSize(displaySizeRect);

//        mWidth = displaySizeRect.width();
//        mHeight = displaySizeRect.height();

        // ASDK-484 - Marketplace: Pre-Lollipop: Rotating the device with the redemption modal open causes visual issues (blackout only extends halfway across the background).
        mWidth = Math.max(displaySizeRect.width(), displaySizeRect.height());
        mHeight = mWidth;
    }

    private PopupWindow dimBackground() {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View pview = inflater.inflate(RUtil.getLayoutId(context, "com_lootsie_fadepopup"), null);

        // fadePopup is the id of LinearLayout inside layout/com_lootsie_fadepopup        
        View layout = pview.findViewById(RUtil.getResourceId(context, "fadePopup", "id", context.getPackageName()));
        
        
        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            getDisplayDimensions_HoneyComb(display);
        } else {
//            mWidth = display.getWidth();
//            mHeight = display.getHeight();

            // ASDK-484 - Marketplace: Pre-Lollipop: Rotating the device with the redemption modal open causes visual issues (blackout only extends halfway across the background).
            mWidth = Math.max(display.getWidth(), display.getHeight());
            mHeight = mWidth;
            
        }


        PopupWindow fadePopup = new PopupWindow(layout, mWidth, mHeight, false);
        if (fadePopup != null) {
            fadePopup.showAtLocation(layout, Gravity.NO_GRAVITY, 0, 0);
        } else {
            Logs.e(TAG, "dimBackground: fadePopup is null!");
        }
        return fadePopup;
    }

    class RedeemButtonToPopupDialogListener implements OnClickListener {

        @Override
        public void onClick(View arg0) {
            RewardViewHolder holder = (RewardViewHolder) arg0.getTag();
            int position = holder.position;

            //Logs.d(TAG, "onClickRedeem[" + position +"]:");
            DebugLog("onClickRedeem[%d]:", position);

            if (redeemPopup != null) {
                Logs.e(TAG,"Error: onClickRedeem: dialog is already showing!");
                return;
            }

            if (dimPopupEnabled) {
                dimPopup = dimBackground();
            }

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View pview;
            //pview = inflater.inflate(R.layout.com_lootsie_reward_redeem, null);
            pview = inflater.inflate(RUtil.getLayoutId(context, "com_lootsie_reward_redeem"), null);

            redeemPopup = new PopupWindow(pview, ActionBar.LayoutParams.WRAP_CONTENT,
                    ActionBar.LayoutParams.WRAP_CONTENT, true);


            redeemPopup.showAtLocation(arg0, Gravity.CENTER, 0, 0);
            redeemPopup.setOutsideTouchable(false); // don't allow touches outside to dismiss it
            redeemPopup.setFocusable(true);
            //cp.update(0, 0, 500, 350);


            //Button termsAndConditionsButton = (Button) pview.findViewById(R.id.buttonTerms);
            Button cancelButton = (Button) pview.findViewById(RUtil.getResourceId(context, "cancelButton", "id", context.getPackageName()));
            if (cancelButton != null) {
                cancelButton.setOnClickListener(new CancelButtonListener());
                cancelButton.setTag(holder);
            }

            //Button sendRewardButton = (Button) pview.findViewById(R.id.sendRewardButton);
            Button sendRewardButton = (Button) pview.findViewById(RUtil.getResourceId(context, "sendRewardButton", "id", context.getPackageName()));
            if (sendRewardButton != null) {
                sendRewardButton.setTag(holder);
                sendRewardButton.setOnClickListener(new SendRewardButtonListener());
            }


            //EditText editTextEmail = (EditText) pview.findViewById(R.id.editTextEmail);
            EditText editTextEmail = (EditText) pview.findViewById(RUtil.getResourceId(context, "editTextEmail", "id", context.getPackageName()));
            if (editTextEmail != null) {
                holder.editTextEmail = editTextEmail;

                // ASDK-478 - Marketplace: User’s email address does not automatically populate redemption blank.
                if ((DataModel.user.isGuest == false) && (DataModel.user.email != null)) {
                    editTextEmail.setText(DataModel.user.email);
                }

            }

        }
    }


//    class RedeemButtonToDialogListener implements  OnClickListener {
//        @Override
//        public void onClick(View arg0) {
//            RewardViewHolder holder = (RewardViewHolder) arg0.getTag();
//            int position = holder.position;
//
//            //Logs.d(TAG, "onClickRedeem[" + position +"]:");
//            DebugLog("onClickRedeem[%d]:", position);
//
//            try {
//                Intent intent = new Intent(context, RewardRedeemActivity.class);
//                context.startActivity(intent);
//            } catch (Exception e) {
//                Logs.e(TAG, "RedeemButtonToDialogListener: exception: " + e.getMessage());
//            }
//        }
//
//    }

    class CancelButtonListener implements OnClickListener {
        @Override
        public void onClick(View arg0) {
            //int position=(Integer)arg0.getTag();
            RewardViewHolder holder = (RewardViewHolder) arg0.getTag();
            int position = holder.position;

            DebugLog("onClickCancel[%d]:", position);

            RewardListEntryModel currentReward = values.get(position);

            if (currentReward.redeemVisible) {
                holder.redeemLayout.setVisibility(View.GONE);
                currentReward.redeemVisible = false;

                // hide keyboard
                InputMethodManager inputManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(holder.editTextEmail.getWindowToken(), 0);
            }

            if (redeemPopup != null) {
                redeemPopup.dismiss();
                redeemPopup = null;
            }

            if (dimPopupEnabled && (dimPopup != null)) {
                dimPopup.dismiss();
                dimPopup = null;
            }
        }
    }

    class RewardAdapterRedeemRewardCallback implements IRedeemReward {

        public RewardAdapterRedeemRewardCallback() {
        }

        @Override
        public void onRedeemFailed(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT result, String json) {
            DebugLog("RewardAdapterRedeemRewardCallback: onRedeemFailed: %s", result.toString());
        }

        @Override
        public void onRedeemSuccess(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT result, String json) {
            DebugLog("RewardAdapterRedeemRewardCallback: onRedeemSuccess: %s : %s", result.toString(), json);

            //LootsieEngine.getInstance().updateCurrentActivitiesAndFragments();
            if (redeemPopup != null) {
                redeemPopup.dismiss();
                redeemPopup = null;
            }

            if (dimPopupEnabled && (dimPopup != null)) {
                dimPopup.dismiss();
                dimPopup = null;
            }
        }


    }

    class RedeemButtonListener implements  OnClickListener {
        @Override
        public void onClick(View arg0) {
            //int position=(Integer)arg0.getTag();
            RewardViewHolder holder = (RewardViewHolder) arg0.getTag();
            int position = holder.position;

            //Logs.d(TAG, "onClickRedeem[" + position +"]:");
            DebugLog("onClickRedeem[%d]:", position);

            RewardListEntryModel currentReward = values.get(position);

            if (currentReward.redeemVisible) {
                holder.redeemLayout.setVisibility(View.GONE);
                currentReward.redeemVisible = false;

                // hide keyboard
                InputMethodManager inputManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(holder.editTextEmail.getWindowToken(), 0);
            } else  {
                holder.redeemLayout.setVisibility(View.VISIBLE);
                currentReward.redeemVisible = true;

                // this entry in the adapter was selected
                // arg0.setSelected(true);  // no change


                // without this, I have to tap the email entry field twice on Nexus tablet!


                // because of samsung device keyboard goofiness
                //holder.editTextEmail.clearFocus();
                //holder.editTextEmail.setFocusable(false); // no change
                //holder.editTextEmail.setFocusableInTouchMode(false); // no change

                holder.editTextEmail.setFocusable(true);
                holder.editTextEmail.setFocusableInTouchMode(true);
                holder.editTextEmail.setTag(holder); // so we can get it inside the OnKeyListener!

                // make the keyboard focus on the edittext immediately
                holder.editTextEmail.clearFocus();
                holder.editTextEmail.requestFocus();
                InputMethodManager inputManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.showSoftInput(holder.editTextEmail, 0);

                // make sure edittext has soft keyboard input show up when it is clicked - will prevent softinput window from disappearing!
//                        holder.editTextEmail.setOnClickListener(new OnClickListener(){
//                            public void onClick(View v) {
//                                DebugLog("editTextEmail: onClick");
//                                InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
//                                inputManager.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
//                            }
//                        });


                //Being inside the box and pressing a key
                holder.editTextEmail.setOnKeyListener(new View.OnKeyListener() {

                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        //If the event is a key-down event on the "enter" button
                        //If enter is pressed while inside the textbox
                        if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                                (keyCode == KeyEvent.KEYCODE_ENTER)) {

                            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                            //InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                            //Example of hiding keyboard inside enter pressed check
                            //imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                            inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
                            DebugLog("editTextEmail: KEYCODE_ENTER");

                            // start redeem reward sequence
                            RewardViewHolder holder = (RewardViewHolder) v.getTag();
                            int position = holder.position;
                            RewardListEntryModel rewardEntry = values.get(position);
                            String emailStr = holder.editTextEmail.getText().toString();
                            //Logs.d(TAG, "onClickSendReward[" + position +"]: " + emailStr );
                            DebugLog("onClickSendReward[%d]: %s", position, emailStr);

                            RewardAdapterRedeemRewardCallback redeemRewardCallback = new RewardAdapterRedeemRewardCallback();
                            LootsieEngine.getInstance().redeemReward(emailStr, rewardEntry.getReward(), redeemRewardCallback);

                        }

                        return false;
                    }
                });

                // show keyboard
                // only have to click once with these calls, I shouldn't have to click on editTextEmail at all!
                //InputMethodManager inputManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
                //inputManager.restartInput(holder.editTextEmail);  // does nothing!
                //inputManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY); // the keyboard actually shows up with this method!
//                        inputManager.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_IMPLICIT_ONLY); // the keyboard actually shows up with this method!


                // show keyboard
                //InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                //inputManager.showSoftInput(holder.editTextEmail, InputMethodManager.SHOW_IMPLICIT);  // doesn't show up?
                //inputManager.showSoftInput(holder.editTextEmail, InputMethodManager.SHOW_FORCED);  // doesn't show up?

                //holder.editTextEmail.requestFocus();  // doesn't actually get focus - damn!


            }
        }
    }

    class SendRewardButtonListener implements OnClickListener {
        @Override
        public void onClick(View arg0) {
            //int position=(Integer)arg0.getTag();
            RewardViewHolder holder = (RewardViewHolder) arg0.getTag();
            int position = holder.position;

            EditText editTextEmail = holder.editTextEmail;

            // hide keyboard
            InputMethodManager inputManager = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(holder.editTextEmail.getWindowToken(), 0);

            String emailStr = editTextEmail.getText().toString();
            //Logs.d(TAG, "onClickSendReward[" + position +"]: " + emailStr );
            DebugLog("onClickSendReward[%d]: %s", position, emailStr);

            // start redeem reward sequence
            RewardListEntryModel rewardEntry = values.get(position);

            RewardAdapterRedeemRewardCallback redeemRewardCallback = new RewardAdapterRedeemRewardCallback();

            if (!TextUtils.isEmpty(emailStr))
                LootsieEngine.getInstance().redeemReward(emailStr, rewardEntry.getReward(), redeemRewardCallback);
            else
                LootsieEngine.redeemRewardWoEmail(rewardEntry.getReward(), redeemRewardCallback);
        }
    }

    class TermsAndConditionsButtonListener implements OnClickListener {
        @Override
        public void onClick(View view) {
            RewardViewHolder holder = (RewardViewHolder) view.getTag();
            int position = holder.position;

            DebugLog("termsAndConditionsButton: onClick: position: %d", position);

            RewardListEntryModel currentReward = values.get(position);

            //AlertDialog alertDialog = new AlertDialog.Builder(LootsieEngine.getInstance().getActivityContext()).create();
            AlertDialog alertDialog = new AlertDialog.Builder(context).create();
            alertDialog.setTitle("Terms & Conditions");
            alertDialog.setMessage(currentReward.getReward().tos_text);
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    //here you can add functions

                } });
            alertDialog.show();

        }
    }

    // ASDK-369 Android-SDK4 Rewards Page/ Marketplace - Lazy/Priority Loading Loading of Rewards Images
    class RewardListListener implements AbsListView.OnScrollListener {
        int oldFirstVisibleItem = 0;
        int oldLastVisibleItem = 0;


        @Override
        public void onScrollStateChanged(AbsListView absListView, int i) {
        }


        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            if (firstVisibleItem > oldFirstVisibleItem) {
                for (int i = oldFirstVisibleItem; i < firstVisibleItem; i++) {
                    DebugLog1("OnScrollListener: exit %d", i);

                    // unloading to save on memory
//                    View childView = view.getChildAt(i);
//                    onExit((ListView) view, childView, i);
                }
            }
            if (firstVisibleItem < oldFirstVisibleItem) {
                for (int i = firstVisibleItem; i < oldFirstVisibleItem; i++) {
                    //DebugLog1("OnScrollListener: enter %d", i);
                    View childView = view.getChildAt(i);
                    onEnter((ListView) view, childView, i);
                }
            }

            int lastVisibleItem = firstVisibleItem + visibleItemCount - 1;
            if (lastVisibleItem < oldLastVisibleItem) {
                for (int i = oldLastVisibleItem + 1; i <= lastVisibleItem; i++) {
                    DebugLog1("OnScrollListener: exit %d", i);

                    // unloading to save on memory
//                    View childView = view.getChildAt(i);
//                    onExit((ListView) view, childView, i);
                }
            }
            if (lastVisibleItem > oldLastVisibleItem) {
                for (int i = oldLastVisibleItem + 1; i <= lastVisibleItem; i++) {
                    //DebugLog1("OnScrollListener: enter %d", i);
                    View childView = view.getChildAt(i);
                    onEnter((ListView) view, childView, i);
                }
            }

            oldFirstVisibleItem = firstVisibleItem;
            oldLastVisibleItem = lastVisibleItem;
        }
    }

    @Override
    public void reportDurationMillisec(long durationMs) {

        totalDurationMs += durationMs;

        DebugLog("RewardArrayAdapter: totalDurationMs: " + totalDurationMs);

    }

    // ASDK-369 Android-SDK4 Rewards Page/ Marketplace - Lazy/Priority Loading Loading of Rewards Images
    public void onEnter(ListView parent, View rowView, int position) {

        RewardListEntryModel rewardListEntryModel = this.getItem(position);

        // I can't always rely on the parent to get the child of listview if I'm using arrayadapter!
//        if (rowView == null) {
//            Logs.e(TAG,"onEnter: error rowView is null at position: " + position);
//        }

        //RewardViewHolder tag = (RewardViewHolder) rowView.getTag();
        RewardViewHolder tag = rewardListEntryModel.rewardViewHolder;
        if (tag != null) {
            if (!tag.startedImageLoad) {
                DebugLog1("OnScrollListener: enter %d", position);

                RewardListEntryModel currentReward = values.get(position);
                LootsieNetworkImageView imageView = (LootsieNetworkImageView) tag.i;
                imageView.setPosition(position);

                // ASDK-504 Test for smaller images' improvements to performance.
                if (testSmallImages) {
                    String smallImageUrl = "";

                    //String urlPrefix = "https://d1no9q2xfha3ui.cloudfront.net/live/ap-image-m/smallImageTest/";
                    String urlPrefix = "https://s3.amazonaws.com/lootsie/live/ap-image-m/smallImageTest/";
                    ArrayList<String> smallImageList = new ArrayList<String>(
                            Arrays.asList("rewardImage1.jpg",
                                    "rewardImage2.jpg",
                                    "rewardImage3.jpg",
                                    "rewardImage4.jpg",
                                    "rewardImage5.png",
                                    "rewardImage6.jpg",
                                    "rewardImage7.jpg",
                                    "rewardImage8.jpg",
                                    "rewardImage9.jpg",
                                    "rewardImage10.jpg",
                                    "rewardImage11.jpg",
                                    "rewardImage12.jpg",
                                    "rewardImage13.jpg",
                                    "rewardImage14.jpg",
                                    "rewardImage15.jpg",
                                    "rewardImage16.jpg",
                                    "rewardImage17.jpg",
                                    "rewardImage18.jpg",
                                    "rewardImage19.jpg",
                                    "rewardImage20.jpg",
                                    "rewardImage21.jpg",
                                    "rewardImage22.jpg"
                                    ));

                    imageView.setDurationCallback(this);
                    imageView.setImageUrl(urlPrefix + smallImageList.get(position), ImageCacheManager.getInstance().getImageLoader());
                } else {

                    imageView.setDurationCallback(this);
                    imageView.setImageUrl(currentReward.getReward().icon.M, ImageCacheManager.getInstance().getImageLoader());
                }

                tag.startedImageLoad = true;
            }
        } else {
            Logs.e(TAG, "onEnter: error tag is invalid!");
        }

    }


    // ASDK-466 - Crashing frequently on the LG phone running 4.0.3.
    public void onExit(ListView parent, View rowView, int position) {

        RewardListEntryModel rewardListEntryModel = this.getItem(position);
        RewardViewHolder tag = rewardListEntryModel.rewardViewHolder;
        if (tag != null) {
            if (tag.startedImageLoad) {
                DebugLog1("OnScrollListener: exit %d", position);

                //RewardListEntryModel currentReward = values.get(position);
                LootsieNetworkImageView imageView = (LootsieNetworkImageView) tag.i;

                // stop the image download, detachfromwindow may be too much...
                //imageView.onDetachedFromWindow();
                imageView.cancelLoadImage();

                tag.startedImageLoad = false;
            }
        } else {
            Logs.e(TAG, "onExit: error tag is invalid!");
        }

    }


    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog1(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 1) {
            Logs.v(TAG, String.format(msg, args));
        }
    }

}