package com.lootsie.sdk.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.lootsie.sdk.viewcontrollers.VideoRewardHelper;

public class TimeNotificationReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        VideoRewardHelper.resetWatchedToday(context);
    }
}
