package com.lootsie.sdk.utils;

/**
 * Global constants used across lootsie
 * Created by jerrylootsie on 2/9/15.
 */
import android.annotation.SuppressLint;

public class LootsieGlobals {

    /**
     *  Before submitting to staging,
     *  1) Always change the debug levels to 0
     *  2) set SEQUENCE_LOGGING = false to turn off the sequence diagram displaying
     *  3) change SDK_VERSION
     *  4) set APPKEY=APPKEY_STAGING or APPKEY=APPKEY_LIVE depending on the request
     *  5) disable and enable INDEXJSON_URL,DEFAULT_WEBAPP_URL for staging or live
     *
     */



	/*
	 * ========== WebAPP related  Information ==================
	 */

    // Index.Json Location
    public static final String INDEX_JSON_VERSION = "3.2";


    // SDK version - note this is independent of the webapp version!
    // Update this string before merging to Staging
    public static final String SDK_VERSION = "4.20151112_1116";


    // AppKeys
    // tictacloot on dev 91BA8D7CF70420A628EABDFECB2E63F0D6CC4F9D256D978B1A7C0F209231CBD8 <could be wrong>

    // tictacloot on staging E083CD1796717647DD9AA459FB8F23B9AA6B2D9A7D9204C108E273209BC097F2 - Android key
    // tictacloot on staging 91BA8D7CF70420A628EABDFECB2E63F0D6CC4F9D256D978B1A7C0F209231CBD8 - IOS key

    // Lootsie on staging F5E99C50E46CD9871C95BD0B2608158C5138F4A1E83180729DB83E2092320C94 - android key
    // Lootsie Hybrid on live F5E99C50E46CD9871C95BD0B2608158C5138F4A1E83180729DB83E2092320C94 - android key

    // tictacloot on live	91BA8D7CF70420A628EABDFECB2E63F0D6CC4F9D256D978B1A7C0F209231CBD8 -IOS key
    // tictacloot on live BD18FB2E5B47DDDBB0977BED7C2D65CC63836454DC8CD038AAB5B92033D9657E - Android key
    private  final static String APPKEY_DEV = "91BA8D7CF70420A628EABDFECB2E63F0D6CC4F9D256D978B1A7C0F209231CBD8";
//    private  final static String APPKEY_STAGING = "F5E99C50E46CD9871C95BD0B2608158C5138F4A1E83180729DB83E2092320C94";
    private  final static String APPKEY_STAGING = "00D9CF885D81E4A69616536C5E512FC924AFB01E76E0C49194564A220F769379"; // consulti.ca
    private  final static String APPKEY_LIVE = "F5E99C50E46CD9871C95BD0B2608158C5138F4A1E83180729DB83E2092320C94";

    // use this in LootsieApp for all testing purposes , Note that APPkey and the URL should point to
    // the same server.
	public static final String APPKEY = APPKEY_STAGING;
//    public static final String APPKEY = APPKEY_LIVE;
//	public static final String APPKEY = APPKEY_DEV;

    public static long MINIMUM_MEMORY_THRESHOLD = 1024 * 1024 * 10;

    public static String DEFAULT_WELCOME_NAME = "Welcome back";
    //====================================================================================================================================

    // HTTP response timer value
    public static final int HTTP_TIMEOUT = 10 * 1000; // 10ms


    // Lootsie Account Manager settings
    public static final String APP_ID_SEPARATOR = ":";
    // make sure this values is present in res/xml/authenticator.xml and res/xml/syncadapter.xml
    // make sure to user underbar (_) in account type
    // http://stackoverflow.com/questions/3774282/securityexception-caller-uid-xxxx-is-different-than-the-authenticators-uid/11177438#11177438
    public static final String ACCOUNT_TYPE = "com.lootsie.sdk.ACCOUNT_SDK";
    public static final String KEY_AUTHTOKEN = "com.lootsie.sdk.auth.token";
    public static final String ACCOUNT_GUEST_NAME = "Guest";
    public static final String AUTHTOKEN_TYPE = "com.lootsie.sdk";


    //======================================================================================================================//

    // Preferneces key values

    public static final String KEY_USER_SESSION_TOKEN = "com.lootsie.sdk.user_session_token";
    public static final String KEY_API_SESSION_TOKEN = "com.lootsie.sdk.api_session_token";
    public static final String KEY_APP = "com.lootsie.sdk.GetApp";
    public static final String KEY_USER_REWARDS = "com.lootsie.sdk.GetUserReward";
    public static final String KEY_SETTINGS = "com.lootsie.sdk.GetSettings";
    public static final String KEY_NETWORK_STATS = "com.lootsie.sdk.networkStats";
    public static final String KEY_LOOTSIE_SESSION_BEGIN_TIMESTAMP = "com.lootsie.sdk.LOOTSIE_SESSION_BEGIN_TIMESTAMP";

    public static final int NETWORK_CACHE_TIME_SECONDS = 60*5;

    //======================================================================================================================//

    // message flags

    public enum LOOTSIE_SEQUENCE_RESULT {
        ACHIEVEMENT_REACHED_SEQUENCE_SUCCESS,
        ACHIEVEMENT_REACHED_SEQUENCE_FAILURE,
        INIT_SEQUENCE_SUCCESS,
        INIT_SEQUENCE_FAILURE,
        MERGE_POINTS_SEQUENCE_SUCCESS,
        MERGE_POINTS_SEQUENCE_FAILURE_ALREADY_REGISTERED, // "User is already registered."
        MERGE_POINTS_SEQUENCE_FAILURE_INVALID_EMAIL, // "Invalid Email Address"
        MERGE_POINTS_SEQUENCE_FAILURE_LP_MAXED, // "Sorry; you have reached the periodic limit. Come back tomorrow!"
        MERGE_POINTS_SEQUENCE_FAILURE_REGISTER_EMAIL, // "Register Email Failure"
        REDEMPTION_SEQUENCE_SUCCESS,
        REDEMPTION_SEQUENCE_FAILURE_INVALID_EMAIL, // "Invalid Email Address"
        REDEMPTION_SEQUENCE_FAILURE_REGISTER_EMAIL, // "Redeem Register Email Failure"
        REDEMPTION_SEQUENCE_FAILURE_NOT_AFFORDABLE, // 402 - "Not affordable"
        REDEMPTION_SEQUENCE_FAILURE_REDEMPTION_LIMIT, // 409 - "Redemption limit reached"
        REDEMPTION_SEQUENCE_FAILURE,
        RESET_USER_SEQUENCE_SUCCESS,
        RESET_USER_SEQUENCE_FAILURE,
        UPDATE_ACHIEVEMENT_PAGE_SEQUENCE_SUCCESS,
        UPDATE_ACHIEVEMENT_PAGE_SEQUENCE_FAILURE,
        UPDATE_USER_ACCOUNT_SEQUENCE_SUCCESS,
        UPDATE_USER_ACCOUNT_SEQUENCE_FAILURE,
        UPDATE_USER_ACCOUNT_SEQUENCE_FAILURE_REGISTER_EMAIL, // "Register Email Failure"
        UPDATE_USER_ACCOUNT_SEQUENCE_FAILURE_INVALID_ZIPCODE, // "Invalid zip code"
        UPDATE_USER_ACCOUNT_SEQUENCE_FAILURE_INVALID_EMAIL; // "Invalid Email Address"

        @SuppressLint("DefaultLocale")
        @Override public String toString() {
            //only capitalize the first letter
            String s = super.toString();
            return s.substring(0, 1) + s.substring(1).toLowerCase();
        }
    }

    //======================================================================================================================//
	/*
	 * ========== Logging Flags ===========
	 */

    // to turn on sequence diagram
    public static final boolean SEQUENCE_LOGGING = true;
    public static final boolean CSS_LAYOUT_DIAGRAM = false;

    // values are from 0 to 2
    public static int debugLevel = 0;
    public static int debugNetworkLevel = 0;
    public static int debugJavascriptLevel = 0;
    public static int debugParserLevel = 0;
    public static int debugRenderLevel = 0;
    public static int debugAnimationLevel = 0;

//		public static int debugLevel = 0;
//		public static int debugJavascriptLevel = 0;
//		public static int debugParserLevel = 0;
//		public static int debugRenderLevel = 0;
//		public static int debugAnimationLevel = 0;



    public enum LOOTSIE_LOGGING_LEVEL {
        DISABLED, VERBOSE,

        ENABLE_BASIC_LOGGING,
        ENABLE_JAVASCRIPT_LOGGING,
        ENABLE_PARSER_LOGGING,
        ENABLE_RENDER_LOGGING,
        ENABLE_ANIMATION_LOGGING,
        ENABLE_NETWORK_LOGGING,

        DISABLE_BASIC_LOGGING,
        DISABLE_JAVASCRIPT_LOGGING,
        DISABLE_PARSER_LOGGING,
        DISABLE_RENDER_LOGGING,
        DISABLE_ANIMATION_LOGGING,
        DISABLE_NETWORK_LOGGING;
        //; is required here.

        @SuppressLint("DefaultLocale")
        @Override public String toString() {
            //only capitalize the first letter
            String s = super.toString();
            return s.substring(0, 1) + s.substring(1).toLowerCase();
        }
    }



    public static void setLogLevel(LOOTSIE_LOGGING_LEVEL loglevel) {

        switch (loglevel) {
            case DISABLED:
                debugLevel = 0;
                debugNetworkLevel = 0;
                debugJavascriptLevel = 0;
                debugParserLevel = 0;
                debugRenderLevel = 0;
                debugAnimationLevel = 0;
                break;
            case VERBOSE:
                debugLevel = 1;
                //debugLevel = 2;
                debugNetworkLevel = 1;
                debugJavascriptLevel = 1;
                debugParserLevel = 1;
                debugRenderLevel = 1;
                debugAnimationLevel = 1;
                break;
            case ENABLE_BASIC_LOGGING:
                debugLevel = 1;
                break;
            case ENABLE_JAVASCRIPT_LOGGING:
                debugJavascriptLevel = 1;
                break;
            case ENABLE_PARSER_LOGGING:
                debugParserLevel = 1;
                break;
            case ENABLE_RENDER_LOGGING:
                debugRenderLevel = 1;
                break;
            case ENABLE_ANIMATION_LOGGING:
                debugAnimationLevel = 1;
                break;
            case ENABLE_NETWORK_LOGGING:
                debugNetworkLevel = 1;
                break;


            case DISABLE_BASIC_LOGGING:
                debugLevel = 0;
                break;
            case DISABLE_JAVASCRIPT_LOGGING:
                debugJavascriptLevel = 0;
                break;
            case DISABLE_PARSER_LOGGING:
                debugParserLevel = 0;
                break;
            case DISABLE_RENDER_LOGGING:
                debugRenderLevel = 0;
                break;
            case DISABLE_ANIMATION_LOGGING:
                debugAnimationLevel = 0;
                break;
            case DISABLE_NETWORK_LOGGING:
                debugNetworkLevel = 0;
                break;


            default:
                break;
        }

    }

    public static boolean getSequenceLogging() {

        return LootsieGlobals.SEQUENCE_LOGGING;
    }
    /**
     * Prints the sequence diagram with arrows and messages between
     * Webapp and SDK
     * @param TAG
     * @param str
     */
    public static void printSequence(String TAG,String str)
    {
        Logs.e(TAG,str);
    }


}
