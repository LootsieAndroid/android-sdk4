package com.lootsie.sdk.viewcontrollers;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;

import android.graphics.Rect;
import android.os.Build;
import android.os.Looper;
import android.view.*;

import android.widget.*;
import android.widget.LinearLayout.LayoutParams;

import android.content.Context;
import android.content.Intent;

import android.os.Handler;


import com.lootsie.sdk.R;
import com.lootsie.sdk.lootsiehybrid.LOOTSIE_NOTIFICATION_CONFIGURATION;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.lootsiehybrid.sequences.RewardedVideoSequence;
import com.lootsie.sdk.model.Achievement;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.utils.RUtil;
import com.lootsie.sdk.utils.FontUtil;

import com.lootsie.sdk.viewcontrollers.base.BaseNotificationManager;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by jerrylootsie on 2/13/15.
 */
public class NotificationManager extends BaseNotificationManager {

    private static String TAG = "NotificationManager";

    private static ArrayList<View> mViewQueue = new ArrayList<View>();
    private static ArrayList<ViewGroup> mViewGroupQueue = new ArrayList<ViewGroup>();

    private static ViewGroup activityViewGroup = null;

    private static int mWidth = 0;
    private static int mHeight = 0;

    private static final String ERROR_CONTEXTNOTACTIVITY = "The Context that you passed was not an Activity!";

    private static final Animator.AnimatorListener popupDismissListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animation) {}

        @Override
        public void onAnimationEnd(Animator animation) {
            removePopup();
        }

        @Override
        public void onAnimationCancel(Animator animation) {}

        @Override
        public void onAnimationRepeat(Animator animation) {}
    };

    public NotificationManager(Activity context) {
        this.mContext = context;
//        LootsieEngine.getInstance().setActivityContext(context);
    }

    @Override
    public void updateContext(Activity context) {
        this.mContext = context;
//        LootsieEngine.getInstance().setActivityContext(context);
    }

    @Override
    public void showNotification(String title, String message) {
        DebugLog("showNotification: title: %s message: %s", title, message);

        final View pop = LayoutInflater.from(mContext).inflate(RUtil.getLayoutId(mContext,
                "com_lootsie_notification"), null);
        FontUtil.applyFont(pop, mContext);

        TextView tv = (TextView) pop.findViewById(RUtil.getId(mContext, "achievementTitle"));
        tv.setText(title);

        tv = (TextView) pop.findViewById(RUtil.getId(mContext, "achievementText"));
        tv.setVisibility(View.VISIBLE);
        tv.setText(message);

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) tv.getLayoutParams();
        params.setMargins(0, 10, 0, 0); //substitute parameters for left, top, right, bottom
        tv.setLayoutParams(params);

        Timer timer = new Timer();

        // from PopupManager
        showPopup(mContext, pop, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DebugLog("showNotification(title,message): showPopup: OnClickListener");

                LootsieEngine.getInstance().getCallback().onLootsieBarExpanded();

                LOOTSIE_NOTIFICATION_CONFIGURATION notifyConfig = LootsieEngine.getInstance()
                        .getNotificationConfiguration();
                DebugLog("showNotification(title,message): showPopup: OnClickListener: " +
                        "LOOTSIE_NOTIFICATION_CONFIGURATION: %s", notifyConfig.toString());

                if (notifyConfig != LOOTSIE_NOTIFICATION_CONFIGURATION.notify_to_customPage) {

                    Intent intent = new Intent(mContext, LootsiePageBorderActivity.class);
                    //                Bundle extras = new Bundle();
                    //                extras.putString("LOOTSIE_NOTIFICATION_CONFIGURATION",notifyConfig.toString());
                    //                intent.putExtras(extras);
                    //intent.putExtra("LOOTSIE_NOTIFICATION_CONFIGURATION",notifyConfig);
                    intent.putExtra("LOOTSIE_NOTIFICATION_CONFIGURATION", notifyConfig.toString());


                    mContext.startActivity(intent);
                }

            }
        }, timer);

    }

    @Override
    public void showNotification(Achievement achievement, int lpEarned, final Boolean isVideo) {
        DebugLog("showNotification: achievement: %s", achievement.name);

        final View pop = LayoutInflater.from(mContext).inflate(R.layout.com_lootsie_notification_view, null);
        FontUtil.applyFont(pop, mContext);

        TextView tv = (TextView) pop.findViewById(R.id.achievementPointText);
        if (tv != null) {
            tv.setText(String.valueOf(lpEarned));
        } else {
            Logs.e(TAG, "showNotification: ian pointsText not found!");
        }

        tv = (TextView) pop.findViewById(R.id.achievementTitleText);
        if (tv != null) {
            tv.setText(achievement.name);
        } else {
            Logs.e(TAG, "showNotification: ian achievementTitle not found!");
        }

        tv = (TextView) pop.findViewById(R.id.achievementDescriptionText);
        if (tv != null) {
            tv.setText(achievement.description);
        } else {
            Logs.e(TAG, "showNotification: ian text not found!");
        }

        final Timer timer = new Timer();

        LootsieEngine.getInstance().getVideoAdProvider().setRewardData(
                RewardedVideoSequence.VIDEO_REWARD_CONTEXT.IN_APP_NOTIFICATION, achievement.id);

        Button rewardsButton = (Button) pop.findViewById(R.id.rewardsButton);
        rewardsButton.setText(isVideo ? R.string.watch_now_button : R.string.my_rewards_button);
        rewardsButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(isVideo) {
                            if (VideoRewardHelper.isWatchedTodayLimitReached(pop.getContext())) {
                                VideoRewardHelper.showReachedPopup((Activity) pop.getContext());
                            } else {
                                // show available ads
                                LootsieEngine.getInstance().getVideoAdProvider().show();
                            }

                        } else {
                            LootsieEngine.getInstance().getCallback().onLootsieBarExpanded();

                            LOOTSIE_NOTIFICATION_CONFIGURATION notifyConfig = LootsieEngine.getInstance()
                                    .getNotificationConfiguration();
                            DebugLog("showNotification: showPopup: OnClickListener: LOOTSIE_NOTIFICATION_CONFIGURATION: %s",
                                    notifyConfig.toString());

                            if (notifyConfig != LOOTSIE_NOTIFICATION_CONFIGURATION.notify_to_customPage) {

                                Activity a = (Activity) LootsieEngine.getInstance().getActivityContext();
                                LootsieEngine.showRewardsPage(a);
                            }
                        }
                        if (pop.getParent() != null) {
                            timer.cancel();
                            pop.clearAnimation();
                            pop.animate()
                                    .y(-pop.getHeight())
                                    .setDuration(300)
                                    .setListener(popupDismissListener)
                                    .start();
                        }
                    }
                }
        );

        pop.findViewById(R.id.closeButton).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (pop.getParent() != null) {
                            timer.cancel();
                            pop.clearAnimation();
                            pop.animate()
                                    .y(-pop.getHeight())
                                    .setDuration(300)
                                    .setListener(popupDismissListener)
                                    .start();
                        }
                    }
                }
        );

        // from PopupManager
        showPopup(mContext, pop, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DebugLog("showNotification: showPopup: OnClickListener");

                LootsieEngine.getInstance().getCallback().onLootsieBarExpanded();

                LOOTSIE_NOTIFICATION_CONFIGURATION notifyConfig = LootsieEngine.getInstance()
                        .getNotificationConfiguration();
                DebugLog("showNotification: showPopup: OnClickListener: LOOTSIE_NOTIFICATION_CONFIGURATION: %s",
                        notifyConfig.toString());

                if (notifyConfig != LOOTSIE_NOTIFICATION_CONFIGURATION.notify_to_customPage) {

                    Activity a = (Activity) LootsieEngine.getInstance().getActivityContext();
                    LootsieEngine.showRewardsPage(a);
                }

            }
        }, timer);
    }


    private static void removePopup() {

        try {
            DebugLog("removePopup: mViewQueue: %d mViewGroupQueue: %d", mViewQueue.size(), mViewGroupQueue.size());

            if (mViewQueue.size() > 0) {
                if (mViewGroupQueue.size() > 0) {

                    ViewGroup tempViewGroup = mViewGroupQueue.get(0);
                    mViewGroupQueue.remove(0);

                    View currentView = mViewQueue.get(0);
                    mViewQueue.remove(0);
                    if ((currentView != null) && (currentView.isShown())) {
                        //activityViewGroup.removeView(currentView);
                        tempViewGroup.removeView(currentView);
                    }

                    activityViewGroup.removeView(tempViewGroup);
                }
            }
        } catch (Exception e) {
            Logs.e(TAG, "PopupViewManager: removePopup: removeView: exception: " + e.getMessage());
            e.printStackTrace();
        }


    }


    @SuppressLint("NewApi")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private static void getDisplayDimensions_HoneyComb(Display display) {
        Rect displaySizeRect = new Rect();
        display.getRectSize(displaySizeRect);

        mWidth = displaySizeRect.width();
        mHeight = displaySizeRect.height();
    }

    private static void showPopup(final Context context, final View pop, final View.OnClickListener l,
                                  final Timer timer) {
        //check context
        if (context == null) {
            Logs.e(TAG, "showPopup: invalid context!");
            return;
        }
        if (!(context instanceof Activity)) {
            throw new IllegalArgumentException(ERROR_CONTEXTNOTACTIVITY);
        }

        DebugLog("showPopup");

        // lookup resource from string
        // RUtil.getLayoutId -> getResourceId -> context.getResources().getIdentifier
        // lookup string from resourceid?
//        ((LootsieImpl) Lootsie.getInstance()).getCallback().onNotification("APPEAR:" +
//                context.getResources().getResourceEntryName(pop.getId()));

        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            getDisplayDimensions_HoneyComb(display);
        } else {
            mWidth = display.getWidth();
            mHeight = display.getHeight();
        }

//        final int mWidth = (int) context.getResources().getDimension(RUtil.getDimenId(context, "notification_width"));
//        int height = (int) context.getResources().getDimension(RUtil.getDimenId(context, "notification_height"));
//        final ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams((int) mWidth, pop.getHeight());

        //View rootView = ((Activity) context).getParent().getWindow().getDecorView().findViewById(android.R.id
        // .content);
        //activityViewGroup = (ViewGroup) ((ViewGroup) ((Activity) context).findViewById(android.R.id.content))
        // .getChildAt(0);
        activityViewGroup = (ViewGroup) ((Activity) context).findViewById(android.R.id.content);
//        activityViewGroup = (ViewGroup) ((Activity) context).findViewById(R.id.content);

        // if there is another popup there presently, remove it!
        if (mViewQueue.size() > 0) {
            ((Activity) context).runOnUiThread(
                    new Runnable() {
                        public void run() {
                            DebugLog("NotificationManager: removeView:");
                            removePopup();
                        }
                    });
        }


        try {
            ((Activity) context).runOnUiThread(
                    new Runnable() {
                        public void run() {
                            //stuff that updates ui
                            try {
                                DebugLog("showPopup: addView: mWidth: %d", mWidth);

                                // activityViewGroup is a framelayout
                                // all children of framelayout are pegged to the top left corner :(
                                // so I make sure lootsie_notification is inside a relativelayout which expands to
                                // fill width
                                RelativeLayout viewGroupRelativeLayout = new RelativeLayout(context);
                                RelativeLayout.LayoutParams viewGroupRelLayLP = new RelativeLayout.LayoutParams(
                                        mWidth,
                                        LayoutParams.WRAP_CONTENT);
                                viewGroupRelLayLP.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
                                viewGroupRelLayLP.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
                                viewGroupRelLayLP.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
                                viewGroupRelativeLayout.setLayoutParams(viewGroupRelLayLP);

                                RelativeLayout.LayoutParams popLP = new RelativeLayout.LayoutParams(
                                        mWidth,
                                        LayoutParams.WRAP_CONTENT);
                                popLP.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
                                popLP.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
                                popLP.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);

                                pop.setLayoutParams(popLP);
                                pop.setMinimumWidth(mWidth);  // actually does something, but messes up alignment!
                                viewGroupRelativeLayout.addView(pop);


                                activityViewGroup.addView(viewGroupRelativeLayout, viewGroupRelLayLP);

                                mViewGroupQueue.add(viewGroupRelativeLayout);
                                mViewQueue.add(pop);

                                pop.getViewTreeObserver().addOnGlobalLayoutListener(
                                        new ViewTreeObserver.OnGlobalLayoutListener() {
                                            @Override
                                            public void onGlobalLayout() {
                                                if (Build.VERSION.SDK_INT < 16) {
                                                    pop.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                                                } else {
                                                    pop.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                                }
                                                pop.clearAnimation();
                                                pop.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                                pop.setY(-pop.getHeight());
                                                pop.animate().y(0).setDuration(300).withEndAction(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        timer.schedule(new TimerTask() {
                                                            @Override
                                                            public void run() {
                                                                ((Activity) context).runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        pop.clearAnimation();
                                                                        pop.animate()
                                                                                .y(-pop.getHeight())
                                                                                .setDuration(300)
                                                                                .setListener(popupDismissListener)
                                                                                .start();
                                                                    }
                                                                });
                                                            }
                                                        }, 5000);
                                                    }
                                                })
                                                        .start();

                                            }
                                        });


                            } catch (Exception e) {
                                Logs.e(TAG, "NotificationManager: showPopup: addView: exception: " + e.getMessage());
                                e.printStackTrace();
                            }
                        }
                    });


        } catch (Exception e) {
            Logs.e(TAG, "NotificationManager: showPopup: addView: exception: " + e.getMessage());
            e.printStackTrace();
        }

//        final int delay = 5000; // DBG //3000; // milliseconds
//        //delay = PreferencesHandler.getInt("application", Constants.SDK_UI_ACHIEVEMENTS_HIDE_DELAY, 10000, context);
//        final Handler h = new Handler();
//
//        final Runnable animDisappearCallback = new Runnable() {
//            @Override
//            public void run() {
//                ((Activity) context).runOnUiThread(
//                        new Runnable() {
//                            public void run() {
//                                if (pop.getParent() != null) {
//                                    pop.animate()
//                                            .y(-pop.getHeight())
//                                            .setDuration(300)
//                                            .setListener(popupDismissListener)
//                                            .start();
//                                }
//                            }
//                        });
//            }
//        };
//
//        h.postDelayed(animDisappearCallback, delay);
//
//        try {
//            pop.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (LootsieGlobals.debugLevel > 0) {
//                        Logs.v(TAG, "NotificationManager: showPopup: onClick");
//                    }
//
//                    if (l != null) {
//                        l.onClick(v);
//                    }
//
//                    h.removeCallbacks(animDisappearCallback);
//                    if (pop.getParent() != null) {
//                        pop.animate()
//                                .y(-pop.getHeight())
//                                .setDuration(300)
//                                .setListener(popupDismissListener)
//                                .start();
//                    }
//                }
//            });
//        } catch (Exception e) {
//            Logs.e(TAG, "NotificationManager: showPopup: setOnClickListener: exception: " + e.getMessage());
//            e.printStackTrace();
//        }
    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     *
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }
}
