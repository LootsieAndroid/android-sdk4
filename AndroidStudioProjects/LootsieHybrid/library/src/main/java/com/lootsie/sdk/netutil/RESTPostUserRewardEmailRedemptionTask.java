package com.lootsie.sdk.netutil;

import android.os.AsyncTask;
import android.util.Log;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.lootsiehybrid.LootsieApi;
import com.lootsie.sdk.net.RestClient;
import com.lootsie.sdk.utils.LootsieGlobals;

/**
 * Created by jerrylootsie on 2/20/15.
 */
//public class RESTPostUserRewardEmailRedemptionTask extends AsyncTask<String, Void, RestResult> {
public class RESTPostUserRewardEmailRedemptionTask extends AsyncTask<String, Void, RestResult> implements IGenericAsyncTask<String> {

    private final static String TAG = "Lootsie.RESTPostUserRewardEmailRedemptionTask";

    IRESTCallback callback = null;

    public RESTPostUserRewardEmailRedemptionTask() {}

    public RESTPostUserRewardEmailRedemptionTask(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    protected RestResult doInBackground(String... params) {
        if (params == null || params.length == 0) {
            return null;
        }

        RestResult response = new RestResult();
        try {
            // pull stuff from Device.java
            //Map<String, Object> map = params[0];

            response = RestClient.doPost(LootsieApi.USER_REWARD_EMAIL_REDEMPTION_URL.getUri(), params[0]);

            if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "RESTPostUserRewardEmailRedemptionTask:" + response.content);

        } catch (Exception e) {
            Log.e(TAG, "RESTPostUserRewardEmailRedemptionTask: processError: getMessage: " + e.getMessage());
        }
        return response;
    }



    @Override
    protected void onPostExecute(final RestResult restResult) {
        if (callback != null) {
            callback.restResult(restResult);
        }
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    public void executeTask(String... params) {
        execute(params);
    }
}
