package com.lootsie.sdk.netutil;

import android.os.AsyncTask;
//import android.util.Log;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.lootsiehybrid.LootsieApi;
import com.lootsie.sdk.net.RestClient;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

/**
 * Created by jerrylootsie on 2/23/15.
 */
//public class RESTPostUserLocationTask extends AsyncTask<String, Void, RestResult> {
public class RESTPostUserLocationTask extends AsyncTask<String, Void, RestResult> implements IGenericAsyncTask<String> {

    private final static String TAG = "Lootsie.RESTPostUserLocationTask";

    @Override
    protected RestResult doInBackground(String... params) {
        if (params == null || params.length == 0) {
            Logs.w(TAG,"RESTPostUserLocationTask: Warning: no latlng value to post!");
            return null;
        }

        RestResult response = new RestResult();
        try {
            // pull stuff from Device.java
            //Map<String, Object> map = params[0];

            response = RestClient.doPost(LootsieApi.USER_LOCATION_URL.getUri(), params[0]);

            if (LootsieGlobals.debugLevel > 0) Logs.v(TAG, "RESTPostUserLocationTask:" + response.content);

        } catch (Exception e) {
            Logs.e(TAG, "RESTPostUserLocationTask: processError: getMessage: " + e.getMessage());
        }
        return response;
    }



    @Override
    protected void onPostExecute(final RestResult restResult) {
        // should I do something here?

    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        // no callback setup for this class
    }

    @Override
    public void executeTask(String... params) {
        execute(params);
    }
}