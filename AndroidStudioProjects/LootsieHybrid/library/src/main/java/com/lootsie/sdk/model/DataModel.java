package com.lootsie.sdk.model;

import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.PreferencesHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jerrylootsie on 2/12/15.
 *
 * A generic class to hold references to other data structures used in initialization
 */
public class DataModel {

    public static String apiSessionToken = null;
    public static String userSessionToken = null;

    public static String currency_name = null;
    public static String currency_abbreviation = null;

    public static App app = null;
//    public static UserAchievements userAchievements = null;
    public static User user = null;
    public static UserRewards userRewards = null;

    // record when updates to various REST api endpoints were made
    private static HashMap<String,NetworkUseEntry> networkStats = null;
    public static boolean networkCacheEnabled = true;

    static class NetworkUseEntry {
        public int counter = 0;
        public Long timestamp = 0L;
        public int cacheHits = 0;

        public JSONObject toJSONObj() throws JSONException {
            JSONObject networkUseJsonObj = new JSONObject();
            networkUseJsonObj.put("timestamp", this.timestamp);
            networkUseJsonObj.put("counter", this.counter);
            networkUseJsonObj.put("cacheHits", this.cacheHits);

            return networkUseJsonObj;
        }

        public void extractDataFromJsonObj(JSONObject networkUseJsonObj) throws JSONException {

            this.timestamp = networkUseJsonObj.getLong("timestamp");
            this.counter = networkUseJsonObj.getInt("counter");
            this.cacheHits = networkUseJsonObj.getInt("cacheHits");

        }
    }


    private static String TAG = "Lootsie DataModel";

    public DataModel() {
    }

    private static DataModel dataModelInstance = null;

    public static void init() {
        if (dataModelInstance == null) {
            dataModelInstance = new DataModel();
        }

        dataModelInstance.networkStats = new HashMap<String, NetworkUseEntry>();
        restoreNetworkStats();
    }

    // determine if it has been too long since the last network update
    public static boolean updateNetwork(String networkCallKey) {

        return updateNetwork(networkCallKey, false);

    }

    public static boolean updateNetworkForced(String networkCallKey) {

        return updateNetwork(networkCallKey, true);

    }

    private static boolean updateNetwork(String networkCallKey, boolean forced) {
        if (dataModelInstance.networkStats == null) {
            DebugLog("updateNetwork: networkStats is null!");
            networkStats = new HashMap<String, NetworkUseEntry>();
            restoreNetworkStats();
        }

        long currentTimeSec = System.currentTimeMillis() / 1000L;

        if (forced) {

            NetworkUseEntry currentUpdate = null;
            if (!networkStats.containsKey(networkCallKey)) {
                currentUpdate = new NetworkUseEntry();
            } else {
                currentUpdate = DataModel.networkStats.get(networkCallKey);
            }

            currentUpdate.counter +=1;
            currentUpdate.timestamp = currentTimeSec;
            DataModel.networkStats.put(networkCallKey, currentUpdate);

            DebugLog("updateNetwork: " + networkCallKey + " true bc forced (count=" + currentUpdate.counter + ")");

            return true;

        }

        if (networkStats.containsKey(networkCallKey) && networkCacheEnabled) {
            // check to see if we have posted within time period

            NetworkUseEntry lastUpdate = DataModel.networkStats.get(networkCallKey);
            long lastUpdateTimeSec = lastUpdate.timestamp;
            long elapsedTime = currentTimeSec - lastUpdateTimeSec;
            //if (elapsedTime > LootsieGlobals.NETWORK_CACHE_TIME_SECONDS) {
            if (elapsedTime > LootsieEngine.getInstance().getCacheDuration()) {
                lastUpdate.counter += 1;
                lastUpdate.timestamp = currentTimeSec;
                DataModel.networkStats.put(networkCallKey, lastUpdate);

                DebugLog(TAG, "updateNetwork: " + networkCallKey + " true bc elapsedTime: " + elapsedTime);
                return true;
            } else {
                // not enough time has elapsed yet, but update the counter?
                lastUpdate.cacheHits += 1;
                DataModel.networkStats.put(networkCallKey, lastUpdate);

                DebugLog("updateNetwork: " + networkCallKey + " false");
                return false;
            }
        } else {
            NetworkUseEntry currentUpdate = null;
            if (!networkStats.containsKey(networkCallKey)) {
                currentUpdate = new NetworkUseEntry();
            } else {
                currentUpdate = DataModel.networkStats.get(networkCallKey);
            }

            currentUpdate.counter +=1;
            currentUpdate.timestamp = currentTimeSec;
            DataModel.networkStats.put(networkCallKey, currentUpdate);

            if (!networkCacheEnabled) {
            	DebugLog("updateNetwork: " + networkCallKey + " true bc network cache is disabled (count=" + currentUpdate.counter + ")");
            } else {
            	DebugLog("updateNetwork: " + networkCallKey + " true bc key missing (count=" + currentUpdate.counter + ")");
            }

            return true;
        }
    }

    public static void saveNetworkStats() {

        String networkStatsJsonStr = "";

        JSONObject jsonObj = new JSONObject();

        try {
//            JSONArray jsonArr = new JSONArray();

            for (HashMap.Entry<String, NetworkUseEntry> entry : networkStats.entrySet()) {
                NetworkUseEntry networkEntry = entry.getValue();
                DebugLog("saveNetworkStats: Key = " + entry.getKey() +
                        " timestamp: " + networkEntry.timestamp +
                        " counter: " + networkEntry.counter +
                        " cacheHits: " + networkEntry.cacheHits);

                JSONObject networkUseJsonObj = networkEntry.toJSONObj();

                jsonObj.put(entry.getKey(),networkUseJsonObj);
            }

            networkStatsJsonStr = jsonObj.toString();

        } catch (JSONException ex) {
            Logs.e(TAG, "saveNetworkStats: exception: " + ex.getMessage());
        }

        // save user session token somewhere it can be restored from
        PreferencesHandler.saveString(LootsieGlobals.KEY_NETWORK_STATS, networkStatsJsonStr, LootsieEngine.getInstance().getApplicationContext());


    }

    public static ArrayList<String> getNetworkStats() {

        ArrayList<String> netStatList = new ArrayList<String>();

        for (HashMap.Entry<String, NetworkUseEntry> entry : networkStats.entrySet()) {
            NetworkUseEntry networkEntry = entry.getValue();


            String tempStr = entry.getKey() +
                   " counter: " + networkEntry.counter +
                   " cacheHits: " + networkEntry.cacheHits;

            DebugLog("getNetworkStats: " + tempStr);

            netStatList.add(tempStr);
        }

        return netStatList;

    }

    public static void restoreNetworkStats() {

        if (networkStats == null) {
        	DebugLog("restoreNetworkStats: networkStats is null!");
            networkStats = new HashMap<String, NetworkUseEntry>();
        }

        DebugLog("restoreNetworkStats:");

        // convert this stuff from a json string stored in preferences
        String networkStatsJsonStr = PreferencesHandler.getString(LootsieGlobals.KEY_NETWORK_STATS, LootsieEngine.getInstance().getApplicationContext());
        if (networkStatsJsonStr != null) {
            try {
                JSONObject jsonObj = new JSONObject(networkStatsJsonStr);

                for (int i = 0; i<jsonObj.names().length(); i++){
                    String name = jsonObj.names().getString(i);


                    JSONObject networkUseJsonObj = jsonObj.getJSONObject(name);

                    NetworkUseEntry networkUseEntry = new NetworkUseEntry();
                    networkUseEntry.extractDataFromJsonObj(networkUseJsonObj);


                    DebugLog("restoreNetworkStats: key = " + name +
                            " timestamp: " + networkUseEntry.timestamp +
                            " counter: " + networkUseEntry.counter +
                            " cacheHits: " + networkUseEntry.cacheHits);

                    networkStats.put(name, networkUseEntry);
                }

            } catch (JSONException ex) {
                Logs.e(TAG, "restoreNetworkStats: exception: " + ex.getMessage());
            }
        } else {
            Logs.e(TAG, "restoreNetworkStats: error key is missing from PreferencesHandler!");
        }

    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private static void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugNetworkLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }

}
