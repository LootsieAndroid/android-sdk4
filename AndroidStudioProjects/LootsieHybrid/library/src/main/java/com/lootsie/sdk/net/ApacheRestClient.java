package com.lootsie.sdk.net;


import android.util.Log;

import com.lootsie.sdk.utils.LootsieGlobals;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import java.security.KeyStore;

/**
 * Created by jerrylootsie on 2/9/15.
 */

/**
 * RestClient
 *
 * @author Alexander Salnikov
 */
public class ApacheRestClient {

    public static final int SERVER_PORT = 443;


    //private static String apiIntegrationUrl = LootsieApi.BASE_API_URL;
    private static String apiIntegrationUrl = "";
    private static int mMaxRetries = 5;
    private static HashMap<String, Header> headers = new HashMap<String, Header>(2);
    private static DefaultHttpClient mHttpClient = getNewHttpClient();

//    private static HashMap<String, String> headersMap = new HashMap<String, String>(2);


    private static String TAG = ApacheRestClient.class.getName();

    // http://forums.androidcentral.com/google-nexus-5/425589-problems-sending-receiving-sms-t-mobile.html
    // Regrettably, T-Mobile does not grant any third-party messaging apps access to the T-Mobile WiFi Calling feature for MMS. T-Mobile use something called GBA authentication which only system apps or apps digitally signed by T-Mobile can use (so not Textra).
    // On Galaxy Note ( I believe it is T-mobile version ), T-mobile modified APACHE HTTP Client to include GBA Client for Authentication over WiFi especially for MMS over WiFi.

    public static Object doPost(String path, Map<String, Object> params) throws ResponseException {
        int retries = 0;

        //url with the post data
        HttpPost httpost = new HttpPost(apiIntegrationUrl + path);

        if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "RestClient: doPost: " + apiIntegrationUrl + path);

        JSONObject holder;
        BufferedReader buf = null;
        try {
            if (params != null) {
                //convert parameters into JSON object
                holder = getJsonObjectFromMap(params);
                //passes the results to a string builder/entity
                StringEntity se = new StringEntity(holder.toString());

                if (LootsieGlobals.debugLevel > 0) {
                    Log.v(TAG, "RestClient: doPost: params: " + holder.toString());
                }

                //sets the post request as the resulting string
                httpost.setEntity(se);
            }

            //sets a request header so the page receving the request
            //will know what to do with it
            httpost.setHeader("Accept", "application/json");
            httpost.setHeader("Content-type", "application/json");

            httpost.setHeader("User-Agent", String.format("%s %s %s %s %s",
                    android.os.Build.MANUFACTURER,
                    android.os.Build.MODEL,
                    android.os.Build.PRODUCT,
                    android.os.Build.CPU_ABI,
                    "Lootsie"));


            for (Header header : headers.values()) {
                httpost.setHeader(header);

                if (LootsieGlobals.debugLevel > 0) {
                    Log.v(TAG, "RestClient: doPost: header: " + header.getName() + " " + header.getValue());
                }

            }

            HttpResponse response = null;
            Exception requestException = null;
            while (retries < mMaxRetries && response == null) {
                try {
                    if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "HTTP request #" + (retries + 1) + " : " + apiIntegrationUrl);
                    response = mHttpClient.execute(httpost);
                } catch (HttpResponseException e) {
                    requestException = e;
                    response = null;
                } catch (IOException e) {
                    requestException = e;
                    response = null;
                } finally {
                    retries++;
                }
            }
            if (response == null) {
                ResponseException exception = new ResponseException(requestException);
                exception.code = ResponseException.NO_INTERNET_EXCEPTION;
                throw exception;
            }

            int statusCode = response.getStatusLine().getStatusCode();

            if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "HTTP response status code:" + statusCode);

            if (statusCode == 204) {
                return "";
            }

            InputStream ips = response.getEntity().getContent();
            buf = new BufferedReader(new InputStreamReader(ips, "UTF-8"));

            StringBuilder sb = new StringBuilder();
            String s;
            while (true) {
                s = buf.readLine();
                if (s == null || s.length() == 0) {
                    break;
                }
                sb.append(s);
            }

            String responseStr = sb.toString();
            if (LootsieGlobals.debugLevel > 1) Log.v(TAG, "HTTP response:" + responseStr);

            if (statusCode != 200) {
                throw new ResponseException(statusCode, responseStr);
            }

            return responseStr;
        } catch (JSONException e) {
            if (LootsieGlobals.debugLevel > 1) Log.v(TAG, "JSONException:" + e.toString());
            Log.e(TAG, e.toString());
            throw new ResponseException(e);
        } catch (UnsupportedEncodingException e) {
            if (LootsieGlobals.debugLevel > 1) Log.v(TAG, "UnsupportedEncodingException:" + e.toString());
            Log.e(TAG, e.toString());
            throw new ResponseException(e);
        } catch (IOException e) {
            if (LootsieGlobals.debugLevel > 1) Log.v(TAG, "IOException:" + e.toString());
            Log.e(TAG, e.toString());
            throw new ResponseException(e);
        } finally {
            if (buf != null) {
                try {
                    buf.close();
                } catch (IOException ignored) {
                }
            }
        }

    }

    private static JSONObject getJsonObjectFromMap(Map<String, Object> params) throws JSONException {
        //all the passed parameters from the post request
        //iterator used to loop through all the parameters
        //passed in the post request
        Iterator<Entry<String, Object>> iter = params.entrySet().iterator();

        //Stores JSON
        JSONObject holder = new JSONObject();

        while (iter.hasNext()) {
            Entry<String, Object> pairs = iter.next();
            if (pairs.getValue() instanceof Collection) {
                holder.put(pairs.getKey(), new JSONArray((Collection) pairs.getValue()));
            } else {
                holder.put(pairs.getKey(), pairs.getValue());
            }
        }

        return holder;
    }

    public static String doGet(String uri) throws ResponseException {
        int retries = 0;
        BufferedReader buf = null;

        try {
            HttpGet request = new HttpGet(apiIntegrationUrl + uri);

            if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "RestClient: doGet: " + apiIntegrationUrl + uri);

            //todo get from initApp method
            for (Header header : headers.values()) {
                if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "RestClient: doGet: header: " + header.getName() + " : " + header.getValue());

                request.setHeader(header);
            }
            StringBuilder sb = new StringBuilder();
            HttpResponse response = null;
            Exception requestException = null;
            while (retries < mMaxRetries && response == null) {
                try {
                    if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "HTTP request #" + (retries + 1) + " : " + apiIntegrationUrl);
                    response = mHttpClient.execute(request);
                } catch (HttpResponseException e) {
                    requestException = e;
                    response = null;
                } catch (IOException e) {
                    requestException = e;
                    response = null;
                } finally {
                    retries++;
                }
            }
            if (response == null) {
                ResponseException exception = new ResponseException(requestException);
                exception.code = ResponseException.NO_INTERNET_EXCEPTION;
                throw exception;
            }
            InputStream ips = response.getEntity().getContent();
            buf = new BufferedReader(new InputStreamReader(ips, "UTF-8"));

            String s;
            while (true) {
                s = buf.readLine();
                if (s == null || s.length() == 0) {
                    break;
                }
                sb.append(s);
            }
            return sb.toString();
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, e.toString());
            throw new ResponseException(e);
        } catch (ClientProtocolException e) {
            Log.e(TAG, e.toString());
            throw new ResponseException(e);
        } catch (IOException e) {
            Log.e(TAG, e.toString());
            throw new ResponseException(e);
        } finally {
            if (buf != null) {
                try {
                    buf.close();
                } catch (IOException ignored) {
                }
            }
        }

    }

    public static String doDelete(String uri) throws ResponseException {

        int retries = 0;
        BufferedReader buf = null;
        try {
            HttpDelete request = new HttpDelete(apiIntegrationUrl + uri);

            if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "RestClient: doDelete: " + apiIntegrationUrl + uri);

            //todo get from initApp method
            for (Header header : headers.values()) {
                if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "RestClient: doDelete: header: " + header.getName() + " : " + header.getValue());

                request.setHeader(header);
            }
            StringBuilder sb = new StringBuilder();
            HttpResponse response = null;
            Exception requestException = null;
            while (retries < mMaxRetries && response == null) {
                try {
                    if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "HTTP request #" + (retries + 1) + " : " + apiIntegrationUrl);
                    response = mHttpClient.execute(request);
                } catch (HttpResponseException e) {
                    requestException = e;
                    response = null;
                } catch (IOException e) {
                    requestException = e;
                    response = null;
                } finally {
                    retries++;
                }
            }
            if (response == null) {
                ResponseException exception = new ResponseException(requestException);
                exception.code = ResponseException.NO_INTERNET_EXCEPTION;
                throw exception;
            }

            int statusCode = response.getStatusLine().getStatusCode();

            if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "HTTP response status code:" + statusCode);

            if (statusCode == 204) {
                return "";
            }

            // need to test getentity, and then getcontent, if I call getcontent twice, the content is gone!
            if (response.getEntity() != null) {
                InputStream ips = response.getEntity().getContent();
                if (ips != null) {
                    buf = new BufferedReader(new InputStreamReader(ips, "UTF-8"));

                    String s;
                    while (true) {
                        s = buf.readLine();
                        if (s == null || s.length() == 0) {
                            break;
                        }
                        sb.append(s);
                    }
                    return sb.toString();
                } else {
                    Log.v(TAG, "HTTP response invalid content in response entity");
                    return "";
                }
            } else {
                Log.v(TAG, "HTTP response invalid entity in response");
                return "";
            }
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, e.toString());
            throw new ResponseException(e);
        } catch (ClientProtocolException e) {
            Log.e(TAG, e.toString());
            throw new ResponseException(e);
        } catch (IOException e) {
            Log.e(TAG, e.toString());
            throw new ResponseException(e);

        } finally {
            if (buf != null) {
                try {
                    buf.close();
                } catch (IOException ignored) {
                }
            }
        }

    }


    public static void addHeader(String header, String value) {
        headers.put(header, new BasicHeader(header, value));
//        headersMap.put(header, value);
    }

    public static void clearSessionToken() {
        headers.remove(Headers.USER_TOKEN);
    }

    public static Header getHeader(String name) {
        if (name == null) return null;
        return headers.get(name);
    }

    private static DefaultHttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            SSLSocketFactory sf = new LSSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
            int timeoutConnection = 5000;
            HttpConnectionParams.setConnectionTimeout(params, timeoutConnection);
            HttpConnectionParams.setSoTimeout(params, timeoutConnection);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, SERVER_PORT));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }

    public static void setTimeout(int timeout) {
        if (LootsieGlobals.debugLevel > 0) Log.v(TAG, "Set timeout: " + timeout/1000 + "s");
        HttpConnectionParams.setConnectionTimeout(mHttpClient.getParams(), timeout);
    }

    public static void setMaxRetries(int maxRetries) {
        mMaxRetries = maxRetries;
    }

    public static void setApiIntegrationUrl(String uri) {
        if ((uri == null) || (uri.length() == 0)) {
        	Log.e(TAG, "RestClient : setApiIntegrationUrl ERROR: apiIntegrationUrl is invalid!");
        } else {
        	apiIntegrationUrl = uri;
        }
    }
}

