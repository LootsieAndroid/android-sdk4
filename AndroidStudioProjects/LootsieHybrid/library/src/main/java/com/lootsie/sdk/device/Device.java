package com.lootsie.sdk.device;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import com.lootsie.sdk.lootsiehybrid.LootsieEngine;

//import com.lootsie.LootsieEngine;
//import com.lootsie.LootsieManager;
import com.lootsie.sdk.utils.LootsieGlobals;
import com.lootsie.sdk.utils.Logs;

import java.util.HashMap;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings.Secure;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Surface;
import android.view.WindowManager;

public class Device {

    public static final String TAG = "LootsieDevice";

    final String PLATFORM_DEFAULT = "Android";
    final String COUNTRY_DEFAULT = "US";
    final String LANGUAGE_DEFAULT = "English";

    private String platform ;
    private String device;
    private String firmware;
    private String language;
    private String country;
    private String idfa;
    private String idfv;

    private int screenWidth;
    private int screenHeight;

    // The screen density expressed as dots-per-inch
    private int screenDpi;

    // The logical density of the display.
    private float deviceDensity;

//    private LocationUpdates location = null;
    private LootsieEngine lootsieEngine;

    public Device(LootsieEngine inputLootsieEngine)
    {
        lootsieEngine = inputLootsieEngine;
        platform = PLATFORM_DEFAULT;
        device = "";
        firmware = "";
        language = LANGUAGE_DEFAULT;
        country = COUNTRY_DEFAULT;
        idfa = "";
        idfv = "";

        // ANDSDK-146 in-game-hook visibility on different screen sizes and
        // densities
        // pass screen width and screen height in pixels
        setDeviceDimensions();

        if (lootsieEngine != null) {
            screenDpi = lootsieEngine.getApp().getResources().getDisplayMetrics().densityDpi;
            deviceDensity = lootsieEngine.getApp().getResources().getDisplayMetrics().density;
        } else {
            Logs.e(TAG, "Device: Error lootsieEngine is null!");
        }

//        location = null;
    }

    @SuppressWarnings("unused")
    private float calculateDensity() {
        float density = lootsieEngine.getApp().getResources().getDisplayMetrics().density;

        // MDPI
        if(density >0.75 && density <=1.0)
            density = 1.0f;

            // MDPI
        else if(density >1.0 && density <= 1.25)
            density = 1.0f;

            // HDPI
        else if(density >1.25 && density <= 1.50)
            density = 1.5f;

            // XHDPI
        else if(density >1.70 && density <= 2.0)
            density = 2.0f;

            // XHDPI
        else if(density >2.0)
            density = 2.0f;

        return density;
    }

    // ASDK-466 - Crashing frequently on the LG phone running 4.0.3.
    // LG-P705 is an unsupported device

    /**
     * return true on supported device configuration
     * return false if device configuration is unsupported
     * @param locationSvcEnabled
     * @return
     */
    public boolean build(boolean locationSvcEnabled) {

        DebugLog("Device: build: locationSvcEnabled: %s", String.valueOf(locationSvcEnabled));

//        if (locationSvcEnabled) {
//            location = new LocationUpdates();
//        }

        device = android.os.Build.MODEL;
        firmware = android.os.Build.VERSION.RELEASE;
        language = Locale.getDefault().getLanguage();
        country = lootsieEngine.getApp().getResources()
                .getConfiguration().locale.getCountry();
        idfa = Secure.getString(lootsieEngine.getApp()
                .getContentResolver(), Secure.ANDROID_ID);

        WifiManager manager = (WifiManager) lootsieEngine
                .getApp().getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();
        idfv = info.getMacAddress();

        // add unsupported devices from flickster
        // http://flixster.desk.com/customer/portal/articles/264133-which-android-devices-do-you-support-

        // lookup with unsupported device list here
        // https://github.com/meetup/android-device-names/blob/master/android_models.properties
        // https://gist.github.com/jaredrummler/16ed4f1c14189375131d

        HashMap<String, Boolean> unsupportedDeviceHash = new HashMap<String, Boolean>() {{
            // ​Motorola Droid Razr Maxx
            put("obake-maxx", false);
            // Motorola Droid Razr HD
            put("DROID_RAZR_HD", false);
            // Motorola Droid Razr M
            put("XT907", false);
            // Motorola Photon
            put("MB855", false);
            // Motorola Electrify
            put("solstice", false);
            // LG Optimus G
            put("LG-E970", false);
            put("LG-LS970", false);
            // LG Venice
            put("LG-LG730", false);
            // LG Viper
            put("LG-LS840", false);
            // LG Motion
            put("LG-MS770", false);
            // LG Spirit ???
            put("c70n", false);
            put("my70", false);
            // LG Optimus L9
            put("LG-P705", false);
            put("LGMS769", false);
            put("LG-P760", false);
            put("LG-P768", false);
            put("LG-P769", false);
            // LG Escape ??
            put("l1a", false);
            // LG Nitro
            put("lgp930", false);
            // LG Spectrum II
            put("d1lv", false);
            // Kyocera Rise
            put("C5155", false);
            // Kyocera Hydro
            put("C5170", false);
            // ZTE Warp II
            put("N861", false);
            // ZTE Avid
            put("N9120", false);
            // Barnes and Noble Nook
            put("hummingbird", false);
            put("ovation", false);
            // Sony Xperia Z - ultra?
            put("C6833", false);
            // Dell Streak 7
            put("streak", false);
            // Lenovo IdeaTab

            // Huawei Ascend G300
            put("hwu8815", false);
            // Google TV
            put("cosmo", false);
            put("eden", false);
            // Acer Iconia Tablets
            put("A1-810", false);

            // HTC phones with OS below 4.3

        }};

        if (unsupportedDeviceHash.containsKey(device)) {
            return false;
        }

//        if (location != null) {
//            location.getCurrentLocation();
//        }

        return true;
    }

    private  void setDeviceDimensions() {

        if (lootsieEngine != null) {
            WindowManager wm = (WindowManager) lootsieEngine.getApp().getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();

            Point size = new Point();
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                display.getSize(size);
            }


            screenWidth = size.x;
            screenHeight = size.y;
        } else {
            Logs.e(TAG, "Device: setDeviceDimensions: Error lootsieEngine is null!");
        }

    }

    public int getScreenWidth() {
//		if (getOrientation() == Surface.ROTATION_0) {
//			// portrait
//			return screenWidth;
//		} else {
//			// landscape
//			return screenHeight;
//		}

        setDeviceDimensions();
        return screenWidth;

    }


    /**
     * Returns the screen DPI like ldpi,mdpi,hdpi
     * @return
     */
    public int getScreenDpi() {
        return screenDpi;
    }
    /**
     * Returns the logical density of the device. The logical density of the display.
     * This is a scaling factor for theDensity Independent Pixel unit, where one DIP is one pixel on an
     * approximately 160 dpi screen (for example a 240x320, 1.5"x2" screen),
     * providing the baseline of the system's display. Thus on a 160dpi screen
     * this density value will be 1; on a 120 dpi screen it would be .75; etc.
     *
     * @return
     */
    public float getDeviceDensity() {
        return deviceDensity;
    }


    public int getScreenHeight() {

//		if (getOrientation() == Surface.ROTATION_0) {
//			// portrait
//			return screenHeight;
//		} else {
//			// landscape
//			return screenWidth;
//		}

        setDeviceDimensions();
        return screenHeight;
    }


    @SuppressWarnings("unused")
    private int  calculateScreenDensity()
    {
        Context ctx = lootsieEngine.getApp();
        // find the device density .
        int deviceDensity = ctx.getResources().getDisplayMetrics().densityDpi;

        DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();

        screenDpi = (int) (metrics.density * deviceDensity);
        DebugLog("Device: Device densityDPI is %d ",deviceDensity);
        DebugLog("Device: Device density is  %f", ctx.getResources().getDisplayMetrics().density);
        DebugLog("Device: Screen DPi %d ", screenDpi);
        if (LootsieGlobals.debugLevel > 0) {
            switch(deviceDensity) {
                case DisplayMetrics.DENSITY_LOW:
                    DebugLog(TAG, "Device density is LDPI "+deviceDensity);
                    break;
                case DisplayMetrics.DENSITY_MEDIUM:
                    DebugLog(TAG, "Device density is MDPI "+deviceDensity);
                    break;
                case DisplayMetrics.DENSITY_HIGH:
                    DebugLog(TAG, "Device density is HDPI "+deviceDensity);
                    break;
                case DisplayMetrics.DENSITY_XHIGH:
                    DebugLog(TAG, "Device density is XHDPI "+deviceDensity);
                    break;
                default :
                    deviceDensity = DisplayMetrics.DENSITY_DEFAULT;
                    DebugLog(TAG, "Device density is  "+deviceDensity);
                    break;
            }
        }
        return deviceDensity;
    }

    public JSONObject getJsonRepresentation() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("country", country);
//            if (location != null) {
//                obj.put("location", location.getLocation());
//            } else {
//                obj.put("location", "-1.0,-1.0");
//            }
            obj.put("platform", platform);
            obj.put("device", device);
            obj.put("firmware", firmware);
            obj.put("language", language);
            obj.put("country", country);
            obj.put("idfa", idfa);
            obj.put("idfv", idfv);
            obj.put("nativeSDKVersion", LootsieGlobals.SDK_VERSION);
            obj.put("screenwidth", screenWidth);
            obj.put("screenheight", screenHeight);
            obj.put("screendpi", screenDpi);

            // new parameter
            obj.put("deviceDensity", deviceDensity);
            // force medium screen for testing
            //obj.put("testABLetter", 65); // 65 forces template B -> banner on webapp
            //obj.put("testABLetter", 66); // 66 forces template A -> coin on webapp
            // don't pass testABleetter for default behavior
//			obj.put("screenwidth", 1920);
//			obj.put("screenheight", 1080);
//			obj.put("screendpi", 149);

        } catch (JSONException e) {
            Logs.e(TAG, "Device: getJsonRepresentation: json exception: " + e);
        }
        return obj;
    }

    public HashMap<String, Object> getHashMapRepresentation() {
        HashMap<String,Object> map = new HashMap<String, Object>();

        // for post api/session
        /*
            ?latlng=-1.0,-1.0
            &sdk_version=v2
            &platform=Unity
            &device=deviceModelStr
            &firmware=operatingSystemStr
            &language=languageStr
            &country=regionNameStr
            X-Lootsie-App-Secret
         */

//        if (location != null) {
//            String locationStr = location.getLocation();
//            if (locationStr != null) {
//                map.put("latlng", locationStr);
//            } else {
//                map.put("latlng", "-1.0,-1.0");
//            }
//        } else {
//            map.put("latlng", "-1.0,-1.0");
//        }
        map.put("sdk_version",LootsieGlobals.SDK_VERSION);
        map.put("platform",platform);
        map.put("device",device);
        map.put("firmware",firmware);
        map.put("language", language);
        map.put("country", country);


        return map;
    }

    /**
     * DebugLog: dump out debugging statements if debugLevel > 0
     * @param msg
     * @param args
     */
    private void DebugLog(String msg, Object... args) {
        if (LootsieGlobals.debugLevel > 0) {
            Logs.v(TAG, String.format(msg, args));
        }
    }

    public int getScreenOrientation() {

        return lootsieEngine.getApp().getResources().getConfiguration().orientation;

    }
    public int getOrientation() {
        //Display display = getWindowManager().getDefaultDisplay();
        WindowManager wm = (WindowManager) lootsieEngine.getApp().getSystemService(Context.WINDOW_SERVICE);

        Display display = wm.getDefaultDisplay();

        int rotation = display.getRotation();

        Point size = new Point();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            display.getSize(size);
        }

        int lock = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
        if (rotation == Surface.ROTATION_0
                || rotation == Surface.ROTATION_180) {
            // if rotation is 0 or 180 and width is greater than height, we have
            // a tablet
            if (size.x > size.y) {
                DebugLog(TAG,"We have a tablet");

                if (rotation == Surface.ROTATION_0) {
                    lock = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                } else {
                    lock = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                }
            } else {
                // we have a phone
                DebugLog(TAG,"We have a phone");

                if (rotation == Surface.ROTATION_0) {
                    lock = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                } else {
                    lock = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                }
            }
        } else {
            // if rotation is 90 or 270 and width is greater than height, we
            // have a phone
            if (size.x > size.y) {
                DebugLog(TAG,"We have a phone");
                if (rotation == Surface.ROTATION_90) {
                    lock = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                } else {
                    lock = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
                }
            } else {
                // we have a tablet
                DebugLog(TAG,"We have a tablet");

                if (rotation == Surface.ROTATION_90) {
                    lock = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
                } else {
                    lock = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                }
            }
        }

        return lock;
    }

    public String getPlatform() {
        return platform;
    }

    public String getDevice() {
        return this.device;
    }

    public String getFirmware() {
        return this.firmware;
    }

    public String getLanguage() {
        return this.language;
    }

    public String getCountry() {
        return this.country;
    }

    public String getLocation() {
//        if (location != null) {
//            return this.location.getLocation();
//        } else {
//            return "";
//        }
        return "";
    }

    public String getIdfa() {
        return this.idfa;
    }

    public String getIdfv() {
        return this.idfv;
    }


}