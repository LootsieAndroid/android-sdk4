package com.lootsie.sdk.adnets;

import android.content.Context;

import com.google.android.gms.ads.AdRequest;
import com.lootsie.sdk.adnets.model.InterstitialAd;

/**
 * Created by Alexander S. Sidorov on 2/25/16.
 */
public class AdMobInterstitialAd implements InterstitialAd
{
    com.google.android.gms.ads.InterstitialAd proxied;


    public AdMobInterstitialAd(Context context) {
        proxied = new com.google.android.gms.ads.InterstitialAd(context);
    }


    @Override
    public void load() {
        AdRequest.Builder builder = new AdRequest.Builder();
        builder.addTestDevice("851FB30F0E850C8248F8E3146E56D639");//
        proxied.loadAd(builder.build());
    }

    @Override
    public boolean isLoaded() {
        return proxied.isLoaded();
    }

    @Override
    public void show() {
        proxied.show();
    }
}
