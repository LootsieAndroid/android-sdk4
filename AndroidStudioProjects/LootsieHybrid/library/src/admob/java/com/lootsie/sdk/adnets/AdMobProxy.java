package com.lootsie.sdk.adnets;

import android.content.Context;

import com.google.android.gms.ads.AdListener;
import com.lootsie.sdk.adnets.model.AdLoadErrorCode;
import com.lootsie.sdk.adnets.model.AdNetProxy;
import com.lootsie.sdk.adnets.model.InterstitialAdListener;

/**
 * Created by Alexander S. Sidorov on 24.02.2016.
 */
public class AdMobProxy implements AdNetProxy
{
    public static final String TAG = AdMobProxy.class.getSimpleName();

    public com.lootsie.sdk.adnets.model.InterstitialAd createInterstitialAd(Context context,
        final String adUnitId, final InterstitialAdListener masterAdListener)
    {
        AdMobInterstitialAd adMobInterstitialAd = new AdMobInterstitialAd(context);
        adMobInterstitialAd.proxied.setAdUnitId(adUnitId);
        adMobInterstitialAd.proxied.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                masterAdListener.onInterstitialAdLoaded();
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
                masterAdListener.onInterstitialAdOpened();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                AdLoadErrorCode[] statuses = AdLoadErrorCode.values();
                AdLoadErrorCode adLoadErrorCode = errorCode < 0 || errorCode > statuses.length - 1?
                        AdLoadErrorCode.ERROR_CODE_UNDEFINED: statuses[errorCode];
                masterAdListener.onInterstitialAdFailedToLoad(adLoadErrorCode);
            }

            @Override
            public void onAdClosed() {
                masterAdListener.onInterstitialAdClosed();
            }

            @Override
            public void onAdLeftApplication() {
                masterAdListener.onInterstitialAdLeftApplication();
            }

        });

        return adMobInterstitialAd;
    }
}