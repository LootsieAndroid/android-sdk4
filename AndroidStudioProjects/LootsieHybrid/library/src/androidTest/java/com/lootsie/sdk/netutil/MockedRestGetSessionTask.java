package com.lootsie.sdk.netutil;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.netutil.RestResult;

import java.util.Map;

/**
 * Created by jerrylootsie on 7/10/15.
 */
public class MockedRestGetSessionTask implements IGenericAsyncTask<Map<String, Object>> {

    IRESTCallback callback = null;

    public MockedRestGetSessionTask() {
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    public void executeTask(Map<String, Object>... params) {
        System.out.println("MockedRestGetSessionTask: execute");

        if (params == null || params.length == 0) {
            return;
        }

        RestResult response = new RestResult();
        response.status = 200;
        response.content = "{\"X-Lootsie-API-Session-Token\": \"d6d14d08-1152-4d42-b8c3-4d29aec4213a\"}";

        if (callback != null) {
            callback.restResult(response);
        }
    }
}
