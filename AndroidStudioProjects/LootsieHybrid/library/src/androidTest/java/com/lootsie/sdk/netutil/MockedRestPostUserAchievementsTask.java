package com.lootsie.sdk.netutil;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.netutil.RestResult;

import org.json.JSONObject;

/**
 * Created by jerrylootsie on 7/13/15.
 */
public class MockedRestPostUserAchievementsTask implements IGenericAsyncTask<JSONObject> {
    IRESTCallback callback = null;

    public MockedRestPostUserAchievementsTask() {
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    public void executeTask(JSONObject... params) {
        System.out.println("MockedRestPostUserLocationTask: execute");

        RestResult response = new RestResult();
        response.status = 200;
        response.content = "{\"total_lp\": 40, \"lp\": 20}";

        if (callback != null) {
            callback.restResult(response);
        }
    }
}