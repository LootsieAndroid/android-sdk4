package com.lootsie.sdk;

import android.widget.Toast;

import com.lootsie.sdk.callbacks.IAchievementReached;
import com.lootsie.sdk.callbacks.IInitializationCallback;
import com.lootsie.sdk.callbacks.IMergePointsCallback;
import com.lootsie.sdk.callbacks.IRedeemReward;
import com.lootsie.sdk.device.Device;
import com.lootsie.sdk.lootsiehybrid.LOOTSIE_NOTIFICATION_CONFIGURATION;
import com.lootsie.sdk.lootsiehybrid.Lootsie;
import com.lootsie.sdk.lootsiehybrid.LootsieAccountManager;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.lootsiehybrid.sequences.AchievementReachedSequence;
import com.lootsie.sdk.lootsiehybrid.sequences.InitSequence;
import com.lootsie.sdk.lootsiehybrid.sequences.MergePointsSequence;
import com.lootsie.sdk.lootsiehybrid.sequences.RedemptionSequence;
import com.lootsie.sdk.model.App;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.model.RedemptionResult;
import com.lootsie.sdk.model.Reward;
import com.lootsie.sdk.model.User;
import com.lootsie.sdk.net.Headers;
import com.lootsie.sdk.net.RestClient;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.runner.RunWith;
import org.junit.Test;

import org.robolectric.*;
import org.robolectric.annotation.Config;


/**
 * Created by jerrylootsie on 7/8/15.
 */

//@RunWith(RobolectricGradleTestRunner.class)
//@Config(constants = BuildConfig.class)
//public class RoboElectricTest {
// }
// MyShadowAsyncTask fails! -- http://stackoverflow.com/questions/26290867/asynctask-test-doesnt-execute
//@Config(shadows={MyShadowAsyncTask.class}, constants = BuildConfig.class, sdk = 21)
@Config(constants = BuildConfig.class, sdk = 21)
//@Config(constants = BuildConfig.class, manifest = "src/main/AndroidManifest.xml", sdk = 21)
//@Config(constants = BuildConfig.class, manifest = "manifests/AndroidManifest.xml", sdk = 21)
//@Config(constants = BuildConfig.class, manifest = "build/intermediates/manifests/androidTest/debug/AndroidManifest.xml", sdk = 21)
//@RunWith(RobolectricGradleTestRunner.class)
@RunWith(CustomRobolectricRunner.class)
public class RobolectricTest {

    @Test
    public void testDevice() {
        try {
            System.out.println("RobolectricTest: testDevice");
            //Device device = new Device(null);

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);
            //LootsieEngine.getInstance().getApplicationContext();

            Device device = new Device(testInstance);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testDevice Exception: " + ex.getMessage());
            ex.printStackTrace();

        }

    }





}
