package com.lootsie.sdk;

import com.lootsie.sdk.callbacks.IMergePointsCallback;
import com.lootsie.sdk.lootsiehybrid.Lootsie;
import com.lootsie.sdk.lootsiehybrid.LootsieAccountManager;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.lootsiehybrid.sequences.MergePointsSequence;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.model.User;
import com.lootsie.sdk.net.Headers;
import com.lootsie.sdk.net.RestClient;
import com.lootsie.sdk.netutil.MockedRestGenericStringResponseTask;
import com.lootsie.sdk.netutil.MockedRestGetUserAccountTask;
import com.lootsie.sdk.netutil.MockedRestPostUserSessionEmailOnlyTask;
import com.lootsie.sdk.netutil.RestResult;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

/**
 * Created by jerrylootsie on 7/14/15.
 */
@Config(constants = BuildConfig.class, sdk = 21)
@RunWith(CustomRobolectricRunner.class)
public class RobolectricTest_MergePointsSequence {

    static enum TestResultStatus {
        SUCCESS,
        PENDING,
        FAILURE;

        private TestResultStatus() {
        }
    }

    static class MergePointsCallback implements IMergePointsCallback {

        public TestResultStatus resultStatus = TestResultStatus.PENDING;

        @Override
        public void onMergePointsSuccess(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT result) {
            System.out.println("MergePointsCallback: onMergePointsSuccess: " + result.toString());
            resultStatus = TestResultStatus.SUCCESS;
        }

        @Override
        public void onMergePointsFailure(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT result) {
            System.out.println("MergePointsCallback: onMergePointsFailure: " + result.toString());
            resultStatus = TestResultStatus.FAILURE;
        }
    }

    @Test
    public void testSequence_MergePointsSequence() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_MergePointsSequence");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);
            testInstance.setAppId(LootsieGlobals.APPKEY);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);

            DataModel.user = new User();

            RestClient.addHeader(Headers.APP_SECRET, LootsieEngine.getInstance().getAppId());
            RestClient.addHeader(Headers.SESSION_TOKEN, "d6d14d08-1152-4d42-b8c3-4d29aec4213a");
            RestClient.addHeader(Headers.USER_TOKEN, "656CB89715253D064E9961D7CCB30B479DC5555868BBB25C7F150121727DE214");


            MergePointsCallback mergePointsCallback = new MergePointsCallback();

            MockedRestPostUserSessionEmailOnlyTask restPostUserSessionEmailOnlyTask = new MockedRestPostUserSessionEmailOnlyTask();
            MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();

            MergePointsSequence.restPostUserSessionEmailOnlyTask = restPostUserSessionEmailOnlyTask;
            MergePointsSequence.restGetUserAccountTask = restGetUserAccountTask;

            MergePointsSequence.start("lootsietest+3@lootsie.com",mergePointsCallback);

            org.junit.Assert.assertTrue("Email invalid for user", DataModel.user.email.compareToIgnoreCase("lootsietest+3@lootsie.com") == 0);
            org.junit.Assert.assertTrue("Guest status invalid for user", DataModel.user.isGuest == false);

            org.junit.Assert.assertTrue("MergePoints callback should succeed", mergePointsCallback.resultStatus == TestResultStatus.SUCCESS);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_MergePointsSequence Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_MergePointsSequence finished");
    }



    @Test
    public void testSequence_MergePointsSequence_registerUser_conflict() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_MergePointsSequence_registerUser_conflict");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);
            testInstance.setAppId(LootsieGlobals.APPKEY);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);

            DataModel.user = new User();

            RestClient.addHeader(Headers.APP_SECRET, LootsieEngine.getInstance().getAppId());
            RestClient.addHeader(Headers.SESSION_TOKEN, "d6d14d08-1152-4d42-b8c3-4d29aec4213a");
            RestClient.addHeader(Headers.USER_TOKEN, "656CB89715253D064E9961D7CCB30B479DC5555868BBB25C7F150121727DE214");


            MergePointsCallback mergePointsCallback = new MergePointsCallback();

            //MockedRestPostUserSessionEmailOnlyTask restPostUserSessionEmailOnlyTask = new MockedRestPostUserSessionEmailOnlyTask();
            MockedRestGenericStringResponseTask restPostUserSessionEmailOnlyTask = new MockedRestGenericStringResponseTask();
            // 409 Conflict {} When the user has already registered successfully:
            RestResult response = new RestResult();
            response.status = 409;
            response.content = "{}";
            restPostUserSessionEmailOnlyTask.setRestResult(response);

            MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();

            MergePointsSequence.restPostUserSessionEmailOnlyTask = restPostUserSessionEmailOnlyTask;
            MergePointsSequence.restGetUserAccountTask = restGetUserAccountTask;

            MergePointsSequence.start("lootsietest+3@lootsie.com", mergePointsCallback);

            org.junit.Assert.assertTrue("Email invalid for user", DataModel.user.email.compareToIgnoreCase("lootsietest+3@lootsie.com") == 0);
            org.junit.Assert.assertTrue("Guest status invalid for user", DataModel.user.isGuest == false);

            org.junit.Assert.assertTrue("MergePoints callback should succeed", mergePointsCallback.resultStatus == TestResultStatus.SUCCESS);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_MergePointsSequence_registerUser_conflict Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_MergePointsSequence_registerUser_conflict finished");
    }

    @Test
    public void testSequence_MergePointsSequence_getUserAccount_appsecret_invalid() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_MergePointsSequence_getUserAccount_appsecret_invalid");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);
            testInstance.setAppId(LootsieGlobals.APPKEY);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);

            DataModel.user = new User();

            RestClient.addHeader(Headers.APP_SECRET, LootsieEngine.getInstance().getAppId());
            RestClient.addHeader(Headers.SESSION_TOKEN, "d6d14d08-1152-4d42-b8c3-4d29aec4213a");
            RestClient.addHeader(Headers.USER_TOKEN, "656CB89715253D064E9961D7CCB30B479DC5555868BBB25C7F150121727DE214");


            MergePointsCallback mergePointsCallback = new MergePointsCallback();

            MockedRestPostUserSessionEmailOnlyTask restPostUserSessionEmailOnlyTask = new MockedRestPostUserSessionEmailOnlyTask();

            //MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();
            MockedRestGenericStringResponseTask restGetUserAccountTask = new MockedRestGenericStringResponseTask();
            // 403 Forbidden {"errors": [{"field": "app_secret", "message": "invalid"}]}
            RestResult response = new RestResult();
            response.status = 403;
            response.content = "{\"errors\": [{\"field\": \"app_secret\", \"message\": \"invalid\"}]}";
            restGetUserAccountTask.setRestResult(response);

            MergePointsSequence.restPostUserSessionEmailOnlyTask = restPostUserSessionEmailOnlyTask;
            MergePointsSequence.restGetUserAccountTask = restGetUserAccountTask;

            MergePointsSequence.start("lootsietest+3@lootsie.com", mergePointsCallback);

            org.junit.Assert.assertTrue("Email invalid for user", DataModel.user.email.compareToIgnoreCase("lootsietest+3@lootsie.com") == 0);
            org.junit.Assert.assertTrue("Guest status invalid for user", DataModel.user.isGuest == false);

            org.junit.Assert.assertTrue("MergePoints callback should succeed", mergePointsCallback.resultStatus == TestResultStatus.SUCCESS);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_MergePointsSequence_getUserAccount_appsecret_invalid Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_MergePointsSequence_getUserAccount_appsecret_invalid finished");
    }
}
