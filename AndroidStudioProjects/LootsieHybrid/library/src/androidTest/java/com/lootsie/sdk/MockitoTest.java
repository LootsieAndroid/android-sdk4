package com.lootsie.sdk;



import android.os.AsyncTask;

import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.device.Device;
import com.lootsie.sdk.lootsiehybrid.sequences.InitSequence;
import com.lootsie.sdk.netutil.RESTGetSessionTask;
import com.lootsie.sdk.netutil.RestResult;



import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.junit.Assert;
import org.junit.Assert.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.mockito.stubbing.OngoingStubbing;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.LinkedList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import org.mockito.Mockito;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.spy;

import org.mockito.Matchers.*;

/**
 * Created by jerrylootsie on 7/7/15.
 */
//@RunWith(Suite.class)
//@SuiteClasses({ MockitoTest.class })
//public class MockitoTest {

public class MockitoTest {

    @Test
    public void test1()  {
        System.out.println("MockitoTest: test1");

        // mock creation
        List mockedList = mock(List.class);

        // using mock object - it does not throw any "unexpected interaction" exception
        mockedList.add("one");
        mockedList.clear();

        // selective, explicit, highly readable verification
        verify(mockedList).add("one");
        verify(mockedList).clear();

        assertEquals("mockedList size should be 0", 0, mockedList.size());
    }

    @Test
    public void test2() {
        System.out.println("MockitoTest: test2");

        // you can mock concrete classes, not only interfaces
        LinkedList mockedList = mock(LinkedList.class);

        // stubbing appears before the actual execution
        when(mockedList.get(0)).thenReturn("first");

        // the following prints "first"
        System.out.println("MockitoTest: test2: first element: " + mockedList.get(0));

        // the following prints "null" because get(999) was not stubbed
        System.out.println("MockitoTest: test2: element 999: " + mockedList.get(999));
    }

//    public static enum Status {
//        FINISHED,
//        PENDING,
//        RUNNING;
//
//        private Status() {
//        }
//    }

    abstract class MockedAsyncTask<Params, Progress, Result> {
        public final Executor THREAD_POOL_EXECUTOR = null;
        public final Executor SERIAL_EXECUTOR = null;

        public MockedAsyncTask() {
            //throw new RuntimeException("Stub!");
        }

        public final AsyncTask.Status getStatus() {
            //throw new RuntimeException("Stub!");
            return AsyncTask.Status.FINISHED;
        }

        protected abstract Result doInBackground(Params... var1);

        protected void onPreExecute() {
            //throw new RuntimeException("Stub!");
        }

        protected void onPostExecute(Result result) {
            //throw new RuntimeException("Stub!");
        }

        protected void onProgressUpdate(Progress... values) {
            //throw new RuntimeException("Stub!");
        }

        protected void onCancelled(Result result) {
            //throw new RuntimeException("Stub!");
        }

        protected void onCancelled() {
            //throw new RuntimeException("Stub!");
        }

        public final boolean isCancelled() {
            //throw new RuntimeException("Stub!");
            return true;
        }

        public final boolean cancel(boolean mayInterruptIfRunning) {
            //throw new RuntimeException("Stub!");
            System.out.println("MockedAsyncTask: cancel");
            return true;
        }

        public final Result get() throws InterruptedException, ExecutionException {
            //throw new RuntimeException("Stub!");
            System.out.println("MockedAsyncTask: get");
            return null;
        }

        public final Result get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            //throw new RuntimeException("Stub!");
            System.out.println("MockedAsyncTask: get");
            return null;
        }

        public final MockedAsyncTask<Params, Progress, Result> execute(Params... params) {
            //throw new RuntimeException("Stub!");
            System.out.println("MockedAsyncTask: execute");
            return this;
        }

        public final MockedAsyncTask<Params, Progress, Result> executeOnExecutor(Executor exec, Params... params) {
            //throw new RuntimeException("Stub!");
            System.out.println("MockedAsyncTask: executeOnExecutor");
            return this;
        }

        public void execute(Runnable runnable) {
            System.out.println("MockedAsyncTask: execute(Runnable)");
            //throw new RuntimeException("Stub!");
        }

        protected final void publishProgress(Progress... values) {
            System.out.println("MockedAsyncTask: publishProgress");
            //throw new RuntimeException("Stub!");
        }

    }

    //class MockedAsyncTask extends AsyncTask< Map<String, Object>, Void, RestResult> {
    //class MockedRestAsyncTask extends MockedAsyncTask< Map<String, Object>, Void, RestResult> {
    class MockedRestAsyncTask {

        IRESTCallback callback = null;

        public MockedRestAsyncTask() {

        }

        public MockedRestAsyncTask(IRESTCallback restCallback) {
            callback = restCallback;
        }

        public MockedRestAsyncTask execute(Map<String, Object>... params) {
            //throw new RuntimeException("Stub!");
            System.out.println("MockedRestAsyncTask: execute");

            return this;
        }

        //@Override
        protected RestResult doInBackground( Map<String, Object> ... params) {

            if (params == null || params.length == 0) {
                return null;
            }

            RestResult response = new RestResult();

            return response;
        }

    }


    @Test
    public void test3()  {
        System.out.println("MockitoTest: test3");

        // from InitSequence : start

        // create  a signal to let us know when our task is done.
        final CountDownLatch signal = new CountDownLatch(1);

        final IRESTCallback restCallback = new IRESTCallback() {
            @Override
            public void restResult(RestResult result) {

                System.out.println("MockitoTest: restCallback");

                signal.countDown();
            }
        };


        try {
            //    In a mock all methods are stubbed and return "smart return types". This means that calling any
            //    method on a mocked class will do nothing unless you specify behaviour.
            // this will throw stub exceptions
            //RESTGetSessionTask test = mock(RESTGetSessionTask.class);
            //System.out.println("MockitoTest: test3: mock class1");

            //    In a spy the original functionality of the class is still there but you can validate method
            //    invocations in a spy and also override method behaviour.
//            System.out.println("MockitoTest: test3: spy class2");
//            RESTGetSessionTask restGetSessionTask = new RESTGetSessionTask(restCallback);
//            RESTGetSessionTask test = Mockito.spy(restGetSessionTask);
            //RESTGetSessionTask test = Mockito.spy(new RESTGetSessionTask(restCallback));

            System.out.println("MockitoTest: test3: spy class3");
            MockedRestAsyncTask test = Mockito.spy(new MockedRestAsyncTask(restCallback));


            doAnswer(new Answer<Void>() {
                public Void answer(InvocationOnMock invocation) {
                    System.out.println("MockedRestAsyncTask: execute: Answer");

                    //Object[] args = invocation.getArguments();
                    //HashMap<String, Object>[] args = (HashMap<String, Object>[]) invocation.getArguments();
                    //Object mock = invocation.getMock();
                    MockedRestAsyncTask objectMock = (MockedRestAsyncTask) invocation.getMock();

                    RestResult response = new RestResult();

                    IRESTCallback callback = objectMock.callback;
                    callback.restResult(response);


//                    restCallback.restResult(response);

                    return null;
                }
            }).when(test).execute((HashMap<String,Object>[]) any());

            //when(test.execute((HashMap<String,Object>[]) any())).thenReturn(null);

            //when(test.execute(org.mockito.Matchers.<Map<String, Object>>any())).thenReturn(null);

            //doNothing().when(test.execute(org.mockito.Matchers.<Map<String, Object>>any()));

            //when(test.execute(anyMapOf(String.class, Object.class))).thenReturn(null);




            try {
                // TODO use mock in test....
                HashMap<String, Object> map = new HashMap<String, Object>();
                test.execute(map);

                /* The testing thread will wait here until the UI thread releases it
                 * above with the countDown() or 30 seconds passes and it times out.
                 */
                signal.await(10, TimeUnit.SECONDS);
            } catch (InterruptedException ex) {
                System.out.println("MockitoTest: test3 InterruptedException: " + ex.getMessage());
                ex.printStackTrace();
            }

        } catch (Exception ex) {
            System.out.println("MockitoTest: test3 Exception: " + ex.getMessage());
            ex.printStackTrace();

        }

        // when I use the mockito stub interface Answer for callbacks, it never gets here?
        System.out.println("MockitoTest: test3 finished");
    }

    // need to test this stuff using robolectric
//    @Test
//    public void testSequence_InitSequence() {
//        System.out.println("MockitoTest: testSequence_InitSequence");
//
//        try {
//            Device device = new Device(null);
//
//            InitSequence.start(device, "test_app_key", null);
//
//        } catch (Exception ex) {
//            System.out.println("MockitoTest: testSequence_InitSequence Exception: " + ex.getMessage());
//            ex.printStackTrace();
//
//        }
//
//
//        System.out.println("MockitoTest: testSequence_InitSequence finished");
//    }


}
