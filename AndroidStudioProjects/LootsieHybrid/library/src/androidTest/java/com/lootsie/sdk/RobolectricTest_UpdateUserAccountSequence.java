package com.lootsie.sdk;

import android.widget.Toast;

import com.lootsie.sdk.callbacks.IUpdateUserAccountCallback;
import com.lootsie.sdk.lootsiehybrid.Lootsie;
import com.lootsie.sdk.lootsiehybrid.LootsieAccountManager;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.lootsiehybrid.sequences.UpdateUserAccountSequence;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.model.User;
import com.lootsie.sdk.netutil.MockedRestGenericStringResponseTask;
import com.lootsie.sdk.netutil.MockedRestGetUserAccountTask;
import com.lootsie.sdk.netutil.MockedRestPostUserSessionEmailOnlyTask;
import com.lootsie.sdk.netutil.RestResult;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

/**
 * Created by jerrylootsie on 7/14/15.
 */
@Config(constants = BuildConfig.class, sdk = 21)
@RunWith(CustomRobolectricRunner.class)
public class RobolectricTest_UpdateUserAccountSequence {

    static enum TestResultStatus {
        SUCCESS,
        PENDING,
        FAILURE;

        private TestResultStatus() {
        }
    }

    /**
     * tasks to perform when succesfully updating the user account
     */
    static class UpdateUserAccountCallback implements IUpdateUserAccountCallback {

        public TestResultStatus resultStatus = TestResultStatus.PENDING;

        @Override
        public void onUpdateUserAccountSuccess(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT mesg) {
            System.out.println("UpdateUserAccountCallback: onUpdateUserAccountSuccess: " + mesg.toString());
            resultStatus = TestResultStatus.SUCCESS;
        }

        @Override
        public void onUpdateUserAccountFailure(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT mesg) {
            System.out.println("UpdateUserAccountCallback: onUpdateUserAccountFailure: " + mesg.toString());
            resultStatus = TestResultStatus.FAILURE;
        }
    }

    @Test
    public void testSequence_UpdateUserAccountSequence() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_UpdateUserAccountSequence");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);
            testInstance.setAppId(LootsieGlobals.APPKEY);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);

            DataModel.user = new User();

            MockedRestPostUserSessionEmailOnlyTask restPostUserSessionEmailOnlyTask = new MockedRestPostUserSessionEmailOnlyTask();

            MockedRestGenericStringResponseTask restPutUserAccountTask = new MockedRestGenericStringResponseTask();
            RestResult response = new RestResult();
            response.status = 204;
            response.content = "{}";
            restPutUserAccountTask.setRestResult(response);


            //MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();
            MockedRestGenericStringResponseTask restGetUserAccountTask  = new MockedRestGenericStringResponseTask();
            RestResult getUserAccountResponse = new RestResult();
            getUserAccountResponse.status = 200;
            getUserAccountResponse.content = "{\"registered_by_facebook\": false," +
                    " \"last_name\": \"Lootsie\", " +
                    "\"accepted_tos\": false, " +
                    "\"partner_optin\": false, " +
                    "\"total_lp\": 14, " +
                    "\"address\": \"\", " +
                    "\"photo_url\": null," +
                    "\"city\": \"Culver City\", " +
                    "\"first_name\": \"Jerry\", " +
                    "\"gender\": \"\", " +
                    "\"confirmed_age\": 0, " +
                    "\"zipcode\": 90232, " +
                    "\"birthdate\": null, " +
                    "\"lootsie_optin\": false, " +
                    "\"state\": \"\", " +
                    "\"overall_lp\": \"14\", " +
                    "\"is_guest\": false, " +
                    "\"email\": \"lootsietest+3@lootsie.com\"}";
            restGetUserAccountTask.setRestResult(getUserAccountResponse);

            UpdateUserAccountSequence.restPostUserSessionEmailOnlyTask = restPostUserSessionEmailOnlyTask;
            UpdateUserAccountSequence.restPutUserAccountTask = restPutUserAccountTask;
            UpdateUserAccountSequence.restGetUserAccountTask = restGetUserAccountTask;


            User testUser = new User();
            testUser.email = "lootsietest+3@lootsie.com";
            testUser.firstName = "Jerry";
            testUser.lastName = "Lootsie";
            testUser.city = "Culver City";
            testUser.zipcode = 90232;

            UpdateUserAccountCallback updateUserAccountCallback = new UpdateUserAccountCallback();
            UpdateUserAccountSequence.start(testUser, updateUserAccountCallback);

            // test results of post user/sessionemailonly
            org.junit.Assert.assertTrue("User email is wrong!", DataModel.user.email.equalsIgnoreCase("lootsietest+3@lootsie.com"));

            // test results of valid put user/account
            // test results of valid get user/account
            org.junit.Assert.assertTrue("User FirstName is wrong!", DataModel.user.firstName.equalsIgnoreCase("Jerry"));
            org.junit.Assert.assertTrue("User Lastname is wrong!", DataModel.user.lastName.equalsIgnoreCase("Lootsie"));
            org.junit.Assert.assertTrue("User City is wrong!", DataModel.user.city.equalsIgnoreCase("Culver City"));
            org.junit.Assert.assertTrue("User zipcode is wrong!", DataModel.user.zipcode == 90232);

            org.junit.Assert.assertTrue("UpdateUserAccountSequence result status is wrong!", updateUserAccountCallback.resultStatus == TestResultStatus.SUCCESS);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_UpdateUserAccountSequence Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_UpdateUserAccountSequence finished");
    }

    @Test
    public void testSequence_UpdateUserAccountSequence_already_registered() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_UpdateUserAccountSequence_already_registered");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);
            testInstance.setAppId(LootsieGlobals.APPKEY);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);

            DataModel.user = new User();

            //MockedRestPostUserSessionEmailOnlyTask restPostUserSessionEmailOnlyTask = new MockedRestPostUserSessionEmailOnlyTask();
            MockedRestGenericStringResponseTask restPostUserSessionEmailOnlyTask = new MockedRestGenericStringResponseTask();
            RestResult userRegisterResponse = new RestResult();
            // 409 Conflict {} When the user has already registered successfully:
            userRegisterResponse.status = 409;
            userRegisterResponse.content = "{}";

            MockedRestGenericStringResponseTask restPutUserAccountTask = new MockedRestGenericStringResponseTask();
            RestResult response = new RestResult();
            response.status = 204;
            response.content = "{}";
            restPutUserAccountTask.setRestResult(response);


            MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();

            UpdateUserAccountSequence.restPostUserSessionEmailOnlyTask = restPostUserSessionEmailOnlyTask;
            UpdateUserAccountSequence.restPutUserAccountTask = restPutUserAccountTask;
            UpdateUserAccountSequence.restGetUserAccountTask = restGetUserAccountTask;


            User testUser = new User();
            testUser.email = "lootsietest+3@lootsie.com";

            UpdateUserAccountCallback updateUserAccountCallback = new UpdateUserAccountCallback();
            UpdateUserAccountSequence.start(testUser, updateUserAccountCallback);

            // test results of post user/sessionemailonly
            org.junit.Assert.assertTrue("User email is wrong!", DataModel.user.email.equalsIgnoreCase("lootsietest+3@lootsie.com"));

            org.junit.Assert.assertTrue("UpdateUserAccountSequence result status is wrong!", updateUserAccountCallback.resultStatus == TestResultStatus.SUCCESS);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_UpdateUserAccountSequence_already_registered Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_UpdateUserAccountSequence_already_registered finished");
    }

    @Test
    public void testSequence_UpdateUserAccountSequence_invalid_zipcode() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_UpdateUserAccountSequence_invalid_zipcode");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);
            testInstance.setAppId(LootsieGlobals.APPKEY);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);

            DataModel.user = new User();

            MockedRestPostUserSessionEmailOnlyTask restPostUserSessionEmailOnlyTask = new MockedRestPostUserSessionEmailOnlyTask();

            MockedRestGenericStringResponseTask restPutUserAccountTask = new MockedRestGenericStringResponseTask();
            RestResult response = new RestResult();
            // 400 Bad Request {"errors": [{"field": "zipcode", "message": "invalid"}]}
            response.status = 400;
            response.content = "{\"errors\": [{\"field\": \"zipcode\", \"message\": \"invalid\"}]}";
            restPutUserAccountTask.setRestResult(response);


            MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();

            UpdateUserAccountSequence.restPostUserSessionEmailOnlyTask = restPostUserSessionEmailOnlyTask;
            UpdateUserAccountSequence.restPutUserAccountTask = restPutUserAccountTask;
            UpdateUserAccountSequence.restGetUserAccountTask = restGetUserAccountTask;

            User testUser = new User();
            testUser.email = "lootsietest+3@lootsie.com";
            testUser.firstName = "Jerry";
            testUser.lastName = "Lootsie";
            testUser.city = "Culver City";
            testUser.zipcode = 90232;

            // added callback to  UpdateUserAccountSequence
            UpdateUserAccountCallback updateUserAccountCallback = new UpdateUserAccountCallback();
            UpdateUserAccountSequence.start(testUser, updateUserAccountCallback);

            // test results of post user/sessionemailonly
            org.junit.Assert.assertTrue("User email is wrong!", DataModel.user.email.equalsIgnoreCase("lootsietest+3@lootsie.com"));

            // DataModel.user.print();

            // test results of failed put user/account
            org.junit.Assert.assertTrue("User FirstName is wrong!", DataModel.user.firstName == null);
            org.junit.Assert.assertTrue("User Lastname is wrong!", DataModel.user.lastName == null);
            org.junit.Assert.assertTrue("User City is wrong!", DataModel.user.city == null);
            org.junit.Assert.assertTrue("User zipcode is wrong!", DataModel.user.zipcode == 0);


            // test results of valid put user/account
//            org.junit.Assert.assertTrue("User FirstName is wrong!", DataModel.user.firstName.equalsIgnoreCase("Jerry"));
//            org.junit.Assert.assertTrue("User Lastname is wrong!", DataModel.user.lastName.equalsIgnoreCase("Lootsie"));
//            org.junit.Assert.assertTrue("User City is wrong!", DataModel.user.city.equalsIgnoreCase("Culver City"));
//            org.junit.Assert.assertTrue("User zipcode is wrong!", DataModel.user.zipcode == 90232);

            org.junit.Assert.assertTrue("UpdateUserAccountSequence result status is wrong!", updateUserAccountCallback.resultStatus == TestResultStatus.FAILURE);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_UpdateUserAccountSequence_invalid_zipcode Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_UpdateUserAccountSequence_invalid_zipcode finished");
    }

    @Test
    public void testSequence_UpdateUserAccountSequence_putUserAccount_appsecret_invalid() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_UpdateUserAccountSequence_putUserAccount_appsecret_invalid");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);
            testInstance.setAppId(LootsieGlobals.APPKEY);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);

            DataModel.user = new User();

            MockedRestPostUserSessionEmailOnlyTask restPostUserSessionEmailOnlyTask = new MockedRestPostUserSessionEmailOnlyTask();

            MockedRestGenericStringResponseTask restPutUserAccountTask = new MockedRestGenericStringResponseTask();
            RestResult response = new RestResult();
            // 403 Forbidden {"errors": [{"field": "app_secret", "message": "invalid"}]}
            response.status = 403;
            response.content = "{\"errors\": [{\"field\": \"app_secret\", \"message\": \"invalid\"}]}";
            restPutUserAccountTask.setRestResult(response);


            MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();

            UpdateUserAccountSequence.restPostUserSessionEmailOnlyTask = restPostUserSessionEmailOnlyTask;
            UpdateUserAccountSequence.restPutUserAccountTask = restPutUserAccountTask;
            UpdateUserAccountSequence.restGetUserAccountTask = restGetUserAccountTask;

            User testUser = new User();
            testUser.email = "lootsietest+3@lootsie.com";
            testUser.firstName = "Jerry";
            testUser.lastName = "Lootsie";
            testUser.city = "Culver City";
            testUser.zipcode = 90232;

            // added callback to  UpdateUserAccountSequence
            UpdateUserAccountCallback updateUserAccountCallback = new UpdateUserAccountCallback();
            UpdateUserAccountSequence.start(testUser, updateUserAccountCallback);

            // test results of post user/sessionemailonly
            org.junit.Assert.assertTrue("User email is wrong!", DataModel.user.email.equalsIgnoreCase("lootsietest+3@lootsie.com"));

            // DataModel.user.print();

            // test results of failed put user/account
            org.junit.Assert.assertTrue("User FirstName is wrong!", DataModel.user.firstName == null);
            org.junit.Assert.assertTrue("User Lastname is wrong!", DataModel.user.lastName == null);
            org.junit.Assert.assertTrue("User City is wrong!", DataModel.user.city == null);
            org.junit.Assert.assertTrue("User zipcode is wrong!", DataModel.user.zipcode == 0);


            // test results of valid put user/account
//            org.junit.Assert.assertTrue("User FirstName is wrong!", DataModel.user.firstName.equalsIgnoreCase("Jerry"));
//            org.junit.Assert.assertTrue("User Lastname is wrong!", DataModel.user.lastName.equalsIgnoreCase("Lootsie"));
//            org.junit.Assert.assertTrue("User City is wrong!", DataModel.user.city.equalsIgnoreCase("Culver City"));
//            org.junit.Assert.assertTrue("User zipcode is wrong!", DataModel.user.zipcode == 90232);

            org.junit.Assert.assertTrue("UpdateUserAccountSequence result status is wrong!", updateUserAccountCallback.resultStatus == TestResultStatus.FAILURE);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_UpdateUserAccountSequence_putUserAccount_appsecret_invalid Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_UpdateUserAccountSequence_putUserAccount_appsecret_invalid finished");
    }

    @Test
    public void testSequence_UpdateUserAccountSequence_getUserAccount_appsecret_invalid() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_UpdateUserAccountSequence_getUserAccount_appsecret_invalid");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);
            testInstance.setAppId(LootsieGlobals.APPKEY);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);

            DataModel.user = new User();

            MockedRestPostUserSessionEmailOnlyTask restPostUserSessionEmailOnlyTask = new MockedRestPostUserSessionEmailOnlyTask();

            MockedRestGenericStringResponseTask restPutUserAccountTask = new MockedRestGenericStringResponseTask();
            RestResult putUserAccountResponse = new RestResult();
            putUserAccountResponse.status = 204;
            putUserAccountResponse.content = "{}";
            restPutUserAccountTask.setRestResult(putUserAccountResponse);


            //MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();
            MockedRestGenericStringResponseTask restGetUserAccountTask = new MockedRestGenericStringResponseTask();
            RestResult getUserAccountResponse = new RestResult();
            // 403 Forbidden {"errors": [{"field": "app_secret", "message": "invalid"}]}
            getUserAccountResponse.status = 403;
            getUserAccountResponse.content = "{\"errors\": [{\"field\": \"app_secret\", \"message\": \"invalid\"}]}";
            restGetUserAccountTask.setRestResult(getUserAccountResponse);

            UpdateUserAccountSequence.restPostUserSessionEmailOnlyTask = restPostUserSessionEmailOnlyTask;
            UpdateUserAccountSequence.restPutUserAccountTask = restPutUserAccountTask;
            UpdateUserAccountSequence.restGetUserAccountTask = restGetUserAccountTask;

            User testUser = new User();
            testUser.email = "lootsietest+3@lootsie.com";
            testUser.firstName = "Jerry";
            testUser.lastName = "Lootsie";
            testUser.city = "Culver City";
            testUser.zipcode = 90232;

            // added callback to  UpdateUserAccountSequence
            UpdateUserAccountCallback updateUserAccountCallback = new UpdateUserAccountCallback();
            UpdateUserAccountSequence.start(testUser, updateUserAccountCallback);

            // test results of post user/sessionemailonly
            org.junit.Assert.assertTrue("User email is wrong!", DataModel.user.email.equalsIgnoreCase("lootsietest+3@lootsie.com"));

            // DataModel.user.print();

            // test results of failed put user/account
            org.junit.Assert.assertTrue("User FirstName is wrong!", DataModel.user.firstName == null);
            org.junit.Assert.assertTrue("User Lastname is wrong!", DataModel.user.lastName == null);
            org.junit.Assert.assertTrue("User City is wrong!", DataModel.user.city == null);
            org.junit.Assert.assertTrue("User zipcode is wrong!", DataModel.user.zipcode == 0);


            // test results of valid put user/account
//            org.junit.Assert.assertTrue("User FirstName is wrong!", DataModel.user.firstName.equalsIgnoreCase("Jerry"));
//            org.junit.Assert.assertTrue("User Lastname is wrong!", DataModel.user.lastName.equalsIgnoreCase("Lootsie"));
//            org.junit.Assert.assertTrue("User City is wrong!", DataModel.user.city.equalsIgnoreCase("Culver City"));
//            org.junit.Assert.assertTrue("User zipcode is wrong!", DataModel.user.zipcode == 90232);

            org.junit.Assert.assertTrue("UpdateUserAccountSequence result status is wrong!", updateUserAccountCallback.resultStatus == TestResultStatus.FAILURE);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_UpdateUserAccountSequence_getUserAccount_appsecret_invalid Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_UpdateUserAccountSequence_getUserAccount_appsecret_invalid finished");
    }
}
