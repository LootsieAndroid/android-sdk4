package com.lootsie.sdk;

import com.lootsie.sdk.callbacks.IUpdateAchievementsCallback;
import com.lootsie.sdk.lootsiehybrid.Lootsie;
import com.lootsie.sdk.lootsiehybrid.LootsieAccountManager;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.lootsiehybrid.sequences.UpdateAchievementPageSequence;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.netutil.MockedRestGenericStringResponseTask;
import com.lootsie.sdk.netutil.RestResult;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

/**
 * Created by jerrylootsie on 7/14/15.
 */
@Config(constants = BuildConfig.class, sdk = 21)
@RunWith(CustomRobolectricRunner.class)
public class RobolectricTest_UpdateAchievementPageSequence {

    static enum TestResultStatus {
        SUCCESS,
        PENDING,
        FAILURE;

        private TestResultStatus() {
        }
    }

    class UpdateAchievementsCallback implements IUpdateAchievementsCallback {

        public TestResultStatus resultStatus = TestResultStatus.PENDING;

        @Override
        public void onUpdateAchievementsSuccess() {
            System.out.println("UpdateAchievementsCallback: onUpdateAchievementsSuccess");
            resultStatus = TestResultStatus.SUCCESS;
        }

        @Override
        public void onUpdateAchievementsFailure() {
            System.out.println("UpdateAchievementsCallback: onUpdateAchievementsFailure");
            resultStatus = TestResultStatus.FAILURE;
        }
    }

    @Test
    public void testSequence_UpdateAchievementPageSequence() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_UpdateAchievementPageSequence");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);
            testInstance.setAppId(LootsieGlobals.APPKEY);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);

            UpdateAchievementsCallback updateAchievementsCallback = new UpdateAchievementsCallback();

            MockedRestGenericStringResponseTask restGetUserAchievementsTask = new MockedRestGenericStringResponseTask();
            RestResult response = new RestResult();
            response.status = 200;
            response.content = "{\"achievements\": [{\"date\": \"2015-07-10T18:26:08\", \"id\": \"back1\", \"lp\": 20}], \"total_lp\": 20, \"achieved_lp\": 20}";
            restGetUserAchievementsTask.setRestResult(response);

            UpdateAchievementPageSequence.restGetUserAchievementsTask = restGetUserAchievementsTask;
            UpdateAchievementPageSequence.start(updateAchievementsCallback);

            org.junit.Assert.assertTrue("UpdateAchievementPageSequence result status is wrong!", updateAchievementsCallback.resultStatus == TestResultStatus.SUCCESS);
            org.junit.Assert.assertTrue("Achievements list count is wrong!", DataModel.user.achievements.size() == 1);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_UpdateAchievementPageSequence Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_UpdateAchievementPageSequence finished");
    }



}
