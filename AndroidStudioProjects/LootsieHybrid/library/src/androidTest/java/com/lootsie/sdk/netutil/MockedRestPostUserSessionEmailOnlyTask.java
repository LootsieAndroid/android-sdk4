package com.lootsie.sdk.netutil;

import com.lootsie.sdk.callbacks.IGenericAsyncTask;
import com.lootsie.sdk.callbacks.IRESTCallback;
import com.lootsie.sdk.netutil.RestResult;

/**
 * Created by jerrylootsie on 7/13/15.
 */
public class MockedRestPostUserSessionEmailOnlyTask implements IGenericAsyncTask<String> {
    IRESTCallback callback = null;

    public MockedRestPostUserSessionEmailOnlyTask() {
    }

    @Override
    public void setCallback(IRESTCallback restCallback) {
        callback = restCallback;
    }

    @Override
    public void executeTask(String... params) {
        System.out.println("MockedRestPostUserSessionEmailOnlyTask: execute");

        RestResult response = new RestResult();
        response.status = 204;
        response.content = "{}";

        if (callback != null) {
            callback.restResult(response);
        }
    }
}