package com.lootsie.sdk;

import com.lootsie.sdk.callbacks.IRedeemReward;
import com.lootsie.sdk.lootsiehybrid.Lootsie;
import com.lootsie.sdk.lootsiehybrid.LootsieAccountManager;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.lootsiehybrid.sequences.RedemptionSequence;
import com.lootsie.sdk.model.DataModel;
import com.lootsie.sdk.model.RedemptionResult;
import com.lootsie.sdk.model.Reward;
import com.lootsie.sdk.model.User;
import com.lootsie.sdk.net.Headers;
import com.lootsie.sdk.net.RestClient;
import com.lootsie.sdk.netutil.MockedRestPostUserRewardEmailRedemptionTask;
import com.lootsie.sdk.netutil.MockedRestPostUserSessionEmailOnlyTask;
import com.lootsie.sdk.netutil.MockedRestGenericStringResponseTask;
import com.lootsie.sdk.netutil.RestResult;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

/**
 * Created by jerrylootsie on 7/14/15.
 */
@Config(constants = BuildConfig.class, sdk = 21)
@RunWith(CustomRobolectricRunner.class)
public class RobolectricTest_RedemptionSequence {

    static enum TestResultStatus {
        SUCCESS,
        PENDING,
        FAILURE;

        private TestResultStatus() {
        }
    }

    class RedeemRewardCallback implements IRedeemReward {

        public TestResultStatus resultStatus = TestResultStatus.PENDING;

        @Override
        public void onRedeemFailed(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT mesg, String json) {
            System.out.println("RedeemRewardCallback: onRedeemFailed: " + mesg.toString());
            resultStatus = TestResultStatus.FAILURE;
        }

        @Override
        public void onRedeemSuccess(LootsieGlobals.LOOTSIE_SEQUENCE_RESULT mesg, String jsonStr) {
            System.out.println("RedeemRewardCallback: onRedeemSuccess: " + mesg.toString());

            try {
                JSONObject json = new JSONObject(jsonStr);
                RedemptionResult redemptionResult = new RedemptionResult();
                redemptionResult.parseFromJSON(json);

                // {"reward_redemption_id": "-1", "lp": 50, "total_lp": 150}";
                org.junit.Assert.assertTrue("RedeemRewardCallback reward_redemtion_id is incorrect!", redemptionResult.rewardRedemptionId == -1);

            } catch (JSONException ex) {
                System.out.println("RedeemRewardCallback: onRedeemSuccess: exception: " + ex.getMessage());
            } catch (Exception ex2) {
                org.junit.Assert.assertTrue("RedeemRewardCallback: Failed with exception: " + ex2.getMessage(), false);
            }

            resultStatus = TestResultStatus.SUCCESS;
        }

    }

    @Test
    public void testSequence_RedemptionSequence() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_RedemptionSequence");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);
            testInstance.setAppId(LootsieGlobals.APPKEY);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);

            DataModel.user = new User();

            RestClient.addHeader(Headers.APP_SECRET, LootsieEngine.getInstance().getAppId());
            RestClient.addHeader(Headers.SESSION_TOKEN, "d6d14d08-1152-4d42-b8c3-4d29aec4213a");
            RestClient.addHeader(Headers.USER_TOKEN, "656CB89715253D064E9961D7CCB30B479DC5555868BBB25C7F150121727DE214");


            Reward testReward = new Reward();
            testReward.generateTestReward();

            RedeemRewardCallback redeemRewardCallback = new RedeemRewardCallback();

            MockedRestPostUserSessionEmailOnlyTask restPostUserSessionEmailOnlyTask = new MockedRestPostUserSessionEmailOnlyTask();

            // {“reward_redemption_id”: 1, "lp": 50, "total_lp": 150}
            //MockedRestPostUserRewardEmailRedemptionTask restPostUserRewardEmailRedemptionTask = new MockedRestPostUserRewardEmailRedemptionTask();
            MockedRestGenericStringResponseTask restPostUserRewardEmailRedemptionTask = new MockedRestGenericStringResponseTask();
            RestResult response = new RestResult();
            response.status = 200;
            response.content ="{\"reward_redemption_id\": \"-1\", \"lp\": 50, \"total_lp\": 150}";
            restPostUserRewardEmailRedemptionTask.setRestResult(response);


            RedemptionSequence.restPostUserSessionEmailOnlyTask = restPostUserSessionEmailOnlyTask;
            RedemptionSequence.restPostUserRewardEmailRedemptionTask = restPostUserRewardEmailRedemptionTask;

            RedemptionSequence.start("lootsietest+3@lootsie.com", testReward, redeemRewardCallback);



        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_RedemptionSequence Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_RedemptionSequence finished");
    }

    @Test
    public void testSequence_RedemptionSequence_not_affordable() {

        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_RedemptionSequence_not_affordable");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);
            testInstance.setAppId(LootsieGlobals.APPKEY);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);

            DataModel.user = new User();

            RestClient.addHeader(Headers.APP_SECRET, LootsieEngine.getInstance().getAppId());
            RestClient.addHeader(Headers.SESSION_TOKEN, "d6d14d08-1152-4d42-b8c3-4d29aec4213a");
            RestClient.addHeader(Headers.USER_TOKEN, "656CB89715253D064E9961D7CCB30B479DC5555868BBB25C7F150121727DE214");


            Reward testReward = new Reward();
            testReward.generateTestReward();

            RedeemRewardCallback redeemRewardCallback = new RedeemRewardCallback();

            MockedRestPostUserSessionEmailOnlyTask restPostUserSessionEmailOnlyTask = new MockedRestPostUserSessionEmailOnlyTask();

            // {“reward_redemption_id”: 1, "lp": 50, "total_lp": 150}
            //MockedRestPostUserRewardEmailRedemptionTask restPostUserRewardEmailRedemptionTask = new MockedRestPostUserRewardEmailRedemptionTask();
            // 402 Payment Required {"errors": [{"field": "reward_ids", "message": "not affordable"}]}
            MockedRestGenericStringResponseTask restPostUserRewardEmailRedemptionTask = new MockedRestGenericStringResponseTask();
            RestResult response = new RestResult();
            response.status = 402;
            response.content = "{\"errors\": [{\"field\": \"reward_ids\", \"message\": \"not affordable\"}]}";
            restPostUserRewardEmailRedemptionTask.setRestResult(response);


            RedemptionSequence.restPostUserSessionEmailOnlyTask = restPostUserSessionEmailOnlyTask;
            RedemptionSequence.restPostUserRewardEmailRedemptionTask = restPostUserRewardEmailRedemptionTask;

            RedemptionSequence.start("lootsietest+3@lootsie.com", testReward, redeemRewardCallback);

            org.junit.Assert.assertTrue("RedeemRewardCallback should be failed", redeemRewardCallback.resultStatus == TestResultStatus.FAILURE);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_RedemptionSequence_not_affordable Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_RedemptionSequence_not_affordable finished");
    }


    @Test
    public void testSequence_RedemptionSequence_redemption_limit() {

        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_RedemptionSequence_redemption_limit");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);
            testInstance.setAppId(LootsieGlobals.APPKEY);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);

            DataModel.user = new User();

            RestClient.addHeader(Headers.APP_SECRET, LootsieEngine.getInstance().getAppId());
            RestClient.addHeader(Headers.SESSION_TOKEN, "d6d14d08-1152-4d42-b8c3-4d29aec4213a");
            RestClient.addHeader(Headers.USER_TOKEN, "656CB89715253D064E9961D7CCB30B479DC5555868BBB25C7F150121727DE214");


            Reward testReward = new Reward();
            testReward.generateTestReward();

            RedeemRewardCallback redeemRewardCallback = new RedeemRewardCallback();

            MockedRestPostUserSessionEmailOnlyTask restPostUserSessionEmailOnlyTask = new MockedRestPostUserSessionEmailOnlyTask();

            // {“reward_redemption_id”: 1, "lp": 50, "total_lp": 150}
            //MockedRestPostUserRewardEmailRedemptionTask restPostUserRewardEmailRedemptionTask = new MockedRestPostUserRewardEmailRedemptionTask();
            // 409 Conflict {"errors": [{"field": "reward_ids", "message": "redemption limit is reached"}]}
            MockedRestGenericStringResponseTask restPostUserRewardEmailRedemptionTask = new MockedRestGenericStringResponseTask();
            RestResult response = new RestResult();
            response.status = 409;
            response.content = "{\"errors\": [{\"field\": \"reward_ids\", \"message\": \"redemption limit is reached\"}]}";
            restPostUserRewardEmailRedemptionTask.setRestResult(response);


            RedemptionSequence.restPostUserSessionEmailOnlyTask = restPostUserSessionEmailOnlyTask;
            RedemptionSequence.restPostUserRewardEmailRedemptionTask = restPostUserRewardEmailRedemptionTask;

            RedemptionSequence.start("lootsietest+3@lootsie.com", testReward, redeemRewardCallback);

            org.junit.Assert.assertTrue("RedeemRewardCallback should be failed", redeemRewardCallback.resultStatus == TestResultStatus.FAILURE);
            //org.junit.Assert.assertTrue("RedeemRewardCallback should be success", redeemRewardCallback.resultStatus == TestResultStatus.SUCCESS);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_RedemptionSequence_redemption_limit Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_RedemptionSequence_redemption_limit finished");
    }


    @Test
    public void testSequence_RedemptionSequence_email_confict() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_RedemptionSequence_email_confict");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);
            testInstance.setAppId(LootsieGlobals.APPKEY);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);

            DataModel.user = new User();

            RestClient.addHeader(Headers.APP_SECRET, LootsieEngine.getInstance().getAppId());
            RestClient.addHeader(Headers.SESSION_TOKEN, "d6d14d08-1152-4d42-b8c3-4d29aec4213a");
            RestClient.addHeader(Headers.USER_TOKEN, "656CB89715253D064E9961D7CCB30B479DC5555868BBB25C7F150121727DE214");


            Reward testReward = new Reward();
            testReward.generateTestReward();

            RedeemRewardCallback redeemRewardCallback = new RedeemRewardCallback();

            //MockedRestPostUserSessionEmailOnlyTask restPostUserSessionEmailOnlyTask = new MockedRestPostUserSessionEmailOnlyTask();
            // 409 Conflict {} When the user has already registered successfully:
            MockedRestGenericStringResponseTask restPostUserSessionEmailOnlyTask = new MockedRestGenericStringResponseTask();
            RestResult emailRegisterResponse = new RestResult();
            emailRegisterResponse.status = 409;
            emailRegisterResponse.content = "{}";
            restPostUserSessionEmailOnlyTask.setRestResult(emailRegisterResponse);

            // {“reward_redemption_id”: 1, "lp": 50, "total_lp": 150}
            //MockedRestPostUserRewardEmailRedemptionTask restPostUserRewardEmailRedemptionTask = new MockedRestPostUserRewardEmailRedemptionTask();
            MockedRestGenericStringResponseTask restPostUserRewardEmailRedemptionTask = new MockedRestGenericStringResponseTask();
            RestResult response = new RestResult();
            response.status = 200;
            response.content ="{\"reward_redemption_id\": \"-1\", \"lp\": 50, \"total_lp\": 150}";
            restPostUserRewardEmailRedemptionTask.setRestResult(response);


            RedemptionSequence.restPostUserSessionEmailOnlyTask = restPostUserSessionEmailOnlyTask;
            RedemptionSequence.restPostUserRewardEmailRedemptionTask = restPostUserRewardEmailRedemptionTask;

            RedemptionSequence.start("lootsietest+3@lootsie.com", testReward, redeemRewardCallback);

            //org.junit.Assert.assertTrue("RedeemRewardCallback should be failed", redeemRewardCallback.resultStatus == TestResultStatus.FAILURE);
            org.junit.Assert.assertTrue("RedeemRewardCallback should be success", redeemRewardCallback.resultStatus == TestResultStatus.SUCCESS);
        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_RedemptionSequence_email_confict Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_RedemptionSequence_email_confict finished");
    }
}
