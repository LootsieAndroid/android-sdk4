package com.lootsie.sdk;

import com.lootsie.sdk.callbacks.IInitializationCallback;
import com.lootsie.sdk.device.Device;
import com.lootsie.sdk.lootsiehybrid.Lootsie;
import com.lootsie.sdk.lootsiehybrid.LootsieAccountManager;
import com.lootsie.sdk.lootsiehybrid.LootsieEngine;
import com.lootsie.sdk.lootsiehybrid.sequences.InitSequence;
import com.lootsie.sdk.netutil.MockedRestGenericMapResponseTask;
import com.lootsie.sdk.netutil.MockedRestGenericStringResponseTask;
import com.lootsie.sdk.netutil.MockedRestGetAppTask;
import com.lootsie.sdk.netutil.MockedRestGetSessionTask;
import com.lootsie.sdk.netutil.MockedRestGetSettingsTask;
import com.lootsie.sdk.netutil.MockedRestGetUserAccountTask;
import com.lootsie.sdk.netutil.MockedRestGetUserAchievementsTask;
import com.lootsie.sdk.netutil.MockedRestGetUserRewardTask;
import com.lootsie.sdk.netutil.MockedRestPostUserLocationTask;
import com.lootsie.sdk.netutil.MockedRestPostUserSessionGuestTask;
import com.lootsie.sdk.netutil.MockedTrackingSessionAsyncTask;
import com.lootsie.sdk.netutil.RestResult;
import com.lootsie.sdk.utils.Logs;
import com.lootsie.sdk.utils.LootsieGlobals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

/**
 * Created by jerrylootsie on 7/14/15.
 */
@Config(constants = BuildConfig.class, sdk = 21)
@RunWith(CustomRobolectricRunner.class)
public class RobolectricTest_InitSequence {

    static enum TestResultStatus {
        SUCCESS,
        PENDING,
        FAILURE;

        private TestResultStatus() {
        }
    }

    class InitCallback implements IInitializationCallback {

        public TestResultStatus resultStatus = TestResultStatus.PENDING;

        @Override
        public void onInitSuccess() {
            System.out.println("InitCallback: onInitSuccess");
            resultStatus = TestResultStatus.SUCCESS;
        }

        @Override
        public void onInitFailure() {
            System.out.println("InitCallback: onInitFailure");

            resultStatus = TestResultStatus.FAILURE;
        }
    }


    @Test
    public void testSequence_InitSequence() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_InitSequence");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);
            //LootsieEngine.getInstance().getApplicationContext();

            Device device = new Device(testInstance);

            // default location services off
            device.build(false);

            MockedRestGetSessionTask restGetSessionTask = new MockedRestGetSessionTask();
            MockedRestGetSettingsTask restGetSettingsTask = new MockedRestGetSettingsTask();
            MockedRestPostUserSessionGuestTask restPostUserSessionGuestTask = new MockedRestPostUserSessionGuestTask();
            MockedRestGetAppTask restGetAppTask = new MockedRestGetAppTask();
            MockedRestGetUserAchievementsTask restGetUserAchievementsTask = new MockedRestGetUserAchievementsTask();
            MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();
            MockedRestGetUserRewardTask restGetUserRewardTask = new MockedRestGetUserRewardTask();
            MockedTrackingSessionAsyncTask trackingSessionAsyncTask = new MockedTrackingSessionAsyncTask();


            InitSequence.restGetSessionTask = restGetSessionTask;
            InitSequence.restGetSettingsTask = restGetSettingsTask;
            InitSequence.restPostUserSessionGuestTask = restPostUserSessionGuestTask;
            InitSequence.restGetAppTask = restGetAppTask;
            InitSequence.restGetUserAchievementsTask = restGetUserAchievementsTask;
            InitSequence.restGetUserAccountTask = restGetUserAccountTask;
            InitSequence.restGetUserRewardTask = restGetUserRewardTask;
            InitSequence.trackingSessionTask = trackingSessionAsyncTask;

            InitCallback initCallback = new InitCallback();

            InitSequence.start(device, LootsieGlobals.APPKEY, initCallback);

            org.junit.Assert.assertTrue("initialization success", initCallback.resultStatus == TestResultStatus.SUCCESS);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_InitSequence Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_InitSequence finished");
    }

    @Test
    public void testSequence_InitSequence_with_location() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_InitSequence_with_location");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);
            //LootsieEngine.getInstance().getApplicationContext();

            Device device = new Device(testInstance);

            // default location services on
            device.build(true);

            MockedRestGetSessionTask restGetSessionTask = new MockedRestGetSessionTask();
            MockedRestGetSettingsTask restGetSettingsTask = new MockedRestGetSettingsTask();
            MockedRestPostUserSessionGuestTask restPostUserSessionGuestTask = new MockedRestPostUserSessionGuestTask();
            MockedRestGetAppTask restGetAppTask = new MockedRestGetAppTask();
            MockedRestGetUserAchievementsTask restGetUserAchievementsTask = new MockedRestGetUserAchievementsTask();
            MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();
            MockedRestGetUserRewardTask restGetUserRewardTask = new MockedRestGetUserRewardTask();
            MockedTrackingSessionAsyncTask trackingSessionAsyncTask = new MockedTrackingSessionAsyncTask();
            MockedRestPostUserLocationTask restPostUserLocationTask = new MockedRestPostUserLocationTask();

            InitSequence.restGetSessionTask = restGetSessionTask;
            InitSequence.restGetSettingsTask = restGetSettingsTask;
            InitSequence.restPostUserSessionGuestTask = restPostUserSessionGuestTask;
            InitSequence.restGetAppTask = restGetAppTask;
            InitSequence.restGetUserAchievementsTask = restGetUserAchievementsTask;
            InitSequence.restGetUserAccountTask = restGetUserAccountTask;
            InitSequence.restGetUserRewardTask = restGetUserRewardTask;
            InitSequence.trackingSessionTask = trackingSessionAsyncTask;
            InitSequence.restPostUserLocationTask = restPostUserLocationTask;

            InitCallback initCallback = new InitCallback();

            InitSequence.start(device, LootsieGlobals.APPKEY, initCallback);

            InitSequence.setLocation("{ \"latlng\": \"30.456456,-150.39482\" }");
            InitSequence.PostUserLocation();

            org.junit.Assert.assertTrue("initialization success", initCallback.resultStatus == TestResultStatus.SUCCESS);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_InitSequence_with_location Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_InitSequence_with_location finished");
    }

    @Test
    public void testSequence_InitSequence_session_field_missing() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_InitSequence_session_field_missing");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);
            //LootsieEngine.getInstance().getApplicationContext();

            Device device = new Device(testInstance);

            // default location services off
            device.build(false);

            //MockedRestGetSessionTask restGetSessionTask = new MockedRestGetSessionTask();
            MockedRestGenericMapResponseTask restGetSessionTask = new MockedRestGenericMapResponseTask();
            // 400 Bad Request {"errors": [{"field": "sdk_version", "message": "missing"}]}
            RestResult response = new RestResult();
            response.status = 400;
            response.content = "{\"errors\": [{\"field\": \"sdk_version\", \"message\": \"missing\"}]}";
            restGetSessionTask.setRestResult(response);

            MockedRestGetSettingsTask restGetSettingsTask = new MockedRestGetSettingsTask();
            MockedRestPostUserSessionGuestTask restPostUserSessionGuestTask = new MockedRestPostUserSessionGuestTask();
            MockedRestGetAppTask restGetAppTask = new MockedRestGetAppTask();
            MockedRestGetUserAchievementsTask restGetUserAchievementsTask = new MockedRestGetUserAchievementsTask();
            MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();
            MockedRestGetUserRewardTask restGetUserRewardTask = new MockedRestGetUserRewardTask();
            MockedTrackingSessionAsyncTask trackingSessionAsyncTask = new MockedTrackingSessionAsyncTask();


            InitSequence.restGetSessionTask = restGetSessionTask;
            InitSequence.restGetSettingsTask = restGetSettingsTask;
            InitSequence.restPostUserSessionGuestTask = restPostUserSessionGuestTask;
            InitSequence.restGetAppTask = restGetAppTask;
            InitSequence.restGetUserAchievementsTask = restGetUserAchievementsTask;
            InitSequence.restGetUserAccountTask = restGetUserAccountTask;
            InitSequence.restGetUserRewardTask = restGetUserRewardTask;
            InitSequence.trackingSessionTask = trackingSessionAsyncTask;

            InitCallback initCallback = new InitCallback();

            InitSequence.start(device, LootsieGlobals.APPKEY, initCallback);

            org.junit.Assert.assertTrue("should receive initialization failure", initCallback.resultStatus == TestResultStatus.FAILURE);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_InitSequence_session_field_missing Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_InitSequence_session_field_missing finished");
    }

    @Test
    public void testSequence_InitSequence_session_app_secret_missing() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_InitSequence_session_app_secret_missing");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);
            //LootsieEngine.getInstance().getApplicationContext();

            Device device = new Device(testInstance);

            // default location services off
            device.build(false);

            //MockedRestGetSessionTask restGetSessionTask = new MockedRestGetSessionTask();
            MockedRestGenericMapResponseTask restGetSessionTask = new MockedRestGenericMapResponseTask();
            // 403 Forbidden {"errors": [{"field": "app_secret", "message": "invalid"}]}
            RestResult response = new RestResult();
            response.status = 403;
            response.content = "{\"errors\": [{\"field\": \"app_secret\", \"message\": \"invalid\"}]}";
            restGetSessionTask.setRestResult(response);

            MockedRestGetSettingsTask restGetSettingsTask = new MockedRestGetSettingsTask();
            MockedRestPostUserSessionGuestTask restPostUserSessionGuestTask = new MockedRestPostUserSessionGuestTask();
            MockedRestGetAppTask restGetAppTask = new MockedRestGetAppTask();
            MockedRestGetUserAchievementsTask restGetUserAchievementsTask = new MockedRestGetUserAchievementsTask();
            MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();
            MockedRestGetUserRewardTask restGetUserRewardTask = new MockedRestGetUserRewardTask();
            MockedTrackingSessionAsyncTask trackingSessionAsyncTask = new MockedTrackingSessionAsyncTask();


            InitSequence.restGetSessionTask = restGetSessionTask;
            InitSequence.restGetSettingsTask = restGetSettingsTask;
            InitSequence.restPostUserSessionGuestTask = restPostUserSessionGuestTask;
            InitSequence.restGetAppTask = restGetAppTask;
            InitSequence.restGetUserAchievementsTask = restGetUserAchievementsTask;
            InitSequence.restGetUserAccountTask = restGetUserAccountTask;
            InitSequence.restGetUserRewardTask = restGetUserRewardTask;
            InitSequence.trackingSessionTask = trackingSessionAsyncTask;

            InitCallback initCallback = new InitCallback();

            InitSequence.start(device, LootsieGlobals.APPKEY, initCallback);

            org.junit.Assert.assertTrue("should receive initialization failure", initCallback.resultStatus == TestResultStatus.FAILURE);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_InitSequence_session_app_secret_missing Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_InitSequence_session_app_secret_missing finished");
    }

    @Test
    public void testSequence_InitSequence_with_location_invalid() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_InitSequence_with_location_invalid");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);
            //LootsieEngine.getInstance().getApplicationContext();

            Device device = new Device(testInstance);

            // default location services on
            device.build(true);

            MockedRestGetSessionTask restGetSessionTask = new MockedRestGetSessionTask();
            MockedRestGetSettingsTask restGetSettingsTask = new MockedRestGetSettingsTask();
            MockedRestPostUserSessionGuestTask restPostUserSessionGuestTask = new MockedRestPostUserSessionGuestTask();
            MockedRestGetAppTask restGetAppTask = new MockedRestGetAppTask();
            MockedRestGetUserAchievementsTask restGetUserAchievementsTask = new MockedRestGetUserAchievementsTask();
            MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();
            MockedRestGetUserRewardTask restGetUserRewardTask = new MockedRestGetUserRewardTask();
            MockedTrackingSessionAsyncTask trackingSessionAsyncTask = new MockedTrackingSessionAsyncTask();


            //MockedRestPostUserLocationTask restPostUserLocationTask = new MockedRestPostUserLocationTask();
            // 400 Forbidden {"errors": [{"field": "latlng", "invalid": ""}]}
            MockedRestGenericStringResponseTask restPostUserLocationTask = new MockedRestGenericStringResponseTask();
            RestResult response = new RestResult();
            response.status = 400;
            response.content = "{\"errors\": [{\"field\": \"latlng\", \"invalid\": \"\"}]}";
            restPostUserLocationTask.setRestResult(response);



            InitSequence.restGetSessionTask = restGetSessionTask;
            InitSequence.restGetSettingsTask = restGetSettingsTask;
            InitSequence.restPostUserSessionGuestTask = restPostUserSessionGuestTask;
            InitSequence.restGetAppTask = restGetAppTask;
            InitSequence.restGetUserAchievementsTask = restGetUserAchievementsTask;
            InitSequence.restGetUserAccountTask = restGetUserAccountTask;
            InitSequence.restGetUserRewardTask = restGetUserRewardTask;
            InitSequence.trackingSessionTask = trackingSessionAsyncTask;
            InitSequence.restPostUserLocationTask = restPostUserLocationTask;

            InitCallback initCallback = new InitCallback();

            InitSequence.start(device, LootsieGlobals.APPKEY, initCallback);

            InitSequence.setLocation("{ \"latlng\": \"30.456456,-150.39482\" }");
            InitSequence.PostUserLocation();

            org.junit.Assert.assertTrue("initialization success", initCallback.resultStatus == TestResultStatus.SUCCESS);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_InitSequence_with_location_invalid Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_InitSequence_with_location_invalid finished");
    }

    @Test
    public void testSequence_InitSequence_with_location_field_missing() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_InitSequence_with_location_field_missing");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);
            //LootsieEngine.getInstance().getApplicationContext();

            Device device = new Device(testInstance);

            // default location services on
            device.build(true);

            MockedRestGetSessionTask restGetSessionTask = new MockedRestGetSessionTask();
            MockedRestGetSettingsTask restGetSettingsTask = new MockedRestGetSettingsTask();
            MockedRestPostUserSessionGuestTask restPostUserSessionGuestTask = new MockedRestPostUserSessionGuestTask();
            MockedRestGetAppTask restGetAppTask = new MockedRestGetAppTask();
            MockedRestGetUserAchievementsTask restGetUserAchievementsTask = new MockedRestGetUserAchievementsTask();
            MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();
            MockedRestGetUserRewardTask restGetUserRewardTask = new MockedRestGetUserRewardTask();
            MockedTrackingSessionAsyncTask trackingSessionAsyncTask = new MockedTrackingSessionAsyncTask();


            //MockedRestPostUserLocationTask restPostUserLocationTask = new MockedRestPostUserLocationTask();
            // 422 Unprocessable Entity {"errors": [{"field": "latlng", "message": "missing"}]}
            MockedRestGenericStringResponseTask restPostUserLocationTask = new MockedRestGenericStringResponseTask();
            RestResult response = new RestResult();
            response.status = 422;
            response.content = "{\"errors\": [{\"field\": \"latlng\", \"message\": \"missing\"}]}";
            restPostUserLocationTask.setRestResult(response);



            InitSequence.restGetSessionTask = restGetSessionTask;
            InitSequence.restGetSettingsTask = restGetSettingsTask;
            InitSequence.restPostUserSessionGuestTask = restPostUserSessionGuestTask;
            InitSequence.restGetAppTask = restGetAppTask;
            InitSequence.restGetUserAchievementsTask = restGetUserAchievementsTask;
            InitSequence.restGetUserAccountTask = restGetUserAccountTask;
            InitSequence.restGetUserRewardTask = restGetUserRewardTask;
            InitSequence.trackingSessionTask = trackingSessionAsyncTask;
            InitSequence.restPostUserLocationTask = restPostUserLocationTask;

            InitCallback initCallback = new InitCallback();

            InitSequence.start(device, LootsieGlobals.APPKEY, initCallback);

            //InitSequence.setLocation("{ \"latlng\": \"30.456456,-150.39482\" }");
            InitSequence.setLocation("{}");
            InitSequence.PostUserLocation();

            org.junit.Assert.assertTrue("initialization success", initCallback.resultStatus == TestResultStatus.SUCCESS);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_InitSequence_with_location_field_missing Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_InitSequence_with_location_field_missing finished");
    }

    @Test
    public void testSequence_InitSequence_sdk_disabled() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_InitSequence_sdk_disabled");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);
            //LootsieEngine.getInstance().getApplicationContext();

            Device device = new Device(testInstance);

            // default location services off
            device.build(false);

            MockedRestGetSessionTask restGetSessionTask = new MockedRestGetSessionTask();

            //MockedRestGetSettingsTask restGetSettingsTask = new MockedRestGetSettingsTask();
            MockedRestGenericMapResponseTask restGetSettingsTask = new MockedRestGenericMapResponseTask();
            RestResult response = new RestResult();
            response.status = 200;
            response.content = "{\"settings\": {\"currency_name\": \"Lootsie Points\", \"sdk.enabled\": \"false\", \"currency_abbreviation\": \"LP\", \"sdk.api.rewards.page-size\": \"100\"}}";
            restGetSettingsTask.setRestResult(response);

            MockedRestPostUserSessionGuestTask restPostUserSessionGuestTask = new MockedRestPostUserSessionGuestTask();
            MockedRestGetAppTask restGetAppTask = new MockedRestGetAppTask();
            MockedRestGetUserAchievementsTask restGetUserAchievementsTask = new MockedRestGetUserAchievementsTask();
            MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();
            MockedRestGetUserRewardTask restGetUserRewardTask = new MockedRestGetUserRewardTask();
            MockedTrackingSessionAsyncTask trackingSessionAsyncTask = new MockedTrackingSessionAsyncTask();


            InitSequence.restGetSessionTask = restGetSessionTask;

            InitSequence.restGetSettingsTask = restGetSettingsTask;
            InitSequence.restPostUserSessionGuestTask = restPostUserSessionGuestTask;
            InitSequence.restGetAppTask = restGetAppTask;
            InitSequence.restGetUserAchievementsTask = restGetUserAchievementsTask;
            InitSequence.restGetUserAccountTask = restGetUserAccountTask;
            InitSequence.restGetUserRewardTask = restGetUserRewardTask;
            InitSequence.trackingSessionTask = trackingSessionAsyncTask;

            InitCallback initCallback = new InitCallback();

            InitSequence.start(device, LootsieGlobals.APPKEY, initCallback);

            org.junit.Assert.assertTrue("initialization should fail", initCallback.resultStatus == TestResultStatus.FAILURE);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_InitSequence_sdk_disabled Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_InitSequence_sdk_disabled finished");
    }

    @Test
    public void testSequence_InitSequence_getSettings_token_invalid() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_InitSequence_getSettings_token_invalid");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);
            //LootsieEngine.getInstance().getApplicationContext();

            Device device = new Device(testInstance);

            // default location services off
            device.build(false);

            MockedRestGetSessionTask restGetSessionTask = new MockedRestGetSessionTask();

            //MockedRestGetSettingsTask restGetSettingsTask = new MockedRestGetSettingsTask();
            MockedRestGenericMapResponseTask restGetSettingsTask = new MockedRestGenericMapResponseTask();
            // 403 Forbidden {"errors": [{"field": "user_session_token", "message": "invalid"}]}
            RestResult response = new RestResult();
            response.status = 403;
            response.content = "{\"errors\": [{\"field\": \"user_session_token\", \"message\": \"invalid\"}]}";
            restGetSettingsTask.setRestResult(response);

            MockedRestPostUserSessionGuestTask restPostUserSessionGuestTask = new MockedRestPostUserSessionGuestTask();
            MockedRestGetAppTask restGetAppTask = new MockedRestGetAppTask();
            MockedRestGetUserAchievementsTask restGetUserAchievementsTask = new MockedRestGetUserAchievementsTask();
            MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();
            MockedRestGetUserRewardTask restGetUserRewardTask = new MockedRestGetUserRewardTask();
            MockedTrackingSessionAsyncTask trackingSessionAsyncTask = new MockedTrackingSessionAsyncTask();


            InitSequence.restGetSessionTask = restGetSessionTask;

            InitSequence.restGetSettingsTask = restGetSettingsTask;
            InitSequence.restPostUserSessionGuestTask = restPostUserSessionGuestTask;
            InitSequence.restGetAppTask = restGetAppTask;
            InitSequence.restGetUserAchievementsTask = restGetUserAchievementsTask;
            InitSequence.restGetUserAccountTask = restGetUserAccountTask;
            InitSequence.restGetUserRewardTask = restGetUserRewardTask;
            InitSequence.trackingSessionTask = trackingSessionAsyncTask;

            InitCallback initCallback = new InitCallback();

            InitSequence.start(device, LootsieGlobals.APPKEY, initCallback);

            org.junit.Assert.assertTrue("initialization should fail", initCallback.resultStatus == TestResultStatus.FAILURE);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_InitSequence_getSettings_token_invalid Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_InitSequence_getSettings_token_invalid finished");
    }

    @Test
    public void testSequence_InitSequence_postUserSessionGuest_token_invalid() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_InitSequence_postUserSessionGuest_token_invalid");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);
            //LootsieEngine.getInstance().getApplicationContext();

            Device device = new Device(testInstance);

            // default location services off
            device.build(false);

            MockedRestGetSessionTask restGetSessionTask = new MockedRestGetSessionTask();
            MockedRestGetSettingsTask restGetSettingsTask = new MockedRestGetSettingsTask();

            //MockedRestPostUserSessionGuestTask restPostUserSessionGuestTask = new MockedRestPostUserSessionGuestTask();
            MockedRestGenericStringResponseTask restPostUserSessionGuestTask = new MockedRestGenericStringResponseTask();
            // 400 Bad Request {"errors": [{"field": "api_session", "message": "missing"}]}
            RestResult response = new RestResult();
            response.status = 400;
            response.content = "{\"errors\": [{\"field\": \"api_session\", \"message\": \"missing\"}]}";
            restPostUserSessionGuestTask.setRestResult(response);

            MockedRestGetAppTask restGetAppTask = new MockedRestGetAppTask();
            MockedRestGetUserAchievementsTask restGetUserAchievementsTask = new MockedRestGetUserAchievementsTask();
            MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();
            MockedRestGetUserRewardTask restGetUserRewardTask = new MockedRestGetUserRewardTask();
            MockedTrackingSessionAsyncTask trackingSessionAsyncTask = new MockedTrackingSessionAsyncTask();


            InitSequence.restGetSessionTask = restGetSessionTask;

            InitSequence.restGetSettingsTask = restGetSettingsTask;
            InitSequence.restPostUserSessionGuestTask = restPostUserSessionGuestTask;
            InitSequence.restGetAppTask = restGetAppTask;
            InitSequence.restGetUserAchievementsTask = restGetUserAchievementsTask;
            InitSequence.restGetUserAccountTask = restGetUserAccountTask;
            InitSequence.restGetUserRewardTask = restGetUserRewardTask;
            InitSequence.trackingSessionTask = trackingSessionAsyncTask;

            InitCallback initCallback = new InitCallback();

            InitSequence.start(device, LootsieGlobals.APPKEY, initCallback);

            org.junit.Assert.assertTrue("initialization should fail", initCallback.resultStatus == TestResultStatus.FAILURE);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_InitSequence_postUserSessionGuest_token_invalid Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_InitSequence_postUserSessionGuest_token_invalid finished");
    }

    @Test
    public void testSequence_InitSequence_postUserSessionGuest_appSecret_invalid() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_InitSequence_postUserSessionGuest_appSecret_invalid");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);
            //LootsieEngine.getInstance().getApplicationContext();

            Device device = new Device(testInstance);

            // default location services off
            device.build(false);

            MockedRestGetSessionTask restGetSessionTask = new MockedRestGetSessionTask();
            MockedRestGetSettingsTask restGetSettingsTask = new MockedRestGetSettingsTask();

            //MockedRestPostUserSessionGuestTask restPostUserSessionGuestTask = new MockedRestPostUserSessionGuestTask();
            MockedRestGenericStringResponseTask restPostUserSessionGuestTask = new MockedRestGenericStringResponseTask();
            // 403 Forbidden {"errors": [{"field": "app_secret", "message": "invalid"}]}
            RestResult response = new RestResult();
            response.status = 403;
            response.content = "{\"errors\": [{\"field\": \"app_secret\", \"message\": \"invalid\"}]}";
            restPostUserSessionGuestTask.setRestResult(response);

            MockedRestGetAppTask restGetAppTask = new MockedRestGetAppTask();
            MockedRestGetUserAchievementsTask restGetUserAchievementsTask = new MockedRestGetUserAchievementsTask();
            MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();
            MockedRestGetUserRewardTask restGetUserRewardTask = new MockedRestGetUserRewardTask();
            MockedTrackingSessionAsyncTask trackingSessionAsyncTask = new MockedTrackingSessionAsyncTask();


            InitSequence.restGetSessionTask = restGetSessionTask;

            InitSequence.restGetSettingsTask = restGetSettingsTask;
            InitSequence.restPostUserSessionGuestTask = restPostUserSessionGuestTask;
            InitSequence.restGetAppTask = restGetAppTask;
            InitSequence.restGetUserAchievementsTask = restGetUserAchievementsTask;
            InitSequence.restGetUserAccountTask = restGetUserAccountTask;
            InitSequence.restGetUserRewardTask = restGetUserRewardTask;
            InitSequence.trackingSessionTask = trackingSessionAsyncTask;

            InitCallback initCallback = new InitCallback();

            InitSequence.start(device, LootsieGlobals.APPKEY, initCallback);

            org.junit.Assert.assertTrue("initialization should fail", initCallback.resultStatus == TestResultStatus.FAILURE);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_InitSequence_postUserSessionGuest_appSecret_invalid Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_InitSequence_postUserSessionGuest_appSecret_invalid finished");
    }

    @Test
    public void testSequence_InitSequence_getApp_appSecret_invalid() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_InitSequence_getApp_appSecret_invalid");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);
            //LootsieEngine.getInstance().getApplicationContext();

            Device device = new Device(testInstance);

            // default location services off
            device.build(false);

            MockedRestGetSessionTask restGetSessionTask = new MockedRestGetSessionTask();
            MockedRestGetSettingsTask restGetSettingsTask = new MockedRestGetSettingsTask();
            MockedRestPostUserSessionGuestTask restPostUserSessionGuestTask = new MockedRestPostUserSessionGuestTask();

            //MockedRestGetAppTask restGetAppTask = new MockedRestGetAppTask();
            MockedRestGenericStringResponseTask restGetAppTask = new MockedRestGenericStringResponseTask();
            // 403 Forbidden {"errors": [{"field": "app_secret", "message": "invalid"}]}
            RestResult response = new RestResult();
            response.status = 403;
            response.content = "{\"errors\": [{\"field\": \"app_secret\", \"message\": \"invalid\"}]}";
            restGetAppTask.setRestResult(response);

            MockedRestGetUserAchievementsTask restGetUserAchievementsTask = new MockedRestGetUserAchievementsTask();
            MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();
            MockedRestGetUserRewardTask restGetUserRewardTask = new MockedRestGetUserRewardTask();
            MockedTrackingSessionAsyncTask trackingSessionAsyncTask = new MockedTrackingSessionAsyncTask();


            InitSequence.restGetSessionTask = restGetSessionTask;

            InitSequence.restGetSettingsTask = restGetSettingsTask;
            InitSequence.restPostUserSessionGuestTask = restPostUserSessionGuestTask;
            InitSequence.restGetAppTask = restGetAppTask;
            InitSequence.restGetUserAchievementsTask = restGetUserAchievementsTask;
            InitSequence.restGetUserAccountTask = restGetUserAccountTask;
            InitSequence.restGetUserRewardTask = restGetUserRewardTask;
            InitSequence.trackingSessionTask = trackingSessionAsyncTask;

            InitCallback initCallback = new InitCallback();

            InitSequence.start(device, LootsieGlobals.APPKEY, initCallback);

            org.junit.Assert.assertTrue("initialization should fail", initCallback.resultStatus == TestResultStatus.FAILURE);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_InitSequence_getApp_appSecret_invalid Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_InitSequence_getApp_appSecret_invalid finished");
    }

    @Test
    public void testSequence_InitSequence_getUserAchievements_appSecret_invalid() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_InitSequence_getUserAchievements_appSecret_invalid");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);
            //LootsieEngine.getInstance().getApplicationContext();

            Device device = new Device(testInstance);

            // default location services off
            device.build(false);

            MockedRestGetSessionTask restGetSessionTask = new MockedRestGetSessionTask();
            MockedRestGetSettingsTask restGetSettingsTask = new MockedRestGetSettingsTask();
            MockedRestPostUserSessionGuestTask restPostUserSessionGuestTask = new MockedRestPostUserSessionGuestTask();
            MockedRestGetAppTask restGetAppTask = new MockedRestGetAppTask();

            //MockedRestGetUserAchievementsTask restGetUserAchievementsTask = new MockedRestGetUserAchievementsTask();
            MockedRestGenericStringResponseTask restGetUserAchievementsTask = new MockedRestGenericStringResponseTask();
            // 403 Forbidden {"errors": [{"field": "app_secret", "message": "invalid"}]}
            RestResult response = new RestResult();
            response.status = 403;
            response.content = "{\"errors\": [{\"field\": \"app_secret\", \"message\": \"invalid\"}]}";
            restGetUserAchievementsTask.setRestResult(response);

            MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();
            MockedRestGetUserRewardTask restGetUserRewardTask = new MockedRestGetUserRewardTask();
            MockedTrackingSessionAsyncTask trackingSessionAsyncTask = new MockedTrackingSessionAsyncTask();


            InitSequence.restGetSessionTask = restGetSessionTask;

            InitSequence.restGetSettingsTask = restGetSettingsTask;
            InitSequence.restPostUserSessionGuestTask = restPostUserSessionGuestTask;
            InitSequence.restGetAppTask = restGetAppTask;
            InitSequence.restGetUserAchievementsTask = restGetUserAchievementsTask;
            InitSequence.restGetUserAccountTask = restGetUserAccountTask;
            InitSequence.restGetUserRewardTask = restGetUserRewardTask;
            InitSequence.trackingSessionTask = trackingSessionAsyncTask;

            InitCallback initCallback = new InitCallback();

            InitSequence.start(device, LootsieGlobals.APPKEY, initCallback);

            org.junit.Assert.assertTrue("initialization should fail", initCallback.resultStatus == TestResultStatus.FAILURE);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_InitSequence_getUserAchievements_appSecret_invalid Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_InitSequence_getUserAchievements_appSecret_invalid finished");
    }

    @Test
    public void testSequence_InitSequence_getUserAccount_appSecret_invalid() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_InitSequence_getUserAccount_appSecret_invalid");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);
            //LootsieEngine.getInstance().getApplicationContext();

            Device device = new Device(testInstance);

            // default location services off
            device.build(false);

            MockedRestGetSessionTask restGetSessionTask = new MockedRestGetSessionTask();
            MockedRestGetSettingsTask restGetSettingsTask = new MockedRestGetSettingsTask();
            MockedRestPostUserSessionGuestTask restPostUserSessionGuestTask = new MockedRestPostUserSessionGuestTask();
            MockedRestGetAppTask restGetAppTask = new MockedRestGetAppTask();
            MockedRestGetUserAchievementsTask restGetUserAchievementsTask = new MockedRestGetUserAchievementsTask();

            //MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();
            MockedRestGenericStringResponseTask restGetUserAccountTask = new MockedRestGenericStringResponseTask();
            // 403 Forbidden {"errors": [{"field": "app_secret", "message": "invalid"}]}
            RestResult response = new RestResult();
            response.status = 403;
            response.content = "{\"errors\": [{\"field\": \"app_secret\", \"message\": \"invalid\"}]}";
            restGetUserAccountTask.setRestResult(response);


            MockedRestGetUserRewardTask restGetUserRewardTask = new MockedRestGetUserRewardTask();
            MockedTrackingSessionAsyncTask trackingSessionAsyncTask = new MockedTrackingSessionAsyncTask();


            InitSequence.restGetSessionTask = restGetSessionTask;

            InitSequence.restGetSettingsTask = restGetSettingsTask;
            InitSequence.restPostUserSessionGuestTask = restPostUserSessionGuestTask;
            InitSequence.restGetAppTask = restGetAppTask;
            InitSequence.restGetUserAchievementsTask = restGetUserAchievementsTask;
            InitSequence.restGetUserAccountTask = restGetUserAccountTask;
            InitSequence.restGetUserRewardTask = restGetUserRewardTask;
            InitSequence.trackingSessionTask = trackingSessionAsyncTask;

            InitCallback initCallback = new InitCallback();

            InitSequence.start(device, LootsieGlobals.APPKEY, initCallback);

            org.junit.Assert.assertTrue("initialization should fail", initCallback.resultStatus == TestResultStatus.FAILURE);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_InitSequence_getUserAccount_appSecret_invalid Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_InitSequence_getUserAccount_appSecret_invalid finished");
    }

    @Test
    public void testSequence_InitSequence_getUserReward_appSecret_invalid() {
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("RobolectricTest: testSequence_InitSequence_getUserReward_appSecret_invalid");

        try {
            Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.ENABLE_BASIC_LOGGING);
            //Lootsie.setLogLevel(LootsieGlobals.LOOTSIE_LOGGING_LEVEL.VERBOSE);
            Logs.loggingMode = Logs.LOGGING_MODE.LOG_TO_STDOUT;

            LootsieEngine testInstance = LootsieEngine.testInit();
            LootsieEngine.setApplicationContext(RuntimeEnvironment.application);

            LootsieAccountManager lootsieAccountManager = new LootsieAccountManager(RuntimeEnvironment.application);
            LootsieEngine.setLootsieAccountManager(lootsieAccountManager);
            //LootsieEngine.getInstance().getApplicationContext();

            Device device = new Device(testInstance);

            // default location services off
            device.build(false);

            MockedRestGetSessionTask restGetSessionTask = new MockedRestGetSessionTask();
            MockedRestGetSettingsTask restGetSettingsTask = new MockedRestGetSettingsTask();
            MockedRestPostUserSessionGuestTask restPostUserSessionGuestTask = new MockedRestPostUserSessionGuestTask();
            MockedRestGetAppTask restGetAppTask = new MockedRestGetAppTask();
            MockedRestGetUserAchievementsTask restGetUserAchievementsTask = new MockedRestGetUserAchievementsTask();
            MockedRestGetUserAccountTask restGetUserAccountTask = new MockedRestGetUserAccountTask();

            //MockedRestGetUserRewardTask restGetUserRewardTask = new MockedRestGetUserRewardTask();
            MockedRestGenericStringResponseTask restGetUserRewardTask = new MockedRestGenericStringResponseTask();
            // 403 Forbidden {"errors": [{"field": "app_secret", "message": "invalid"}]}
            RestResult response = new RestResult();
            response.status = 403;
            response.content = "{\"errors\": [{\"field\": \"app_secret\", \"message\": \"invalid\"}]}";
            restGetUserRewardTask.setRestResult(response);


            MockedTrackingSessionAsyncTask trackingSessionAsyncTask = new MockedTrackingSessionAsyncTask();


            InitSequence.restGetSessionTask = restGetSessionTask;

            InitSequence.restGetSettingsTask = restGetSettingsTask;
            InitSequence.restPostUserSessionGuestTask = restPostUserSessionGuestTask;
            InitSequence.restGetAppTask = restGetAppTask;
            InitSequence.restGetUserAchievementsTask = restGetUserAchievementsTask;
            InitSequence.restGetUserAccountTask = restGetUserAccountTask;
            InitSequence.restGetUserRewardTask = restGetUserRewardTask;
            InitSequence.trackingSessionTask = trackingSessionAsyncTask;

            InitCallback initCallback = new InitCallback();

            InitSequence.start(device, LootsieGlobals.APPKEY, initCallback);

            org.junit.Assert.assertTrue("initialization should fail", initCallback.resultStatus == TestResultStatus.FAILURE);

        } catch (Exception ex) {
            System.out.println("RobolectricTest: testSequence_InitSequence_getUserReward_appSecret_invalid Exception: " + ex.getMessage());
            ex.printStackTrace();

        }
        System.out.println("RobolectricTest: testSequence_InitSequence_getUserReward_appSecret_invalid finished");
    }

}
