package com.lootsie.sdk.adnets;

import android.content.Context;

import android.util.Log;
import com.aerserv.sdk.*;
import com.lootsie.sdk.adnets.model.AdLoadErrorCode;
import com.lootsie.sdk.adnets.model.InterstitialAd;
import com.lootsie.sdk.adnets.model.InterstitialAdListener;

import java.util.List;

public class AerServInterstitialAd implements InterstitialAd {
    AerServInterstitial interstitial;
    private Boolean isLoaded = false;

    private AerServEventListener aerServEventListener = new AerServEventListener() {
        @Override
        public void onAerServEvent(AerServEvent event, List<Object> args) {
            String msg = "";
            switch (event) {
                case AD_FAILED:
                    msg = "AD_FAILED:" + args.get(0).toString();
                    if (listener != null) {
                        // TODO: Align with current error codes system.
                        // ERROR_CODE_INTERNAL_ERROR,
                        // ERROR_CODE_INVALID_REQUEST,
                        // ERROR_CODE_NETWORK_ERROR,
                        // ERROR_CODE_NO_FILL,
                        // ERROR_CODE_UNDEFINED
                        listener.onInterstitialAdFailedToLoad(AdLoadErrorCode.ERROR_CODE_UNDEFINED);
                    }
                    break;
                case PRELOAD_READY:
                    msg = "PRELOAD_READY";
                    isLoaded = true;
                    if (listener != null) {
                        listener.onInterstitialAdLoaded();
                    }
                    break;
                case AD_LOADED:
                    msg = "AD_LOADED";
                    isLoaded = true;
                    if (listener != null) {
                        listener.onInterstitialAdLoaded();
                    }
                    break;
                case AD_COMPLETED: //VIDEO_COMPLETED
                    msg = "AD_COMPLETED";
                    if (listener != null) {
                        listener.onInterstitialAdClosed();
                    }
                    break;
                case AD_IMPRESSION:
                    msg = "AD_IMPRESSION / opened";
                    if (listener != null) {
                        listener.onInterstitialAdOpened();
                    }
                    break;
                case AD_DISMISSED:
                    listener.onInterstitialAdLeftApplication();
                    msg = "AD_CLICKED";
                    break;
                default:
                    msg = event.toString() + " event fired with args "
                            + args.toString();
            }
            Log.d("AerServ", msg);
        }
    };
    private InterstitialAdListener listener;
    private final AerServConfig aerServConfig;

    public AerServInterstitialAd(Context context, String unitId, InterstitialAdListener listener) {
        this.listener = listener;

        aerServConfig = new AerServConfig(context, unitId)
                .setPreload(true)
                .setEventListener(aerServEventListener);
        interstitial = new AerServInterstitial(aerServConfig);
    }


    @Override
    public void load() {
        interstitial = new AerServInterstitial(aerServConfig);
    }

    @Override
    public boolean isLoaded() {
        return isLoaded;
    }

    @Override
    public void show() {
        interstitial.show();
    }
}
