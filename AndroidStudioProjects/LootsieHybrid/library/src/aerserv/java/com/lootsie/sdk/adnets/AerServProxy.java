package com.lootsie.sdk.adnets;

import android.content.Context;

import com.lootsie.sdk.adnets.model.AdNetProxy;
import com.lootsie.sdk.adnets.model.InterstitialAd;
import com.lootsie.sdk.adnets.model.InterstitialAdListener;

public class AerServProxy implements AdNetProxy {
    public static final String TAG = AerServProxy.class.getSimpleName();

    public InterstitialAd createInterstitialAd(Context context,
                                               final String adUnitId, final
                                               InterstitialAdListener masterAdListener) {
        AerServInterstitialAd interstitialAd = new AerServInterstitialAd(context, adUnitId, masterAdListener);
        return interstitialAd;
    }
}